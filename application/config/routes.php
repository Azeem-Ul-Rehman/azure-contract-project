<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* webserivce urls  */
$route['iventon'] ='IventonController';
$route['iventon/userlogin'] ='IventonController/userlogin';

/*for admin*/
$route['administrator'] ='admin';
$route['administrator/dashboard'] ='admin';
$route['administrator/user-login'] ='admin/login';
$route['administrator/user-logout'] ='admin/logout';
$route['administrator/my-profile'] ='admin/my_profile';
$route['administrator/updatemyprofile'] ='admin/updatemyprofile';
$route['administrator/updateprofilepicture'] ='admin/updateprofilepicture';
$route['administrator/updatepassword'] ='admin/updatepassword';

$route['administrator/company_list'] ='admin/company_list';
$route['administrator/company_details'] ='admin/company_details';
$route['administrator/addnewcompany'] ='admin/addnewcompany';
$route['administrator/editcompany'] ='admin/editcompany';
$route['administrator/updatecompany_action'] ='admin/updatecompany_action';
$route['administrator/deletecompany'] ='admin/deletecompany';



$route['administrator/partners_list'] ='admin/partners_list';
$route['administrator/addpartners'] ='admin/addpartners';
$route['administrator/editpartners'] ='admin/editpartners';
$route['administrator/deletepartner'] ='admin/deletepartner';


$route['administrator/suppliers_list'] ='admin/suppliers_list';
$route['administrator/addsuppliers'] ='admin/addsuppliers';
$route['administrator/editsuppliers'] ='admin/editsuppliers';
$route['administrator/deletesuppliers'] ='admin/deletesuppliers';
$route['administrator/view_suppliers'] ='admin/view_suppliers';



$route['administrator/contractors_list'] ='admin/contractors_list';
$route['administrator/addnewcontractor'] ='admin/addnewcontractor';
$route['administrator/editcontractor'] ='admin/editcontractor';
$route['administrator/deletecontractor'] ='admin/deletecontractor';
$route['administrator/contractors_details'] ='admin/contractors_details';


$route['administrator/loan_list'] ='admin/loan_list';
$route['administrator/addloan'] ='admin/addloan';
$route['administrator/editloan'] ='admin/editloan';
$route['administrator/deleteloan'] ='admin/deleteloan';


$route['administrator/bankaccountlist'] ='admin/bankaccountlist';
$route['administrator/addbankaccount'] ='admin/addbankaccount';
$route['administrator/editbankaccount'] ='admin/editbankaccount';
$route['administrator/deletebankaccount'] ='admin/deletebankaccount';
$route['administrator/bank_details'] ='admin/bank_details';


$route['administrator/financialinstitutionlist'] ='admin/financialinstitutionlist';
$route['administrator/financial_details'] ='admin/financial_details';
$route['administrator/addfinancialinstitution'] ='admin/addfinancialinstitution';
$route['administrator/editfinancialinstitution'] ='admin/editfinancialinstitution';
$route['administrator/deletefinancialinstitution'] ='admin/deletefinancialinstitution';

$route['administrator/paymentlist'] ='admin/paymentlist';
$route['administrator/addpayment'] ='admin/addpayment';
$route['administrator/editpayment'] ='admin/editpayment';
$route['administrator/deletepayment'] ='admin/deletepayment';


//Supplier Payment
$route['administrator/supplierpaymentlist'] ='admin/supplierpaymentlist';
$route['administrator/addsupplierpayment'] ='admin/addsupplierpayment';
$route['administrator/editsupplierpayment'] ='admin/editsupplierpayment';
$route['administrator/viewsupplierpayment'] ='admin/view_suppliers_payments';


$route['administrator/documentlist'] ='admin/documentlist';
$route['administrator/adddocuments'] ='admin/adddocuments';
$route['administrator/editdocuments'] ='admin/editdocuments';
$route['administrator/deletedocuments'] ='admin/deletedocuments';

$route['administrator/purchaseorders'] ='admin/purchaseorders';
$route['administrator/addpurchaseorder'] ='admin/addpurchaseorder';
$route['administrator/editpurchaseorder'] ='admin/editpurchaseorder';
$route['administrator/orderdetails'] ='admin/orderdetails';
$route['administrator/verifyorders'] ='admin/verifyorders';
$route['administrator/deleteorders'] ='admin/deleteorders';
$route['administrator/completeorder'] ='admin/completeorder';
$route['administrator/changepaidstatusorder'] ='admin/changepaidstatusorder';



$route['administrator/projectlist'] ='admin/projectlist';
$route['administrator/addprojects'] ='admin/addprojects';
$route['administrator/editprojects'] ='admin/editprojects';
$route['administrator/deletedprojects'] ='admin/deletedprojects';
$route['administrator/project_details'] ='admin/project_details';

$route['administrator/userslist'] ='admin/userslist';
$route['administrator/addnewusers'] ='admin/addnewusers';
$route['administrator/editusers'] ='admin/editusers';
$route['administrator/deleteusers'] ='admin/deleteusers';

$route['administrator/myorders'] ='admin/myorders';

$route['administrator/settings'] ='admin/settings';
$route['administrator/managepermissionstatus'] ='admin/managepermissionstatus';

$route['administrator/usersgroup'] ='admin/usersgroup';
$route['administrator/addnewgroups'] ='admin/addnewgroups';
$route['administrator/editdnewgroups'] ='admin/editdnewgroups';
$route['administrator/deletegroups'] ='admin/deletegroups';

$route['administrator/projecttype'] ='admin/projecttype';
$route['administrator/addprojecttype'] ='admin/addprojecttype';
$route['administrator/editprojecttype'] ='admin/editprojecttype';
$route['administrator/deleteprojecttype'] ='admin/deleteprojecttype';


$route['administrator/contractorstype'] ='admin/contractorstype';
$route['administrator/addcontractorstype'] ='admin/addcontractorstype';
$route['administrator/editcontractorstype'] ='admin/editcontractorstype';
$route['administrator/deletecontractorstype'] ='admin/deletecontractorstype';

$route['administrator/paymentmethod'] ='admin/paymentmethod';
$route['administrator/addpaymentmethod'] ='admin/addpaymentmethod';
$route['administrator/editpaymentmethod'] ='admin/editpaymentmethod';
$route['administrator/deletepaymentmethod'] ='admin/deletepaymentmethod';



$route['administrator/projectsection'] ='admin/projectsection';
$route['administrator/addprojectsection'] ='admin/addprojectsection';
$route['administrator/editprojectsection'] ='admin/editprojectsection';
$route['administrator/deleteprojectsection'] ='admin/deleteprojectsection';

$route['administrator/managepermissiontype'] ='admin/managepermissiontype';


$route['administrator/paidinvoiceaddpayment'] ='admin/paidinvoiceaddpayment';
$route['administrator/unpaidinvoicesfrompayment'] ='admin/unpaidinvoicesfrompayment';


// Sell Properties Route
$route['administrator/sellproperties'] ='admin/sellproperties';
$route['administrator/sellpropertydetail'] ='admin/sellpropertydetail';
$route['administrator/addsellproperty'] ='admin/addsellproperty';
$route['administrator/editsellproperty'] ='admin/editsellproperty';
$route['administrator/updatesellpropertyaction'] ='admin/updatesellpropertyaction';
$route['administrator/deletesellproperty'] ='admin/deletesellproperty';
$route['administrator/propertychecklistpdf'] ='admin/propertychecklistpdf';
$route['administrator/propertychecklistprint'] ='admin/propertychecklistprint';
$route['administrator/propertychecklistemail'] ='admin/propertychecklistemail';


/**************************** front end routing ********************************/
$route['(:any)'] ='home/itemlist/$1';


