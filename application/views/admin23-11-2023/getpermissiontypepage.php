<fieldset>
	<h5><?php echo $getuserstype->groupname; ?></h5>
	<table class="table">
		<tr>
			<th>&nbsp;</th>
			<th>Add</th>
			<th>View</th>
			<th>Modify</th>
			<th>Delete</th>
		</tr>
		<tr>
			<td>Compnies</td>
			<td><input <?php if($settingsall[0]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkaddcompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settingsall[0]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditcompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settingsall[0]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifycompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settingsall[0]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletecompany" id="chkaddcompanyid" value="1" /></td>
		</tr>
		
		<tr>
			<td>Projects</td>
			<td><input <?php if($settingsall[1]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkaddprojects"  value="1" /></td>
			<td><input <?php if($settingsall[1]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditprojects" value="1" /></td>
			<td><input <?php if($settingsall[1]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyprojects" value="1" /></td>
			<td><input <?php if($settingsall[1]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeleteprojects" value="1" /></td>
		</tr>
		
		<tr>
			<td>Users</td>
			<td><input <?php if($settingsall[2]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkaddusers"  value="1" /></td>
			<td><input <?php if($settingsall[2]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditusers" value="1" /></td>
			<td><input <?php if($settingsall[2]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyusers" value="1" /></td>
			<td><input <?php if($settingsall[2]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeleteusers" value="1" /></td>
		</tr>
		
		<tr>
			<td>Partners</td>
			<td><input <?php if($settingsall[3]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkaddpartners"  value="1" /></td>
			<td><input <?php if($settingsall[3]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditpartners" value="1" /></td>
			<td><input <?php if($settingsall[3]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifypartners" value="1" /></td>
			<td><input <?php if($settingsall[3]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletepartners" value="1" /></td>
		</tr>
		
		<tr>
			<td>Suppliers</td>
			<td><input <?php if($settingsall[4]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkaddsuppliers"  value="1" /></td>
			<td><input <?php if($settingsall[4]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditsuppliers" value="1" /></td>
			<td><input <?php if($settingsall[4]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifysuppliers" value="1" /></td>
			<td><input <?php if($settingsall[4]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletesuppliers" value="1" /></td>
		</tr>
		
		<tr>
			<td>Contractors</td>
			<td><input <?php if($settingsall[5]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkaddcontractos"  value="1" /></td>
			<td><input <?php if($settingsall[5]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditcontractos" value="1" /></td>
			<td><input <?php if($settingsall[5]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifycontractos" value="1" /></td>
			<td><input <?php if($settingsall[5]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletecontractos" value="1" /></td>
		</tr>
		
		<tr>
			<td>Loans</td>
			<td><input <?php if($settingsall[6]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Loans" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkaddloans"  value="1" /></td>
			<td><input <?php if($settingsall[6]->allowfor_edit==1){ ?> checked="checked" <?php } ?>  rel="Loans" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditloans" value="1" /></td>
			<td><input <?php if($settingsall[6]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Loans" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyloans" value="1" /></td>
			<td><input <?php if($settingsall[6]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Loans" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeleteloans" value="1" /></td>
		</tr>
		
		<tr>
			<td>Bank Accounts</td>
			<td><input <?php if($settingsall[7]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkaddbankaccounts"  value="1" /></td>
			<td><input <?php if($settingsall[7]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditbankaccounts" value="1" /></td>
			<td><input <?php if($settingsall[7]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifybankaccounts" value="1" /></td>
			<td><input <?php if($settingsall[7]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletebankaccounts" value="1" /></td>
		</tr>
		
		<tr>
			<td>Financial Institute</td>
			<td><input <?php if($settingsall[8]->allowfor_add==1){ ?> checked="checked" <?php } ?>  rel="Financial Institute" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkaddfinancialinstitute"  value="1" /></td>
			<td><input <?php if($settingsall[8]->allowfor_edit==1){ ?> checked="checked" <?php } ?>  rel="Financial Institute" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditfinancialinstitute" value="1" /></td>
			<td><input <?php if($settingsall[8]->allowfor_modify==1){ ?> checked="checked" <?php } ?>  rel="Financial Institute" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyfinancialinstitute" value="1" /></td>
			<td><input <?php if($settingsall[8]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Financial Institute" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletefinancialinstitute" value="1" /></td>
		</tr>
		
		<tr>
			<td>Payments</td>
			<td><input <?php if($settingsall[9]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls"  type="checkbox" name="chkaddpayments"  value="1" /></td>
			<td><input <?php if($settingsall[9]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditpayments" value="1" /></td>
			<td><input <?php if($settingsall[9]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifypayments" value="1" /></td>
			<td><input <?php if($settingsall[9]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletepayments" value="1" /></td>
		</tr>
		
		<tr>
			<td>Documents</td>
			<td><input <?php if($settingsall[10]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkadddocument"  value="1" /></td>
			<td><input <?php if($settingsall[10]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditdocument" value="1" /></td>
			<td><input <?php if($settingsall[10]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifydocument" value="1" /></td>
			<td><input <?php if($settingsall[10]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletedocument" value="1" /></td>
		</tr>
		
		<tr>
			<td>Purchase orders</td>
			<td><input <?php if($settingsall[11]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkadddocument"  value="1" /></td>
			<td><input <?php if($settingsall[11]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditdocument" value="1" /></td>
			<td><input <?php if($settingsall[11]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifydocument" value="1" /></td>
			<td><input <?php if($settingsall[11]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletedocument" value="1" /></td>
		</tr>
		
		<tr>
			<td>Dashboard</td>
			<td><input <?php if($settingsall[12]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Dashboard" rel1="<?php echo $permissionusergroup; ?>" rel2="add" class="permissioncls" type="checkbox" name="chkaddcompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settingsall[12]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Dashboard" rel1="<?php echo $permissionusergroup; ?>" rel2="edit" class="permissioncls" type="checkbox" name="chkeditcompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settingsall[12]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Dashboard" rel1="<?php echo $permissionusergroup; ?>" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifycompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settingsall[12]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Dashboard" rel1="<?php echo $permissionusergroup; ?>" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletecompany" id="chkaddcompanyid" value="1" /></td>
		</tr>
		
	</table>
</fieldset>


<?php /*if($permissionusergroup==2){ ?>

<fieldset>
	<h5>Manager</h5>
	<table class="table">
		<tr>
			<th>&nbsp;</th>
			<th>Add</th>
			<th>View</th>
			<th>Modify</th>
			<th>Delete</th>
		</tr>
		<tr>
			<td>Compnies</td>
			<td><input <?php if($settings[0]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkaddcompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settings[0]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditcompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settings[0]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifycompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settings[0]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletecompany" id="chkaddcompanyid" value="1" /></td>
		</tr>
		
		<tr>
			<td>Projects</td>
			<td><input <?php if($settings[1]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkaddprojects"  value="1" /></td>
			<td><input <?php if($settings[1]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditprojects" value="1" /></td>
			<td><input <?php if($settings[1]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyprojects" value="1" /></td>
			<td><input <?php if($settings[1]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeleteprojects" value="1" /></td>
		</tr>
		
		<tr>
			<td>Users</td>
			<td><input <?php if($settings[2]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkaddusers"  value="1" /></td>
			<td><input <?php if($settings[2]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditusers" value="1" /></td>
			<td><input <?php if($settings[2]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyusers" value="1" /></td>
			<td><input <?php if($settings[2]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeleteusers" value="1" /></td>
		</tr>
		
		<tr>
			<td>Partners</td>
			<td><input <?php if($settings[3]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkaddpartners"  value="1" /></td>
			<td><input <?php if($settings[3]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditpartners" value="1" /></td>
			<td><input <?php if($settings[3]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifypartners" value="1" /></td>
			<td><input <?php if($settings[3]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletepartners" value="1" /></td>
		</tr>
		
		<tr>
			<td>Suppliers</td>
			<td><input <?php if($settings[4]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkaddsuppliers"  value="1" /></td>
			<td><input <?php if($settings[4]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditsuppliers" value="1" /></td>
			<td><input <?php if($settings[4]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifysuppliers" value="1" /></td>
			<td><input <?php if($settings[4]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletesuppliers" value="1" /></td>
		</tr>
		
		<tr>
			<td>Contractors</td>
			<td><input <?php if($settings[5]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkaddcontractos"  value="1" /></td>
			<td><input <?php if($settings[5]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditcontractos" value="1" /></td>
			<td><input <?php if($settings[5]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifycontractos" value="1" /></td>
			<td><input <?php if($settings[5]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletecontractos" value="1" /></td>
		</tr>
		
		<tr>
			<td>Loans</td>
			<td><input <?php if($settings[6]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Loans" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkaddloans"  value="1" /></td>
			<td><input <?php if($settings[6]->allowfor_edit==1){ ?> checked="checked" <?php } ?>  rel="Loans" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditloans" value="1" /></td>
			<td><input <?php if($settings[6]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Loans" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyloans" value="1" /></td>
			<td><input <?php if($settings[6]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Loans" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeleteloans" value="1" /></td>
		</tr>
		
		<tr>
			<td>Bank Accounts</td>
			<td><input <?php if($settings[7]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkaddbankaccounts"  value="1" /></td>
			<td><input <?php if($settings[7]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditbankaccounts" value="1" /></td>
			<td><input <?php if($settings[7]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifybankaccounts" value="1" /></td>
			<td><input <?php if($settings[7]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletebankaccounts" value="1" /></td>
		</tr>
		
		<tr>
			<td>Financial Institute</td>
			<td><input <?php if($settings[8]->allowfor_add==1){ ?> checked="checked" <?php } ?>  rel="Financial Institute" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkaddfinancialinstitute"  value="1" /></td>
			<td><input <?php if($settings[8]->allowfor_edit==1){ ?> checked="checked" <?php } ?>  rel="Financial Institute" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditfinancialinstitute" value="1" /></td>
			<td><input <?php if($settings[8]->allowfor_modify==1){ ?> checked="checked" <?php } ?>  rel="Financial Institute" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyfinancialinstitute" value="1" /></td>
			<td><input <?php if($settings[8]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Financial Institute" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletefinancialinstitute" value="1" /></td>
		</tr>
		
		<tr>
			<td>Payments</td>
			<td><input <?php if($settings[9]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="2" rel2="add" class="permissioncls"  type="checkbox" name="chkaddpayments"  value="1" /></td>
			<td><input <?php if($settings[9]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditpayments" value="1" /></td>
			<td><input <?php if($settings[9]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifypayments" value="1" /></td>
			<td><input <?php if($settings[9]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletepayments" value="1" /></td>
		</tr>
		
		<tr>
			<td>Documents</td>
			<td><input <?php if($settings[10]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkadddocument"  value="1" /></td>
			<td><input <?php if($settings[10]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditdocument" value="1" /></td>
			<td><input <?php if($settings[10]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifydocument" value="1" /></td>
			<td><input <?php if($settings[10]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletedocument" value="1" /></td>
		</tr>
		
		<tr>
			<td>Purchase orders</td>
			<td><input <?php if($settings[11]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="2" rel2="add" class="permissioncls" type="checkbox" name="chkadddocument"  value="1" /></td>
			<td><input <?php if($settings[11]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="2" rel2="edit" class="permissioncls" type="checkbox" name="chkeditdocument" value="1" /></td>
			<td><input <?php if($settings[11]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="2" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifydocument" value="1" /></td>
			<td><input <?php if($settings[11]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="2" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletedocument" value="1" /></td>
		</tr>
		
		
	</table>
</fieldset>

<?php }else if($permissionusergroup==3){ ?>

<fieldset>
	<h5>Contractors</h5>
	<table class="table">
		<tr>
			<th>&nbsp;</th>
			<th>Add</th>
			<th>View</th>
			<th>Modify</th>
			<th>Delete</th>
		</tr>
		<tr>
			<td>Compnies</td>
			<td><input <?php if($settingscontractors[0]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkaddcompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settingscontractors[0]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditcompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settingscontractors[0]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifycompany" id="chkaddcompanyid" value="1" /></td>
			<td><input <?php if($settingscontractors[0]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Compnies" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletecompany" id="chkaddcompanyid" value="1" /></td>
		</tr>
		
		<tr>
			<td>Projects</td>
			<td><input <?php if($settingscontractors[1]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkaddprojects"  value="1" /></td>
			<td><input <?php if($settingscontractors[1]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditprojects" value="1" /></td>
			<td><input <?php if($settingscontractors[1]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyprojects" value="1" /></td>
			<td><input <?php if($settingscontractors[1]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Projects" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeleteprojects" value="1" /></td>
		</tr>
		
		<tr>
			<td>Users</td>
			<td><input <?php if($settingscontractors[2]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkaddusers"  value="1" /></td>
			<td><input <?php if($settingscontractors[2]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditusers" value="1" /></td>
			<td><input <?php if($settingscontractors[2]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyusers" value="1" /></td>
			<td><input <?php if($settingscontractors[2]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Users" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeleteusers" value="1" /></td>
		</tr>
		
		<tr>
			<td>Partners</td>
			<td><input <?php if($settingscontractors[3]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkaddpartners"  value="1" /></td>
			<td><input <?php if($settingscontractors[3]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditpartners" value="1" /></td>
			<td><input <?php if($settingscontractors[3]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifypartners" value="1" /></td>
			<td><input <?php if($settingscontractors[3]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Partners" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletepartners" value="1" /></td>
		</tr>
		
		<tr>
			<td>Suppliers</td>
			<td><input <?php if($settingscontractors[4]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkaddsuppliers"  value="1" /></td>
			<td><input <?php if($settingscontractors[4]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditsuppliers" value="1" /></td>
			<td><input <?php if($settingscontractors[4]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifysuppliers" value="1" /></td>
			<td><input <?php if($settingscontractors[4]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Suppliers" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletesuppliers" value="1" /></td>
		</tr>
		
		<tr>
			<td>Contractors</td>
			<td><input <?php if($settingscontractors[5]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkaddcontractos"  value="1" /></td>
			<td><input <?php if($settingscontractors[5]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditcontractos" value="1" /></td>
			<td><input <?php if($settingscontractors[5]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifycontractos" value="1" /></td>
			<td><input <?php if($settingscontractors[5]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Contractors" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletecontractos" value="1" /></td>
		</tr>
		
		<tr>
			<td>Loans</td>
			<td><input <?php if($settingscontractors[6]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Loans" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkaddloans"  value="1" /></td>
			<td><input <?php if($settingscontractors[6]->allowfor_edit==1){ ?> checked="checked" <?php } ?>  rel="Loans" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditloans" value="1" /></td>
			<td><input <?php if($settingscontractors[6]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Loans" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyloans" value="1" /></td>
			<td><input <?php if($settingscontractors[6]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Loans" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeleteloans" value="1" /></td>
		</tr>
		
		<tr>
			<td>Bank Accounts</td>
			<td><input <?php if($settingscontractors[7]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkaddbankaccounts"  value="1" /></td>
			<td><input <?php if($settingscontractors[7]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditbankaccounts" value="1" /></td>
			<td><input <?php if($settingscontractors[7]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifybankaccounts" value="1" /></td>
			<td><input <?php if($settingscontractors[7]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Bank Accounts" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletebankaccounts" value="1" /></td>
		</tr>
		
		<tr>
			<td>Financial Institute</td>
			<td><input <?php if($settingscontractors[8]->allowfor_add==1){ ?> checked="checked" <?php } ?>  rel="Financial Institute" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkaddfinancialinstitute"  value="1" /></td>
			<td><input <?php if($settingscontractors[8]->allowfor_edit==1){ ?> checked="checked" <?php } ?>  rel="Financial Institute" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditfinancialinstitute" value="1" /></td>
			<td><input <?php if($settingscontractors[8]->allowfor_modify==1){ ?> checked="checked" <?php } ?>  rel="Financial Institute" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifyfinancialinstitute" value="1" /></td>
			<td><input <?php if($settingscontractors[8]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Financial Institute" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletefinancialinstitute" value="1" /></td>
		</tr>
		
		<tr>
			<td>Payments</td>
			<td><input <?php if($settingscontractors[9]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="3" rel2="add" class="permissioncls"  type="checkbox" name="chkaddpayments"  value="1" /></td>
			<td><input <?php if($settingscontractors[9]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditpayments" value="1" /></td>
			<td><input <?php if($settingscontractors[9]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifypayments" value="1" /></td>
			<td><input <?php if($settingscontractors[9]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Payments" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletepayments" value="1" /></td>
		</tr>
		
		<tr>
			<td>Documents</td>
			<td><input <?php if($settingscontractors[10]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkadddocument"  value="1" /></td>
			<td><input <?php if($settingscontractors[10]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditdocument" value="1" /></td>
			<td><input <?php if($settingscontractors[10]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifydocument" value="1" /></td>
			<td><input <?php if($settingscontractors[10]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Documents" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletedocument" value="1" /></td>
		</tr>
		
		<tr>
			<td>Purchase orders</td>
			<td><input <?php if($settings[11]->allowfor_add==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="3" rel2="add" class="permissioncls" type="checkbox" name="chkadddocument"  value="1" /></td>
			<td><input <?php if($settings[11]->allowfor_edit==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="3" rel2="edit" class="permissioncls" type="checkbox" name="chkeditdocument" value="1" /></td>
			<td><input <?php if($settings[11]->allowfor_modify==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="3" rel2="modify" class="permissioncls" type="checkbox" name="chkmodifydocument" value="1" /></td>
			<td><input <?php if($settings[11]->allow_delete==1){ ?> checked="checked" <?php } ?> rel="Purchase orders" rel1="3" rel2="delete" class="permissioncls" type="checkbox" name="chkdeletedocument" value="1" /></td>
		</tr>
		
	</table>
</fieldset>

<?php } */ ?>				

<script>
$(document).ready(function(){
	$('.permissioncls').click(function(){
		var usertype = $(this).attr('rel1');
		var menutype = $(this).attr('rel');
		var permissiontype = $(this).attr('rel2');
		var permissionstatus = 0;
		if($(this).prop('checked')==true)
		{
			permissionstatus = $(this).val();
		}
		
		$.ajax({
			 url: "administrator/managepermissionstatus",
			 type:"POST",
			 data:{
				 usertype:usertype,menutype:menutype,permissiontype:permissiontype,permissionstatus,permissionstatus
			 },
			 success: function(data)
			 {  

			 }
		 }); 
	 });
	 
	 
	 $('#permissiontype').change(function(){
		 var permissionusergroup = $(this).val();
		 
		 $.ajax({
			 url: "administrator/managepermissiontype",
			 type:"POST",
			 data:{
				 permissionusergroup:permissionusergroup
			 },
			 success: function(data)
			 {  
				$('#permissionpageid').html(data);
			 }
		 }); 
		 
	 });
	 
});
</script>		