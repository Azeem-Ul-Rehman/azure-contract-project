<?php $usertype = $this->session->userdata('admin_usertype'); 
$where = array('usertype'=>$usertype,'menutab'=>'Documents');
$chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION,$where); 
?>

<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">				<div class="col-8">					<h4 class="card-title">All Documents</h4>				</div>				
				<?php if($chkvalied->allowfor_add==1 || $usertype==1){	?>				<div class="col-4">					<a  class="btn btn-info" href="administrator/adddocuments"><i class="mdi mdi-plus"></i>Add documents</a>				</div>
				<?php } ?>							</div>						<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>					<div class="table-responsive">				<table id="order-listing" class="table">                    <thead>                      <tr>                          <th>Sno</th>						  <th>Project Name</th>                          <th>Name</th>                          <th>Image</th>						  <th>Date</th>						  <th>Actions</th>                      </tr>                    </thead>                    <tbody>					<?php					  if(!empty($documentslist))					  {						  $i=1;							  foreach ($documentslist as $perreq){							  							  $where = array('projectid'=>$perreq->pdid);							  $projectsval = $this->adminmodel->getSingle(TBLPROJECTS,$where);					?>
                      <tr>                          <td><?php echo $i;	 ?></td>						  <td><?php echo $projectsval->project_name;	 ?></td>						  <td><?php echo $perreq->docname;	 ?></td>						  <td>								<?php if($perreq->documenttype=='Image' || $perreq->documenttype=='jpg' || $perreq->documenttype=='JPEG' || $perreq->documenttype=='png' || $perreq->documenttype=='PNG' || $perreq->documenttype=='gif' || $perreq->documenttype=='bmp'){	 ?>								<img src="uploads/documents/<?php echo $perreq->documentimg; ?>" width="100" />								<?php }else if($perreq->documenttype=='Excel' || $perreq->documenttype=='xlsx' || $perreq->documenttype=='csv'){ ?>								<a target="_blank" href="uploads/documents/<?php echo $perreq->documentimg; ?>" download><img src="uploads/excelimg.png" width="100" /></a>								<?php }else if($perreq->documenttype=='Word' || $perreq->documenttype=='docx'){ ?>								<a target="_blank" href="uploads/documents/<?php echo $perreq->documentimg; ?>" download><img src="uploads/wordimg.png" width="100" /></a>								<?php }else if($perreq->documenttype=='pdf' || $perreq->documenttype=='PDF'){ ?>								<a target="_blank" href="uploads/documents/<?php echo $perreq->documentimg; ?>" download><img src="uploads/pdfimg.png" width="100" /></a>								<?php } ?>						  </td>						  <td><?php echo date('d.m.Y',strtotime($perreq->documentadddate));	 ?></td>                          <td>							                             
							<?php if($chkvalied->allowfor_modify==1 || $usertype==1){	?>	
							<a href="administrator/editdocuments?documentid=<?php echo $perreq->documentid; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<?php }else{ ?>
								NA
							<?php } ?>
							
							<?php if($chkvalied->allow_delete==1 || $usertype==1){	?>							<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->documentid; ?>)"><i class="fa fa-trash-o"></i></button>
							<?php }else{ ?>
								NA
							<?php } ?>  
							                          </td>					 </tr>
                  <?php 						$i++;					} 				}				  ?>
                    </tbody>                  </table>				  </div>                </div>              </div>            </div>          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>
	function show_confirmdelte(documentid)
	{
		swal({		  title: "Are you sure?",		  text: "You want to delete this documents?",		  icon: "warning",		  buttons: true,		  dangerMode: true,		})		.then((willDelete) => {		  if (willDelete) {			location.href="<?php echo base_url();?>administrator/deletedocuments?documentid="+documentid;		  } else {			//swal("Your imaginary file is safe!");		  }		});	}			
</script>
