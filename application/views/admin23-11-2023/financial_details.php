<?php include('partials/header.php'); ?>    <!-- partial -->    <div class="container-fluid page-body-wrapper"><?php include('partials/settings.php'); ?><?php include('partials/sidebar.php'); ?>   <style>.tab-minimal-success .nav-tabs .nav-item .nav-link.active {    color: #fb8332 !Important;}.tab-content {    border: none !Important;}</style>           <!-- partial -->      <div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-10">					<h4 class="card-title">Financial Institute Details</h4>				</div>				<div class="col-2">					<a  class="btn btn-info" href="administrator/financialinstitutionlist"><i class="mdi mdi-list"></i>Financial Institutes</a>				</div>			</div>					<div class="row">            <div class="col-lg-12">			
				<?php if($this->session->flashdata('error')){ ?>
				 
				 <div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
				  </div>

				<?php } ?>
				  
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                  <form  id="customerForm" name="customerForm" method="post" action="administrator/updatecompany_action">
                    <fieldset>                      
                      <div class="form-group">                        <label for="companyname"><b>Name : </b>  <?php echo $financialinstitutes->financialinstitutename; ?> </label>                      </div>					  					   <div class="form-group">                        <label for="companyname"><b>Phone : </b>  <?php echo $financialinstitutes->financialinstitutephone; ?> </label>                      </div>					  					  <div class="form-group">                        <label for="companyname"><b>Address : </b>  <?php echo $financialinstitutes->financialinstituteaddress; ?> </label>                      </div>					  					  <div class="form-group">                        <label for="companyname"><b>Account no : </b>  <?php echo $financialinstitutes->financialinstituteaccountno; ?> </label>                      </div>					  					  <div class="form-group">                        <label for="companyname"><b>Account Loan : </b>  <?php echo $financialinstitutes->financialinstituteaccountloan; ?> </label>                      </div>					  					 				  
                    </fieldset>
                  </form>
                </div>
              </div>			  			  			  			  			  			  <div class="tab-minimal tab-minimal-success">							<ul class="nav nav-tabs" role="tablist">							  <li class="nav-item" style="width:33%">								<a class="nav-link active" id="tab-2-1" data-toggle="tab" href="#home-2-1" role="tab" aria-controls="home-2-1" aria-selected="true">Loans</a>							  </li>							  							</ul>							<div class="tab-content">							  							  <div class="tab-pane fade show active" id="home-2-1" role="tabpanel" aria-labelledby="tab-2-1">										 																				<div class="table-responsive">										<table id="order-listingnew" class="table table-bordered">										<thead>										  <tr>											  											  <th>Project Name</th>											  <th>Loan Amount</th>											  <th>Interest Rate</th>											  <th>Period</th>											  <th>Redemption Amount</th>										  </tr>										</thead>										<tbody>										<?php 										foreach($financialloans as $rowsloan){ 																						$where = array('projectid'=>$rowsloan->project);											$projectvals = $this->adminmodel->getSingle(TBLPROJECTS,$where);													?>										  <tr>											  <td><a href="administrator/projectlist"><?php echo $projectvals->project_name; ?></a></td>											  <td><?php echo $rowsloan->loanamount;	 ?></td>											  <td><?php echo $rowsloan->interestrate;	 ?></td>											  <td><?php echo $rowsloan->period;	 ?></td>											  <td><?php echo $rowsloan->redemptionamount;	 ?></td>										 </tr>										<?php } ?>										</tbody>										</table>										</div>							  </div>														  							  													  							  							</div>						  </div>			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

		<script>
//firstname lastname username password email phone smssupport
/*$(document).ready(function(){ 
	$("#customerForm").validate({  
		rules: { 
			firstname: "required",
			lastname: "required",
			username: "required",
			email: {
				required: true,
				email: true
			},
			password:"required",
			phone: "required",
			smssupport: "required"
		}, 
		messages: {
			firstname: "Please enter first name",
			lastname: "Please enter last name",
			username: "please enter username",
			email: {
				required:"Please enter email address",
				email:"Please enter a valid email address",
			},
			password: "please enter password",
			phone: "Please enter phone number",
			smssupport: "Please select sms port"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	//$('#customerForm').validate();
});*/

function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "admin/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "admin/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


 function blockSpecialChar(e){	var k;	document.all ? k = e.keyCode : k = e.which;	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));}  

</script>		
		
<?php include('partials/footer.php'); ?>     
<script src="js/form-validation.js"></script> <script src="adminassets/js/formpickers.js"></script> 
