<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">
				<div class="col-10">
				  <h4 class="card-title">Edit User</h4>

				</div>
				<div class="col-2">
					<a  class="btn btn-info" href="administrator/userslist"><i class="fa fa-list"></i>Users</a>
				</div>
			</div>
				<div class="row">
            <div class="col-lg-12">
			
				<?php if($this->session->flashdata('error')){ ?>
				 
				 <div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
				  </div>

				<?php } ?>
				  
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                  <form  id="customerForm" name="customerForm" method="post" action="">
                    
					 <input value="<?php echo $this->input->get('userid'); ?>" name="hdnid" type="hidden">
					 
					<fieldset>
					    
					 <div class="form-group">
                        <label for="firstname">Usertype</label>
                        <select id="usertype" class="form-control required" name="usertype">
							<?php foreach($usersgroups as $rowsgroup){ ?>
								<option <?php if($usersinfo->usertype==$rowsgroup->id){ ?> selected="selected" <?php } ?> value="<?php echo $rowsgroup->id; ?>"><?php echo $rowsgroup->groupname; ?></option>
							<?php } ?>
							
                            <!--<option <?php if($usersinfo->usertype==2){ ?> selected="selected"  <?php } ?> value="2">Managers</option>
                            <option <?php if($usersinfo->usertype==3){ ?> selected="selected"  <?php } ?> value="3">Contractors</option>-->
                        </select>
                      </div>
                      
                      <div class="form-group">
                        <label for="firstname">Firstname</label>
                        <input id="firstname" value="<?php echo $usersinfo->firstname; ?>" class="form-control required" name="firstname" type="text">
                      </div>
                      <div class="form-group">
                        <label for="lastname">Lastname</label>
                        <input id="lastname" value="<?php echo $usersinfo->lastname; ?>" class="form-control required" name="lastname" type="text">
                      </div>              
                      <div class="form-group">
                        <label for="username">Username</label>
                        <input id="username" readonly="readonly" value="<?php echo $usersinfo->username; ?>" onchange="check_username(this.value);" class="form-control required" name="username" type="text">
						<p style="display:none;color:red;" class="chkusername_cls"></p>
                      </div>
					  
					  <div class="form-group">
                        <label for="phone">Phone</label>
                        <input id="phone" value="<?php echo $usersinfo->phone; ?>" class="form-control required" name="phone" type="text">
                      </div>     
					  
					  
                      <div class="form-group">
                        <label for="password">Password</label>
                        <input id="password" class="form-control required" 
						value="<?php echo $usersinfo->show_passkey; ?>" minlength="5" name="password" type="password">
                      </div>                
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" readonly="readonly" value="<?php echo $usersinfo->email; ?>" onchange="check_email(this.value);" class="form-control required email" name="email" type="email">
						<p style="display:none;color:red;" class="chkmail_cls"></p>
                      </div>
			
				       <button class="btn btn-primary" type="submit">Update</button>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

		<script>
function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "odgenius2671989/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "admin/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


$('.smssupportcls').click(function(){
	 if($(this).val()=='yes'){
		$('.smssupportcls_field').css('display','block');
	 }else{
		$('.smssupportcls_field').css('display','none');
	 }
 });

</script>		
		
<?php include('partials/footer.php'); ?> 
<script src="js/form-validation.js"></script> 
<script src="js/formpickers.js"></script>        
