<?php include('partials/header.php'); ?>    <!-- partial -->    <div class="container-fluid page-body-wrapper"><?php include('partials/settings.php'); ?><?php include('partials/sidebar.php'); ?>            <!-- partial -->      <div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-10">					<h4 class="card-title">Add New Loan</h4>				</div>								<div class="col-2">					<a  class="btn btn-info" href="administrator/loan_list">Loan List</a>				</div>			</div>					<div class="row">            <div class="col-lg-12">			
				<?php if($this->session->flashdata('error')){ ?>
				 
				 <div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
				  </div>

				<?php } ?>
				  
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                 <form  id="customerFormLoans" name="customerFormLoans" method="post" action="">										<input type="hidden" name="hdncompanyid" id="hdncompanyid" value="<?php echo $company_id;	?>" />										<fieldset>																				  <div class="form-group">											<label for="companyname">Company</label>											<select name="companyname" id="companyname" class="form-control required">												<option value="">Select Company</option>												<?php foreach($companylist as $rowscompany){ ?>													<option value="<?php echo $rowscompany->company_id; ?>"><?php echo $rowscompany->company_name; ?></option>												<?php } ?>											</select>										  </div>										  										  <div class="form-group">											<label for="financialinstitution">Financial institution</label>											<select name="financialinstitution" id="financialinstitution" class="form-control required">												<option value="">Select Financial Institution</option>												<?php foreach($financialinstitutelist as $rowsfinancialinstitute){ ?>													<option value="<?php echo $rowsfinancialinstitute->financialinstituteid; ?>"><?php echo $rowsfinancialinstitute->financialinstitutename; ?></option>												<?php } ?>											</select>										  </div>										  										  <div class="form-group">											<label for="project">Project</label>											<select name="project" id="project" class="form-control required">												<option value="">Select Project</option>												<?php foreach($projectlist as $rowsproject){ ?>													<option value="<?php echo $rowsproject->projectid; ?>"><?php echo $rowsproject->project_name; ?></option>												<?php } ?>											</select>										  </div>																		 										  										  <div class="form-group">											<label for="loanamount">Loan Amount</label>											<input id="loanamount" class="form-control required" name="loanamount" type="number">										  </div>										  										  <div class="form-group">											<label for="interestrate">Interest rate</label>											<input id="interestrate" class="form-control required" name="interestrate" type="number">										  </div>										  										  <div class="form-group">											<label for="period">Period</label>											<input id="period" class="form-control required" name="period" type="number">										  </div>										  										   <div class="form-group">											<label for="startdate">First Date</label>											<input id="startdate" class="form-control required" name="firstdate" type="text">										  </div>										  										  <div class="form-group">											<label for="enddate">End Date</label>											<input id="enddate" class="form-control required" name="enddate" type="text">										  </div>										  										  <div class="form-group">											<label for="redemptionamount">Redemption Amount</label>											<input id="redemptionamount" class="form-control required" name="redemptionamount" type="number">										  </div>										 										  <button class="btn btn-primary" id="loanbtn" type="submit">Submit</button>										</fieldset>									  </form>									  
                </div>
              </div>
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

		<script>
//firstname lastname username password email phone smssupport
/*$(document).ready(function(){ 
	$("#customerForm").validate({  
		rules: { 
			firstname: "required",
			lastname: "required",
			username: "required",
			email: {
				required: true,
				email: true
			},
			password:"required",
			phone: "required",
			smssupport: "required"
		}, 
		messages: {
			firstname: "Please enter first name",
			lastname: "Please enter last name",
			username: "please enter username",
			email: {
				required:"Please enter email address",
				email:"Please enter a valid email address",
			},
			password: "please enter password",
			phone: "Please enter phone number",
			smssupport: "Please select sms port"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	//$('#customerForm').validate();
});*/

function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "admin/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "admin/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


 function blockSpecialChar(e){	var k;	document.all ? k = e.keyCode : k = e.which;	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));}  

</script>		
		
<?php include('partials/footer.php'); ?>     
<script src="js/form-validation.js"></script> <script src="adminassets/js/formpickers.js"></script> 
