<?php $usertype = $this->session->userdata('admin_usertype'); 
$where = array('usertype'=>$usertype,'menutab'=>'Payments');
$chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION,$where); 
?>

<?php include('partials/header.php'); ?>
    <!-- partial -->    <div class="container-fluid page-body-wrapper">
	<?php include('partials/settings.php'); ?>
	<?php include('partials/sidebar.php'); ?>            
	<!-- partial -->      
	<div class="main-panel">        
	<div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-8">					<h4 class="card-title">All Payment List</h4>				</div>								
	
	<?php if($chkvalied->allowfor_add==1 || $usertype==1){	?>
		<div class="col-4"><a  class="btn btn-info" href="administrator/addpayment"><i class="mdi mdi-plus"></i>Add Payment</a></div>
	<?php } ?>
	
	
	
	

	</div>						<div class="row">                <div class="col-12">								<?php if($this->session->flashdata('success')){ ?>				  				  <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				  				<?php } ?>								<div class="table-responsive">				<table id="order-listing" class="table">                    <thead>                      <tr>                          <!--<th>Sno</th>-->						  <th>Date</th>						  <th>Amount</th>						  <th>Paid to</th>						  <th>Method</th>						  <th>Paid By</th>                          <th>Project</th>						  <!--<th>Purchase Order</th>-->						  <!--<th>Description</th>-->                          <th>Actions</th>                      </tr>                    </thead>                    <tbody>					<?php					  if(!empty($paymentslist))					  {						  $i=1;							  foreach ($paymentslist as $perreq){							  							  $where = array('projectid'=>$perreq->project);							  $projects = $this->adminmodel->getSingle(TBLPROJECTS,$where);					?>                      <tr>                          <!--<td><?php echo $i; ?></td>-->						  <td><?php echo date('d.m.Y',strtotime($perreq->paymentdate)); ?></td>						  <td><?php echo $perreq->Amount; ?></td>						  <td><?php echo $perreq->paidto; ?></td>						  <td><?php echo $perreq->paymentmethod; ?></td>						  <td><?php echo $perreq->paidbycompany; ?></td>						  <td><?php echo $projects->project_name; ?></td>						  <!--<td><?php echo $perreq->purchaseorder; ?></td>-->						  <!--<td><?php echo $perreq->paymentdescription; ?></td>-->                          
	
	<td>							               			
	
	<?php if($chkvalied->allowfor_modify==1 || $usertype==1){	?>	
	<a href="administrator/editpayment?paymentid=<?php echo $perreq->paymentid; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>	
	<?php }else{ ?>
		NA
	<?php } ?>

	<?php if($chkvalied->allow_delete==1 || $usertype==1){	?>
	<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->paymentid; ?>)"><i class="fa fa-trash-o"></i></button>  
	<?php }else{ ?>
		NA
	<?php } ?>  



	</td>					 </tr>                  <?php 						$i++;					} 				}				  ?>                    </tbody>                  </table>				  				  </div>                </div>              </div>            </div>          </div>        </div>        
	
	
	
	
<!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>	function show_confirmdelte(paymentid)	{		swal({		  title: "Are you sure?",		  text: "You want to delete this payment?",		  icon: "warning",		  buttons: true,		  dangerMode: true,		})		.then((willDelete) => {		  if (willDelete) {			location.href="<?php echo base_url();?>administrator/deletepayment?paymentid="+paymentid;		  } else {			  		  }		});	}			

</script>