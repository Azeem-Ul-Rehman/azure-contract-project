<?php $usertype = $this->session->userdata('admin_usertype'); 
$where = array('usertype'=>$usertype,'menutab'=>'Loans');
$chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION,$where); 
?>
<?php include('partials/header.php'); ?>    
<!-- partial --> 
<div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>            
<!-- partial -->      
<div class="main-panel">        
<div class="content-wrapper">          
<div class="card">           
 <div class="card-body">			
 <div class="row">				
 <div class="col-8">					
	<h4 class="card-title">All Loan List</h4>				
 </div>								
 
 <?php if($chkvalied->allowfor_add==1 || $usertype==1){	?>
 <div class="col-4">					
	<a  class="btn btn-info" href="administrator/addloan"><i class="mdi mdi-plus"></i>Add Loan</a>
 </div>	
 <?php } ?>
 
 
</div>						
<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>								<div class="table-responsive">					<table id="order-listing" class="table">					<thead>					  <tr>						  <!--<th>Sno</th>-->						  <th>Company</th>						  <th>Institution</th>						  <th>Project</th>						  <th>Loan</th>						  <th>Interest</th>						  <th>Period</th>						  <th>FirstDate</th>						  <th>EndDate</th>						  <th>Redemption</th>						  <th>Actions</th>					  </tr>					</thead>					<tbody>					<?php 					  if(!empty($loanlist))					  {						  $i=1;							foreach($loanlist as $rowsproject){ 												$where=array('is_deleted'=>0,'company_id'=>$rowsproject->companyid);						$companyvalue = $this->adminmodel->getSingle(TBLCOMPANY,$where);												$where=array('is_deleted'=>0,'financialinstituteid'=>$rowsproject->financialinstitution);						$financialinstitutevalue = $this->adminmodel->getSingle(TBLFINANCIALINSTITUTE,$where);												$where=array('projectid'=>$rowsproject->project);						$projects = $this->adminmodel->getSingle(TBLPROJECTS,$where);					?>					  

<tr>						  <!--<td><?php echo $i;	 ?></td>-->						  <td><?php echo $companyvalue->company_name;	 ?></td>						  <td><?php echo $financialinstitutevalue->financialinstitutename;	?></td>						  <td><?php echo $projects->project_name;	 ?></td>						  <td><?php echo $rowsproject->loanamount;	 ?></td>						  <td><?php echo $rowsproject->interestrate;	 ?></td>						  <td><?php echo $rowsproject->period;	 ?></td>						  <td><?php echo $rowsproject->firstdate;	 ?></td>						  <td><?php echo $rowsproject->enddate;	 ?></td>						  <td><?php echo $rowsproject->redemptionamount;	 ?></td>						   <td>							                             

<?php if($chkvalied->allowfor_modify==1 || $usertype==1){	?>	
<a href="administrator/editloan?loanid=<?php echo $rowsproject->companyloanid; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>	
<?php }else{ ?>
NA
<?php } ?>						

<?php if($chkvalied->allow_delete==1 || $usertype==1){	?>
<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $rowsproject->companyloanid; ?>)"><i class="fa fa-trash-o"></i></button>    
<?php }else{ ?>
NA
<?php } ?>                     


 </td>					 


</tr>					<?php 						$i++;						} 					} ?>					</tbody>					</table>					</div>                </div>              </div>            </div>          </div>        </div>        <!-- content-wrapper ends --><?php include('partials/footer.php'); ?>    <script src="js/data-table.js"></script>   <script src="js/alerts.js"></script><script>	function show_confirmdelte(loanid)	{		swal({		  title: "Are you sure?",		  text: "You want to delete this loan?",		  icon: "warning",		  buttons: true,		  dangerMode: true,		})		.then((willDelete) => {		  if (willDelete) {			location.href="<?php echo base_url();?>administrator/deleteloan?loanid="+loanid;		  } else {					  }		});	}			</script>