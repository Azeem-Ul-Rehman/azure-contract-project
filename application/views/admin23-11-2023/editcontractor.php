<?php include('partials/header.php'); ?>    <!-- partial -->    <div class="container-fluid page-body-wrapper"><?php include('partials/settings.php'); ?><?php include('partials/sidebar.php'); ?>            <!-- partial -->      <div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-10">					<h4 class="card-title">Edit Contractors</h4>				</div>				<div class="col-2">					<a  class="btn btn-info" href="administrator/contractors_list"><i class="mdi mdi-list"></i>Contractors List</a>				</div>			</div>					<div class="row">            <div class="col-lg-12">			
				<?php if($this->session->flashdata('error')){ ?>
				 
				 <div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
				  </div>

				<?php } ?>
				  
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                  <form  id="customerForm" name="customerForm" method="post" action="">
                    <fieldset>                       					<input id="contractor_id" value="<?php echo $contractors->contractor_id; ?>" class="form-control" name="hdncontractor" type="hidden">				  
                      <div class="form-group">                        <label for="contractor_name">Contractor Name</label>                        <input id="contractor_name" value="<?php echo $contractors->contractor_name; ?>"  class="form-control required" name="contractor_name" type="text">                      </div>										  <div class="form-group">                        <label for="contractor_phone">Contractor Phone No</label>                        <input id="contractor_phone" value="<?php echo $contractors->contractor_phone; ?>" class="form-control required" name="contractor_phone" type="number">                      </div>					  					  <div class="form-group">                        <label for="phoneno">Contractor Type</label>                        <select name="contractor_type" id="contractor_type"  class="form-control required">														<?php if(!empty($contractortypes)){ ?>								<?php foreach($contractortypes as $rowscontractors){ ?>									<option <?php if($rowscontractors->contractortype==$contractors->contractor_type){ ?> selected="selected" <?php } ?> value="<?php echo $rowscontractors->contractortype; ?>"><?php echo $rowscontractors->contractortype; ?></option>							<?php }							} 							?>													</select>                      </div>					  					  <div class="form-group">                        <label for="contractor_person">Contractor persons</label>                        <input id="contractor_person" value="<?php echo $contractors->contractor_person; ?>" class="form-control required" name="contractor_person" type="text">                      </div>					  					   <div class="form-group">                        <label for="contractor_address">Contractor Address</label>                        <textarea id="contractor_address" class="form-control required" name="contractor_address"><?php echo $contractors->contractor_address; ?></textarea>                      </div>					  					 <!--div class="form-group">                        <label for="contractor_assignproject">Assigned Project</label>                        <select name="contractor_assignproject[]" class="form-control required" id="contractor_assignproject" multiple>												<?php foreach($projectslist as  $rowsval){ ?>													<?php								$selectedvalue = '';								$loansarr = explode(',',$contractors->contractor_assignproject);																if(in_array($rowsval->projectid,$loansarr)){									$selectedvalue = "selected='selected'";								}							 ?>													  <option <?php echo $selectedvalue; ?> value="<?php echo $rowsval->projectid; ?>"><?php echo $rowsval->project_name; ?></option>						  						<?php } ?>						  						</select>                      </div>-->										  					  <div class="form-group">                        <label for="contractor_cribno">CRIB No</label>                        <input id="contractor_cribno" class="form-control required" value="<?php echo $contractors->contractor_cribno; ?>" name="contractor_cribno" type="number">                      </div>					  					   <div class="form-group">                        <label for="contractor_kvkno">KVK No</label>                        <input id="contractor_kvkno" class="form-control required"  value="<?php echo $contractors->contractor_kvkno; ?>" name="contractor_kvkno" type="number">                      </div>					  					  <div class="form-group">                        <label for="contractor_payments">Contractor payments</label>                        <input readonly="readonly" id="contractor_payments" class="form-control required" value="<?php echo $contractoramount; ?>" name="contractor_payments" type="number">                      </div>					  					  					 
                      <button class="btn btn-primary" type="submit">Submit</button>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

		<script>
//firstname lastname username password email phone smssupport
/*$(document).ready(function(){ 
	$("#customerForm").validate({  
		rules: { 
			firstname: "required",
			lastname: "required",
			username: "required",
			email: {
				required: true,
				email: true
			},
			password:"required",
			phone: "required",
			smssupport: "required"
		}, 
		messages: {
			firstname: "Please enter first name",
			lastname: "Please enter last name",
			username: "please enter username",
			email: {
				required:"Please enter email address",
				email:"Please enter a valid email address",
			},
			password: "please enter password",
			phone: "Please enter phone number",
			smssupport: "Please select sms port"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	//$('#customerForm').validate();
});*/

function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "admin/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "admin/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


 function blockSpecialChar(e){	var k;	document.all ? k = e.keyCode : k = e.which;	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));}  

</script>		
		
<?php include('partials/footer.php'); ?>     
<script src="js/form-validation.js"></script> <script src="adminassets/js/formpickers.js"></script> 
