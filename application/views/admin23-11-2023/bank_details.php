<?php include('partials/header.php'); ?>    <!-- partial -->    <div class="container-fluid page-body-wrapper"><?php include('partials/settings.php'); ?><?php include('partials/sidebar.php'); ?>   <style>.tab-minimal-success .nav-tabs .nav-item .nav-link.active {    color: #fb8332 !Important;}.tab-content {    border: none !Important;}</style>           <!-- partial -->      <div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-10">					<h4 class="card-title">Bank Details</h4>				</div>				<div class="col-2">					<a  class="btn btn-info" href="administrator/bankaccountlist"><i class="mdi mdi-list"></i>Bank List</a>				</div>			</div>					<div class="row">            <div class="col-lg-12">			
				<?php if($this->session->flashdata('error')){ ?>
				 
				 <div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
				  </div>

				<?php } ?>
				  				<?php 									$where=array('is_deleted'=>0,'company_id'=>$banks->companyid);					$companylist = $this->adminmodel->getSingle(TBLCOMPANY,$where);									?>
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                  <form  id="customerForm" name="customerForm" method="post" action="administrator/updatecompany_action">
                    <fieldset>                      
                      <div class="form-group">                        <label for="companyname"><b>Company Name : </b>  <?php echo $companylist->company_name;	 ?> </label>                      </div>					  					  <div class="form-group">                        <label for="companyname"><b>Bank Name : </b>  <?php echo $banks->bankname; ?> </label>                      </div>					  					  <div class="form-group">                        <label for="companyname"><b>Account Type : </b>  <?php echo $banks->accounttype; ?> </label>                      </div>					  					  <div class="form-group">                        <label for="companyname"><b>Account Name : </b>  <?php echo $banks->accountname; ?> </label>                      </div>					  					  <div class="form-group">                        <label for="companyname"><b>Start Ballence : </b>  <?php echo $banks->startballence; ?> </label>                      </div>					  					  <div class="form-group">                        <label for="companyname"><b>Current Ballence : </b>  <?php echo $banks->currentballence; ?> </label>                      </div>					  					  <!--<div class="form-group">                        <label for="companyname"><b>Payments : </b>  <?php echo $contractorsval->contractor_payments; ?> </label>                      </div>					  					   <div class="form-group">                        <label for="companyname"><b>CRIB No : </b>  <?php echo $contractorsval->contractor_cribno; ?> </label>                      </div>					  					   <div class="form-group">                        <label for="companyname"><b>KVK No : </b>  <?php echo $contractorsval->contractor_kvkno; ?> </label>                      </div>-->					  
                    </fieldset>
                  </form>
                </div>
              </div>			  			  			  						  			  			  			  			  
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

		<script>
//firstname lastname username password email phone smssupport
/*$(document).ready(function(){ 
	$("#customerForm").validate({  
		rules: { 
			firstname: "required",
			lastname: "required",
			username: "required",
			email: {
				required: true,
				email: true
			},
			password:"required",
			phone: "required",
			smssupport: "required"
		}, 
		messages: {
			firstname: "Please enter first name",
			lastname: "Please enter last name",
			username: "please enter username",
			email: {
				required:"Please enter email address",
				email:"Please enter a valid email address",
			},
			password: "please enter password",
			phone: "Please enter phone number",
			smssupport: "Please select sms port"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	//$('#customerForm').validate();
});*/

function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "admin/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "admin/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


 function blockSpecialChar(e){	var k;	document.all ? k = e.keyCode : k = e.which;	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));}  

</script>		
		
<?php include('partials/footer.php'); ?>     
<script src="js/form-validation.js"></script> <script src="adminassets/js/formpickers.js"></script> 
