<?php $usertype = $this->session->userdata('admin_usertype'); 
$where = array('usertype'=>$usertype,'menutab'=>'Compnies');
$chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION,$where); 
?>
<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">				<div class="col-8">					<h4 class="card-title">All Companies</h4>				</div>								<div class="col-4">					<a  class="btn btn-info" href="administrator/addnewcompany"><i class="mdi mdi-plus"></i>Add Company</a>				</div>							</div>						<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>					<div class="table-responsive">				<table id="order-listing" class="table">                    <thead>                      <tr>                          <!--<th>Sno</th>-->                          <th>Company name</th>                          <!--<th>Found date</th>-->						  <th>Phone</th>						  <!--<th>Partners</th>-->                          <th>Email</th>						  <th>Address</th>						  <th>CRIB No</th>                          <!--<th>KVK No</th>-->						  <th>Actions</th>                      </tr>                    </thead>                    <tbody>					<?php					  if(!empty($companylist))					  {						  $i=1;							  foreach ($companylist as $perreq){					?>
                      <tr>                          <!--<td><?php echo $i;	 ?></td>-->
			
			
			<?php if($chkvalied->allowfor_edit==1 || $usertype==1){	?>				  
				<td><a href="administrator/company_details/?company_id=<?php echo $perreq->company_id; ?>"><?php echo $perreq->company_name; ?></a></td>
			<?php }else{ ?>
				<td><a href="javascript:void(0);"><?php echo $perreq->company_name; ?></a></td>
			<?php } ?>
					  
					  
					  
					  
					  <!--<td><?php echo $perreq->found_date;	 ?></td>-->						  <td><?php echo $perreq->phoneno;	 ?></td>						  <!--<td><?php echo $perreq->company_partners;	 ?></td>-->						  <td><?php echo $perreq->email;	 ?></td>						  <td><?php echo $perreq->address;	 ?></td>						  <td><?php echo $perreq->crib_no;	 ?></td>						  <!--<td><?php echo $perreq->kvk_no;	 ?></td>-->                          
					  
					  
					  <td>
					  
					  <?php if($chkvalied->allowfor_modify==1 || $usertype==1){ ?>	
					  
					  <a href="administrator/editcompany?company_id=<?php echo $perreq->company_id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
					  
					  <?php }else{ ?>
						NA
					  <?php } ?>

					  <?php if($chkvalied->allow_delete==1 || $usertype==1){ ?>	
					  <button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->company_id; ?>)"><i class="fa fa-trash-o"></i></button>
					  <?php }else{ ?>
						NA
					  <?php } ?>   

					  
					  
					  </td>					

					  </tr>
					  
					  
					  
                  <?php 						$i++;					} 				}				  ?>
                    </tbody>                  </table>				  </div>                </div>              </div>            </div>          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>
	function show_confirmdelte(company_id)
	{
		swal({
		  title: "Are you sure?",
		  text: "You want to delete this company?",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			location.href="<?php echo base_url();?>administrator/deletecompany?company_id="+company_id;
		  } else {
			//swal("Your imaginary file is safe!");
		  }
		});
	}			
</script>
