<?php include('partials/header.php'); ?>    <!-- partial -->    <div class="container-fluid page-body-wrapper"><?php include('partials/settings.php'); ?><?php include('partials/sidebar.php'); ?>            <!-- partial -->      <div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-10">					<h4 class="card-title">Edit New Bank Account</h4>				</div>								<div class="col-2">					<a  class="btn btn-info" href="administrator/bankaccountlist">Bank Account List</a>				</div>			</div>					<div class="row">            <div class="col-lg-12">			
				<?php if($this->session->flashdata('error')){ ?>
				 
				 <div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
				  </div>

				<?php } ?>
				  
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                <form  id="customerFormbankaccounts" name="customerFormbankaccounts" method="post" action="">										<input type="hidden" name="hdnbankid" id="hdnbankid" value="<?php echo $banks->companybankid; ?>" />										<fieldset>										  										  <div class="form-group">											<label for="companyname">Company</label>											<select name="companyname" id="companyname" class="form-control required">												<option value="">Select Company</option>												<?php foreach($companylist as $rowscompany){ ?>													<option <?php if($banks->companyid==$rowscompany->company_id){ ?> selected="selected" <?php } ?> value="<?php echo $rowscompany->company_id; ?>"><?php echo $rowscompany->company_name; ?></option>												<?php } ?>											</select>										  </div>										  										  										  <div class="form-group">											<label for="bankname">Bank Name</label>											<input id="bankname" class="form-control required" value="<?php echo $banks->bankname; ?>" name="bankname" type="text">										  </div>										 										  <div class="form-group">											<label for="accounttype">Account Type</label>											<input id="accounttype" class="form-control required" value="<?php echo $banks->accounttype; ?>" name="accounttype" type="text">										  </div>										  										  <div class="form-group">											<label for="accountname">Account Name</label>											<input id="accountname" class="form-control required" value="<?php echo $banks->accountname; ?>" name="accountname" type="text">										  </div>										  										  <div class="form-group">											<label for="startballence">Start Ballence</label>											<input id="startballence" class="form-control required" value="<?php echo $banks->startballence; ?>" name="startballence" type="number">										  </div>										  										  <div class="form-group">											<label for="currentballence">Current Ballence</label>											<input id="currentballence" class="form-control required" value="<?php echo $banks->currentballence; ?>" name="currentballence" type="number">										  </div>										 										  <button class="btn btn-primary" id="bankbtn" type="Submit">Submit</button>										</fieldset>									  </form>									  
                </div>
              </div>
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

		<script>
//firstname lastname username password email phone smssupport
/*$(document).ready(function(){ 
	$("#customerForm").validate({  
		rules: { 
			firstname: "required",
			lastname: "required",
			username: "required",
			email: {
				required: true,
				email: true
			},
			password:"required",
			phone: "required",
			smssupport: "required"
		}, 
		messages: {
			firstname: "Please enter first name",
			lastname: "Please enter last name",
			username: "please enter username",
			email: {
				required:"Please enter email address",
				email:"Please enter a valid email address",
			},
			password: "please enter password",
			phone: "Please enter phone number",
			smssupport: "Please select sms port"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	//$('#customerForm').validate();
});*/

function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "admin/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "admin/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


 function blockSpecialChar(e){	var k;	document.all ? k = e.keyCode : k = e.which;	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));}  

</script>		
		
<?php include('partials/footer.php'); ?>     
<script src="js/form-validation.js"></script> <script src="adminassets/js/formpickers.js"></script> 
