<?php $usertype = $this->session->userdata('admin_usertype'); 
$where = array('usertype'=>$usertype,'menutab'=>'Projects');
$chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION,$where); 
?>
<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">				<div class="col-8">					<h4 class="card-title">All Projects</h4>				</div>								<div class="col-4">					<a  class="btn btn-info" href="administrator/addprojects"><i class="mdi mdi-plus"></i>Add Projects</a>				</div>							</div>						<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>						<div class="table-responsive">				<table id="order-listing" class="table">                    <thead>                      <tr>                          <!--<th>Sno</th>-->						  <th>Project Name</th>                           <!--<th>Company name</th>                          <th>Description</th>						  <th>Details</th>-->						  <th>Type</th>                          <th>Location</th>						 <!-- <th>Startdate</th>						  <th>Enddate</th>-->                          <th>Estimated cost</th>						  <th>Total payment</th>						  <th>Balance</th>						  <th>Status</th>						  <th>Actions</th>                      </tr>                    </thead>                    <tbody>					<?php					  if(!empty($projectlists))					  {						  $i=1;							  foreach ($projectlists as $perreq){							  							  $where=array('company_id'=>$perreq->companyname);							  $companylist = $this->adminmodel->getSingle(TBLCOMPANY,$where);							  							  $sql = $this->db->query('select SUM(Amount) as totalpayment from '.TBLPAYMENTS.' where project="'.$perreq->projectid.'"');							  $getrows = $sql->row();							  $totalpayment = $getrows->totalpayment;					?>
                      <tr>                           <!--<td><?php echo $i;	 ?></td>-->						  
						  <?php if($chkvalied->allowfor_edit==1 || $usertype==1){	?>	
						  <td><a href="administrator/project_details/?projectid=<?php echo $perreq->projectid; ?>"><?php echo $perreq->project_name;	 ?></a></td>
						  <?php }else{ ?>
							<td><a href="javascript:void(0);"><?php echo $perreq->project_name; ?></a></td>
						<?php } ?>
			
						  
						  						  <!--<td><?php echo $companylist->company_name; ?></td>						  <td><?php echo $perreq->projectdesc;	 ?></td>						  <td><?php echo $perreq->projectdetails;	 ?></td>-->						  <td><?php echo $perreq->projecttype;	 ?></td>						  <td><?php echo $perreq->location;	 ?></td>						   <!--<td><?php echo $perreq->startdate;	 ?></td>						  <td><?php echo $perreq->enddate;	 ?></td>-->						  <td><?php echo $perreq->totalestimatedcost;	 ?></td>						  <td><?php echo $totalpayment; //$perreq->totalpayment;	 ?></td>						  <td><?php echo $perreq->totalestimatedcost-$totalpayment;	 ?></td>						  <td><?php echo $perreq->status;	 ?></td>                          <td>							                             
							 <?php if($chkvalied->allowfor_modify==1 || $usertype==1){ ?>	
							<a href="administrator/editprojects?projectid=<?php echo $perreq->projectid; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							 <?php }else{ ?>
								NA
							<?php } ?>
							
							<?php if($chkvalied->allow_delete==1 || $usertype==1){ ?>								<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->projectid; ?>);"><i class="fa fa-trash-o"></i></button>
							<?php }else{ ?>
								NA
							<?php } ?>   
							
							                          </td>					 </tr>
                  <?php 						$i++;					} 				}				  ?>
                    </tbody>                  </table>				  </div>                </div>              </div>            </div>          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>	function show_confirmdelte(projectid)	{		swal({		  title: "Are you sure?",		  text: "You want to delete this projects?",		  icon: "warning",		  buttons: true,		  dangerMode: true,		})		.then((willDelete) => {		  if (willDelete) {			location.href="<?php echo base_url();?>administrator/deletedprojects?projectid="+projectid;		  } else {			  		  }		});	}			</script>
