<?php include('partials/header.php'); ?>    <!-- partial -->    <div class="container-fluid page-body-wrapper"><?php include('partials/settings.php'); ?><?php include('partials/sidebar.php'); ?>            <!-- partial -->      <div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-10">					<h4 class="card-title">Edit Documents</h4>				</div>								<div class="col-2">					<a  class="btn btn-info" href="administrator/documentlist">Documents List</a>				</div>			</div>					<div class="row">            <div class="col-lg-12">			
				<?php if($this->session->flashdata('error')){ ?>			 				  <div class="alert alert-danger alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>				  </div>
				<?php } ?>
				  
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                  <form  id="customerForm" enctype="multipart/form-data" name="customerForm" method="post" action="">					<input type="hidden" name="documentid"  id="documentid" value="<?php echo $documentsval->documentid; ?>" />
                    <fieldset>										 <div class="form-group">                        <label for="docname">Project</label>                        <select name="projectname" id="projectname" class="form-control required">							<option value="">Select Project</option>														<?php foreach($projectlist as $rowsprojects){ ?>								<option <?php if($documentsval->pdid==$rowsprojects->projectid){ ?> selected="selected" <?php } ?> value="<?php echo $rowsprojects->projectid; ?>"><?php echo $rowsprojects->project_name; ?></option>							<?php } ?>													</select>						                      </div>					  						
                      <div class="form-group">                        <label for="docname">Documents Name</label>                        <input id="docname" value="<?php echo $documentsval->docname; ?>" class="form-control required"  name="docname" type="text">                      </div>					  					   <!--<div class="form-group">                        <label for="documenttype">Document Type</label>                        <select name="documenttype" id="documenttype" class="form-control required">							<option <?php if($documentsval->documenttype=='Image'){ ?> selected="selected" <?php } ?> value="Image">Image</option>							<option <?php if($documentsval->documenttype=='Excel'){ ?> selected="selected" <?php } ?> value="Excel">Excel</option>							<option <?php if($documentsval->documenttype=='Pdf'){ ?> selected="selected" <?php } ?> value="Pdf">Pdf</option>							<option  <?php if($documentsval->documenttype=='Word'){ ?> selected="selected" <?php } ?> value="Word">Word</option>						</select>                      </div>-->					  					  <input type="hidden" name="hdndocumenttype" value="<?php echo $documentsval->documenttype; ?>" />					 					   <div class="form-group">                         <label for="documentimg">Documents</label>                        <input id="documentimg" class="form-control"  name="documentimg" type="file">                      </div>					  								<?php if($documentsval->documenttype=='Image'){	 ?>								<img src="uploads/documents/<?php echo $documentsval->documentimg; ?>" width="100" />								<?php }else if($documentsval->documenttype=='Excel'){ ?>								<a target="_blank" href="uploads/documents/<?php echo $documentsval->documentimg; ?>" download><img src="uploads/excelimg.png" width="100" /></a>								<?php }else if($documentsval->documenttype=='Word'){ ?>								<a target="_blank" href="uploads/documents/<?php echo $documentsval->documentimg; ?>" download><img src="uploads/wordimg.png" width="100" /></a>								<?php }else if($documentsval->documenttype=='Pdf'){ ?>								<a target="_blank" href="uploads/documents/<?php echo $documentsval->documentimg; ?>" download><img src="uploads/pdfimg.png" width="100" /></a>								<?php } ?>																				<br/><br/>					  					  <input type="hidden" name="hdnimg" id="hdnimg" value="<?php echo $documentsval->documentimg; ?>" />										  					
					 
                      <button class="btn btn-primary" type="submit">Update</button>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

		<script>
//firstname lastname username password email phone smssupport
/*$(document).ready(function(){ 
	$("#customerForm").validate({  
		rules: { 
			firstname: "required",
			lastname: "required",
			username: "required",
			email: {
				required: true,
				email: true
			},
			password:"required",
			phone: "required",
			smssupport: "required"
		}, 
		messages: {
			firstname: "Please enter first name",
			lastname: "Please enter last name",
			username: "please enter username",
			email: {
				required:"Please enter email address",
				email:"Please enter a valid email address",
			},
			password: "please enter password",
			phone: "Please enter phone number",
			smssupport: "Please select sms port"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	//$('#customerForm').validate();
});*/

function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "admin/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "admin/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


 function blockSpecialChar(e){	var k;	document.all ? k = e.keyCode : k = e.which;	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));}  

</script>		
		
<?php include('partials/footer.php'); ?>     
<script src="js/form-validation.js"></script> <script src="adminassets/js/formpickers.js"></script> 
