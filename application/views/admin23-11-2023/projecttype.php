<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">
				<div class="col-10">
				  <h4 class="card-title">Project Types</h4>
				</div>
				<div class="col-2 add-btn">
					<a  class="btn btn-info" href="administrator/addprojecttype"><i class="mdi mdi-plus"></i>Add Project Type</a>
				</div>
			</div>
				<div class="row">
                <div class="col-12">
				<?php if($this->session->flashdata('success')){ ?>
				 
				 <div class="alert alert-success alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>
				  </div>

				<?php } ?>
				
                  <table id="order-listing" class="table">
                    <thead>
                      <tr>
                          <th>Sno</th>
						  <th>Project Type</th>
                          <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php
					  if(!empty($projectypes))
					  {
						  
						  $i=1;	
						  foreach ($projectypes as $perreq){
					?>
                      <tr>
                          <td><?php echo $i;	 ?></td>
						  <td><?php echo $perreq->projecttype;	 ?></td>
                          <td class="action-clm">
                            
							<a href="administrator/editprojecttype?id=<?php echo $perreq->id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							
							<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->id ; ?>)"><i class="fa fa-trash-o"></i></button>
							
                          </td>
                      </tr>
                  <?php 
						$i++;
					} 
				}
				  ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="adminassets/js/data-table.js"></script>   
<script src="adminassets/js/alerts.js"></script>
<script>
	function show_confirmdelte(id)
	{
		swal({
		  title: "Are you sure?",
		  text: "You want to delete this project type!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			location.href="<?php echo base_url();?>administrator/deleteprojecttype?id="+id;
		  } else {
			//swal("Your imaginary file is safe!");
		  }
		});

		
	}	
	
	/*function show_activated(userid)	
	{		
		swal({		   
			  title: "Are you sure?",		
			  text: "You want to activate this subadmin!",		
			   icon: "warning",		
			   buttons: true,		
			   dangerMode: true,		
			   }).then((willDelete) => {		
						  if (willDelete) {			
							  location.href="<?php echo base_url();?>odgenius2671989/subadmin-activated?uid="+userid;		
							}		
				});	
	}	
	
	
	function show_inactivated(userid)	
	{	
		swal({		  
			title: "Are you sure?",	
		    text: "You want to in-activate this subadmin!",	
			icon: "warning",		  
			buttons: true,		
			dangerMode: true,		
			}).then((willDelete) => {		
			  if (willDelete) {		
					location.href="<?php echo base_url();?>odgenius2671989/subadmin-inactivated?uid="+userid;		  		
					  }		
				});	
	}*/	
	
</script>
