<?php $usertype = $this->session->userdata('admin_usertype'); 
$where = array('usertype'=>$usertype,'menutab'=>'Financial Institute');
$chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION,$where); 
?>

<?php include('partials/header.php'); ?>    
<!-- partial -->    
<div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>

<?php include('partials/sidebar.php'); ?>            
<!-- partial -->      
<div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-8">					<h4 class="card-title">All Financial Institutes</h4>				</div>	

							
				<?php if($chkvalied->allowfor_add==1 || $usertype==1){	?>
					<div class="col-4">
						<a class="btn btn-info" href="administrator/addfinancialinstitution"><i class="mdi mdi-plus"></i>Add Financial Institute</a>
					</div>
				<?php } ?>
				
					</div>

						<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>								<div class="table-responsive">				<table id="order-listing" class="table">				<thead>				  <tr>					  <th>Name</th>					  <th>Address</th>					  <th>Phone</th>					  <th>Account No.</th>					  <!--<th>Loans</th>-->					  <th>Actions</th>				  </tr>				</thead>				<tbody>				<?php 				 if(!empty($financialinstitutes))				  {					  $i=1;						foreach($financialinstitutes as $rowsproject){ ?>				  <tr>					  
						
						<?php if($chkvalied->allowfor_edit==1 || $usertype==1){	?>	
						
						<td><a href="administrator/financial_details/?financialinstituteid=<?php echo $rowsproject->financialinstituteid; ?>"><?php echo $rowsproject->financialinstitutename; ?></a></td>	
						
						<?php }else{ ?>
						
						<td><a href="javascript:void(0);"><?php echo $rowsproject->financialinstitutename; ?></a></td>	
						
						<?php } ?>
						
						
						
						<td><?php echo $rowsproject->financialinstituteaddress;	 ?></td>					  <td><?php echo $rowsproject->financialinstitutephone;	 ?></td>					  <td><?php echo $rowsproject->financialinstituteaccountno;	 ?></td>					 <!--<td><?php echo $rowsproject->financialinstituteaccountloan;	 ?></td>-->					  					<td>							 
						
						<?php if($chkvalied->allowfor_modify==1 || $usertype==1){	?>	
						<a href="administrator/editfinancialinstitution?financialinstituteid=<?php echo $rowsproject->financialinstituteid; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>	
						<?php }else{ ?>
							NA
						<?php } ?>
						
						<?php if($chkvalied->allow_delete==1 || $usertype==1){	?>
						<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $rowsproject->financialinstituteid; ?>)"><i class="fa fa-trash-o"></i></button><?php }else{ ?>
							NA
						<?php } ?>  

						
						
						</td>										  				 </tr>				<?php 						$i++;					} 				} ?>				</tbody>				</table>				</div>				                </div>              </div>            </div>          </div>        </div>        <!-- content-wrapper ends --><?php include('partials/footer.php'); ?>    <script src="js/data-table.js"></script>   <script src="js/alerts.js"></script><script>	function show_confirmdelte(financialinstituteid)	{		swal({		  title: "Are you sure?",		  text: "You want to delete this Financial Institute?",		  icon: "warning",		  buttons: true,		  dangerMode: true,		})		.then((willDelete) => {		  if (willDelete) {			location.href="<?php echo base_url();?>administrator/deletefinancialinstitution?financialinstituteid="+financialinstituteid;		  } else {					  }		});	}			</script>