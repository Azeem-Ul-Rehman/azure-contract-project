<?php $usertype = $this->session->userdata('admin_usertype'); 
$where = array('usertype'=>$usertype,'menutab'=>'Contractors');
$chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION,$where); 
?>

<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">				
			<div class="col-8">					
				<h4 class="card-title">All Contractors</h4>				
			</div>								
			
			<?php if($chkvalied->allowfor_add==1 || $usertype==1){	?>
			<div class="col-4">
				<a  class="btn btn-info" href="administrator/addnewcontractor"><i class="mdi mdi-plus"></i>Add Contractors</a>
			</div>	
			<?php } ?>

			</div>						<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>								<div class="table-responsive">				<table id="order-listing" class="table">                    <thead>                      <tr>                      <!--<th>Sno</th>-->                          <th>Contractors Name</th>						  <th>Address</th>						  <th>Phone</th>						  <th>Type</th>                          <th>Persons</th>					  <!--<th>Assign project</th>						  <th>Payments</th>-->						  <th>CRIB No</th>                         <!-- <th>KVK No</th>-->						  <th>Actions</th>                      </tr>                    </thead>                    <tbody>					<?php					  if(!empty($contractors))					  {						  $i=1;							  foreach ($contractors as $perreq){					?>
                      <tr>					 <!-- <td><?php echo $i;	 ?></td>-->						  
					  
					  <?php if($chkvalied->allowfor_edit==1 || $usertype==1){	?>	
					  <td><a href="administrator/contractors_details/?contractor_id=<?php echo $perreq->contractor_id; ?>"><?php echo $perreq->contractor_name; ?></a></td>
					  <?php }else{ ?>
					  <td><a href="javascript:void(0);"><?php echo $perreq->contractor_name; ?></a></td>
					  <?php } ?>
					  
					  
					  
					  <td><?php echo $perreq->contractor_address; ?></td>						  <td><?php echo $perreq->contractor_phone;	 ?></td>						  <td><?php echo $perreq->contractor_type;	 ?></td>						  <td><?php echo $perreq->contractor_person; ?></td>						  <!--<td><?php echo $perreq->contractor_assignproject;	 ?></td>						  <td><?php echo $perreq->contractor_payments;	 ?></td>-->						  <td><?php echo $perreq->contractor_cribno;	 ?></td>						  <!--<td><?php echo $perreq->contractor_kvkno;	 ?></td>-->                          <td>							                
					  <?php if($chkvalied->allowfor_modify==1 || $usertype==1){	?>	
					  <a href="administrator/editcontractor?contractor_id=<?php echo $perreq->contractor_id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
					  <?php }else{ ?>
							NA
					  <?php } ?>
					  
					  <?php if($chkvalied->allow_delete==1 || $usertype==1){	?>
					  <button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->contractor_id; ?>)"><i class="fa fa-trash-o"></i></button> 
					  <?php }else{ ?>
								NA
					  <?php } ?>
					  

					  </td>					 </tr>
					  
					  
                  <?php 						$i++;					} 				}				  ?>
                    </tbody>                  </table>				  </div>                </div>              </div>            </div>          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>
	function show_confirmdelte(contractor_id)
	{
		swal({
		  title: "Are you sure?",
		  text: "You want to delete this contractor?",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			location.href="<?php echo base_url();?>administrator/deletecontractor?contractor_id="+contractor_id;
		  } else {
			//swal("Your imaginary file is safe!");
		  }
		});
	}			
</script>
