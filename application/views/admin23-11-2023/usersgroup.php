<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">
				<div class="col-10">
				  <h4 class="card-title">Users Group</h4>
				</div>
				<div class="col-2 add-btn">
					<a  class="btn btn-info" href="administrator/addnewgroups"><i class="mdi mdi-plus"></i>Add user group</a>
				</div>
			</div>
				<div class="row">
                <div class="col-12">
				<?php if($this->session->flashdata('success')){ ?>
				 
				 <div class="alert alert-success alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>
				  </div>

				<?php } ?>
				
                  <table id="order-listing" class="table">
                    <thead>
                      <tr>
                          <th>Sno</th>
						  <th>Group Name</th>
                          <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php
					  if(!empty($usergroups))
					  {
						  
						  $i=1;	
						  foreach ($usergroups as $perreq){
					?>
                      <tr>
                          <td><?php echo $i;	 ?></td>
						  <td><?php echo $perreq->groupname;	 ?></td>
                          <td class="action-clm">
                            
							<a href="administrator/editdnewgroups?id=<?php echo $perreq->id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							
							<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->id ; ?>)"><i class="fa fa-trash-o"></i></button>
							
                          </td>
                      </tr>
                  <?php 
						$i++;
					} 
				}
				  ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="adminassets/js/data-table.js"></script>   
<script src="adminassets/js/alerts.js"></script>
<script>
	function show_confirmdelte(groupid)
	{
		swal({
		  title: "Are you sure?",
		  text: "You want to delete this usergroup!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			location.href="<?php echo base_url();?>administrator/deletegroups?groupid="+groupid;
		  } else {
			//swal("Your imaginary file is safe!");
		  }
		});

		
	}	
	
	/*function show_activated(userid)	
	{		
		swal({		   
			  title: "Are you sure?",		
			  text: "You want to activate this subadmin!",		
			   icon: "warning",		
			   buttons: true,		
			   dangerMode: true,		
			   }).then((willDelete) => {		
						  if (willDelete) {			
							  location.href="<?php echo base_url();?>odgenius2671989/subadmin-activated?uid="+userid;		
							}		
				});	
	}	
	
	
	function show_inactivated(userid)	
	{	
		swal({		  
			title: "Are you sure?",	
		    text: "You want to in-activate this subadmin!",	
			icon: "warning",		  
			buttons: true,		
			dangerMode: true,		
			}).then((willDelete) => {		
			  if (willDelete) {		
					location.href="<?php echo base_url();?>odgenius2671989/subadmin-inactivated?uid="+userid;		  		
					  }		
				});	
	}*/	
	
</script>
