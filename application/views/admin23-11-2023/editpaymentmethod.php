<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">
				<div class="col-10">
				  <h4 class="card-title">Edit Payment Method</h4>
				</div>
				<div class="col-2">
					<a  class="btn btn-info" href="administrator/paymentmethod"><i class="fa fa-list"></i>Payment Method</a>
				</div>
			</div>
				<div class="row">
            <div class="col-lg-12">
			
				<?php if($this->session->flashdata('error')){ ?>
				 
				 <div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
				  </div>

				<?php } ?>
				  
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                  <form  id="customerForm" name="customerForm" method="post" action="">
                    <fieldset>
                      
					  <input type="hidden" name="hdnid" value="<?php echo $paymentmethodval->id; ?>" />
					  
                      <div class="form-group">
                        <label for="paymentmethod">Payment Method</label>
                        <input id="paymentmethod" value="<?php echo $paymentmethodval->paymentmethod; ?>" class="form-control required" name="paymentmethod" type="text">
                      </div>
			
                      <button class="btn btn-primary" type="submit">Submit</button>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>     
<script src="js/form-validation.js"></script> 
<script src="js/formpickers.js"></script> 
<script>
function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "AdminOferadities/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('#email').val('');
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "adminOferadities/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('#username').val('');
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


 //smssupportcls smssupportcls_field
 $('.smssupportcls').click(function(){
	 if($(this).val()=='yes'){
		$('.smssupportcls_field').css('display','block');
	 }else{
		$('.smssupportcls_field').css('display','none');
	 }
 });

</script>		
		
