<?php $usertype = $this->session->userdata('admin_usertype'); 
$where = array('usertype'=>$usertype,'menutab'=>'Users');
$chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION,$where); 
?>
<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">
				<div class="col-10">
				  <h4 class="card-title">Users</h4>
				</div>
				
				 <?php if($chkvalied->allowfor_add==1 || $usertype==1){	?>	
					<div class="col-2 add-btn">
						<a  class="btn btn-info" href="administrator/addnewusers"><i class="mdi mdi-plus"></i>Add user</a>
					</div>
				<?php } ?>
				 
			</div>
				<div class="row">
                <div class="col-12">
				<?php if($this->session->flashdata('success')){ ?>
				 
				 <div class="alert alert-success alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>
				  </div>

				<?php } ?>
				
                  <table id="order-listing" class="table">
                    <thead>
                      <tr>
                          <th>Sno</th>
						  <th>User Type</th>
                          <th>Name</th>
						  <th>Username</th>
						  <th>Phone</th>
                          <th>Password</th>
						  <th>Added On</th>
                          <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php
					  if(!empty($all_data))
					  {
						  
						  $i=1;	
						  foreach ($all_data as $perreq){
							  
							  $where = array('id'=>$perreq->usertype);
							  $usersgroups =$this->adminmodel->getSingle(USERGROUP,$where);
					?>
                      <tr>
                          <td><?php echo $i;	 ?></td>
						  <td><?php echo $usersgroups->groupname; ?></td>
                          <td><?php echo $perreq->firstname.' '.$perreq->lastname;	 ?></td>
						  <td><?php echo $perreq->username;	 ?></td>
						  <td><?php echo $perreq->phone;	 ?></td>
						  <!--<td><?php echo $perreq->email;	 ?></td>-->             
                          <td><?php echo $perreq->show_passkey;	 ?></td>
						  <td><?php echo date('d F, Y',strtotime($perreq->createddate)); ?></td>
                          <!--<td>
						   <?php  if($perreq->status==1){  ?>
                            <label class="badge-pill badge-success">Active</label>
						   <?php }else{ ?>
							<label class="badge-pill badge-info">In-active</label>
						   <?php } ?>
                          </td>-->
                          <td class="action-clm">							<!-- <?php if($perreq->status==1){  ?>																	<button type="button" class="btn btn-outline-primary" onclick="show_inactivated(<?php echo $perreq->userid; ?>)">in-active</button>														<?php }else{ ?>															<button  type="button" class="btn btn-outline-success" onclick="show_activated(<?php echo $perreq->userid; ?>)">active</button> 							 							<?php } ?>	-->						 														
                            <?php if($chkvalied->allowfor_modify==1 || $usertype==1){	?>	
							<a href="administrator/editusers?userid=<?php echo $perreq->userid; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<?php }else{ ?>
								NA
							<?php } ?>
							
							<?php if($chkvalied->allow_delete==1 || $usertype==1){	?>
							<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->userid; ?>)"><i class="fa fa-trash-o"></i></button>
							<?php }else{ ?>
								NA
							<?php } ?>
							
							
							<!--<a href="odgenius2671989/view-subadmin?userid=<?php echo $perreq->userid; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>-->
							<!--<a href="delete-customer?userid=<?php echo $perreq->userid; ?>" class="btn btn-danger"></a>-->
                          </td>
                      </tr>
                  <?php 
						$i++;
					} 
				}
				  ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="adminassets/js/data-table.js"></script>   
<script src="adminassets/js/alerts.js"></script>
<script>
	function show_confirmdelte(userid)
	{
		swal({
		  title: "Are you sure?",
		  text: "You want to delete this user!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			location.href="<?php echo base_url();?>administrator/deleteusers?userid="+userid;
		  } else {
			//swal("Your imaginary file is safe!");
		  }
		});

		
	}	
	
	/*function show_activated(userid)	
	{		
		swal({		   
			  title: "Are you sure?",		
			  text: "You want to activate this subadmin!",		
			   icon: "warning",		
			   buttons: true,		
			   dangerMode: true,		
			   }).then((willDelete) => {		
						  if (willDelete) {			
							  location.href="<?php echo base_url();?>odgenius2671989/subadmin-activated?uid="+userid;		
							}		
				});	
	}	
	
	
	function show_inactivated(userid)	
	{	
		swal({		  
			title: "Are you sure?",	
		    text: "You want to in-activate this subadmin!",	
			icon: "warning",		  
			buttons: true,		
			dangerMode: true,		
			}).then((willDelete) => {		
			  if (willDelete) {		
					location.href="<?php echo base_url();?>odgenius2671989/subadmin-inactivated?uid="+userid;		  		
					  }		
				});	
	}*/	
	
</script>
