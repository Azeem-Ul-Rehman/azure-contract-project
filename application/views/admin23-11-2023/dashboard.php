<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">

			   <div class="row">                 				 
				  

				<?php if(!empty($getprojects)){ ?>
				
					<?php 
						foreach($getprojects as $rowsproject){  
							
							$where=array('projectid'=>$rowsproject->projectid);
							$sqlresult = $this->adminmodel->getwhere(PROJECTCONTRACTORS,$where);
							
							$rowscontractors = '';
							$totalbudgetused = 0;
							$totalpaymenttocontractors = 0;
							foreach($sqlresult as $rowsresult)
							{
								$wherecontracors=array('contractor_id'=>$rowsresult->contractorid);
								$getcontractorsval = $this->adminmodel->getSingle(TBLCONTRACTORS,$wherecontracors);
							
								$cotnractorsname = $getcontractorsval->contractor_name.',';
								
								$sql = $this->db->query('select SUM(Amount) as totalpayment,paymentmethod from '.TBLPAYMENTS.' where project="'.$rowsproject->projectid.'" and contractorid='.$rowsresult->contractorid.' and is_deleted=0');
								$getrows = $sql->row();
								$totalpayment = $getrows->totalpayment;
								
								$totalpaymenttocontractors = $totalpaymenttocontractors+$totalpayment;
																
							}
							
							
							$where=array('project'=>$rowsproject->projectid,'suppliersval !='=>0,'is_deleted'=>0);
							$payments = $this->adminmodel->getwhere(TBLPAYMENTS,$where);
							
						    $totalsupplierspayment = 0; 	
						    if(!empty($payments))
						    {
								
								foreach($payments as $rowspayments)
								{ 
									$totalsupplierspayment = $totalsupplierspayment+$rowspayments->Amount;
								}
						    }
							
							
							$whereotherpayments=array('project'=>$rowsproject->projectid,'suppliersval'=>0,'contractorid'=>0,'is_deleted'=>0);
							$otherpayments = $this->adminmodel->getwhere(TBLPAYMENTS,$whereotherpayments);
							
						    $totalotherpayments = 0; 	
						    if(!empty($otherpayments))
						    {
								
								foreach($otherpayments as $rowsotherspayments)
								{ 
									$totalotherpayments = $totalotherpayments+$rowsotherspayments->Amount;
								}
						    }
		
							
							
							
							$totalusedbudget = $totalpaymenttocontractors + $totalsupplierspayment + $totalotherpayments;
							
							$getpercents = $totalusedbudget*100/$rowsproject->totalestimatedcost; 	
							
							
							
							
					?>
				   
					   <div class="col-6 grid-margin stretch-card">                  
							<div class="card">                    
								<div class="card-body">                     
									<table class="table">
										<tr>
											<th colspan="2" style="text-align:center;"><?php echo $rowsproject->project_name;?><th>
										</tr>
										<tr>
											<td>Budget</td>
											<td><?php echo $rowsproject->totalestimatedcost;?></td>
										</tr>
										<tr>
											<td>Contractors</td>
											<td><?php echo rtrim($cotnractorsname, ','); ?></td>
										</tr>
										
										<tr>
											<td>Start date</td>
											<td><?php echo date('d-M-y',strtotime($rowsproject->startdate));?></td>
										</tr>
										
										<tr>
											<td>End date</td>
											<td><?php echo date('d-M-y',strtotime($rowsproject->enddate));?></td>
										</tr>
										
										<tr>
											<td>Budget used</td>
											<td><?php echo round($getpercents,2); ?>%</td>
										</tr>
										
										<tr>
											<td>Remain Budget</td>
											<td><?php echo $rowsproject->totalestimatedcost-$totalusedbudget;?> </td>
										</tr>
										
										<tr>
											<td>Payment to contractors</td>
											<td>
												<?php   if($totalpaymenttocontractors!=''){ 
															echo $totalpaymenttocontractors; 
														}else{
															echo '0.00'; 
														}
												?>
															
											</td>
										</tr>
										
										<tr>
											<td>Payment to suppliers</td>
											<td>
											
												<?php   if($totalsupplierspayment!=''){ 
															echo $totalsupplierspayment; 
														}else{
															echo '0.00'; 
														}
												?>
											
											</td>
										</tr>
										
										<tr>
											<td>Payment to others</td>
											<td>
												<?php   if($totalotherpayments!=''){ 
															echo $totalotherpayments; 
														}else{
															echo '0.00'; 
														}
												?>
											</td>
										</tr>
										
									</table>
								</div>                    
							</div>                  
					   </div>  
					   
					<?php } ?>

				<?php } ?>
				
				</div>
				
				<div style="clear:both;"></div>
				
			   <!--<div class="row">                 				 
					<div class="col-4 grid-margin stretch-card">                  
					<div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Company</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalcompany; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>        
                          				 
				 <div class="col-4 grid-margin stretch-card">                  
				   <div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Contractors</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalcontractors; ?></strong></h4>                        
							</div>                     
						 </div>                  
					  </div>                  
					  </div>                
				   </div>     
				   
					 <div class="col-4 grid-margin stretch-card">                  
				   <div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Projects</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalprojects; ?></strong></h4>                        
							</div>                     
						 </div>                
					  </div>                  
					  </div>                
				   </div>     
									   
				</div>
				
				
			
				 <div class="row">                 				 
					<div class="col-4 grid-margin stretch-card">                  
					<div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Partners</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalpartners; ?></strong></h4>                        
							</div>                     
						 </div>                   
					  </div>                  
					  </div>                
				   </div>        
                          				 
				 <div class="col-4 grid-margin stretch-card">                  
				   <div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Suppliers</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalsuppliers; ?></strong></h4>                        
							</div>                     
						 </div>                        
					  </div>                  
					  </div>                
				   </div>   

					<div class="col-4 grid-margin stretch-card">                  
				   <div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Loans</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalloans; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>       
				</div>
				
				
				
				 <div class="row">                 				 
					<div class="col-4 grid-margin stretch-card">                  
					<div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Bank Account</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalbankaccount; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>        
                          				 
				 <div class="col-4 grid-margin stretch-card">                  
				   <div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Projects</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalprojects; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>        
				
				
				 <div class="col-4 grid-margin stretch-card">                  
				   <div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Financial Institute</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalfinancialinstitute; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>        
				</div>-->
				
				
        </div>  
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>       
