<?php include('partials/header.php'); ?>    <!-- partial -->    <div class="container-fluid page-body-wrapper"><?php include('partials/settings.php'); ?><?php include('partials/sidebar.php'); ?>            <!-- partial -->      <div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-10">					<h4 class="card-title">Add New Supplier</h4>				</div>				<div class="col-2">					<a  class="btn btn-info" href="administrator/suppliers_list"><i class="mdi mdi-list"></i>List</a>				</div>			</div>					<div class="row">            <div class="col-lg-12">			
				<?php if($this->session->flashdata('error')){ ?>
				 
				 <div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
				  </div>

				<?php } ?>
				  
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                  <form  id="customerForm" name="customerForm" method="post" action="">
                    <fieldset>			 					 
                      <div class="form-group">                        <label for="suppliers_name">Suppliers Name</label>                        <input id="suppliers_name" class="form-control required" name="suppliers_name" type="text">                      </div>					  					  <div class="form-group">                        <label for="suppliers_type">Suppliers Type</label>                        <input id="suppliers_type" class="form-control required" name="suppliers_type" type="text">                      </div>										<div class="form-group">                        <label for="suppliers_address">Suppliers address</label>                        <textarea id="suppliers_address" class="form-control required" name="suppliers_address"></textarea>                    </div>										 <div class="form-group">                        <label for="suppliers_phone">Suppliers phone</label>                        <input id="suppliers_phone" class="form-control required" name="suppliers_phone" type="number">                     </div>					 					 <div class="form-group">                        <label for="suppliers_bankacno">Bank Account No.</label>                        <input id="suppliers_bankacno" class="form-control required" name="suppliers_bankacno" type="number">                     </div>					 					 <!--<div class="form-group">                        <label for="suppliers_outstandingamount">Outstanding Amount</label>                        <input id="suppliers_outstandingamount" class="form-control required" name="suppliers_outstandingamount" type="number">                     </div>					 					 <div class="form-group">                        <label for="suppliers_payments">Suppliers Amount</label>                        <input id="suppliers_payments" class="form-control required" name="suppliers_payments" type="number">                     </div>-->					 					 <input class="form-control" value="" name="suppliers_outstandingamount" type="hidden">					 <input class="form-control" value="" name="suppliers_payments" type="hidden">					 					 <div class="form-group">                        <label for="suppliers_kvkno">Suppliers KVK No.</label>                        <input id="suppliers_kvkno" class="form-control required" name="suppliers_kvkno" type="number">                     </div>					  
                      <button class="btn btn-primary" type="submit">Submit</button>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

		<script>
//firstname lastname username password email phone smssupport
/*$(document).ready(function(){ 
	$("#customerForm").validate({  
		rules: { 
			firstname: "required",
			lastname: "required",
			username: "required",
			email: {
				required: true,
				email: true
			},
			password:"required",
			phone: "required",
			smssupport: "required"
		}, 
		messages: {
			firstname: "Please enter first name",
			lastname: "Please enter last name",
			username: "please enter username",
			email: {
				required:"Please enter email address",
				email:"Please enter a valid email address",
			},
			password: "please enter password",
			phone: "Please enter phone number",
			smssupport: "Please select sms port"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	//$('#customerForm').validate();
});*/

function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "admin/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "admin/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


 function blockSpecialChar(e){	var k;	document.all ? k = e.keyCode : k = e.which;	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));}  

</script>		
		
<?php include('partials/footer.php'); ?>     
<script src="js/form-validation.js"></script> <script src="adminassets/js/formpickers.js"></script> 
