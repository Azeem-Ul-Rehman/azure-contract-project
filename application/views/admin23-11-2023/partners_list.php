<?php $usertype = $this->session->userdata('admin_usertype'); 
$where = array('usertype'=>$usertype,'menutab'=>'Partners');
$chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION,$where); 
?>

<?php include('partials/header.php'); ?>    
<!-- partial -->    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?><?php include('partials/sidebar.php'); ?>            <!-- partial -->      <div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-8">					<h4 class="card-title">All Partners List</h4>				</div>								
<?php if($chkvalied->allowfor_add==1 || $usertype==1){	?>	
	<div class="col-4">
		<a class="btn btn-info" href="administrator/addpartners"><i class="mdi mdi-plus"></i>Add Partners</a>
	</div>
<?php } ?>


							</div>						<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>				<div class="table-responsive">				<table id="order-listing" class="table">                    <thead>                      <tr>                          <!--<th>Sno</th>-->                          <th>Name</th>						  <th>Email</th>						  <th>Phone</th>						  <th>Address</th>						  <th>Crib Nr</th>                          <th>Actions</th>                      </tr>                    </thead>                    <tbody>					<?php					  if(!empty($partners))					  {						  $i=1;							  foreach ($partners as $perreq){					?>                      <tr>                          <!--<td><?php echo $i;	 ?></td>-->						  <td><?php echo $perreq->partner_name; ?></td>						  <td><?php echo $perreq->partner_email; ?></td>						  <td><?php echo $perreq->partner_phone; ?></td>						  <td><?php echo $perreq->partner_address; ?></td>						  <td><?php echo $perreq->partner_cribno; ?></td>                          <td>							                             
							<?php if($chkvalied->allowfor_modify==1 || $usertype==1){	?>	
							<a href="administrator/editpartners?partner_id=<?php echo $perreq->partner_id ; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<?php }else{ ?>
								NA
							<?php } ?>
							
							<?php if($chkvalied->allow_delete==1 || $usertype==1){	?>
							<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->partner_id; ?>)"><i class="fa fa-trash-o"></i></button>
							
							<?php }else{ ?>
								NA
							<?php } ?>                          
							
							</td>					 </tr>                  
							
							
							<?php 						$i++;					} 				}				  ?>                    </tbody>                  </table>				  				  </div>                </div>              </div>            </div>          </div>        </div>        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    

<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>	function show_confirmdelte(partner_id)	{		swal({		  title: "Are you sure?",		  text: "You want to delete this partner?",		  icon: "warning",		  buttons: true,		  dangerMode: true,		})		.then((willDelete) => {		  if (willDelete) {			location.href="<?php echo base_url();?>administrator/deletepartner?partner_id="+partner_id;		  } else {					  }		});	}			</script>