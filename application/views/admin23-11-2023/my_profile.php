<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>     
    
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row user-profile">
            <div class="col-lg-4 side-left d-flex align-items-stretch">
              <div class="row">
                <div class="col-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body avatar">
                      <h4 class="card-title">Info</h4>
					  <?php if($usersinfo->profile_pic!=''){ ?>
					  
						<img src="uploads/profile/<?php echo $usersinfo->profile_pic; ?>" alt="">
					  
					  <?php }else{ ?>
					  
						<img src="https://placehold.it/100x100" alt="">
					  
					  <?php } ?>
					  
                      <p class="name"><?php echo $usersinfo->firstname.' '.$usersinfo->lastname;	 ?></p>
                      <!--<p class="designation">-  UI/UX  -</p>-->
                      <a class="d-block text-center text-dark" href="#"><?php echo $usersinfo->email; ?></a>
                      <a class="d-block text-center text-dark" href="#"><?php echo $usersinfo->phone; ?></a>
                    </div>
                  </div>
                </div>
                <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body overview">
                  <!--<ul class="achivements">
                        <li><p>34</p><p>Projects</p></li>
                        <li><p>23</p><p>Task</p></li>
                        <li><p>29</p><p>Completed</p></li>
                      </ul>
                      <div class="wrapper about-user">
                        <h4 class="card-title mt-4 mb-3">About</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam consectetur ex quod.</p>
                      </div>
                      <div class="info-links">
                        <a class="website" href="http://bootstrapdash.com/">
                          <i class="mdi mdi-earth text-gray"></i>
                          <span>http://bootstrapdash.com/</span>
                        </a>
                        <a class="social-link" href="#">
                          <i class="mdi mdi-facebook text-gray"></i>
                          <span>https://www.facebook.com/johndoe</span>
                        </a>
                        <a class="social-link" href="#">
                          <i class="mdi mdi-linkedin text-gray"></i>
                          <span>https://www.linkedin.com/johndoe</span>
                        </a>
                      </div>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-8 side-right stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
                    <h4 class="card-title mb-0">Details</h4>
                    <ul class="nav nav-tabs tab-solid tab-solid-primary mb-0" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-expanded="true">Info</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="avatar-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="avatar">Profile picture</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="security-tab" data-toggle="tab" href="#security" role="tab" aria-controls="security">Change password</a>
                      </li>
                    </ul>
                  </div>
                  <div class="wrapper">
                    <hr>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info">
						
						<?php if($this->session->flashdata('error')){ ?>
				 
						 <div class="alert alert-danger alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
						  </div>

						<?php } ?>
						
						<?php if($this->session->flashdata('success')){ ?>
				 
						 <div class="alert alert-success alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>
						  </div>

						<?php } ?>
				
                        <form action="administrator/updatemyprofile" id="updateformprofile" method="post">
                                          
						  <div class="form-group">
                            <label for="name">First name</label>
                             <input id="firstname" value="<?php echo $usersinfo->firstname; ?>" class="form-control required" name="firstname" type="text">
                          </div>
                          
						  <div class="form-group">
                            <label for="designation">Last name</label>
                              <input id="lastname" value="<?php echo $usersinfo->lastname; ?>" class="form-control required email" name="lastname" type="text">
                          </div>
						  
                          <div class="form-group">
                            <label for="mobile">Username</label>
							<input id="username" value="<?php echo $usersinfo->username; ?>" readonly="readonly" class="form-control required" name="username" type="text">
                          </div>
						  
                          <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" value="<?php echo $usersinfo->email; ?>" readonly="readonly" class="form-control" name="email" id="email" placeholder="Change email address">
                          </div>
						  
                          <div class="form-group">
                            <label for="address">Phone</label>
                            <input class="form-control required" value="<?php echo $usersinfo->phone; ?>" id="phone" type="number" name="phone"  />
                          </div>
						
			
			
                          <div class="form-group mt-5">
                            <button type="submit" class="btn btn-success mr-2">Update</button>
                            <button class="btn btn-outline-danger">Cancel</button>
                          </div>
                        </form>
                      </div><!-- tab content ends -->
                      <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                        <div class="wrapper mb-5 mt-4">
                          <span class="badge badge-warning text-white">Note : </span>
                          <p class="d-inline ml-3 text-muted">Image size is limited to not greater than 5MB .</p>
                        </div>
                        <form method="post" action="administrator/updateprofilepicture" enctype= multipart/form-data>
                          <input type="file" name="file" class="dropify" data-max-file-size="1mb" data-default-file="https://placehold.it/100x100"/>
                          <div class="form-group mt-5">
                            <button type="submit" class="btn btn-success mr-2">Update</button>
                            <button class="btn btn-outline-danger">Cancel</button>
                          </div>
                        </form>
                      </div>
                      <div class="tab-pane fade" id="security" role="tabpanel" aria-labelledby="security-tab">
                        <form method="post" action="administrator/updatepassword">
                          <div class="form-group">
                            <label for="change-password">Change password</label>
                            <input type="password" required  name="currentpassword" class="form-control" id="change-password" placeholder="Enter you current password">
                          </div>
                          <div class="form-group">
                            <input type="password" required class="form-control" name="newpassword" id="new-password" placeholder="Enter you new password">
                          </div>
                          <div class="form-group mt-5">
                            <button type="submit" class="btn btn-success mr-2">Update</button>
                            <button class="btn btn-outline-danger">Cancel</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>   
<script src="js/dropify.js"></script>   
<script src="js/form-validation.js"></script> 
<script src="js/formpickers.js"></script>   
