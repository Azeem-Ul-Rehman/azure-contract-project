<?php $usertype = $this->session->userdata('admin_usertype');

?>
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link d-flex">
                <div class="profile-image">
                    <?php

                    if ($this->session->userdata('admin_profilepic') != '') {
                        ?>
                        <img src="uploads/profile/<?php echo $this->session->userdata('admin_profilepic'); ?>"
                             alt="image"/>
                    <?php } else { ?>
                        <img src="https://placehold.it/100x100" alt="image"/>
                    <?php } ?>

                    <span class="online-status online"></span> <!--change class online to offline or busy as needed-->
                </div>
                <div class="profile-name">
                    <p class="name">
                        <?php echo $this->session->userdata('admin_username'); ?>
                    </p>
                    <p class="designation">
                        <?php if ($this->session->userdata('admin_usertype') == 1) echo 'SUPER ADMIN'; ?>
                    </p>
                </div>
            </div>
        </li>

        <li class="nav-item nav-category">
            <span class="nav-link">Main</span>
        </li>

        <?php if ($this->session->userdata('admin_usertype') == 1) { ?>

            <!--<li class="nav-item">
              <a class="nav-link" href="administrator/settings">
                  <i class="fa fa-cog menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Settings</span>
                <span class="badge badge-primary badge-pill"></span>
              </a>
            </li>-->


            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#maps" aria-expanded="false" aria-controls="maps">
                    <i class="fa fa-cog menu-icon" aria-hidden="true"></i>
                    <span class="menu-title">Settings</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="maps">
                    <ul class="nav flex-column sub-menu">
                        <!--<li class="nav-item"> <a class="nav-link" href="administrator/settings">Permission</a></li>
                        <li class="nav-item"> <a class="nav-link" href="administrator/usersgroup">User groups</a></li>-->
                        <li class="nav-item"><a class="nav-link" href="administrator/projecttype">Project type</a></li>
                        <li class="nav-item"><a class="nav-link" href="administrator/contractorstype">Contractors
                                type</a></li>
                        <li class="nav-item"><a class="nav-link" href="administrator/paymentmethod">Payment method</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="administrator/projectsection">Project Section</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#mapsnew" aria-expanded="false"
                   aria-controls="mapsnew">
                    <i class="fa fa-cog menu-icon" aria-hidden="true"></i>
                    <span class="menu-title">Users Access</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="mapsnew">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"><a class="nav-link" href="administrator/userslist">Users</a></li>
                        <li class="nav-item"><a class="nav-link" href="administrator/usersgroup">Groups</a></li>
                        <li class="nav-item"><a class="nav-link" href="administrator/settings">Permissions</a></li>
                    </ul>
                </div>
            </li>

        <?php } ?>


        <?php
        $where = array('usertype' => $usertype, 'menutab' => 'Dashboard', 'allowfor_edit' => 1);
        $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
        if ($chkvalied->settingsid != '' || $usertype == 1) {
            ?>
            <li class="nav-item">
                <a class="nav-link" href="administrator/dashboard">
                    <i class="icon-layout menu-icon"></i>
                    <span class="menu-title">Dashboard</span>
                    <span class="badge badge-primary badge-pill"></span>
                </a>
            </li>

        <?php } ?>

        <!--<li class="nav-item">
          <a class="nav-link" href="administrator/userslist">
            <i class="icon-layout menu-icon"></i>
            <span class="menu-title">Users</span>
            <span class="badge badge-primary badge-pill"></span>
          </a>
        </li>-->

        <?php //if($this->session->userdata('admin_usertype')==1){ ?>

        <li class="nav-item">
            <a class="nav-link" href="administrator/company_list">
                <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Companies</span>
                <span class="badge badge-primary badge-pill"></span>
            </a>
        </li>

        <?php //} ?>


        <li class="nav-item">
            <a class="nav-link" href="administrator/projectlist">
                <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Projects</span>
                <span class="badge badge-primary badge-pill"></span>
            </a>
        </li>

        <?php if ($this->session->userdata('admin_usertype') == 1) { ?>

            <li class="nav-item">
                <a class="nav-link" href="administrator/purchaseorders">
                    <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                    <span class="menu-title">Purchase Order</span>
                    <span class="badge badge-primary badge-pill"></span>
                </a>
            </li>

        <?php } else { ?>

            <li class="nav-item">
                <a class="nav-link" href="administrator/myorders">
                    <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                    <span class="menu-title">My Orders</span>
                    <span class="badge badge-primary badge-pill"></span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="administrator/addpurchaseorder">
                    <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                    <span class="menu-title">Add Order</span>
                    <span class="badge badge-primary badge-pill"></span>
                </a>
            </li>

        <?php } ?>


        <li class="nav-item">
            <a class="nav-link" href="administrator/partners_list">
                <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Partners</span>
                <span class="badge badge-primary badge-pill"></span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="administrator/suppliers_list">
                <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Suppliers</span>
                <span class="badge badge-primary badge-pill"></span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="administrator/contractors_list">
                <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Contractors</span>
                <span class="badge badge-primary badge-pill"></span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="administrator/loan_list">
                <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Loans</span>
                <span class="badge badge-primary badge-pill"></span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="administrator/bankaccountlist">
                <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Bank Accounts </span>
                <span class="badge badge-primary badge-pill"></span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="administrator/financialinstitutionlist">
                <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Financial Institutions</span>
                <span class="badge badge-primary badge-pill"></span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="administrator/paymentlist">
                <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Payments</span>
                <span class="badge badge-primary badge-pill"></span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="administrator/documentlist">
                <i class="fa fa-user menu-icon" aria-hidden="true"></i>
                <span class="menu-title">Documents</span>
                <span class="badge badge-primary badge-pill"></span>
            </a>
        </li>


    </ul>
</nav>
