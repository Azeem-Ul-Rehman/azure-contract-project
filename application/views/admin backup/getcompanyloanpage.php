<br/><br/><br/>
<div class="table-responsive">
<table id="order-listing" class="table">
<thead>
  <tr>
	  <th>Institution</th>
	  <th>Project</th>
	  <th>Loan</th>
	  <th>Interest</th>
	  <th>Period</th>
	  <th>FirstDate</th>
	  <th>EndDate</th>
	  <th>Redemption</th>
	  <!--<th>Actions</th>-->
  </tr>
</thead>
<tbody>
<?php foreach($loanlist as $rowsproject){ ?>
  <tr>
	  <td><?php echo $rowsproject->financialinstitution;	 ?></td>
	  <td><?php echo $rowsproject->project;	 ?></td>
	  <td><?php echo $rowsproject->loanamount;	 ?></td>
	  <td><?php echo $rowsproject->interestrate;	 ?></td>
	  <td><?php echo $rowsproject->period;	 ?></td>
	  <td><?php echo $rowsproject->firstdate;	 ?></td>
	  <td><?php echo $rowsproject->enddate;	 ?></td>
	  <td><?php echo $rowsproject->redemptionamount;	 ?></td>
 </tr>
<?php } ?>
</tbody>
</table>
</div>