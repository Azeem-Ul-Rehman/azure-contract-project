<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">				<div class="col-10">					<h4 class="card-title">Manage Catelog</h4>				</div>								<div class="col-2">					<a  class="btn btn-info" href="administrator/addcatelog"><i class="mdi mdi-plus"></i>Add</a>				</div>							</div>						<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>								<table id="order-listing" class="table">                    <thead>                      <tr>                          <th>Sno</th>						  <th>Name</th>                          <th>Item code</th>						  <th>Category</th>						   <th>Price</th>						  <th>Image</th>						  <th>Added On</th>                          <th>Actions</th>                      </tr>                    </thead>                    <tbody>					<?php					  if(!empty($managecatelog))					  {						  $i=1;							  foreach ($managecatelog as $perreq){ 						  						  $wherecategory = array('category_id'=>$perreq->category_id);						  $categoryval = $this->adminmodel->getSingle(CATALOGCATEGORY,$wherecategory);					?>
                      <tr>                          <td><?php echo $i;	 ?></td>                          <td><?php echo $perreq->name;	 ?></td>						  <td><?php echo $perreq->itemcode;	 ?></td>						  <td><?php echo $categoryval->categoryname;	 ?></td>						   <td>Rs <?php echo $perreq->price;	 ?></td>						  						  <td><img src="uploads/product_pic/<?php echo $perreq->item_img; ?>" width="100" /></td>						  						  <td><?php echo date('d F, Y',strtotime($perreq->adddate)); ?></td>                          <td>							                             <a href="administrator/editproduct?id=<?php echo $perreq->id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>							<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->id; ?>)"><i class="fa fa-trash-o"></i></button>                          </td>					 </tr>
                  <?php 						$i++;					} 				}				  ?>
                    </tbody>                  </table>                </div>              </div>            </div>          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>
	function show_confirmdelte(id)
	{
		swal({
		  title: "Are you sure?",
		  text: "You won't to delete this item!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			location.href="<?php echo base_url();?>administrator/deleteproduct?id="+id;
		  } else {
			//swal("Your imaginary file is safe!");
		  }
		});
	}			
</script>
