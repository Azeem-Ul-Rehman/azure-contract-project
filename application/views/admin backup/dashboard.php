<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">

			
			   <div class="row">                 				 
					<div class="col-6 grid-margin stretch-card">                  
					<div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Company</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalcompany; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>        
                          				 
				 <div class="col-6 grid-margin stretch-card">                  
				   <div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Partners</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalpartners; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>        
				</div>
				
				
				 <div class="row">                 				 
					<div class="col-6 grid-margin stretch-card">                  
					<div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Contractors</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalcontractors; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>        
                          				 
				 <div class="col-6 grid-margin stretch-card">                  
				   <div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Suppliers</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong><?php echo $totalsuppliers; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>        
				</div>
				
				
				 <div class="row">                 				 
					<div class="col-6 grid-margin stretch-card">                  
					<div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Projects</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong>0<?php //echo $totalcompany; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>        
                          				 
				 <div class="col-6 grid-margin stretch-card">                  
				   <div class="card">                    
					  <div class="card-body">                     
						 <h4 class="card-title">Total Loans</h4>                     
							<div class="row d-flex justify-content-between align-items-end mb-3">                        
							 <div class="col-12">                          
							  <h4 class="mb-0"><strong>0<?php //echo $totalpartners; ?></strong></h4>                        
							</div>                     
						 </div>                    
					  </div>                  
					  </div>                
				   </div>        
				</div>
				
				
        </div>  
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>       
