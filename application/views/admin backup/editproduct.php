<?php include('partials/header.php'); ?>    <!-- partial -->    <div class="container-fluid page-body-wrapper"><?php include('partials/settings.php'); ?><?php include('partials/sidebar.php'); ?>            <!-- partial -->      <div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-10">					<h4 class="card-title">Update Catelog</h4>				</div>				<div class="col-2">					<a  class="btn btn-info" href="administrator/manage_catelog"><i class="mdi mdi-list"></i>List</a>				</div>			</div>					<div class="row">            <div class="col-lg-12">			
				<?php if($this->session->flashdata('error')){ ?>

				 <div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
				  </div>
				<?php } ?>
				  
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                  <form  id="customerForm" enctype="multipart/form-data" name="customerForm" method="post" action="administrator/editproduct">				  				  <input id="hdnid" value="<?php echo $getproduct->id; ?>" name="hdnid" type="hidden">
                    <fieldset>                       					  <div class="form-group">                        <label for="category">Category</label>                        <select name="category_id" id="category_id" class="form-control">							<?php 								foreach($categoryval as $rowsval)								{									$selectedval = '';									if($rowsval->category_id==$getproduct->category_id)									{										$selectedval = 'selected="selected"';									}																		echo "<option ".$selectedval." value='".$rowsval->category_id."'>".$rowsval->categoryname."</option>";								}							?>						</select>                      </div>					 
                      <div class="form-group">                        <label for="name">Name</label>                        <input id="name" value="<?php echo $getproduct->name; ?>" class="form-control required" name="name" type="text">                      </div>
                      <div class="form-group">                        <label for="itemcode">Item code</label>                        <input id="itemcode" value="<?php echo $getproduct->itemcode; ?>" class="form-control required" name="itemcode" type="text">                      </div>					  					  <div class="form-group">                        <label for="price">Price</label>                        <input id="price" value="<?php echo $getproduct->price; ?>" class="form-control required" name="price" type="text">                      </div>
                      <div class="form-group">                        <label for="notes">Image</label>                        <input id="productimg" class="form-control" name="productimg" type="file">					  </div>					  					  <input id="hdnimg" value="<?php echo $getproduct->item_img; ?>" name="hdnimg" type="hidden">					  					  <img src="uploads/product_pic/<?php echo $getproduct->item_img; ?>" width="120" >					  <div style="clear:both;"></div><br>
					 
                      <button class="btn btn-primary" type="submit">Submit</button>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

		<script>
//firstname lastname username password email phone smssupport
/*$(document).ready(function(){ 
	$("#customerForm").validate({  
		rules: { 
			firstname: "required",
			lastname: "required",
			username: "required",
			email: {
				required: true,
				email: true
			},
			password:"required",
			phone: "required",
			smssupport: "required"
		}, 
		messages: {
			firstname: "Please enter first name",
			lastname: "Please enter last name",
			username: "please enter username",
			email: {
				required:"Please enter email address",
				email:"Please enter a valid email address",
			},
			password: "please enter password",
			phone: "Please enter phone number",
			smssupport: "Please select sms port"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	//$('#customerForm').validate();
});*/

function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "admin/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "admin/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


 function blockSpecialChar(e){	var k;	document.all ? k = e.keyCode : k = e.which;	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));}  

</script>		
		
<?php include('partials/footer.php'); ?>     
<script src="js/form-validation.js"></script> <script src="adminassets/js/formpickers.js"></script> 
