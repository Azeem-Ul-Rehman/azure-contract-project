<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">				<div class="col-10">					<h4 class="card-title">View Astrologer</h4>				</div>								<div class="col-2">					<a  class="btn btn-info" href="administrator/manage_astrologer"><i class="mdi mdi-list"></i>List</a>				</div>							</div>						<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>								<table id="order-listing" class="table">                                         <tr>						  <th>Name</th>						  <td><?php echo $perreq->post_title;?></td>                      </tr>					  					  <tr>						  <th>Images</th>						  <td><img src="uploads/astrologer/<?php echo $perreq->post_images;?>" width="150"></td>                      </tr>					  					   <tr>						  <th>Category</th>						  <td><?php echo $perreq->category;?></td>                      </tr>					  					  <tr>						  <th>City</th>						  <td><?php echo $perreq->city;?></td>                      </tr>					  					   <tr>						  <th>Language</th>						  <td><?php echo $perreq->language;?></td>                      </tr>					  					   <tr>						  <th>Skills</th>						  <td><?php echo $perreq->skills;?></td>                      </tr>					  					  <tr>						  <th>Call Price</th>						  <td><?php echo $perreq->call_price;?></td>                      </tr>					 					  					  <tr>						  <th>Chat Price</th>						  <td><?php echo $perreq->chat_price;?></td>                      </tr>					  <tr>						  <th>Phone number</th>						  <td><?php echo $perreq->phonenumber;?></td>                      </tr>										  <tr>						  <th>Rating</th>						  <td><?php echo $perreq->rating;?></td>                      </tr>					  					  <tr>						  <th>Experience</th>						  <td><?php echo $perreq->experience;?></td>                      </tr>					  					  <tr>						  <th>Consultation</th>						  <td><?php echo $perreq->consultation;?></td>                      </tr>                                     </table>                </div>              </div>            </div>          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>
	function show_confirmdelte(id)
	{
		swal({
		  title: "Are you sure?",
		  text: "You won't to delete this item!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			location.href="<?php echo base_url();?>administrator/deleteproduct?id="+id;
		  } else {
			//swal("Your imaginary file is safe!");
		  }
		});
	}			
</script>
