<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">				<div class="col-10">					<h4 class="card-title">Manage Astrologer</h4>				</div>								<div class="col-2">					<a  class="btn btn-info" href="administrator/add_astrologer"><i class="mdi mdi-plus"></i>Add</a>				</div>							</div>						<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>								<table id="order-listing" class="table">                    <thead>                      <tr>                          <th>Sno</th>                          <th>Name</th>						  <th>Image</th>						  <th>City</th>						  <th>Call price</th>						  <th>Chat price</th>						  <th>phonenumber</th>						  <th>Rating</th>						  <th>Experience</th>						  <th>Consultation</th>						  <th>Actions</th>                      </tr>                    </thead>                    <tbody>					<?php					  if(!empty($manage_astrologer))					  {						  $i=1;							  foreach ($manage_astrologer as $perreq){					?>
                      <tr>                          <td><?php echo $i;?></td>						  <td><?php echo $perreq->post_title;?></td>						  <td><img src="uploads/astrologer/<?php echo $perreq->post_images;?>" width="150"></td>						  <td><?php echo $perreq->city;?></td>						  <td><?php echo $perreq->call_price;?></td>						  <td><?php echo $perreq->chat_price;?></td>						  <td><?php echo $perreq->phonenumber;?></td>						  <td><?php echo $perreq->rating;?></td>						  <td><?php echo $perreq->experience;?></td>						  <td><?php echo $perreq->consultation;?></td>                          <td>							                             <a href="administrator/edit_astrologer?astro_id=<?php echo $perreq->astro_id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>							<a href="administrator/view_astrologer?astro_id=<?php echo $perreq->astro_id; ?>" class="btn btn-success"><i class="fa fa-eye"></i></a>							<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->astro_id; ?>)"><i class="fa fa-trash-o"></i></button>                          </td>					 </tr>
                  <?php 						$i++;					} 				}				  ?>
                    </tbody>                  </table>                </div>              </div>            </div>          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>
	function show_confirmdelte(category_id)
	{
		swal({
		  title: "Are you sure?",
		  text: "You won't to delete this category!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			location.href="<?php echo base_url();?>administrator/deletecategory?category_id="+category_id;
		  } else {
			//swal("Your imaginary file is safe!");
		  }
		});
	}			
</script>
