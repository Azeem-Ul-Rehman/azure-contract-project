<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
          <div class="nav-link d-flex">
            <div class="profile-image">
              <?php 
              
              if($this->session->userdata('admin_profilepic') !='')
              {
					    ?>
              <img src="uploads/profile/<?php echo $this->session->userdata('admin_profilepic'); ?>" alt="image"/>
                <?php } else{ ?>
                  <img src="https://placehold.it/100x100" alt="image"/>
                <?php } ?>
               
              <span class="online-status online"></span> <!--change class online to offline or busy as needed-->
            </div>
            <div class="profile-name">
                <p class="name">
                 <?php echo $this->session->userdata('admin_username'); ?>
                </p>
                <p class="designation">
                 <?php if($this->session->userdata('admin_usertype') == 1) echo 'SUPER ADMIN'; ?>
                </p>
              </div>
            </div>
        </li>
		
        <li class="nav-item nav-category">
            <span class="nav-link">Main</span>
        </li>
        
		<li class="nav-item">
          <a class="nav-link" href="administrator/dashboard">
            <i class="icon-layout menu-icon"></i>
            <span class="menu-title">Dashboard</span>
            <span class="badge badge-primary badge-pill"></span>
          </a>
        </li>
		
		 <?php //if($this->session->userdata('admin_usertype')==1){ ?>
		 
  		  <li class="nav-item">
      			<a class="nav-link" href="administrator/company_list">
      			   <i class="fa fa-user menu-icon" aria-hidden="true"></i>
      			  <span class="menu-title">Company List</span>
      			  <span class="badge badge-primary badge-pill"></span>
      			</a>
  		  </li>
		  
		 <?php //} ?>
		 
		  <li class="nav-item">
      			<a class="nav-link" href="administrator/partners_list">
      			    <i class="fa fa-user menu-icon" aria-hidden="true"></i>
      			  <span class="menu-title">Partners List</span>
      			  <span class="badge badge-primary badge-pill"></span>
      			</a>
  		  </li>
		  
		  <li class="nav-item">
      			<a class="nav-link" href="administrator/suppliers_list">
      			   <i class="fa fa-user menu-icon" aria-hidden="true"></i>
      			  <span class="menu-title">Suppliers List</span>
      			  <span class="badge badge-primary badge-pill"></span>
      			</a>
  		  </li>
		  
		 <li class="nav-item">
      			<a class="nav-link" href="administrator/contractors_list">
      			   <i class="fa fa-user menu-icon" aria-hidden="true"></i>
      			  <span class="menu-title">Contractors List</span>
      			  <span class="badge badge-primary badge-pill"></span>
      			</a>
  		  </li>
		  
		   <!--<li class="nav-item">
      			<a class="nav-link" href="administrator/manage_astrologer">
      			   <i class="fa fa-user menu-icon" aria-hidden="true"></i>
      			  <span class="menu-title">Manage Astrologer</span>
      			  <span class="badge badge-primary badge-pill"></span>
      			</a>
  		  </li>-->
		 
		 
        
    </ul>
</nav>
