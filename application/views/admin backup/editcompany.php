<?php include('partials/header.php'); ?>    <!-- partial -->    <div class="container-fluid page-body-wrapper"><?php include('partials/settings.php'); ?><?php include('partials/sidebar.php'); ?>            <!-- partial -->      <div class="main-panel">        <div class="content-wrapper">          <div class="card">            <div class="card-body">			<div class="row">				<div class="col-10">					<h4 class="card-title">Edit Company</h4>				</div>				<div class="col-2">					<a  class="btn btn-info" href="administrator/company_list"><i class="mdi mdi-list"></i>Company List</a>				</div>			</div>					<div class="row">            <div class="col-lg-12">			
				<?php if($this->session->flashdata('error')){ ?>
				 
				 <div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Error!</strong> <?php echo $this->session->flashdata('error');  ?>
				  </div>

				<?php } ?>
				  
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"></h4>
                  <form  id="customerForm" name="customerForm" method="post" action="administrator/updatecompany_action">
                    <fieldset>                       <input id="company_id" value="<?php echo $editcompany->company_id; ?>" class="form-control" name="hdncompany" type="hidden">				 
                      <div class="form-group">                        <label for="companyname">Company Name</label>                        <input id="companyname" value="<?php echo $editcompany->company_name; ?>" class="form-control required" name="companyname" type="text">                      </div>					  					  <div class="form-group">                        <label for="phoneno">Phone No</label>                        <input id="phoneno" value="<?php echo $editcompany->phoneno; ?>" class="form-control required" name="phoneno" type="text">                      </div>					  					  <div class="form-group">                        <label for="email">Email</label>                        <input id="email" value="<?php echo $editcompany->email; ?>" class="form-control required" name="email" type="text">                      </div>					  					   <div class="form-group">                        <label for="found_date">Found date</label>                        <input id="found_date" value="<?php echo $editcompany->found_date; ?>" class="form-control required" name="found_date" type="date">                      </div>					  					   <!--<div class="form-group">                        <label for="company_partners">Company Partners</label>                        <select name="company_partners[]" class="form-control required" id="company_partners" multiple>						  <option value="volvo">Volvo</option>						  <option value="saab">Saab</option>						  <option value="opel">Opel</option>						  <option value="audi">Audi</option>						</select>                      </div>-->					  					  					  <div class="form-group">                        <label for="address">Address</label>                        <textarea id="address" class="form-control required" name="address"><?php echo $editcompany->address; ?></textarea>                      </div>					  					   <div class="form-group">                        <label for="crib_no">CRIB No</label>                        <input id="crib_no" value="<?php echo $editcompany->crib_no; ?>" class="form-control required" name="crib_no" type="text">                      </div>					  					   <div class="form-group">                        <label for="kvk_no">KVK No</label>                        <input id="kvk_no" value="<?php echo $editcompany->kvk_no; ?>" class="form-control required" name="kvk_no" type="text">                      </div>					 
                      <button class="btn btn-primary" type="submit">Submit</button>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

		<script>
//firstname lastname username password email phone smssupport
/*$(document).ready(function(){ 
	$("#customerForm").validate({  
		rules: { 
			firstname: "required",
			lastname: "required",
			username: "required",
			email: {
				required: true,
				email: true
			},
			password:"required",
			phone: "required",
			smssupport: "required"
		}, 
		messages: {
			firstname: "Please enter first name",
			lastname: "Please enter last name",
			username: "please enter username",
			email: {
				required:"Please enter email address",
				email:"Please enter a valid email address",
			},
			password: "please enter password",
			phone: "Please enter phone number",
			smssupport: "Please select sms port"
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	//$('#customerForm').validate();
});*/

function check_email(email)
{
	$('.chkmail_cls').css('display','none');
	$('.chkmail_cls').text('');
	if(email!='')
	{
		$.ajax({
			 url: "admin/chkuser_email",
			 type:"POST",
			 data:{
				 email:email
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkmail_cls').css('display','block');
					$('.chkmail_cls').text('This email is already exist!');
					return false;
				}else{
					$('.chkmail_cls').css('display','none');
					$('.chkmail_cls').text('');
				}	
			 }
		 }); 
	 }
}	

function check_username(username)
{
	$('.chkusername_cls').css('display','none');
	$('.chkusername_cls').text('');
	if(username!='')
	{
		$.ajax({
			 url: "admin/chkuser_username",
			 type:"POST",
			 data:{
				 username:username
			 },
			 success: function(data)
			 {  
				if(data!='true')
				{
					$('.chkusername_cls').css('display','block');
					$('.chkusername_cls').text('This username is already exist!');
					return false;
				}else{
					$('.chkusername_cls').css('display','none');
					$('.chkusername_cls').text('');
				}	
			 }
		 }); 
	 }
}


 function blockSpecialChar(e){	var k;	document.all ? k = e.keyCode : k = e.which;	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));}  

</script>		
		
<?php include('partials/footer.php'); ?>     
<script src="js/form-validation.js"></script> <script src="adminassets/js/formpickers.js"></script> 
