<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">				<div class="col-8">					<h4 class="card-title">All Suppliers List</h4>				</div>								<div class="col-4">					<a  class="btn btn-info" href="administrator/addsuppliers"><i class="mdi mdi-plus"></i>Add Suppliers</a>				</div>							</div>						<div class="row">                <div class="col-12">				<?php if($this->session->flashdata('success')){ ?>				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>												<div class="table-responsive">				<table id="order-listing" class="table">                    <thead>                      <tr>                          <th>Sno</th>                          <th>Name</th>						  <th>Type</th>						  <th>Address</th>						  <th>Phone</th>						  <th>Bank Account no.</th>						  <th>Outstanding Amount</th>						  <th>Payments</th>						  <th>KVK No.</th>                          <th>Actions</th>                      </tr>                    </thead>                    <tbody>					<?php					  if(!empty($supplierslist))					  {						  $i=1;							  foreach ($supplierslist as $perreq){					?>
                      <tr>                          <td><?php echo $i;	 ?></td>						  <td><?php echo $perreq->suppliers_name; ?></td>						  <td><?php echo $perreq->suppliers_type; ?></td>						  <td><?php echo $perreq->suppliers_address; ?></td>						  <td><?php echo $perreq->suppliers_phone; ?></td>						  <td><?php echo $perreq->suppliers_bankacno; ?></td>						  <td><?php echo $perreq->suppliers_outstandingamount; ?></td>						  <td><?php echo $perreq->suppliers_payments; ?></td>						  <td><?php echo $perreq->suppliers_kvkno; ?></td>                          <td>							                             <a href="administrator/editsuppliers?suppliers_id=<?php echo $perreq->suppliers_id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>							<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->suppliers_id; ?>)"><i class="fa fa-trash-o"></i></button>                          </td>					 </tr>
                  <?php 						$i++;					} 				}				  ?>
                    </tbody>                  </table>				  </div>                </div>              </div>            </div>          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>
	function show_confirmdelte(suppliers_id)
	{
		swal({
		  title: "Are you sure?",
		  text: "You want to delete this suppliers?",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			location.href="<?php echo base_url();?>administrator/deletesuppliers?suppliers_id="+suppliers_id;
		  } else {
			//swal("Your imaginary file is safe!");
		  }
		});
	}			
</script>
