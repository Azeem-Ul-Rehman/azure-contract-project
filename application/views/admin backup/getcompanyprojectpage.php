<br/><br/><br/>
<div class="table-responsive">
<table id="order-listing" class="table">
<thead>
  <tr>
	  <th>Name</th>
	  <th>Start Date</th>
	  <th>End Date</th>
	  <th>Estimated Cost</th>
	  <th>Payments</th>
	  <!--<th>Actions</th>-->
  </tr>
</thead>
<tbody>
<?php foreach($projectlist as $rowsproject){ ?>
  <tr>
	  <td><?php echo $rowsproject->projectname;	 ?></td>
	  <td><?php echo $rowsproject->startdate;	 ?></td>
	  <td><?php echo $rowsproject->enddate;	 ?></td>
	  <td><?php echo $rowsproject->estimatedcost;	 ?></td>
	  <td><?php echo $rowsproject->payments;	 ?></td>
 </tr>
<?php } ?>
</tbody>
</table>
</div>