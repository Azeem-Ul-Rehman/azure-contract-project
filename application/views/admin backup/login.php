<!DOCTYPE html>
<html lang="en">

<head>
<base href="<?php echo site_url();?>">  
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Contractors Admin | Login</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/iconfonts/puse-icons-feather/feather.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="adminassets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="adminassets/images/logo.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth">
        <div class="row w-100">
          <div class="col-lg-8 mx-auto">
            <div class="row">
              <div class="col-lg-6 bg-white">
                <div class="auth-form-light text-left p-5">
                  <h2>Login</h2>
                  <h4 class="font-weight-light">Contractors Admin Login</h4>
                  <?php if($this->session->flashdata('message') != ''){	 ?>	
						<div class="alert alert-danger fade in">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>Error!</strong> <?php echo $this->session->flashdata('message'); ?>
						</div>
                   <?php } ?>     
				   
                  <form class="pt-5" name="loginform" id="loginform" method="post" action="administrator/user-login">        
                    <div class="form-group">
                      <label for="exampleInputEmail1">Username</label>
                      <input type="text" class="form-control required" id="loginusername" name="loginusername" placeholder="Username">
                      <i class="mdi mdi-account"></i>
                      <div class="error"><?php echo form_error('loginusername'); ?></div> 
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control required showpassword" id="loginpass"  name="loginpass" placeholder="Password">
                      <i class="mdi mdi-eye eyepwd_cls"></i>
                      <div class="error"><?php echo form_error('loginpass'); ?></div>   
                    </div>
                    <div class="mt-5">
                      <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium">Login</button>
                    </div>
                   <!-- <div class="mt-3 text-center">
                      <a href="#" class="auth-link text-black">Forgot password?</a>
                    </div>-->
                  </form>
                </div>
              </div>
              <div style="padding-left:0px !important; background-image:none;background-color: #868484;" class="col-lg-6 login-half-bg d-flex flex-row" style="padding:0px;">
					<img src="adminassets/img/logo512x512.png" alt="bticoin" />
                <!--<p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2018  All rights reserved.</p>-->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="adminassets/js/off-canvas.js"></script>
  <script src="adminassets/js/hoverable-collapse.js"></script>
  <script src="adminassets/js/misc.js"></script>
  <script src="adminassets/js/settings.js"></script>
  <script src="adminassets/js/todolist.js"></script>
  <!-- endinject -->
</body>
<script type="text/javascript">
$(document).ready(function(){ 
	$('#loginform').validate();
});	

$(function () {
  $(".showpassword").each(function (index, input) {
    var $input = $(input);
    $(".eyepwd_cls").click(function () {
      var change = "";
      if ($('.showpassword').attr('type') === "password") {
         change = "text";
      } else {
        //$(this).html("Show Password");
        change = "password";
      }
		$('.showpassword').attr('type',change);
	 
      
    });
  });
});
</script>
</html>
