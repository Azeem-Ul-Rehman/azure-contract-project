<h5><?php echo $getuserstype->groupname; ?></h5>
<div class="table-responsive">
    <table class="table table-flush-spacing">

        <tbody>
        <tr>
            <td class="text-nowrap fw-medium">Companies</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[0]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Compnies" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddcompany" id="chkaddcompanyid"
                                value="1"/>
                        <label class="form-check-label" for="chkaddcompanyid">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[0]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Compnies" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditcompany" id="chkaddcompanyid"
                                value="1"/>
                        <label class="form-check-label" for="chkeditcompany">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[0]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Compnies" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifycompany" id="chkaddcompanyid"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifycompany">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[0]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Compnies" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletecompany" id="chkaddcompanyid"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletecompany">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Projects</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[1]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Projects" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddprojects" id="chkaddprojects"
                                value="1"/>
                        <label class="form-check-label" for="chkaddprojects">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[1]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Projects" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditprojects" id="chkeditprojects"
                                value="1"/>
                        <label class="form-check-label" for="chkeditprojects">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[1]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Projects" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifyprojects" id="chkmodifyprojects"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifyprojects">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[1]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Projects" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeleteprojects" id="chkdeleteprojects"
                                value="1"/>
                        <label class="form-check-label" for="chkdeleteprojects">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Users</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[2]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Users" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddusers" id="chkaddusers"
                                value="1"/>
                        <label class="form-check-label" for="chkaddusers">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[2]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Users" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditusers" id="chkeditusers"
                                value="1"/>
                        <label class="form-check-label" for="chkeditusers">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[2]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Users" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifyusers" id="chkmodifyusers"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifyusers">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[2]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Users" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeleteusers" id="chkdeleteusers"
                                value="1"/>
                        <label class="form-check-label" for="chkdeleteusers">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Partners</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[3]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Partners" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddpartners" id="chkaddpartners"
                                value="1"/>
                        <label class="form-check-label" for="chkaddpartners">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[3]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Partners" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditpartners" id="chkeditpartners"
                                value="1"/>
                        <label class="form-check-label" for="chkeditpartners">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[3]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Partners" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifypartners" id="chkmodifypartners"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifypartners">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[3]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Partners" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletepartners" id="chkdeletepartners"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletepartners">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Suppliers</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[4]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Suppliers" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddsuppliers" id="chkaddsuppliers"
                                value="1"/>
                        <label class="form-check-label" for="chkaddsuppliers">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[4]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Suppliers" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditsuppliers" id="chkeditsuppliers"
                                value="1"/>
                        <label class="form-check-label" for="chkeditsuppliers">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[4]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Suppliers" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifysuppliers" id="chkmodifysuppliers"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifysuppliers">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[4]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Suppliers" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletesuppliers" id="chkdeletesuppliers"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletesuppliers">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Contractors</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[5]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Contractors" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddcontractos" id="chkaddcontractos"
                                value="1"/>
                        <label class="form-check-label" for="chkaddcontractos">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[5]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Contractors" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditcontractos" id="chkeditcontractos"
                                value="1"/>
                        <label class="form-check-label" for="chkeditcontractos">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[5]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Contractors" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifycontractos" id="chkmodifycontractos"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifycontractos">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[5]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Contractors" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletecontractos" id="chkdeletecontractos"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletecontractos">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Loans</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[6]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Loans" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddloans" id="chkaddloans"
                                value="1"/>
                        <label class="form-check-label" for="chkaddloans">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[6]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Loans" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditloans" id="chkeditloans"
                                value="1"/>
                        <label class="form-check-label" for="chkeditloans">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[6]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Loans" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifyloans" id="chkmodifyloans"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifyloans">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[6]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Loans" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeleteloans" id="chkdeleteloans"
                                value="1"/>
                        <label class="form-check-label" for="chkdeleteloans">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Bank Accounts</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[7]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Bank Accounts" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddbankaccounts" id="chkaddbankaccounts"
                                value="1"/>
                        <label class="form-check-label" for="chkaddbankaccounts">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[7]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Bank Accounts" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditbankaccounts" id="chkeditbankaccounts"
                                value="1"/>
                        <label class="form-check-label" for="chkeditbankaccounts">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[7]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Bank Accounts" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifybankaccounts" id="chkmodifybankaccounts"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifybankaccounts">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[7]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Bank Accounts" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletebankaccounts" id="chkdeletebankaccounts"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletebankaccounts">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Financial Institute</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[8]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Financial Institute" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddfinancialinstitute"
                                id="chkaddfinancialinstitute"
                                value="1"/>
                        <label class="form-check-label" for="chkaddfinancialinstitute">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[8]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Financial Institute" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditfinancialinstitute"
                                id="chkeditfinancialinstitute"
                                value="1"/>
                        <label class="form-check-label" for="chkeditfinancialinstitute">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[8]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Financial Institute" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifyfinancialinstitute"
                                id="chkmodifyfinancialinstitute"
                                value="1"/>
                        <label class="form-check-label"
                               for="chkmodifyfinancialinstitute">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[8]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Financial Institute" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletefinancialinstitute"
                                id="chkdeletefinancialinstitute"
                                value="1"/>
                        <label class="form-check-label"
                               for="chkdeletefinancialinstitute">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Payments</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[9]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Payments" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddpayments" id="chkaddpayments"
                                value="1"/>
                        <label class="form-check-label" for="chkaddpayments">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[9]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Payments" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditpayments" id="chkeditpayments"
                                value="1"/>
                        <label class="form-check-label" for="chkeditpayments">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[9]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Payments" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifypayments" id="chkmodifypayments"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifypayments">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[9]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Payments" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletepayments" id="chkdeletepayments"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletepayments">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Documents</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[10]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Documents" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkadddocument" id="chkadddocument"
                                value="1"/>
                        <label class="form-check-label" for="chkadddocument">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[10]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Documents" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditdocument" id="chkeditdocument"
                                value="1"/>
                        <label class="form-check-label" for="chkeditdocument">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[10]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Documents" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifydocument" id="chkmodifydocument"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifydocument">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[10]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Documents" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletedocument" id="chkdeletedocument"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletedocument">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Purchase orders</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[11]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkadddocument" id="chkadddocument"
                                value="1"/>
                        <label class="form-check-label" for="chkadddocument">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[11]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditdocument" id="chkeditdocument"
                                value="1"/>
                        <label class="form-check-label" for="chkeditdocument">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[11]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifydocument" id="chkmodifydocument"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifydocument">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[11]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletedocument" id="chkdeletedocument"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletedocument">
                            Delete </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[11]->allowfor_verify == 1) { ?> checked="checked" <?php } ?>
                                rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="verify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkverifydocument" id="chkverifydocument"
                                value="1"/>
                        <label class="form-check-label" for="chkverifydocument">
                            Verify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[11]->allowfor_complete == 1) { ?> checked="checked" <?php } ?>
                                rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="complete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkcompletedocument" id="chkcompletedocument"
                                value="1"/>
                        <label class="form-check-label" for="chkcompletedocument">
                            Complete </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[11]->allowfor_pay == 1) { ?> checked="checked" <?php } ?>
                                rel="Purchase orders" rel1="<?php echo $permissionusergroup; ?>" rel2="pay"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkpaydocument" id="chkpaydocument"
                                value="1"/>
                        <label class="form-check-label" for="chkpaydocument">
                            Pay </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Dashboard</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[12]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Dashboard" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkadddashboard" id="chkadddashboard"
                                value="1"/>
                        <label class="form-check-label" for="chkadddashboard">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[12]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Dashboard" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditdashboard" id="chkeditdashboard"
                                value="1"/>
                        <label class="form-check-label" for="chkeditdashboard">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[12]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Dashboard" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifydashboard" id="chkmodifydashboard"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifydashboard">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[12]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Dashboard" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletedashboard" id="chkdeletedashboard"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletedashboard">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Sell Properties</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[13]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Sell Properties" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddsellproperties" id="chkaddsellproperties"
                                value="1"/>
                        <label class="form-check-label" for="chkaddsellproperties">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[13]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Sell Properties" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditsellproperties" id="chkeditsellproperties"
                                value="1"/>
                        <label class="form-check-label" for="chkeditsellproperties">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[13]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Sell Properties" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifysellproperties" id="chkmodifysellproperties"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifysellproperties">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[13]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Sell Properties" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletesellproperties" id="chkdeletesellproperties"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletesellproperties">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-nowrap fw-medium">Settings</td>
            <td>
                <div class="d-flex">
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[14]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                rel="Settings" rel1="<?php echo $permissionusergroup; ?>" rel2="add"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkaddsettings" id="chkaddsettings"
                                value="1"/>
                        <label class="form-check-label" for="chkaddsettings">
                            Add </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[14]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                rel="Settings" rel1="<?php echo $permissionusergroup; ?>" rel2="edit"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkeditsettings" id="chkeditsettings"
                                value="1"/>
                        <label class="form-check-label" for="chkeditsettings">
                            View </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[14]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                rel="Settings" rel1="<?php echo $permissionusergroup; ?>" rel2="modify"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkmodifysettings" id="chkmodifysettings"
                                value="1"/>
                        <label class="form-check-label" for="chkmodifysettings">
                            Modify </label>
                    </div>
                    <div class="form-check me-3 me-lg-5">
                        <input <?php if ($settingsall[14]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                rel="Settings" rel1="<?php echo $permissionusergroup; ?>" rel2="delete"
                                class="permissioncls form-check-input" type="checkbox"
                                name="chkdeletesettings" id="chkdeletesettings"
                                value="1"/>
                        <label class="form-check-label" for="chkdeletesettings">
                            Delete </label>
                    </div>
                </div>
            </td>
        </tr>

        </tbody>
    </table>
</div>
