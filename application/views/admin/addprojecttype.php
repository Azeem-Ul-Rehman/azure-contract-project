<?php include('partials/header.php'); ?>
<!-- partial -->
<!-- Content wrapper -->
<!-- Content wrapper -->
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Project Type
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div
                                class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Add Project Type</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row">
                                <!-- First column-->
                                <div class="col-md-12">
                                    <!-- Product Information -->


                                    <div class="form-floating form-floating-outline mb-4">
                                        <input
                                                type="text"
                                                class="form-control required"
                                                id="projecttype"
                                                placeholder="Project Type"
                                                name="projecttype"
                                                aria-label="Project Type"/>
                                        <label for="projecttype">Name</label>
                                    </div>


                                </div>
                                <!-- /Second column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

