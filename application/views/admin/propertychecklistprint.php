<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CheckList PDF</title>
    <style>
        h2{
            text-align: center;
        }

        table{
            border: 1px solid black;
            border-spacing: 0;
            margin: auto;
        }
        td, th{
            border: 1px solid #000000;
            padding: 5px;
        }

        .main-door-head{
            background-color: #f2ca9c;
        }
        #content{
            padding: 0 2rem;
        }

        .contractors{
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 20rem;
            margin-top: 4rem;
        }
        .signature-continer div{
            margin-top: 2.5rem;
            border-bottom: 1px dashed #000000;
            width: 200px;
        }
        .signature-continer label{
            text-align: center;
        }
    </style>
</head>
<body>
    <div id="content">
        <table id="pdf-content">
            <tbody>
                <?php if ($sectionList != null && !empty($sectionList)) {
                    $SectionName = "";
                    foreach ($sectionList as $key => $sectionData) {
               ?>
               <?php if($SectionName != $sectionData->section){ ?>
                <tr class="main-door-head">
                    <th colspan="5"><?php echo $sectionData->section;?></th>
                </tr>
                <?php $SectionName = $sectionData->section; }?>

                <tr>
                    <td><?php echo $sectionData->item;?></td>
                    <td>
                        <div class="form-group1">
                            <input <?php if($sectionData->item_condition == "Goed"){ echo "checked"; } ?> class="form-check-input" type="radio" value="Goed" id="radioGoed" disabled>
                            <label class="form-check-label" for="inlineCheckbox1">Goed</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-group1">
                            <input <?php if($sectionData->item_condition == "Slecht"){ echo "checked"; } ?> class="form-check-input" type="radio" value="Slecht" id="radiSlecht" disabled>
                            <label class="form-check-label" for="inlineCheckbox1">Slecht</label>
                        </div>
                    </td>
                    <td><?php echo $sectionData->remarks;?></td>
                </tr>
               <?php }}?>
                
            </tbody>
        </table>

        <div class="contractors">
            <div class="signature-continer">
                <label>Buyer:</label><br>
                <div class="signature"></div>
            </div>

            <div class="signature-continer">
                <label>Seller:</label><br>
                <div></div>
            </div>
        </div>
    </div>
</body>
</html>