<?php include('partials/header.php'); ?>
<style>
    .container-p-y:not([class^=pb-]):not([class*=" pb-"]) {
        padding-bottom: 0rem !important;
    }
</style>
<div class="content-wrapper">
    <!-- Content -->
    <form id="my_form" name="my_form" method="post" action="administrator/updatesellpropertyaction" enctype="multipart/form-data" onsubmit="return checklasttab()">
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Edit Sell Property
        </h4>

            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Edit Sell Property</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
						    <input id="id" value="<?php echo $editsellproperty->id; ?>" class="form-control" name="hdnsellproperty" type="hidden">

                            <input id="signatureType" value="0" class="form-control" name="signatureType" type="hidden">
                            
                            <input id="signatureTypeBuyer" value="0" class="form-control" name="signatureTypeBuyer" type="hidden">
                            
                            <input id="signatureTypeSeller" value="0" class="form-control" name="signatureTypeSeller" type="hidden">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="project_id" id="project_id" class="form-control required">
											<option value="">Select Project</option>
											<?php foreach($projects as $project){ ?>
												<option <?php if($editsellproperty->project_id == $project->projectid){ echo "selected";} ?> value="<?php echo $project->projectid; ?>"><?php echo $project->project_name; ?></option>
											<?php } ?>
										</select>
                                        <label for="project_id">Project</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="project_type_id" id="project_type_id" class="form-control required">
											<option value="">Select Project Type</option>
											<?php foreach($projectTypes as $projectType){ ?>
												<option <?php if($editsellproperty->project_type_id == $projectType->id){ echo "selected";} ?> value="<?php echo $projectType->id; ?>"><?php echo $projectType->projecttype; ?></option>
											<?php } ?>
										</select>
                                        <label for="project_type_id">Project Type</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                    <textarea id="address" class="form-control required" name="address" placeholder="Kavel Nr. / Address" aria-label="Kavel Nr. / Address"><?php echo $editsellproperty->address; ?></textarea>
                                        <label for="address">Kavel Nr. / Address</label>
                                    </div>
                                </div>
                                <!-- /Second column -->
                            </div>
							<div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="real_estate" placeholder="Real Estate" name="real_estate" aria-label="Real Estate" value="<?php echo $editsellproperty->real_estate; ?>"/>
                                        <label for="real_estate">Real Estate</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="buyer_name" placeholder="Buyer Name" name="buyer_name" aria-label="Buyer Name" value="<?php echo $editsellproperty->buyer_name; ?>"/>
                                        <label for="buyer_name">Buyer Name</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <textarea id="buyer_address" class="form-control required" name="buyer_address" placeholder="Buyer Address" aria-label="Buyer Address"><?php echo $editsellproperty->buyer_address; ?></textarea>
                                        <label for="buyer_address">Buyer Address</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="buyer_phone" placeholder="Buyer Phone" name="buyer_phone" aria-label="Buyer Phone" value="<?php echo $editsellproperty->buyer_phone; ?>"/>
                                        <label for="buyer_phone">Buyer Phone</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="email" class="form-control required" id="buyer_email" placeholder="Buyer Email" name="buyer_email" aria-label="Buyer Email" value="<?php echo $editsellproperty->buyer_email; ?>"/>
                                        <label for="buyer_email">Buyer Email</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="numnber" class="form-control required" id="selling_price" placeholder="Selling Price" name="selling_price" aria-label="Selling Price" value="<?php echo $editsellproperty->selling_price; ?>"/>
                                        <label for="selling_price">Selling Price</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="other_charges" placeholder="Other Charges" name="other_charges" aria-label="Other Charges" value="<?php echo $editsellproperty->other_charges; ?>"/>
                                        <label for="other_charges">Other Charges</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="date" class="form-control required" id="agreement_date" placeholder="Agreement Date" name="agreement_date" aria-label="Agreement Date" value="<?php echo date('Y-m-d', strtotime($editsellproperty->agreement_date)); ?>"/>
                                        <label for="agreement_date">Agreement Date</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="date" class="form-control required" id="delivery_date" placeholder="Delivery Date" name="delivery_date" aria-label="Delivery Date" value="<?php echo date('Y-m-d', strtotime($editsellproperty->delivery_date)); ?>"/>
                                        <label for="delivery_date">Delivery Date</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Document Add -->
							<div id="newRowdocuments">
                            <?php if(!empty($projectdocuments)){
                                foreach($projectdocuments as $rowsdocument){?>
								<div class="row mt-4 g-2" id="inputFormdocuments">
									<div class="col-md-5">
										<div class="form-floating form-floating-outline">
											<input type="text" name="documentname[]" id="documentname" class="form-control m-input" value="<?php echo $rowsdocument->document_name; ?>" placeholder="Document Name" autocomplete="off" aria-label="Document Name">
											<label for="documentname">Document Name</label>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-floating form-floating-outline">
											<input type="file" name="filename[]" id="filename" class="form-control m-input" placeholder="Upload Document" autocomplete="off" aria-label="Upload Document">
											<label for="filename">Upload Document</label>
                                            <input type="hidden" name="hdndocumentname[]" value="<?php echo $rowsdocument->document_file; ?>" />
										</div>
									</div>
                                    <div class="col-md-1">
                                        <?php $getimgtype = explode('.',$rowsdocument->document_file);
                                        if($getimgtype[1]=='jpg' || $getimgtype[1]=='png' || $getimgtype[1]=='jpeg' || $getimgtype[1]=='gif'){?>
                                        <a href="uploads/documents/<?php echo $rowsdocument->document_file; ?>" download> <img src="uploads/documents/<?php echo $rowsdocument->document_file; ?>" style="width:50px; height:50px;" /></a>
                                        <?php }else if($getimgtype[1]=='docs'){	echo '<a href="uploads/documents/'.$rowsdocument->document_file.'" download> <img src="uploads/wordimg.png" style="width:50px; height:50px;"  /></a>';}
                                        else if($getimgtype[1]=='xlsx' || $getimgtype[1]=='csv'){ echo '<a href="uploads/documents/'.$rowsdocument->document_file.'"  download> <img src="uploads/excelimg.png" style="width:50px; height:50px;" /></a>';}
                                        else if($getimgtype[1]=='pdf'){ echo '<a href="uploads/documents/'.$rowsdocument->document_file.'"  download> <img src="uploads/pdfimg.png" style="width:50px; height:50px;" /></a>';}
                                        else{ echo '<a href="uploads/documents/'.$rowsdocument->document_file.'" download> <img src="uploads/wordimg.png" style="width:50px; height:50px;"  /></a>';?>
                                        <?php } ?>  
                                    </div>
									<div class="col-md-2">
									<button id="removedocuments" type="button" class="btn btn-danger">Remove</button>
									</div>
									
								</div>
                                <?php }}?>
							</div>
							<div class="row g-4 mb-4 mt-4">
								<div class="action-btns">
									<button id="addRowdocument" type="button" class="btn btn-info">Add New Document</button>
								</div>
                            </div>

                            <!-- Check List Add -->

                            <div class="row">
                                <h4>House Inspection Checklist</h4>
                            </div>

                            <?php
                                if(!empty($checkList)){
                                $SectionName = "";
                                foreach($checkList as $sectionData){ 
                            ?>
                            <?php if($SectionName != $sectionData->section_sort){ ?>
                                <div class="row border-top border-bottom border-2 border-dark mb-2">
                                    <div class="col-md-12 mt-2 mb-2">
                                        <strong><?php echo $sectionData->section;?> </strong>
                                    </div>
                                </div>
                            <?php $SectionName = $sectionData->section_sort; }?>
                            <div class="row g-2">
                                <div class="col-md-3">
                                    <?php echo $sectionData->item;?>
                                </div>
                                <?php
                                    $where = array('sell_property_id' => $this->input->get('id',true), 'check_list_id' => $sectionData->id);
                                    $itemList = $this->adminmodel->getSingle(TBLCHECKLISTITEMS,$where);
                                ?>
                                <div class="col-md-2">
                                    <div class="form-group1">
                                        <input <?php if($itemList->item_condition == "Goed"){ echo "checked"; } ?> class="form-check-input" type="radio" value="Goed" id="radioGoed" name="condition<?php echo $sectionData->id;?>" onclick="updateSignature();">
                                        <label class="form-check-label" for="inlineCheckbox1">Goed</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group1">
                                        <input <?php if($itemList->item_condition == "Slecht"){ echo "checked"; } ?> class="form-check-input" type="radio" value="Slecht" id="radiSlecht" name="condition<?php echo $sectionData->id;?>" onclick="updateSignature();">
                                        <label class="form-check-label" for="inlineCheckbox1">Slecht</label>
                                    </div>
                                </div>
                                <div class="col-md-5 mb-2">
                                    <input type="text" id="remarks" style="height: 25px;" class="form-control" name="remarks<?php echo $sectionData->id;?>" placeholder="Remarks" value="<?php echo $itemList->remarks;?>" aria-label="Remarks" onchange="updateSignature();"/>
                                </div>
                            </div>
                            <?php }}?>
                            <input type="hidden" id="saveSignatureHidden" value="" name="saveSignature"/>
                            <input type="hidden" id="saveSignatureSellerHidden" name="saveSignatureSeller" value=""/>
                            
                        </div>
                    </div>
                </div>
            </div>

    </div>
    <!-- / Content -->

    <div class="d-flex flex-column justify-content-center justify-content-lg-between  flex-sm-column flex-md-column flex-lg-row gap-2" style="padding-left:1.5rem;padding-right:1.5rem;">
            <div class="">
                <div class="text-center my-1">
                    <strong> Buyer Signature</strong>        
                </div>
                
                <div id="canvas" style="<?php if(isset($editsellproperty->buyer_signature) && $editsellproperty->buyer_signature != null) {echo "display:none"; } else{echo "display:block";}?>;">

                    <canvas class="roundCorners" id="newSignature" style="position: relative; padding: 0; border: 1px solid #c4caac !important;border-radius: 8px;"></canvas>
                </div>

                <div id="saveSignature" style="<?php if(isset($editsellproperty->buyer_signature) && $editsellproperty->buyer_signature != null) {echo "display:flex"; } else{echo "display:none";}?>; padding: 0; border: 1px solid #c4caac !important;border-radius: 8px;align-items: center;justify-content: center;">
                    <img src="uploads/signature/<?php echo $editsellproperty->buyer_signature;?>"  alt="Saved image png"/>
                </div>

                <div class="text-center my-1">
                    <button type="button" class="btn btn-sm btn-primary mt-2" style="margin:auto;" onclick="signatureClear()">Clear signature</button>
                </div>
                
                
            </div>

            <div class="">
                <div class="text-center my-1">
                    <strong> Seller Signature</strong>        
                </div>
                <div id="canvasSeller" style="<?php if(isset($editsellproperty->seller_signature) && $editsellproperty->seller_signature != null) {echo "display:none"; } else{echo "display:block";}?>;">
                    <canvas class="roundCorners" id="newSignatureSeller" style="position: relative; padding: 0; border: 1px solid #c4caac;border-radius: 8px;"></canvas>
                </div>
                <div id="saveSignatureSeller" style="<?php if(isset($editsellproperty->seller_signature) && $editsellproperty->seller_signature != null) {echo "display:flex"; } else{echo "display:none";}?>;     align-items: center;justify-content: center; padding: 0; border: 1px solid #c4caac !important;border-radius: 8px;">
                    <img src="uploads/signature/<?php echo $editsellproperty->seller_signature;?>" alt="Saved image png"/>
                </div>
                <div class="text-center my-1">
                    <button type="button" class="btn btn-sm btn-primary mt-2" onclick="signatureClearSeller()">Clear signature</button>
                </div>
                
            </div>
        </div>
    <div class="d-flex flex-column justify-content-center justify-content-lg-between  flex-sm-column flex-md-column flex-lg-row gap-2 float-end" style="padding-left:1.5rem;padding-right:1.5rem;">
        <div class="action-btns">
            <button class="btn btn-primary">Submit</button>
        </div>
    </div>
    </form>
    <?php include('partials/sticky-footer.php'); ?>
</div>

<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

<script>

    $(document).ready(function(){
        signatureCapture();
        signatureCaptureSeller();
    });

    $("#addRowdocument").click(function () {
		var html = '';
		html += '<div class="row mt-4 g-2" id="inputFormdocuments">';
		html += '<div class="col-md-5">';
		html += '<div class="form-floating form-floating-outline ">';
		html += '<input type="text" name="documentname[]" id="documentname" class="form-control m-input" placeholder="Document Name" autocomplete="off" aria-label="Document Name">';
		html += '<label for="documentname">Document Name</label>';
		html += '</div>';
		html += '</div>';

		html += '<div class="col-md-5">';
		html += '<div class="form-floating form-floating-outline">';
		html += '<input type="file" name="filename[]" id="filename" class="form-control m-input" placeholder="Upload Document" autocomplete="off" aria-label="Upload Document">';
		html += '<label for="filename">Upload Document</label>';
		html += '</div>';
		html += '</div>';
		html += '<div class="col-md-2">';
		html += '<button id="removedocuments" type="button" class="btn btn-danger">Remove</button>';
		html += '</div>';
		html += '</div>';
		
		$('#newRowdocuments').append(html);
	});

	// remove row
	$(document).on('click', '#removedocuments', function () {
		$(this).closest('#inputFormdocuments').remove();
	});

    var checkValue = false;
    var checkBuyerSignature = false;
    var checkSellerSignature = false;
    function checklasttab(){
        $value = false;
        if(checkSellerSignature == true){

            signatureSaveSeller();
            $("#signatureTypeSeller").val('0');
        }
        if(checkBuyerSignature == true){

            signatureSave();
            $("#signatureTypeBuyer").val('0');
        }
        if($("#signatureType").val() == "1" && checkValue == false){
            Swal.fire({
                title: "Are you sure?",
                text: "Seller and buyer signature will be add again",
                icon: "warning",
                showCancelButton: !0,
                confirmButtonText: "OK",
                customClass: {
                    confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                    cancelButton: "btn btn-outline-secondary waves-effect"
                },
                buttonsStyling: !1
            }).then(function (e) {
                if (e.value) {
                    $value = true;
                    checkValue = true;
                    $("#my_form").submit();
                } else {
                    if (e.dismiss === Swal.DismissReason.cancel) {
                        Swal.fire({
                            title: "Cancelled",
                            text: "Cancelled Suspension :)",
                            icon: "error",
                            customClass: {
                                confirmButton: "btn btn-success waves-effect"
                            }
                        })
                    }
                    $value = false;
                }
            })
        }else{
            $value = true;
        }
        return $value;
    }

    function signatureCapture() {
        var canvas = document.getElementById("newSignature");
        var context = canvas.getContext("2d");
        if(screen.width < 800){
            canvas.width = screen.width - 50;
            canvas.height = 250;
        }else{

            canvas.width = 400;
            canvas.height = 200;
        }
        context.fillStyle = "#fff";
        context.strokeStyle = "#3a87ad";
        context.lineWidth = 1.5;
        context.lineCap = "round";
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.fillRect(0, 0, canvas.width, canvas.height);

        context.fillStyle = "#3a87ad";
        context.strokeStyle = "#3a87ad";
        context.lineWidth = 1;
        context.moveTo((canvas.width * 0.042), (canvas.height * 0.7));
        context.lineTo((canvas.width * 0.958), (canvas.height * 0.7));
        context.stroke();

        context.fillStyle = "#fff";
        context.strokeStyle = "#3a87ad";

        var disableSave = true;
        var pixels = [];
        var cpixels = [];
        var xyLast = {};
        var xyAddLast = {};
        var calculate = false;
        {   //functions
            function remove_event_listeners() {
            canvas.removeEventListener('mousemove', on_mousemove, false);
            canvas.removeEventListener('mouseup', on_mouseup, false);
            canvas.removeEventListener('touchmove', on_mousemove, false);
            canvas.removeEventListener('touchend', on_mouseup, false);

            document.body.removeEventListener('mouseup', on_mouseup, false);
            document.body.removeEventListener('touchend', on_mouseup, false);
            }

            function get_coords(e) {
                checkBuyerSignature = true;
                var x, y;

                if (e.changedTouches && e.changedTouches[0]) {
                    var offsety = canvas.offsetTop || 0;
                    var offsetx = canvas.offsetLeft || 0;

                    x = e.changedTouches[0].pageX - offsetx;
                    y = e.changedTouches[0].pageY - offsety;
                } else if (e.layerX || 0 == e.layerX) {
                    x = e.layerX;
                    y = e.layerY;
                } else if (e.offsetX || 0 == e.offsetX) {
                    x = e.offsetX;
                    y = e.offsetY;
                }

                return {
                    x : x, y : y
                };
            };

            function on_mousedown(e) {
                e.preventDefault();
                e.stopPropagation();

                canvas.addEventListener('mouseup', on_mouseup, false);
                canvas.addEventListener('mousemove', on_mousemove, false);
                canvas.addEventListener('touchend', on_mouseup, false);
                canvas.addEventListener('touchmove', on_mousemove, false);
                document.body.addEventListener('mouseup', on_mouseup, false);
                document.body.addEventListener('touchend', on_mouseup, false);

                empty = false;
                var xy = get_coords(e);
                context.beginPath();
                pixels.push('moveStart');
                context.moveTo(xy.x, xy.y);
                pixels.push(xy.x, xy.y);
                xyLast = xy;
            };

            function on_mousemove(e, finish) {
                e.preventDefault();
                e.stopPropagation();

                var xy = get_coords(e);
                var xyAdd = {
                    x : (xyLast.x + xy.x) / 2,
                    y : (xyLast.y + xy.y) / 2
                };

                if (calculate) {
                    var xLast = (xyAddLast.x + xyLast.x + xyAdd.x) / 3;
                    var yLast = (xyAddLast.y + xyLast.y + xyAdd.y) / 3;
                    pixels.push(xLast, yLast);
                } else {
                    calculate = true;
                }

                context.quadraticCurveTo(xyLast.x, xyLast.y, xyAdd.x, xyAdd.y);
                pixels.push(xyAdd.x, xyAdd.y);
                context.stroke();
                context.beginPath();
                context.moveTo(xyAdd.x, xyAdd.y);
                xyAddLast = xyAdd;
                xyLast = xy;

            };

            function on_mouseup(e) {
                remove_event_listeners();
                disableSave = false;
                context.stroke();
                pixels.push('e');
                calculate = false;
            };
        }
        canvas.addEventListener('touchstart', on_mousedown, false);
        canvas.addEventListener('mousedown', on_mousedown, false);
    }

    function signatureSave() {
        var canvas = document.getElementById("newSignature");
        var dataURL = canvas.toDataURL("image/png");
        $("#signatureType").val('0');
        $("#saveSignatureHidden").val(dataURL);
    };

    function signatureClear() {
        checkBuyerSignature = false;
        $("#canvas").css('display', 'block');
        var canvas = document.getElementById("newSignature");
        var context = canvas.getContext("2d");
        context.clearRect(0, 0, canvas.width, canvas.height);
        $("#saveSignature").css('display', 'none');
        $("#saveSignatureHidden").val('');
        $("#signatureTypeBuyer").val('-1');
        signatureCapture();
    }


    // Seller Signature
    function signatureCaptureSeller() {
        var canvas = document.getElementById("newSignatureSeller");
        var context = canvas.getContext("2d");
        if(screen.width < 800){
            canvas.width = screen.width - 50;
            canvas.height = 250;
        }else{

            canvas.width = 400;
            canvas.height = 200;
        }



        context.fillStyle = "#fff";
        context.strokeStyle = "#3a87ad";
        context.lineWidth = 1.5;
        context.lineCap = "round";
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.fillRect(0, 0, canvas.width, canvas.height);

        context.fillStyle = "#3a87ad";
        context.strokeStyle = "#3a87ad";
        context.lineWidth = 1;
        context.moveTo((canvas.width * 0.042), (canvas.height * 0.7));
        context.lineTo((canvas.width * 0.958), (canvas.height * 0.7));
        context.stroke();

        context.fillStyle = "#fff";
        context.strokeStyle = "#3a87ad";


        var disableSave = true;
        var pixels = [];
        var cpixels = [];
        var xyLast = {};
        var xyAddLast = {};
        var calculate = false;
        {   //functions
            function remove_event_listeners() {
            canvas.removeEventListener('mousemove', on_mousemove, false);
            canvas.removeEventListener('mouseup', on_mouseup, false);
            canvas.removeEventListener('touchmove', on_mousemove, false);
            canvas.removeEventListener('touchend', on_mouseup, false);

            document.body.removeEventListener('mouseup', on_mouseup, false);
            document.body.removeEventListener('touchend', on_mouseup, false);
            }

            function get_coords(e) {
                var x, y;
                checkSellerSignature = true;

                if (e.changedTouches && e.changedTouches[0]) {
                    var offsety = canvas.offsetTop || 0;
                    var offsetx = canvas.offsetLeft || 0;

                    x = e.changedTouches[0].pageX - offsetx;
                    y = e.changedTouches[0].pageY - offsety;
                } else if (e.layerX || 0 == e.layerX) {
                    x = e.layerX;
                    y = e.layerY;
                } else if (e.offsetX || 0 == e.offsetX) {
                    x = e.offsetX;
                    y = e.offsetY;
                }

                return {
                    x : x, y : y
                };
            };

            function on_mousedown(e) {
                e.preventDefault();
                e.stopPropagation();

                canvas.addEventListener('mouseup', on_mouseup, false);
                canvas.addEventListener('mousemove', on_mousemove, false);
                canvas.addEventListener('touchend', on_mouseup, false);
                canvas.addEventListener('touchmove', on_mousemove, false);
                document.body.addEventListener('mouseup', on_mouseup, false);
                document.body.addEventListener('touchend', on_mouseup, false);

                empty = false;
                var xy = get_coords(e);
                context.beginPath();
                pixels.push('moveStart');
                context.moveTo(xy.x, xy.y);
                pixels.push(xy.x, xy.y);
                xyLast = xy;
            };

            function on_mousemove(e, finish) {
                e.preventDefault();
                e.stopPropagation();

                var xy = get_coords(e);
                var xyAdd = {
                    x : (xyLast.x + xy.x) / 2,
                    y : (xyLast.y + xy.y) / 2
                };

                if (calculate) {
                    var xLast = (xyAddLast.x + xyLast.x + xyAdd.x) / 3;
                    var yLast = (xyAddLast.y + xyLast.y + xyAdd.y) / 3;
                    pixels.push(xLast, yLast);
                } else {
                    calculate = true;
                }

                context.quadraticCurveTo(xyLast.x, xyLast.y, xyAdd.x, xyAdd.y);
                pixels.push(xyAdd.x, xyAdd.y);
                context.stroke();
                context.beginPath();
                context.moveTo(xyAdd.x, xyAdd.y);
                xyAddLast = xyAdd;
                xyLast = xy;

            };

            function on_mouseup(e) {
                remove_event_listeners();
                disableSave = false;
                context.stroke();
                pixels.push('e');
                calculate = false;
            };
        }
        canvas.addEventListener('touchstart', on_mousedown, false);
        canvas.addEventListener('mousedown', on_mousedown, false);
    }

    function signatureSaveSeller() {
        var canvas = document.getElementById("newSignatureSeller");
        var dataURL = canvas.toDataURL("image/png");
        $("#saveSignatureSellerHidden").val(dataURL);
        $("#signatureType").val('0');
    };

    function signatureClearSeller() {
        $("#canvasSeller").css('display', 'block');
        checkSellerSignature = false;
        var canvas = document.getElementById("newSignatureSeller");
        var context = canvas.getContext("2d");
        context.clearRect(0, 0, canvas.width, canvas.height);
        $("#saveSignatureSeller").css('display', 'none');
        $("#saveSignatureSellerHidden").val('');
        signatureCaptureSeller();
        $("#signatureTypeSeller").val('-1');
    }

    function updateSignature() {
        $("#signatureType").val('1');
    }

</script>

