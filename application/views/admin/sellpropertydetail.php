<?php include('partials/header.php'); ?>
<style>
    p {
        display: inline-flex;
    }
</style>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Sell Property Details
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Sell Property Details</h5>

                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row">
                                <!-- First column-->
                                <div class="col-md-12">
                                    <!-- Product Information -->
                                    <div class="card mb-4">
                                        <div class="card-header p-0">
                                            <div class="nav-align-top">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect active"
                                                                role="tab"
                                                                data-bs-toggle="tab" data-bs-target="#tab-property-detail"
                                                                aria-controls="tab-property-detail" aria-selected="false"
                                                                tabindex="-1">
																Sell Property Details
                                                        </button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect" role="tab"
                                                                data-bs-toggle="tab" data-bs-target="#tab-check-list"
                                                                aria-controls="tab-check-list" aria-selected="false"
                                                                tabindex="-1">
																House Inspection Checklist
                                                        </button>
                                                    </li>
                                                    <span class="tab-slider"
                                                          style="left: 198.3px; width: 127.65px; bottom: 0px;"></span>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content p-0">
                                                <div class="tab-pane fade active show" id="tab-property-detail"
                                                     role="tabpanel">
													 <fieldset>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Project Name : </b>
                                                                    </label>

                                                                </div>
                                                                <div class="col-2 text-left">
                                                                    <?php 
                                                                    $where = array('projectid' => $editsellproperty->project_id);
                                                                    $projectDetail = $this->adminmodel->getSingle(TBLPROJECTS, $where);
                                                                    ?>
                                                                    <p><?php echo $projectDetail->project_name; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Project Type : </b>
                                                                    </label>

                                                                </div>
                                                                <div class="col-2">
                                                                <?php 
                                                                    $where = array('id' => $editsellproperty->project_type_id);
                                                                    $projectTypeDetail = $this->adminmodel->getSingle(PROJECTTYPE, $where);
                                                                ?>
                                                                    <p><?php echo $projectTypeDetail->projecttype; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Email : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p><?php echo $editsellproperty->email; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Kavel Nr. / Address : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p><?php echo $editsellproperty->address; ?>
                                                                    </p>
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Real Estate : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p><?php echo $editsellproperty->real_estate; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Buyer Name : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p> <?php echo $editsellproperty->buyer_name; ?></p>
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Buyer Address : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p><?php echo $editsellproperty->buyer_address; ?></p>
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Buyer Phone : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p><?php echo $editsellproperty->buyer_phone; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Buyer Email : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p><?php echo $editsellproperty->buyer_email; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Selling Price : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p>
                                                                        <?php echo number_format($editsellproperty->selling_price, 2, ".", ","); ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Other Charges : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p><?php echo number_format($editsellproperty->other_charges, 2, ".", ","); ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Agreement Date : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p><?php echo date('d-m-Y', strtotime($editsellproperty->agreement_date)); ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <label for="companyname"><b>Delivery Date : </b>
                                                                    </label>
                                                                </div>
                                                                <div class="col-2">
                                                                    <p><?php echo date('d-m-Y', strtotime($editsellproperty->delivery_date)); ?></p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </fieldset>
                                                </div>
                                                <div class="tab-pane fade" id="tab-check-list" role="tabpanel">
                                                    <?php
                                                        if(!empty($sectionList)){
                                                        $SectionName = "";
                                                        foreach($sectionList as $sectionData){ 
                                                    ?>
                                                    <?php if($SectionName != $sectionData->section_sort){ ?>
                                                        <div class="row border-top border-bottom border-2 border-dark mt-2 mb-2">
                                                            <div class="col-md-12 mt-2 mb-2">
                                                                <strong><?php echo $sectionData->section;?> </strong>
                                                            </div>
                                                        </div>
                                                    <?php $SectionName = $sectionData->section_sort; }?>
                                                    <div class="row mt-2 mb-2">
                                                        <div class="col-md-3">
                                                            <?php echo $sectionData->item;?>
                                                        </div>
                                                        <div class="col-md-2">
                                                             <div class="form-group1">
                                                                <input <?php if($sectionData->item_condition == "Goed"){ echo "checked"; } ?> class="form-check-input" type="radio" value="Goed" id="radioGoed" disabled>
                                                                <label class="form-check-label" for="inlineCheckbox1">Goed</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group1">
                                                                <input <?php if($sectionData->item_condition == "Slecht"){ echo "checked"; } ?> class="form-check-input" type="radio" value="Slecht" id="radiSlecht" disabled>
                                                                <label class="form-check-label" for="inlineCheckbox1">Slecht</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <?php echo $sectionData->remarks;?>
                                                        </div>
                                                    </div>
                                                    <?php }}?>

                                                    <div class="row mt-2">
                                                        <?php if($editsellproperty->buyer_signature != null) {?>
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12 mt-2">
                                                                    <img id="saveSignature" src="uploads/signature/<?php echo $editsellproperty->buyer_signature;?>"  alt="Saved image png"/>
                                                                </div>
                                                                <div class="col-md-12 mt-2 border-top border-2">
                                                                    <strong> Buyer Signature</strong>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php }?>
                                                        <?php if($editsellproperty->seller_signature != null) {?>
                                                        <div class="offset-md-4 col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12 mt-2">
                                                                    <img id="saveSignatureSeller" src="uploads/signature/<?php echo $editsellproperty->seller_signature;?>" alt="Saved image png"/>
                                                                </div>
                                                                <div class="col-md-12 mt-2 border-top border-2">
                                                                    <strong> Seller Signature</strong>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php }?>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /Second column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>
