<?php include('partials/header.php'); ?>
<style>    p {
        display: inline-flex;
    }</style>
<div class="content-wrapper">    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y"><h4 class="py-3 mb-4"><span
                    class="text-muted fw-light">Home/</span>
            Suppliers Details </h4>
        <form id="my_form" name="my_form" method="post" action="">            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Suppliers Details</h5></div>
                        <!-- Add Project Type --> <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible"><a href="#" class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>                        <?php } ?>
                        <div class="card-body">
                            <div class="row">                                <!-- First column-->
                                <div class="col-md-12">
                                    <!-- Product Information -->
                                    <fieldset>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Name : </b> </label>
                                                </div>
                                                <div class="col-2 text-left">
                                                    <p><?php echo $viewsuppliers->suppliers_name; ?></p></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Phone No : </b>
                                                    </label></div>
                                                <div class="col-2">


                                                    <p>
                                                        <?php echo $viewsuppliers->suppliers_phone; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Type : </b> </label>
                                                </div>
                                                <div class="col-2">
                                                    <p>
                                                        <?php echo $viewsuppliers->suppliers_type; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Address : </b>
                                                    </label></div>
                                                <div class="col-2">

                                                    <p>
                                                        <?php echo $viewsuppliers->suppliers_address; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Bank Account No
                                                            : </b> </label></div>
                                                <div class="col-2">
                                                    <p>
                                                        <?php echo $viewsuppliers->suppliers_bankacno; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Total Payments
                                                            : </b> </label></div>
                                                <div class="col-2">
                                                    <p>
                                                        <?php echo number_format($paidpayments, 2, ".", ","); ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Outstanding Payment
                                                            : </b> </label></div>
                                                <div class="col-2">
                                                    <p>
                                                        <?php echo number_format($outstandingpayment, 2, ".", ","); ?>                                                                                  </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Kvk No : </b>
                                                    </label></div>
                                                <div class="col-2">
                                                    <p>
                                                        <?php echo $viewsuppliers->suppliers_kvkno; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="card mb-4">
                                        <div class="card-header p-0">
                                            <div class="nav-align-top">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect active"
                                                                role="tab" data-bs-toggle="tab"
                                                                data-bs-target="#navs-top-home"
                                                                aria-controls="navs-top-home" aria-selected="false"
                                                                tabindex="-1"> All Invoices
                                                        </button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect"
                                                                role="tab" data-bs-toggle="tab"
                                                                data-bs-target="#navs-top-profile"
                                                                aria-controls="navs-top-profile"
                                                                aria-selected="false" tabindex="-1"> Outstanding
                                                            Invoices
                                                        </button>
                                                    </li>
                                                    <span class="tab-slider"
                                                          style="left: 198.3px; width: 127.65px; bottom: 0px;"></span>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content p-0">
                                                <div class="tab-pane fade active show" id="navs-top-home"
                                                     role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew"
                                                               class="datatables-products table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Invoice nr.</th>
                                                                <th>Purchase Order</th>
                                                                <th>Invoice Amount</th>
                                                                <th>Invoice Date</th>
                                                                <th>Payment Status</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php if (!empty($listofinvoice)) {
                                                                foreach ($listofinvoice as $rowsinvoice) { ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="administrator/orderdetails?orderid=<?php echo $rowsinvoice->orderid; ?>"><?php echo $rowsinvoice->invoicenr; ?></a>
                                                                        </td>

                                                                        <td>
                                                                            <?php if ($rowsinvoice->perchaseorderimage != '') { ?>
                                                                                <a href="javascript:void(0)"
                                                                                   id="image-open"
                                                                                   data-title="Purchase Order Image"
                                                                                   data-src="uploads/invoices/<?php echo $rowsinvoice->perchaseorderimage; ?>">View</a>
                                                                            <?php } else { ?>
                                                                                No Image
                                                                            <?php } ?>

                                                                        </td>
                                                                        <td> <?php echo number_format($rowsinvoice->invoiceamount, 2, ".", ","); ?></td>
                                                                        <td><?php echo date('d/m/Y', strtotime($rowsinvoice->invoicedate)); ?></td>
                                                                        <td><?php if ($rowsinvoice->ispaid == 1) {
                                                                                echo 'Paid';
                                                                            } else {
                                                                                echo 'Unpaid';
                                                                            } ?></td>
                                                                    </tr>
                                                                <?php }
                                                            } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="navs-top-profile" role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew"
                                                               class=" datatables-products table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Invoice nr.</th>
                                                                <th>Purchase Order</th>
                                                                <th>Invoice Amount</th>
                                                                <th>Invoice Date</th>
                                                                <th>Payment Status</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php if (!empty($listofinvoicenotpaid) && count($listofinvoicenotpaid) > 0) {
                                                                foreach ($listofinvoicenotpaid as $rowspaidinvoice) { ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="administrator/orderdetails?orderid=<?php echo $rowspaidinvoice->orderid; ?>"><?php echo $rowspaidinvoice->invoicenr; ?></a>
                                                                        </td>
                                                                        <td>
                                                                            <?php if ($rowspaidinvoice->perchaseorderimage != '') { ?>
                                                                                <a href="javascript:void(0)"
                                                                                   id="image-open"
                                                                                   data-title="Purchase Order Image"
                                                                                   data-src="uploads/invoices/<?php echo $rowspaidinvoice->perchaseorderimage; ?>">View</a>
                                                                            <?php } else { ?>
                                                                                No Image
                                                                            <?php } ?>

                                                                        </td>
                                                                        <td><?php echo number_format($rowspaidinvoice->invoiceamount, 2, ".", ","); ?></td>
                                                                        <td><?php echo date('d/m/Y', strtotime($rowspaidinvoice->invoicedate)); ?></td>
                                                                        <td>
                                                                            <?php if ($rowspaidinvoice->ispaid == 1) {
                                                                                echo 'Paid';
                                                                            } else {
                                                                                echo 'Unpaid';
                                                                            } ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php }
                                                            } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                <!-- /Second column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>    <!-- / Content --> <?php include('partials/sticky-footer.php'); ?>

</div>

<div class="modal fade" id="image-modal" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg modal-simple modal-enable-otp modal-dialog-centered">
        <div class="modal-content p-3 p-md-5">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center mb-4">
                    <h3 class="mb-2" id="title">Purchase order image</h3>
                </div>
                <div class="text-center">
                    <img src="" alt="img" class="img-fluid image-src" style="width: 50%">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Content wrapper --><?php include('partials/footer.php'); ?>
<script>
    $(document).ready(function () {

        $('.datatables-products').DataTable({
            dom: '<"card-header d-flex border-top rounded-0 flex-wrap py-md-0"<"me-5 ms-n2"f><"d-flex justify-content-start justify-content-md-end align-items-baseline"<"dt-action-buttons d-flex align-items-start align-items-md-center justify-content-sm-center mb-3 mb-sm-0 gap-3"lB>>>t<"row mx-1"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            lengthMenu: [7, 10, 20, 50, 70, 100],
            buttons: [],
        });

    });
</script>
<script>
    $(document).on('click', '#image-open', function () {
        var title = $(this).data('title');
        var src = $(this).data('src');
        $('.image-src').attr('src', src);
        $('#title').text(title);
        $('#image-modal').modal('show');
    })
</script>
