<?php include('partials/header.php'); ?>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Add Payments
        </h4>
        <form id="customerForm" name="customerForm" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Add Payments</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row">
                                <input type="hidden" name="paidto" value="Supplier">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select onchange="getsuppliersorders(this.value);" name="suppliersval"
                                                id="suppliersval" class="form-control required">
                                            <option value="">Select Suppliers</option>
                                            <?php foreach ($supplierslist as $supplierval) { ?>
                                                <option value="<?php echo $supplierval->suppliers_id; ?>"><?php echo $supplierval->suppliers_name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="suppliersval">Suppliers</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="paymentmethod" id="paymentmethod" class="form-control required">
                                            <?php if (!empty($paymentmethods)) { ?>
                                                <?php foreach ($paymentmethods as $rowspaymentmethods) { ?>
                                                    <option value="<?php echo $rowspaymentmethods->paymentmethod; ?>"><?php echo $rowspaymentmethods->paymentmethod; ?></option>
                                                <?php }
                                            } ?>
                                            <option value="Other">Other</option>
                                        </select>
                                        <label for="paymentmethod">Method</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="number" class="form-control required" id="Amount" value="0.00"
                                               placeholder="Amount" name="Amount" aria-label="Amount"/>
                                        <label for="Amount">Amount</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="paidbycompany" id="paidbycompany" class="form-control required">
                                            <?php foreach ($companylist as $rowscompany) { ?>
                                                <option value="<?php echo $rowscompany->company_name; ?>"><?php echo $rowscompany->company_name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="paidbycompany">Paid by (Company)</label>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="date" class="form-control valid" aria-invalid="false"
                                               id="paymentdate" placeholder="Payment date" name="paymentdate"
                                               aria-label="Payment date"/>
                                        <label for="paymentdate">Payment date</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <textarea id="paymentdescription" class="form-control required"
                                                  name="paymentdescription" aria-label="Payment Description"
                                                  placeholder="Payment Description"></textarea>
                                        <label for="paymentdescription">Payment Description</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive displaysuppliercls" id="getsuppliersordersid"">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
    </div>
    </form>
</div>
<!-- / Content -->

<?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

<script src="adminassets/js/formpickers.js"></script>

<script src="//code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>
<script>
    function getsuppliersorders(suppliers_id) {
        $.ajax({
            url: "admin/getsuppliersoutstandingorders",
            type: "POST",
            data: {
                suppliers_id: suppliers_id
            },
            success: function (data) {
                $('#getsuppliersordersid').html(data);
            }
        });

    }

    function check_username(username) {
        $('.chkusername_cls').css('display', 'none');
        $('.chkusername_cls').text('');
        if (username != '') {
            $.ajax({
                url: "admin/chkuser_username",
                type: "POST",
                data: {
                    username: username
                },
                success: function (data) {
                    if (data != 'true') {
                        $('.chkusername_cls').css('display', 'block');
                        $('.chkusername_cls').text('This username is already exist!');
                        return false;
                    } else {
                        $('.chkusername_cls').css('display', 'none');
                        $('.chkusername_cls').text('');
                    }
                }
            });
        }
    }

    function blockSpecialChar(e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));
    }
</script>
<script>
    $(document).ready(function () {
        $('#paidto').change(function () {
            if ($(this).val() == 'Supplier') {
                $('.shownotsuppliercls').css('display', 'none');
                $('.displaysuppliercls').css('display', 'block');
                $('.displaycontractorcls').css('display', 'block');

            } else if ($(this).val() == 'Contractor') {

                $('.shownotsuppliercls').css('display', 'none');
                $('.displaysuppliercls').css('display', 'none');
                $('.displaycontractorcls').css('display', 'block');
                $('#getsuppliersordersid').html('');

            } else if ($(this).val() == 'Financial Institution') {

                $('.shownotsuppliercls').css('display', 'none');
                $('.displaysuppliercls').css('display', 'none');
                $('.displaycontractorcls').css('display', 'none');
                $('#getsuppliersordersid').html('');

            } else {
                $('.shownotsuppliercls').css('display', 'block');
                $('.displaysuppliercls').css('display', 'none');
                $('.displaycontractorcls').css('display', 'block');
                $('#purchaseorder').removeClass('required');
                $('#purchaseorderlabelid').html('Purchase Order/ Invoice');
                $('#getsuppliersordersid').html('');
            }

            /*if($(this).val()=='Contractor')
            {
                //$('.shownotsuppliercls').css('display','none');
                $('.displaycontractorcls').css('display','block');
            }else{
                $('.displaycontractorcls').css('display','none');
                //$('.shownotsuppliercls').css('display','block');
            }*/

        });







    });
</script>

