</div>
<!-- / Layout page -->
</div>

<!-- Overlay -->
<div class="layout-overlay layout-menu-toggle"></div>

<!-- Drag Target Area To SlideIn Menu On Small Screens -->
<div class="drag-target"></div>
</div>
<!-- / Layout wrapper -->

<!-- Core JS -->
<script src="backend-assets/vendor/js/core.js"></script>

<!-- Vendors JS -->
<script src="backend-assets/vendor/libs/apex-charts/apexcharts.js"></script>
<script src="backend-assets/vendor/libs/swiper/swiper.js"></script>
<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->


<!-- Vendors JS -->
<script src="backend-assets/vendor/libs/datatables-bs5/datatables-bootstrap5.js"></script>
<script src="backend-assets/vendor/libs/bootstrap-select/bootstrap-select.js"></script>
<script src="backend-assets/vendor/libs/select2/select2.js"></script>
<script src="backend-assets/vendor/libs/sweetalert2/sweetalert2.js"></script>


<!-- endbuild -->

<!-- Vendors JS -->
<script src="backend-assets/vendor/libs/jquery-sticky/jquery-sticky.js"></script>
<script src="backend-assets/vendor/libs/cleavejs/cleave.js"></script>
<script src="backend-assets/vendor/libs/cleavejs/cleave-phone.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script src="backend-assets/vendor/libs/dropzone/dropzone.js"></script>
<!-- Main JS -->
<script src="backend-assets/js/main.js"></script>

<!-- Page JS -->
<script src="backend-assets/js/forms-selects.js"></script>
<!-- Page JS -->
<script src="backend-assets/js/dashboards-analytics.js"></script>
<script src="backend-assets/js/forms-file-upload.js"></script>
<script>
    $("#my_form").validate({
        errorElement: "div",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            error.insertAfter(element);
        },
        highlight: function (element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function (element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        }
    });


    $("#customerForm").validate({
        rules: {
            suppliersval: "required",
            paymentmethod: "required",
            Amount: "required",
            paidbycompany: "required",
            paymentdate: "required",
        },
        messages: {
            suppliersval: "Please select supplier",
            paymentmethod: "Please select payment method",
            Amount: "Please enter amount",
            paidbycompany: "Please select paid by company",
            paymentdate: "Please enter payment date",
        },
        errorElement: "div",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            error.insertAfter(element);
        },
        highlight: function (element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function (element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        },
        submitHandler: function (form) {
            var amount = parseFloat($('#Amount').val()).toFixed(2);
            var checkboxes = $('.chksinglecls');
            let invoice = false;
            $.each(checkboxes, function () {
                var $this = $(this);
                // check if the checkbox is checked
                if ($this.is(":checked") === true) {
                    invoice = true;
                }
            });
            if (parseFloat(amount) <= 0) {
                // Swal.fire("Amount Should be greater than zero");

                Swal.fire({
                    title: "",
                    text: "Amount Should be greater than zero",
                    icon: "error",
                    showCancelButton: 0,
                    confirmButtonText: "OK",
                    customClass: {
                        confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                    }
                })


            } else if (invoice === false) {
                Swal.fire({
                    title: "",
                    text: "Atleast once invoice should be selected.",
                    icon: "error",
                    showCancelButton: 0,
                    confirmButtonText: "OK",
                    customClass: {
                        confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                    }
                })
            } else {
                var text = '';
                var difference = parseFloat($('.difference').text()).toFixed(2);
                if (parseFloat(difference) === 0) {
                    text = 'You want to make this payment.';
                } else {
                    text = 'You want to continue saving with the difference'
                }
                Swal.fire({
                    title: "Are you sure?",
                    text: text,
                    icon: "warning",
                    showCancelButton: !0,
                    confirmButtonText: "OK",
                    customClass: {
                        confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                        cancelButton: "btn btn-outline-secondary waves-effect"
                    },
                    buttonsStyling: !1
                }).then(function (e) {
                    if (e.value) {
                        form.submit();
                    } else {
                    }
                })
            }


        }
    });
</script>
</body>
</html>
