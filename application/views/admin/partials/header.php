<?php
//$this->load->helper('count_siderbar');
//cronjobfunctions();
?>
<html
        lang="en"
        class="light-style layout-navbar-fixed layout-menu-fixed layout-compact"
        dir="ltr"
        data-theme="theme-default"
        data-assets-path="backend-assets/"
        data-template="vertical-menu-template">
<head>
    <meta charset="utf-8"/>
    <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"/>
    <base href="<?php echo site_url(); ?>">
    <!-- Required meta tags -->
    <title><?php echo $title; ?></title>

    <meta name="description" content=""/>

    <!-- Favicon -->
    <!--    <link rel="icon" type="image/x-icon" href="backend-assets/img/favicon/favicon.ico" />-->
    <link rel="shortcut icon" href="backend-assets/images/logo_icon.ico"/>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com"/>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
    <link
            href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&ampdisplay=swap"
            rel="stylesheet"/>

    <!-- Icons -->
    <link rel="stylesheet" href="backend-assets/vendor/fonts/materialdesignicons.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/fonts/flag-icons.css"/>

    <!-- Menu waves for no-customizer fix -->
    <link rel="stylesheet" href="backend-assets/vendor/libs/node-waves/node-waves.css"/>

    <!-- Core CSS -->
    <link rel="stylesheet" href="backend-assets/vendor/css/rtl/core.css" class="template-customizer-core-css"/>
    <link rel="stylesheet" href="backend-assets/vendor/css/rtl/theme-default.css"
          class="template-customizer-theme-css"/>
    <link rel="stylesheet" href="backend-assets/css/demo.css"/>

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="backend-assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/typeahead-js/typeahead.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/apex-charts/apex-charts.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/swiper/swiper.css"/>

    <!-- Page CSS -->
    <link rel="stylesheet" href="backend-assets/vendor/css/pages/cards-statistics.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/css/pages/cards-analytics.css"/>


    <!-- Vendors CSS -->
    <link rel="stylesheet" href="backend-assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/typeahead-js/typeahead.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/datatables-bs5/datatables.bootstrap5.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/datatables-responsive-bs5/responsive.bootstrap5.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/datatables-buttons-bs5/buttons.bootstrap5.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/datatables-checkboxes-jquery/datatables.checkboxes.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/bootstrap-select/bootstrap-select.css" />
    <link rel="stylesheet" href="backend-assets/vendor/libs/select2/select2.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/sweetalert2/sweetalert2.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <!-- Helpers -->
    <script src="backend-assets/vendor/js/helpers.js"></script>
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->
    <script src="backend-assets/vendor/js/template-customizer.js"></script>
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="backend-assets/js/config.js"></script>
    <!-- Page CSS -->
    <link rel="stylesheet" href="backend-assets/vendor/css/pages/page-profile.css"/>
    <link rel="stylesheet" href="backend-assets/vendor/libs/dropzone/dropzone.css" />
    <style>
        .alert-success {
            color: #078b51;
            background-color: rgba(9, 183, 107, 0.2);
            border-color: #08a862;
        }

        .alert {
            font-size: 0.875rem;
        }

        .alert-success {
            color: #055f38;
            background-color: #cef1e1;
            border-color: #baebd6;
        }

        .alert-dismissible {
            padding-right: 4rem;
        }

        .alert {
            position: relative;
            padding: 0.75rem 1.25rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: 0.25rem;
        }
    </style>
</head>
<body>
<!-- Layout wrapper -->
<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">

        <?php include('sidebar.php'); ?>

        <!-- Layout container -->
        <div class="layout-page">

            <?php include('navbar.php'); ?>

