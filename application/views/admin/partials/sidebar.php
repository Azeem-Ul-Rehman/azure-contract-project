<!-- Menu -->
<?php $usertype = $this->session->userdata('admin_usertype');

?>
<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="javascript:void(0)" class="app-brand-link">
              <span class="app-brand-logo demo">
              </span>
            <span class="app-brand-text demo menu-text fw-bold ms-2">Azure Properties</span>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto">
            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                        d="M11.4854 4.88844C11.0081 4.41121 10.2344 4.41121 9.75715 4.88844L4.51028 10.1353C4.03297 10.6126 4.03297 11.3865 4.51028 11.8638L9.75715 17.1107C10.2344 17.5879 11.0081 17.5879 11.4854 17.1107C11.9626 16.6334 11.9626 15.8597 11.4854 15.3824L7.96672 11.8638C7.48942 11.3865 7.48942 10.6126 7.96672 10.1353L11.4854 6.61667C11.9626 6.13943 11.9626 5.36568 11.4854 4.88844Z"
                        fill="currentColor"
                        fill-opacity="0.6"/>
                <path
                        d="M15.8683 4.88844L10.6214 10.1353C10.1441 10.6126 10.1441 11.3865 10.6214 11.8638L15.8683 17.1107C16.3455 17.5879 17.1192 17.5879 17.5965 17.1107C18.0737 16.6334 18.0737 15.8597 17.5965 15.3824L14.0778 11.8638C13.6005 11.3865 13.6005 10.6126 14.0778 10.1353L17.5965 6.61667C18.0737 6.13943 18.0737 5.36568 17.5965 4.88844C17.1192 4.41121 16.3455 4.41121 15.8683 4.88844Z"
                        fill="currentColor"
                        fill-opacity="0.38"/>
            </svg>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">


        <?php

        if ($usertype == 1) {
            ?>
            <li class="menu-item <?php echo active_link('administrator/dashboard'); ?>">
                <a href="administrator/dashboard" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-view-dashboard"></i>
                    <div data-i18n="Dashboard">Dashboard</div>
                </a>
            </li>

            <!-- settings menu start -->
            <li class="menu-item <?php echo active_link('administrator/projecttype'); ?> <?php echo active_open('administrator/projecttype'); ?> <?php echo active_link('administrator/contractorstype'); ?> <?php echo active_open('administrator/contractorstype'); ?> <?php echo active_link('administrator/paymentmethod'); ?> <?php echo active_open('administrator/paymentmethod'); ?> <?php echo active_link('administrator/projectsection'); ?> <?php echo active_open('administrator/projectsection'); ?>">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons mdi mdi-cog-outline"></i>
                    <div data-i18n="Settings">Settings</div>
                </a>
                <ul class="menu-sub">
                    <li class="menu-item <?php echo active_link('administrator/projecttype'); ?>">
                        <a href="administrator/projecttype" class="menu-link">
                            <div data-i18n="Project Type">Project Type</div>
                        </a>
                    </li>
                    <li class="menu-item <?php echo active_link('administrator/contractorstype'); ?>">
                        <a href="administrator/contractorstype" class="menu-link">
                            <div data-i18n="Contractors Type">Contractors Type</div>
                        </a>
                    </li>
                    <li class="menu-item <?php echo active_link('administrator/paymentmethod'); ?>">
                        <a href="administrator/paymentmethod" class="menu-link">
                            <div data-i18n="Payment method">Payment method</div>
                        </a>
                    </li>
                    <li class="menu-item <?php echo active_link('administrator/projectsection'); ?>">
                        <a href="administrator/projectsection" class="menu-link">
                            <div data-i18n="Project Section">Project Section</div>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- user access menu start -->
            <li class="menu-item <?php echo active_link('administrator/userslist'); ?> <?php echo active_open('administrator/userslist'); ?> <?php echo active_link('administrator/usersgroup'); ?> <?php echo active_open('administrator/usersgroup'); ?> <?php echo active_link('administrator/settings'); ?> <?php echo active_open('administrator/settings'); ?>">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons mdi mdi-account-settings"></i>
                    <div data-i18n="User Access">User Access</div>
                </a>
                <ul class="menu-sub">
                    <li class="menu-item <?php echo active_link('administrator/userslist'); ?>">
                        <a href="administrator/userslist" class="menu-link">
                            <div data-i18n="Users">Users</div>
                        </a>
                    </li>
                    <li class="menu-item <?php echo active_link('administrator/usersgroup'); ?>">
                        <a href="administrator/usersgroup" class="menu-link">
                            <div data-i18n="Groups">Groups</div>
                        </a>
                    </li>
                    <li class="menu-item <?php echo active_link('administrator/settings'); ?>">
                        <a href="administrator/settings" class="menu-link">
                            <div data-i18n="Permissions">Permissions</div>
                        </a>
                    </li>
                </ul>
            </li>
        <?php } else {
            $where = array('usertype' => $usertype, 'menutab' => 'Dashboard', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/dashboard'); ?>">
                    <a href="administrator/dashboard" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-view-dashboard"></i>
                        <div data-i18n="Dashboard">Dashboard</div>
                    </a>
                </li>
            <?php } ?>

            <?php
            $where = array('usertype' => $usertype, 'menutab' => 'Settings', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <!-- settings menu start -->
                <li class="menu-item <?php echo active_link('administrator/projecttype'); ?> <?php echo active_open('administrator/projecttype'); ?> <?php echo active_link('administrator/contractorstype'); ?> <?php echo active_open('administrator/contractorstype'); ?> <?php echo active_link('administrator/paymentmethod'); ?> <?php echo active_open('administrator/paymentmethod'); ?> <?php echo active_link('administrator/projectsection'); ?> <?php echo active_open('administrator/projectsection'); ?>">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons mdi mdi-cog-outline"></i>
                        <div data-i18n="Settings">Settings</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item <?php echo active_link('administrator/projecttype'); ?>">
                            <a href="administrator/projecttype" class="menu-link">
                                <div data-i18n="Project Type">Project Type</div>
                            </a>
                        </li>
                        <li class="menu-item <?php echo active_link('administrator/contractorstype'); ?>">
                            <a href="administrator/contractorstype" class="menu-link">
                                <div data-i18n="Contractors Type">Contractors Type</div>
                            </a>
                        </li>
                        <li class="menu-item <?php echo active_link('administrator/paymentmethod'); ?>">
                            <a href="administrator/paymentmethod" class="menu-link">
                                <div data-i18n="Payment method">Payment method</div>
                            </a>
                        </li>
                        <li class="menu-item <?php echo active_link('administrator/projectsection'); ?>">
                            <a href="administrator/projectsection" class="menu-link">
                                <div data-i18n="Project Section">Project Section</div>
                            </a>
                        </li>
                    </ul>
                </li>

            <?php }
            $where = array('usertype' => $usertype, 'menutab' => 'Users', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <!-- user access menu start -->
                <li class="menu-item <?php echo active_link('administrator/userslist'); ?> <?php echo active_open('administrator/userslist'); ?> <?php echo active_link('administrator/usersgroup'); ?> <?php echo active_open('administrator/usersgroup'); ?> <?php echo active_link('administrator/settings'); ?> <?php echo active_open('administrator/settings'); ?>">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons mdi mdi-account-settings"></i>
                        <div data-i18n="User Access">User Access</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item <?php echo active_link('administrator/userslist'); ?>">
                            <a href="administrator/userslist" class="menu-link">
                                <div data-i18n="Users">Users</div>
                            </a>
                        </li>
                        <li class="menu-item <?php echo active_link('administrator/usersgroup'); ?>">
                            <a href="administrator/usersgroup" class="menu-link">
                                <div data-i18n="Groups">Groups</div>
                            </a>
                        </li>
                        <li class="menu-item <?php echo active_link('administrator/settings'); ?>">
                            <a href="administrator/settings" class="menu-link">
                                <div data-i18n="Permissions">Permissions</div>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php }
        } ?>
        <?php if ($usertype == 1) { ?>
            <li class="menu-item <?php echo active_link('administrator/company_list'); ?>">
                <a href="administrator/company_list" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-domain"></i>
                    <div data-i18n="Companies">Companies</div>
                </a>
            </li>
            <li class="menu-item <?php echo active_link('administrator/projectlist'); ?>">
                <a href="administrator/projectlist" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-arrow-projectile-multiple"></i>
                    <div data-i18n="Projects">Projects</div>
                </a>
            </li>
            <li class="menu-item <?php echo active_link('administrator/sellproperties'); ?>">
                <a href="administrator/sellproperties" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-bag-personal-tag-outline"></i>
                    <div data-i18n="Sell Properties">Sell Properties</div>
                </a>
            </li>
        <?php } else { ?>

            <?php
            $where = array('usertype' => $usertype, 'menutab' => 'Compnies', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/company_list'); ?>">
                    <a href="administrator/company_list" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-domain"></i>
                        <div data-i18n="Companies">Companies</div>
                    </a>
                </li>
            <?php } ?>
            <?php
            $where = array('usertype' => $usertype, 'menutab' => 'Projects', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/projectlist'); ?>">
                    <a href="administrator/projectlist" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-arrow-projectile-multiple"></i>
                        <div data-i18n="Projects">Projects</div>
                    </a>
                </li>
            <?php } ?>
            <?php
            $where = array('usertype' => $usertype, 'menutab' => 'Sell Properties', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/sellproperties'); ?>">
                    <a href="administrator/sellproperties" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-bag-personal-tag-outline"></i>
                        <div data-i18n="Sell Properties">Sell Properties</div>
                    </a>
                </li>
            <?php } ?>
        <?php } ?>
        <?php if ($this->session->userdata('admin_usertype') == 1) { ?>
            <li class="menu-item <?php echo active_link('administrator/purchaseorders'); ?>">
                <a href="administrator/purchaseorders" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-cart-outline"></i>
                    <div data-i18n="Purchase Orders">Purchase Orders</div>
                </a>
            </li>
        <?php } else { ?>

            <?php
            $where = array('usertype' => $usertype, 'menutab' => 'Purchase orders');
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);

            if ($chkvalied->settingsid != '' && $chkvalied->allowfor_verify == 1) { ?>
                <li class="menu-item <?php echo active_link('administrator/purchaseorders'); ?>">
                    <a href="administrator/purchaseorders" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-cart-outline"></i>
                        <div data-i18n="Purchase Orders">Purchase Orders</div>
                    </a>
                </li>
            <?php } elseif ($chkvalied->settingsid != '' && $chkvalied->allowfor_edit == 1) { ?>
                <li class="menu-item <?php echo active_link('administrator/myorders'); ?>">
                    <a href="administrator/myorders" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-cart-outline"></i>
                        <div data-i18n="My Purchase Orders">My Purchase Orders</div>
                    </a>
                </li>
            <?php } ?>
            <?php
            $where = array('usertype' => $usertype, 'menutab' => 'Purchase orders', 'allowfor_add' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/addpurchaseorder'); ?>">
                    <a href="administrator/addpurchaseorder" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-cart-plus"></i>
                        <div data-i18n="Add Purchase Order">Add Purchase Order</div>
                    </a>
                </li>
            <?php } ?>


        <?php } ?>
        <?php if ($usertype == 1) { ?>
            <li class="menu-item <?php echo active_link('administrator/partners_list'); ?>">
                <a href="administrator/partners_list" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-handshake-outline"></i>
                    <div data-i18n="Partners">Partners</div>
                </a>
            </li>
            <li class="menu-item <?php echo active_link('administrator/suppliers_list'); ?>">
                <a href="administrator/suppliers_list" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-truck-delivery-outline"></i>
                    <div data-i18n="Suppliers">Suppliers</div>
                </a>
            </li>
            <li class="menu-item <?php echo active_link('administrator/contractors_list'); ?>">
                <a href="administrator/contractors_list" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-file-sign"></i>
                    <div data-i18n="Contractors">Contractors</div>
                </a>
            </li>
            <li class="menu-item <?php echo active_link('administrator/loan_list'); ?>">
                <a href="administrator/loan_list" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-cash-refund"></i>
                    <div data-i18n="Loans">Loans</div>
                </a>
            </li>
            <li class="menu-item <?php echo active_link('administrator/bankaccountlist'); ?>">
                <a href="administrator/bankaccountlist" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-bank-circle-outline"></i>
                    <div data-i18n="Bank Accounts">Bank Accounts</div>
                </a>
            </li>
            <li class="menu-item <?php echo active_link('administrator/financialinstitutionlist'); ?>">
                <a href="administrator/financialinstitutionlist" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-finance"></i>
                    <div data-i18n="Financial Institutions">Financial Institutions</div>
                </a>
            </li>

            <li class="menu-item <?php echo active_link('administrator/paymentlist'); ?> <?php echo active_link('administrator/supplierpaymentlist'); ?>">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons mdi mdi-cash-plus"></i>
                    <div data-i18n="Payments">Payments</div>
                </a>
                <ul class="menu-sub">
                    <li class="menu-item <?php echo active_link('administrator/supplierpaymentlist'); ?>">
                        <a href="administrator/supplierpaymentlist" class="menu-link">
                            <div data-i18n="Payments to Supplier">Payments to Supplier</div>
                        </a>
                    </li>
                    <li class="menu-item <?php echo active_link('administrator/paymentlist'); ?>">
                        <a href="administrator/paymentlist" class="menu-link">
                            <div data-i18n="Payments to Contractors">Payments to Contractors</div>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="menu-item <?php echo active_link('administrator/documentlist'); ?>">
                <a href="administrator/documentlist" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-file-document-edit-outline"></i>
                    <div data-i18n="Documents">Documents</div>
                </a>
            </li>
        <?php } else { ?>
            <?php
            $where = array('usertype' => $usertype, 'menutab' => 'Partners', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/partners_list'); ?>">
                    <a href="administrator/partners_list" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-handshake-outline"></i>
                        <div data-i18n="Partners">Partners</div>
                    </a>
                </li>
            <?php }
            $where = array('usertype' => $usertype, 'menutab' => 'Suppliers', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/suppliers_list'); ?>">
                    <a href="administrator/suppliers_list" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-truck-delivery-outline"></i>
                        <div data-i18n="Suppliers">Suppliers</div>
                    </a>
                </li>
            <?php }
            $where = array('usertype' => $usertype, 'menutab' => 'Contractors', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/contractors_list'); ?>">
                    <a href="administrator/contractors_list" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-file-sign"></i>
                        <div data-i18n="Contractors">Contractors</div>
                    </a>
                </li>
            <?php }
            $where = array('usertype' => $usertype, 'menutab' => 'Loans', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/loan_list'); ?>">
                    <a href="administrator/loan_list" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-cash-refund"></i>
                        <div data-i18n="Loans">Loans</div>
                    </a>
                </li>
            <?php }
            $where = array('usertype' => $usertype, 'menutab' => 'Bank Accounts', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/bankaccountlist'); ?>">
                    <a href="administrator/bankaccountlist" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-bank-circle-outline"></i>
                        <div data-i18n="Bank Accounts">Bank Accounts</div>
                    </a>
                </li>
            <?php }
            $where = array('usertype' => $usertype, 'menutab' => 'Financial Institute', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/financialinstitutionlist'); ?>">
                    <a href="administrator/financialinstitutionlist" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-finance"></i>
                        <div data-i18n="Financial Institutions">Financial Institutions</div>
                    </a>
                </li>
            <?php }
            $where = array('usertype' => $usertype, 'menutab' => 'Payments', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>


                <li class="menu-item <?php echo active_link('administrator/paymentlist'); ?> <?php echo active_link('administrator/supplierpaymentlist'); ?>">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons mdi mdi-cash-plus"></i>
                        <div data-i18n="Payments">Payments</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item <?php echo active_link('administrator/supplierpaymentlist'); ?>">
                            <a href="administrator/supplierpaymentlist" class="menu-link">
                                <div data-i18n="Payments to Supplier">Payments to Supplier</div>
                            </a>
                        </li>
                        <li class="menu-item <?php echo active_link('administrator/paymentlist'); ?>">
                            <a href="administrator/paymentlist" class="menu-link">
                                <div data-i18n="Payments to Contractors">Payments to Contractors</div>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php }
            $where = array('usertype' => $usertype, 'menutab' => 'Documents', 'allowfor_edit' => 1);
            $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
            if ($chkvalied->settingsid != '') { ?>
                <li class="menu-item <?php echo active_link('administrator/documentlist'); ?>">
                    <a href="administrator/documentlist" class="menu-link">
                        <i class="menu-icon tf-icons mdi mdi-file-document-edit-outline"></i>
                        <div data-i18n="Documents">Documents</div>
                    </a>
                </li>
            <?php }
            ?>
        <?php } ?>
    </ul>
</aside>
<!-- / Menu -->
