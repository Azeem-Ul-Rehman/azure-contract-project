<?php include('partials/header.php'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
<?php include('partials/settings.php'); ?>
<?php include('partials/sidebar.php'); ?>      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
			<div class="row">				<div class="col-10">					<h4 class="card-title">All Users</h4>				</div>								
			</div>
				<div class="row">
                <div class="col-12">
				<?php if($this->session->flashdata('success')){ ?>
				 <div class="alert alert-success alert-dismissible">					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					  <strong>Success!</strong> <?php echo $this->session->flashdata('success');  ?>				  </div>				<?php } ?>
				<table id="order-listing" class="table">                    <thead>                      <tr>                          <th>Sno</th>						  <th>Name</th>                          <th>Mobile</th>						  <th>Ballence</th>                          <th>Actions</th>                      </tr>                    </thead>                    <tbody>					<?php					  if(!empty($allusers))					  {
						  $i=1;	
						  foreach ($allusers as $perreq){
					?>
                      <tr>
                          <td><?php echo $i;	 ?></td>
                          <td><?php echo $perreq->fullname;	 ?></td>
						  <td><?php echo $perreq->phoneno;	 ?></td>
						  <td><?php echo $perreq->my_ballence;	 ?></td>
					 
                          <td>							 

                            <a href="administrator/edituser?user_id=<?php echo $perreq->user_id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							<button class="btn btn-outline-danger" onclick="show_confirmdelte(<?php echo $perreq->user_id; ?>)"><i class="fa fa-trash-o"></i></button>
							
                          </td>
                      </tr>
                  <?php 
						$i++;
					} 
				}
				  ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
<?php include('partials/footer.php'); ?>    
<script src="js/data-table.js"></script>   
<script src="js/alerts.js"></script>
<script>
	function show_confirmdelte(userid)
	{
		swal({
		  title: "Are you sure?",
		  text: "You won't to delete this user!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
			location.href="<?php echo base_url();?>administrator/deleteuser?user_id="+userid;
		  } else {
			//swal("Your imaginary file is safe!");
		  }
		});
	}			
</script>
