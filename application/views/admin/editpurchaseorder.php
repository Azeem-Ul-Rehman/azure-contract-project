<?php include('partials/header.php'); ?>
<!-- partial -->
<!-- Content wrapper -->
<!-- Content wrapper -->
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Purchase Orders
        </h4>
        <form id="my_form" name="my_form" method="post" action="" enctype="multipart/form-data">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <?php
                    $usertype = $this->session->userdata('admin_usertype');
                    $where = array('usertype' => $usertype, 'menutab' => 'Purchase orders');
                    $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
                    ?>
                    <div class="card">
                        <div
                                class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Edit Order</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary" value="Save" name="savebtn">Submit</button>
                                <?php if ($chkvalied->allowfor_verify == 1 || $this->session->userdata('admin_usertype') == 1) {
                                    if ($orderval->is_verified == 0) {
                                        ?>
                                        <button class="btn btn-outline-danger" type="button"
                                                onclick="show_confirmverify(<?php echo $orderval->orderid; ?>)">
                                            Verify
                                        </button>
                                    <?php }
                                } ?>

                                <?php if ($chkvalied->allowfor_complete == 1 || $this->session->userdata('admin_usertype') == 1) { ?>
                                    <?php if ($orderval->is_verified == 1) { ?>
                                        <button class="btn btn-primary" value="Complete" name="completebtn"
                                                type="submit">Complete
                                        </button>
                                    <?php } ?>
                                <?php } ?>

                                <?php if ($chkvalied->allowfor_pay == 1 || $this->session->userdata('admin_usertype') == 1) { ?>
                                    <?php if ($orderval->is_verified == 2 && $orderval->ispaid == 0) { ?>
                                        <button class="btn btn-primary" value="Pay" id="paybtn" name="paybtn"
                                                type="button">Pay
                                        </button>
                                        <input type="hidden" id="hdnpay" value="" name="paybtn"/>

                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-success alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row g-4 mb-4">
                                <!-- First column-->
                                <div class="col-md-4">
                                    <!-- Product Information -->
                                    <input id="is_verified" name="is_verified" type="hidden"
                                           value="<?php echo $orderval->is_verified; ?>"/>
                                    <input id="ispaid" name="ispaid" type="hidden"
                                           value="<?php echo $orderval->ispaid; ?>"/>
                                    <input id="hdnorderid" class="form-control" name="hdnorderid" type="hidden"
                                           value="<?php echo $orderval->orderid; ?>">
                                    <div class="form-floating form-floating-outline">
                                        <select
                                            <?php if ($orderval->is_verified != 0) { ?> disabled="true" <?php } ?>
                                                id="suppliers" name="suppliers" class="form-select required"
                                                data-allow-clear="true">
                                            <option value="">Select Suppliers</option>
                                            <?php
                                            if (!empty($supplierslist)) {
                                                foreach ($supplierslist as $rowssuppliers) {
                                                    ?>
                                                    <option <?php if ($rowssuppliers->suppliers_id == $orderval->suppliers) { ?> selected="selected" <?php } ?>
                                                            value="<?php echo $rowssuppliers->suppliers_id; ?>"><?php echo $rowssuppliers->suppliers_name; ?></option>

                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <label for="suppliers">Suppliers</label>
                                    </div>


                                </div>
                                <div class="col-md-4">

                                    <div class="form-floating form-floating-outline">
                                        <select
                                            <?php if ($orderval->is_verified != 0) { ?> disabled="true" <?php } ?>
                                                id="projects" name="projects" class="form-select required"
                                                data-allow-clear="true">
                                            <option value="">Select project</option>
                                            <?php
                                            if (!empty($projectlist)) {
                                                foreach ($projectlist as $rowsprojects) {
                                                    ?>
                                                    <option <?php if ($rowsprojects->projectid == $orderval->projectid) { ?> selected="selected" <?php } ?>
                                                            value="<?php echo $rowsprojects->projectid; ?>"><?php echo $rowsprojects->project_name; ?></option>

                                                    <?php
                                                }
                                            }

                                            ?>
                                        </select>
                                        <label for="projects">Projects</label>
                                    </div>


                                </div>
                                <div class="col-md-4">


                                    <div class="form-floating form-floating-outline">
                                        <select
                                            <?php if ($orderval->is_verified != 0) { ?> disabled="true" <?php } ?>
                                                id="contractors" name="contractors" class="form-select required"
                                                data-allow-clear="true">
                                            <option value="">Select Contractor</option>
                                            <?php
                                            if (!empty($contractorslist)) {
                                                foreach ($contractorslist as $rowscontractors) {
                                                    ?>
                                                    <option <?php if ($rowscontractors->contractor_id == $orderval->contractorsid) { ?> selected="selected" <?php } ?>
                                                            value="<?php echo $rowscontractors->contractor_id; ?>"><?php echo $rowscontractors->contractor_name; ?></option>

                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <label for="contractors">Contractors</label>
                                    </div>


                                </div>
                                <!-- /Second column -->
                            </div>
                            <?php if ($orderval->is_verified == 0) { ?>
                                <div class="row g-4 mb-4">
                                    <input type="hidden" name="paymentmethod" id="paymentmethod" value="other">
                                    <label for="projectsection">Project Section</label>
                                    <?php
                                    $projectSections = ['Foundation', 'Doors', 'Roof', 'Windows'];
                                    foreach ($projectSections as $projectSection) {

                                        $selectedvalue = '';
                                        $projectSectionssarr = explode(',', $orderval->projectsection);
                                        if (in_array($projectSection, $projectSectionssarr)) {
                                            $selectedvalue = "checked='checked'";
                                        }

                                        ?>
                                        <div class="col-3 col-lg-3">
                                            <div class="form-group1" style="margin-left:20px;">
                                                <input class="form-check-input" <?php echo $selectedvalue; ?>
                                                       name="projectsection[]" type="checkbox"
                                                       value="<?php echo $projectSection; ?>"
                                                       id="projectsection<?php echo $$projectSection; ?>">
                                                <label class="form-check-label"
                                                       for="projectsection<?php echo $projectSection; ?>"><?php echo $projectSection; ?></label>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="row g-4 mb-4">
                                    <div class="col-md-6">
                                        <div class="form-floating form-floating-outline">
                                            <input id="perchaseorderimage" class="form-control"
                                                   name="perchaseorderimage"
                                                   onchange="return ValidateFileUpload()" type="file">
                                            <label for="perchaseorderimage">Purchase Order</label>

                                            <div class="mt-4">
                                                <input id="hdnperchaseorderimage" class="form-control"
                                                       name="hdnperchaseorderimage" type="hidden"
                                                       value="<?php echo $orderval->perchaseorderimage; ?>">
                                                <?php if ($orderval->perchaseorderimage != '') { ?>

                                                    <?php
                                                    $extension = explode('.', $orderval->perchaseorderimage);
                                                    if ($extension[1] === 'docx' || $extension[1] === 'pdf' || $extension[1] === 'xlsx' || $extension[1] === 'doc' || $extension[1] === 'xls') {
                                                        $file = true;
                                                    } else {
                                                        $file = false;
                                                    }
                                                    if ($file) {
                                                        ?>
                                                        <a href="uploads/invoices/<?php echo $orderval->perchaseorderimage; ?>"
                                                           download>
                                                            <img src="/uploads/file_image.png"
                                                                 style="width: 100px;height: 100px;margin-bottom: 10px;cursor: pointer">
                                                        </a>
                                                    <?php } else { ?>
                                                        <img id="image-open"
                                                             src="uploads/invoices/<?php echo $orderval->perchaseorderimage; ?>"
                                                             width="100"/><br/>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="row g-4 mb-4">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="perchaseorderimage">Purchase Order</label>

                                            <?php if ($orderval->perchaseorderimage != '') { ?>

                                                <div class="mt-4">

                                                    <?php
                                                    $extension = explode('.', $orderval->perchaseorderimage);
                                                    if ($extension[1] === 'docx' || $extension[1] === 'pdf' || $extension[1] === 'xlsx' || $extension[1] === 'doc' || $extension[1] === 'xls') {
                                                        $file = true;
                                                    } else {
                                                        $file = false;

                                                    }
                                                    if ($file) {
                                                        ?>
                                                        <a href="uploads/invoices/<?php echo $orderval->perchaseorderimage; ?>"
                                                           download>
                                                            <img src="/uploads/file_image.png"
                                                                 style="width: 100px;height: 100px;margin-bottom: 10px;cursor: pointer">
                                                        </a>
                                                    <?php } else { ?>

                                                        <?php if ($orderval->perchaseorderimage != '') { ?>
                                                            <a href="javascript:void(0)"
                                                               id="image-open"
                                                               data-title="Purchase Order Image"
                                                               data-src="uploads/invoices/<?php echo $orderval->perchaseorderimage; ?>"><img
                                                                        style="width: 30px; height: 30px;border-radius: 100%;"
                                                                        src="uploads/invoices/<?php echo $orderval->perchaseorderimage; ?>"
                                                                        width="100"/></a>
                                                        <?php }  ?>
                                                        <br/>
                                                    <?php } ?>


                                                    <br/>
                                                </div>
                                            <?php } ?>
                                            <input id="hdnperchaseorderimage" class="form-control"
                                                   name="hdnperchaseorderimage" type="hidden"
                                                   value="<?php echo $orderval->perchaseorderimage; ?>">
                                        </div>


                                    </div>
                                </div>
                            <?php } ?>



                            <?php if ($orderval->is_verified != 0) { ?>
                                <div class="row g-4 mb-4">
                                    <div class="col-md-4">

                                        <div class="form-floating form-floating-outline">
                                            <input type="number"
                                                <?php if ($orderval->is_verified == 2) { ?> readonly="readonly" <?php } ?>
                                                   id="invoicenr" class="form-control required"
                                                   value="<?php echo $orderval->invoicenr; ?>" name="invoicenr"
                                                   placeholder="Invoice NR"/>
                                            <label for="invoicenr">Invoice NR</label>
                                        </div>


                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-floating form-floating-outline">
                                            <input type="number"
                                                   id="invoiceamount"
                                                   class="form-control required"
                                                   value="<?php echo $orderval->invoiceamount; ?>"
                                                   name="invoiceamount"
                                                   placeholder="Invoice Amount"/>
                                            <label for="invoiceamount">Invoice Amount</label>
                                        </div>


                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-floating form-floating-outline">
                                            <input <?php if ($orderval->is_verified == 2) { ?> readonly="readonly" <?php } ?>
                                                    id="invoicedate" class="form-control required"
                                                    aria-invalid="false"
                                                    value="<?php if ($orderval->invoicedate != '1970-01-01 00:00:00') {
                                                        echo date('Y-m-d', strtotime($orderval->invoicedate));
                                                    } ?>" name="invoicedate" type="date">
                                            <label for="invoicedate">Invoice date</label>
                                        </div>


                                    </div>
                                </div>
                                <div class="row g-4 mb-4">
                                    <div class="col-md-6">
                                        <div class="form-floating form-floating-outline">
                                            <input id="invoiceimg" onchange="return ValidateInvoiceFileUpload()"
                                                   class="form-control" name="invoiceimg" type="file">
                                            <label for="invoiceimg">Invoice Images</label>
                                            <input id="hdninvoiceimg" class="form-control" name="hdninvoiceimg"
                                                   type="hidden" value="<?php echo $orderval->invoiceimage; ?>">

                                            <?php if ($orderval->invoiceimage !== '' && !is_null($orderval->invoiceimage)) { ?>
                                                <div class="mt-4">
                                                    <a id="image-open"
                                                       href="javascript:void(0)"
                                                       data-title="Invoice Image"
                                                       data-src="uploads/invoices/<?php echo $orderval->invoiceimage; ?>"><img
                                                                src="uploads/invoices/<?php echo $orderval->invoiceimage; ?>"
                                                                width="100"/>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>


                                    </div>

                                </div>
                                <?php if ($orderval->is_verified !== '2' ) { ?>
                                    <div class="row g-4 mb-4">
                                        <div class="col-md-4">

                                            <div class="form-floating form-floating-outline">
                                                <input id="paymentdate" class="form-control"
                                                       aria-invalid="false"
                                                       value="<?php if ($orderval->paymentdate != '1970-01-01 00:00:00') {
                                                           echo date('Y-m-d', strtotime($orderval->paymentdate));
                                                       } ?>" name="paymentdate" type="date">
                                                <label for="paymentdate">Payment date</label>
                                            </div>


                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-floating form-floating-outline">
                                                <input id="is_paid"
                                                    <?php if($orderval->ispaid === '1' || $orderval->ispaid === 1) { echo 'checked'; } ?>
                                                       value="<?php if($orderval->ispaid === '1' || $orderval->ispaid === 1) { echo '1'; }else{ echo '0';}  ?>" name="is_paid" type="checkbox"> &nbsp;Invoice is Paid
                                            </div>


                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($orderval->is_verified == 2) { ?>
                                <div class="row g-4 mb-4">
                                    <div class="col-md-4">

                                        <div class="form-floating form-floating-outline">
                                            <input id="paymentdate" class="form-control valid"
                                                   aria-invalid="false"
                                                   value="<?php if ($orderval->paymentdate != '1970-01-01 00:00:00') {
                                                       echo date('Y-m-d', strtotime($orderval->paymentdate));
                                                   } ?>" name="paymentdate" type="date">
                                            <label for="paymentdate">Payment date</label>
                                        </div>


                                    </div>
                                </div>
                            <?php } ?>

                            <div class="row mt-4 g-2">

                                <h5 class="card-title mb-4">Purchase Order Items</h5>
                                <?php foreach ($ordersitems as $rowsitem) { ?>
                                    <div id="inputFormRow">
                                        <div class="form-group mb-3" style="display: inline-flex;width: 100%">

                                            <input style="width:40%"
                                                   type="text" <?php if ($orderval->is_verified != 0) { ?> readonly="readonly" <?php } ?>
                                                   value="<?php echo $rowsitem->itemname; ?>"
                                                   placeholder="Item Name" name="itemname[]"
                                                   id="itemname" class="form-control"/>&nbsp;&nbsp;

                                            <input style="width:40%"
                                                   type="number" <?php if ($orderval->is_verified != 0) { ?> readonly="readonly" <?php } ?>
                                                   value="<?php echo $rowsitem->qty; ?>"
                                                   placeholder="Item Quantity" name="qty[]" id="qty"
                                                   class="form-control"/>&nbsp;&nbsp;

                                            <div class="input-group-append">
                                                <button id="removeRow" type="button"
                                                        class="btn btn-danger">Remove
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                <?php } ?>
                                <div id="newRowpartner"></div>

                            </div>
                            <div class="row g-4 mb-4">
                                <div class="col-md-2">
                                    <?php if ($orderval->is_verified == 0) { ?>
                                        <button id="addRow" type="button" class="btn btn-info">Add New Item</button>
                                    <?php } ?>
                                </div>
                            </div>


                            <!-- /Variants -->
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->

<div class="modal fade" id="image-modal" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg modal-simple modal-enable-otp modal-dialog-centered">
        <div class="modal-content p-3 p-md-5">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center mb-4">
                    <h3 class="mb-2" id="title">Purchase order image</h3>
                </div>
                <div class="text-center">
                    <img src="" alt="img" class="img-fluid image-src" style="width: 50%">
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('partials/footer.php'); ?>

<script>
    $(document).on('click', '#paybtn', function () {
        if ($('#paymentdate').val() !== '') {

            $('#hdnpay').val('Pay');
            $('#my_form').submit();

        } else {

            Swal.fire({
                title: "",
                text: "Please select payment date first!",
                icon: "error",
                showCancelButton: 0,
                confirmButtonText: "OK",
                customClass: {
                    confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                }
            })
        }
    });

    function show_confirmverify(orderid) {
        Swal.fire({
            title: "Are you sure?",
            text: "You want to verify this order!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            customClass: {
                confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                cancelButton: "btn btn-outline-secondary waves-effect"
            },
            buttonsStyling: !1
        }).then(function (e) {
            if (e.value) {
                location.href = "<?php echo base_url();?>administrator/verifyorders?orderid=" + orderid;
            } else {
                if (e.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: "Cancelled",
                        text: "Cancelled Suspension :)",
                        icon: "error",
                        customClass: {confirmButton: "btn btn-success waves-effect"}
                    })
                }
            }
        });
    }

    function ValidateInvoiceFileUpload() {
        var fuData1 = document.getElementById('invoiceimg');
        var FileUploadPath1 = fuData1.value;

        if (FileUploadPath1 === '') {
            Swal.fire({
                title: "",
                text: "Please upload an image",
                icon: "error",
                showCancelButton: 0,
                confirmButtonText: "OK",
                customClass: {
                    confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                }
            })
        } else {
            var Extension1 = FileUploadPath1.substring(FileUploadPath1.lastIndexOf('.') + 1).toLowerCase();
            if (Extension1 === "gif" || Extension1 === "png" || Extension1 === "bmp" || Extension1 === "jpeg" || Extension1 === "jpg") {
                if (fuData1.files && fuData1.files[0]) {
                }
            } else {
                Swal.fire({
                    title: "",
                    text: "Photo only allows file types of GIF, PNG, JPG, JPEG and BMP.",
                    icon: "error",
                    showCancelButton: 0,
                    confirmButtonText: "OK",
                    customClass: {
                        confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                    }
                })
            }
        }
    }

    function ValidateFileUpload() {
        var fuData = document.getElementById('perchaseorderimage');
        var FileUploadPath = fuData.value;
        if (FileUploadPath === '') {
            Swal.fire({
                title: "",
                text: "Please upload an image",
                icon: "error",
                showCancelButton: 0,
                confirmButtonText: "OK",
                customClass: {
                    confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                }
            })
        } else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
            if (Extension === "gif" || Extension === "png" || Extension === "bmp" || Extension === "jpeg" || Extension === "jpg" || Extension === "pdf" || Extension === "doc" || Extension === "docx" || Extension === "xls") {
                if (fuData.files && fuData.files[0]) {

                }
            } else {
                Swal.fire({
                    title: "",
                    text: "Photo only allows file types of GIF, PNG, JPG, JPEG, BMP, PDF, DOC, DOCX, XLS.",
                    icon: "error",
                    showCancelButton: 0,
                    confirmButtonText: "OK",
                    customClass: {
                        confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                    }
                })
            }
        }
    }


</script>
<script>
    $(document).ready(function () {

        $("#addRow").click(function () {
            var html = '';
            html += '<div id="inputFormRow">';
            html += '<div class="form-group mb-3" style="display: inline-flex;width: 100%">';
            html += '<input style="width:40%" type="text" placeholder="Item Name" name="itemname[]" id="itemname" class="form-control" />&nbsp;&nbsp;';
            html += '<input style="width:40%" type="number" placeholder="Item Quantity" name="qty[]" id="qty" class="form-control" />&nbsp;&nbsp;';
            html += '<div class="input-group-append">';
            html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
            html += '</div>';
            html += '</div>';

            $('#newRowpartner').append(html);
        });


        $(document).on('click', '#removeRow', function () {
            $(this).closest('#inputFormRow').remove();
        });
    });


</script>
<script>
    $(document).on('click', '#image-open', function () {
        var title = $(this).data('title');
        var src = $(this).data('src');
        $('.image-src').attr('src', src);
        $('#title').text(title);
        $('#image-modal').modal('show');
    })
    $(document).on('change','#is_paid',function (){
        if($(this).is(':checked') === true){
            $(this).val(1);
        }else{
            $(this).val(0)
        }
    })
</script>

