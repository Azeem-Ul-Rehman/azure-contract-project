<?php include('partials/header.php'); ?>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Edit Bank Account
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Add New Bank</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
						<input type="hidden" name="hdnbankid" id="hdnbankid" value="<?php echo $banks->companybankid; ?>" />
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
										<select name="companyname" id="companyname" class="form-control required">
											<option value="">Select Company</option>
											<?php foreach($companylist as $rowscompany){ ?> <option <?php if($banks->companyid==$rowscompany->company_id){ ?> selected="selected" <?php } ?> value="<?php echo $rowscompany->company_id; ?>">
											<?php echo $rowscompany->company_name; ?></option> <?php } ?>
										</select>
                                        <label for="companyname">Company Name</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
										<input type="text" class="form-control required" id="bankname" placeholder="Bank Name" name="bankname" value="<?php echo $banks->bankname; ?>" aria-label="Bank Name"/>
                                        <label for="bankname">Bank Name</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
										<input type="text" class="form-control required" id="accounttype" placeholder="Account Type" name="accounttype" value="<?php echo $banks->accounttype; ?>" aria-label="Account Type"/>
                                        <label for="accounttype">Account Type</label>
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="accountname" placeholder="Account Name" name="accountname" value="<?php echo $banks->accountname; ?>" aria-label="Account Name"/>
                                        <label for="accountname">Account Name</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="number" class="form-control required" id="startballence" placeholder="Start Ballence" name="startballence" value="<?php echo $banks->startballence; ?>" aria-label="Start Ballence"/>
                                        <label for="startballence">Start Ballence</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="number" class="form-control required" id="currentballence" placeholder="Current Ballence" name="currentballence" value="<?php echo $banks->currentballence; ?>" aria-label="Current Ballence"/>
                                        <label for="currentballence">Current Ballence</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

<script src="adminassets/js/formpickers.js"></script>

