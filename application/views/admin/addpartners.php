<?php include('partials/header.php'); ?>
<!-- partial -->
<!-- Content wrapper -->
<!-- Content wrapper -->
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Partners
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div
                                class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Add Partner</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row g-4 mb-4">
                                <div class="col-md-6">

                                    <div class="form-floating form-floating-outline">
                                        <input type="text" id="partner_name" name="partner_name" class="form-control required"
                                               placeholder="Name"/>
                                        <label for="partner_name">Name *</label>
                                    </div>


                                </div>
                                <div class="col-md-6">


                                    <div class="form-floating form-floating-outline">
                                        <input id="partner_email" class="form-control required" name="partner_email" type="email"
                                               placeholder="Email">
                                        <label for="partner_email">Email *</label>
                                    </div>


                                </div>
                                <!-- /Second column -->
                            </div>
                            <div class="row g-4 mb-4">
                                <div class="col-md-6">


                                    <div class="form-floating form-floating-outline">
                                        <input id="partner_phone" class="form-control required" name="partner_phone" type="text"
                                               placeholder="Phone">
                                        <label for="partner_phone">Phone *</label>
                                    </div>


                                </div>
                                <div class="col-md-6">

                                    <div class="form-floating form-floating-outline">
                                        <input type="number" minlength="5" id="partner_cribno" name="partner_cribno"
                                               class="form-control required"
                                               placeholder="Crib Nr."/>
                                        <label for="partner_cribno">Crib Nr. *</label>
                                    </div>


                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">


                                    <div class="form-floating form-floating-outline">
                                        <textarea id="partner_address" class="form-control required" name="partner_address" type="text"
                                                  placeholder="Address"></textarea>
                                        <label for="partner_address">Address *</label>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

