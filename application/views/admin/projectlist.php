<?php include('partials/header.php'); ?>
<!-- Content wrapper -->
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4"><span class="text-muted fw-light">Home /</span> All Projects List</h4>

        <!-- Product List Table -->
        <div class="card">

            <div class="card-datatable table-responsive">
                <div class="row">
                    <div class="col-12">
                        <?php if ($this->session->flashdata('success')) { ?>

                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                <table class="datatables-products table">
                    <thead class="table-light">
                    <tr>
                        <th>Project Name</th>
                        <th>Type</th>
                        <th>Location</th>
                        <th>Estimated cost</th>
                        <th>Total payment</th>
                        <th>Balance</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $usertype = $this->session->userdata('admin_usertype');
                    $where = array('usertype' => $usertype, 'menutab' => 'Projects');
                    $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);

                    if (!empty($projectlists)) {
                        $i = 1;
                        foreach ($projectlists as $perreq) {

                            $where = array('company_id' => $perreq->companyname);
                            $companylist = $this->adminmodel->getSingle(TBLCOMPANY, $where);

                            $sql = $this->db->query('select SUM(Amount) as totalpayment from ' . TBLPAYMENTS . ' where project="' . $perreq->projectid . '" AND paidto IN ("Other", "Financial Institution","Contractor") and is_deleted = 0');
                            $getrows = $sql->row();
                            $totalpayment = $getrows->totalpayment;

                            $sql = $this->db->query('select sum(invoiceamount) as supplier_payments from tbl_purchaseorders where tbl_purchaseorders.projectid = "'. $perreq->projectid.'" and ispaid =1 and is_deleted = 0');
                            $getrows = $sql->row();
                            $supplierpayments = $getrows->supplier_payments;
                            ?>
                            <tr>

                                <?php if ($chkvalied->allowfor_edit == 1 || $usertype == 1) { ?>
                                    <td>
                                        <a href="administrator/project_details/?projectid=<?php echo $perreq->projectid; ?>"><?php echo $perreq->project_name; ?></a>
                                    </td>
                                <?php } else { ?>
                                    <td><a href="javascript:void(0);"><?php echo $perreq->project_name; ?></a></td>
                                <?php } ?>

                                <td><?php echo $perreq->projecttype; ?></td>
                                <td><?php echo $perreq->location; ?></td>
                                <td>
                                    <span style="float:right;"><?php echo number_format($perreq->totalestimatedcost, 2, ".", ","); ?></span>
                                </td>
                                <td>
                                    <span style="float:right;"><?php echo number_format($totalpayment + floatval($supplierpayments), 2, ".", ","); ?></span>
                                </td>
                                <td>
                                    <span style="float:right;"><?php echo number_format($perreq->totalestimatedcost - $totalpayment - floatval($supplierpayments), 2, ".", ","); ?></span>
                                </td>
                                <td><?php echo $perreq->status; ?></td>
                                <td>

                                    <?php if ($chkvalied->allowfor_modify == 1 || $usertype == 1) { ?>
                                        <a href="administrator/editprojects?projectid=<?php echo $perreq->projectid; ?>"
                                           class="btn btn-primary"><i class="mdi mdi-pencil-outline"></i></a>
                                    <?php } else { ?>

                                    <?php } ?>

                                    <?php if ($chkvalied->allow_delete == 1 || $usertype == 1) { ?>
                                        <button class="btn btn-outline-danger"
                                                onclick="show_confirmdelte(<?php echo $perreq->projectid; ?>)"><i
                                                    class="mdi mdi-trash-can-outline"></i></button>
                                    <?php } else { ?>

                                    <?php } ?>


                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- / Content -->
    <?php include('partials/sticky-footer.php'); ?>
</div>


<style>
    .display-none {
        display: none !important;
    }
</style>
<!-- Content wrapper -->
<?php include('partials/footer.php'); ?>

<?php if ($chkvalied->allowfor_add == 1 || $usertype == 1) {

    $display = '';
} else {
    $display = 'display-none';
}
?>
<script>
    $(document).ready(function () {
        $('#startdate').on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
        $('#enddate').on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
        $('.datatables-products').DataTable({
            dom: '<"card-header d-flex border-top rounded-0 flex-wrap py-md-0"<"me-5 ms-n2"f><"d-flex justify-content-start justify-content-md-end align-items-baseline"<"dt-action-buttons d-flex align-items-start align-items-md-center justify-content-sm-center mb-3 mb-sm-0 gap-3"lB>>>t<"row mx-1"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            lengthMenu: [7, 10, 20, 50, 70, 100],
            buttons: [{
                extend: "collection",
                className: "btn btn-label-secondary dropdown-toggle me-3",
                text: '<i class="mdi mdi-export-variant me-1"></i><span class="d-none d-sm-inline-block">Export </span>',
                buttons: [{
                    extend: "print",
                    text: '<i class="mdi mdi-printer-outline me-1" ></i>Print',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6]
                    }
                }, {
                    extend: "csv",
                    text: '<i class="mdi mdi-file-document-outline me-1" ></i>Csv',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6]
                    }
                }, {
                    extend: "excel",
                    text: '<i class="mdi mdi-file-excel-outline me-1"></i>Excel',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6]
                    }
                }, {
                    extend: "pdf",
                    text: '<i class="mdi mdi-file-pdf-box me-1"></i>Pdf',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6]
                    }
                }, {
                    extend: "copy",
                    text: '<i class="mdi mdi-content-copy me-1"></i>Copy',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6]
                    }
                }]
            }, {
                text: '<i class="mdi mdi-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add Project</span>',
                className: "add-new btn btn-primary ms-n1 <?php echo $display; ?>",
                action: function () {
                    window.location.href = "administrator/addprojects"
                }
            }],
        });
    });
</script>
<script>
    function show_confirmdelte(id) {

        Swal.fire({
            title: "Are you sure?",
            text: "You want to delete this project!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            customClass: {
                confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                cancelButton: "btn btn-outline-secondary waves-effect"
            },
            buttonsStyling: !1
        }).then(function (e) {

            if (e.value) {
                location.href = "<?php echo base_url();?>administrator/deletedprojects?projectid=" + id;
            } else {
                if (e.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: "Cancelled",
                        text: "Cancelled Suspension :)",
                        icon: "error",
                        customClass: {
                            confirmButton: "btn btn-success waves-effect"
                        }
                    })
                }
            }
        })

    }
</script>
