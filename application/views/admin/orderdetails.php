<?php $usertype = $this->session->userdata('admin_usertype');
$where = array('usertype' => $usertype, 'menutab' => 'Users');
$chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);
?>
<?php include('partials/header.php'); ?>
<!-- partial -->
<!-- Content wrapper -->
<!-- Content wrapper -->

<style>
    p {
        display: inline-flex;
    }
</style>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Order Details
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2"> Order Details</h5>

                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row">
                                <!-- First column-->
                                <div class="col-md-12">
                                    <!-- Product Information -->

                                    <fieldset>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Name : </b>
                                                    </label>

                                                </div>
                                                <div class="col-2 text-left">
                                                    <p><?php echo $usersval->firstname . ' ' . $usersval->lastname; ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Supplier : </b>
                                                    </label>

                                                </div>
                                                <div class="col-2">
                                                    <p>
                                                        <a href="administrator/suppliers_list"><?php echo $suppliersval->suppliers_name; ?></a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Project : </b>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <p>
                                                        <a href="administrator/project_details/?projectid=<?php echo $projectsval->projectid; ?>"><?php echo $projectsval->project_name; ?></a>
                                                    </p>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Contractor : </b>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <p>
                                                        <a href="administrator/contractors_details/?contractor_id=<?php echo $contractorsval->contractor_id; ?>"> <?php echo $contractorsval->contractor_name; ?> </a>
                                                    </p>
                                                </div>
                                            </div>


                                        </div>


                                        <?php if ($ordersval->projectsection != '') { ?>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <label for="companyname"><b>Project
                                                                Sections: </b>
                                                        </label>

                                                    </div>
                                                    <div class="col-2">
                                                        <p><?php echo $projectsval->projectsection; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if ($ordersval->paymentmethod != '') { ?>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <label for="companyname"><b>Payment
                                                                Method: </b>
                                                        </label>
                                                        </div>
                                                    <div class="col-2">
                                                        <p><?php echo $projectsval->paymentmethod; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if ($ordersval->invoicenr != '') { ?>
                                            <div class="form-group">
                                               <div class="row">
                                                   <div class="col-2">
                                                       <label for="companyname"><b>Invoice NR
                                                               : </b> </label>

                                                   </div>
                                                   <div class="col-2">
                                                       <p><?php echo $ordersval->invoicenr; ?></p>
                                                   </div>
                                               </div>
                                            </div>
                                        <?php } ?>

                                        <?php if ($ordersval->invoiceamount != '') { ?>
                                            <div class="form-group">
                                               <div class="row">
                                                   <div class="col-2">
                                                       <label for="companyname"><b>Invoice Amount
                                                               : </b> </label>

                                                   </div>
                                                   <div class="col-2">
                                                       <p><?php echo $ordersval->invoiceamount; ?></p>
                                                   </div>
                                               </div>
                                            </div>
                                        <?php } ?>

                                        <?php if ($ordersval->invoiceimage != '') { ?>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <label for="companyname"><b>Invoice Images : </b> </label>

                                                    </div>
                                                    <div class="col-2">
                                                        <p><a
                                                                    href="uploads/invoices/<?php echo $ordersval->invoiceimage; ?>"
                                                                    download><img
                                                                        src="uploads/invoices/<?php echo $ordersval->invoiceimage; ?>"
                                                                        width="100"/> </a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if ($ordersval->invoicedate != '0000-00-00 00:00:00') { ?>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <label for="companyname"><b>Invoice Date
                                                                : </b>
                                                        </label>

                                                    </div>
                                                    <div class="col-2">
                                                        <p><?php echo date('d-m-Y', strtotime($ordersval->invoicedate)); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>


                                        <?php if ($ordersval->paymentdate != '0000-00-00 00:00:00') { ?>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <label for="companyname"><b>Payment Date
                                                                : </b>
                                                        </label>

                                                    </div>
                                                    <div class="col-2">
                                                        <p><?php echo date('d-m-Y', strtotime($ordersval->paymentdate)); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <div class="form-group">
                                           <div class="row">
                                               <div class="col-2">
                                                   <label for="verifyname"><b>Verify status : </b>

                                                   </label>

                                               </div>
                                               <div class="col-2">
                                                   <p><?php if ($ordersval->is_verified == 1) { ?>
                                                           VERIFIED
                                                       <?php } else if ($ordersval->is_verified == 2) { ?>
                                                           COMPLETED
                                                       <?php } else { ?>
                                                           <span style='color:red;'>New</span>
                                                       <?php } ?></p>
                                               </div>
                                           </div>
                                        </div>

                                        <?php
                                        if ($ordersval->is_verified != 0) {

                                            $whereusers = array('userid' => $ordersval->verifiedby);
                                            $usersval = $this->adminmodel->getSingle(ADMINUSERS, $whereusers);
                                            ?>

                                            <div class="form-group">
                                              <div class="row">
                                                  <div class="col-2">
                                                      <label for="companyname"><b>Verify by
                                                              : </b>
                                                      </label>

                                                  </div>
                                                  <div class="col-2">
                                                      <p><?php echo $usersval->firstname . ' ' . $usersval->lastname; ?></p>
                                                  </div>
                                              </div>
                                            </div>

                                            <div class="form-group">
                                               <div class="row">
                                                   <div class="col-2">
                                                       <label for="verifyname"><b>Verify date
                                                               : </b>
                                                       </label>

                                                   </div>
                                                   <div class="col-2">
                                                       <p><?php echo date('d-m-Y', strtotime($ordersval->verifieddate)); ?></p>
                                                   </div>
                                               </div>
                                            </div>

                                            <div class="form-group">
                                              <div class="row">
                                                  <div class="col-2">
                                                      <label for="verifyname"><b>Verify time
                                                              : </b>
                                                      </label>

                                                  </div>
                                                  <div class="col-2">
                                                      <p> <?php echo date('h:i a', strtotime($ordersval->verifieddate)); ?></p>
                                                  </div>
                                              </div>
                                            </div>

                                        <?php } ?>


                                        <div class="form-group">
                                           <div class="row">
                                               <div class="col-2">
                                                   <label for="companyname"><b>Order date
                                                           : </b>
                                                   </label>

                                               </div>
                                               <div class="col-2">
                                                   <p> <?php echo date('d-m-Y', strtotime($ordersval->orderdate)); ?></p>
                                               </div>
                                           </div>
                                        </div>
                                        <?php if ($ordersval->perchaseorderimage != '') { ?>

                                            <div class="form-group">
                                               <div class="row">
                                                   <div class="col-2">
                                                       <label for="companyname"><b>Purchase Order : </b> </label>

                                                   </div>
                                                   <div class="col-2">
                                                       <p>
                                                               <img id="image-open" src="uploads/invoices/<?php echo $ordersval->perchaseorderimage; ?>"
                                                                       width="100"/>
                                                       </p>
                                                   </div>
                                               </div>
                                            </div>

                                        <?php } ?>

                                    </fieldset>

                                    <br/>
                                    <h3>Item details</h3>
                                    <fieldset>
                                        <div class="table-responsive">
                                            <table id="order-listingnew" class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Sno</th>
                                                    <th>Item Name</th>
                                                    <th>Qty</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($ordersitemlist as $ordersval) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td><?php echo $ordersval->itemname; ?></td>
                                                        <td><?php echo $ordersval->qty; ?></td>
                                                    </tr>
                                                    <?php $i++;
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </fieldset>


                                </div>
                                <!-- /Second column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<div class="modal fade" id="image-modal" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg modal-simple modal-enable-otp modal-dialog-centered">
        <div class="modal-content p-3 p-md-5">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center mb-4">
                    <h3 class="mb-2">Purchase order image</h3>
                </div>
                <div class="text-center">
                    <img src="" alt="img" class="img-fluid image-src" style="width: 50%">
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('partials/footer.php'); ?>
<script>
    $(document).on('click', '#image-open', function () {
        var src = $(this).attr('src');
        $('.image-src').attr('src', src);
        $('#image-modal').modal('show');
    })
</script>
