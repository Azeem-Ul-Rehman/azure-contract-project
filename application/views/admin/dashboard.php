<?php include('partials/header.php'); ?>
<!-- Content wrapper -->
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row gy-4">
            <?php if (!empty($getprojects)) { ?>

                <?php
                foreach ($getprojects as $rowsproject) {
                    $where = array('projectid' => $rowsproject->projectid);
                    $sqlresult = $this->adminmodel->getwhere(PROJECTCONTRACTORS, $where);

                    $cotnractorsname = [];
                    foreach ($sqlresult as $rowsresult) {
                        $wherecontracors = array('contractor_id' => $rowsresult->contractorid);
                        $getcontractorsval = $this->adminmodel->getSingle(TBLCONTRACTORS, $wherecontracors);

                        if ($getcontractorsval) {
                            array_push($cotnractorsname, $getcontractorsval->contractor_name);
                        }
                    }

                    $where = array('project_id' => $rowsproject->projectid);
                    $sellprop = $this->adminmodel->getwhere(TBLSELLPROPERTIES, $where);

                    $totalsellprop = 0;
                    if (!empty($sellprop)) {

                        foreach ($sellprop as $rowssellprop) {
                            $totalsellprop = $totalsellprop + $rowssellprop->selling_price;
                        }
                    }
                    ?>

                    <!-- Project Statistics -->
                    <div class="col-md-6 col-xl-4">
                        <div class="card h-100">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                <h5 class="card-title m-0 me-2"><?php echo ucfirst($rowsproject->project_name); ?></h5>
                            </div>
                            <div class="d-flex justify-content-between py-2 px-4 border-bottom">

                            </div>
                            <div class="card-body">
                                <ul class="p-0 m-0">
                                    <li class="d-flex mb-4">

                                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                            <div class="me-2">
                                                <h6 class="mb-0">Budget</h6>

                                            </div>
                                            <div class="badge bg-label-primary rounded-pill"><?php echo number_format($rowsproject->totalestimatedcost, 2, ".", ","); ?></div>
                                        </div>
                                    </li>
                                    <li class="d-flex mb-4">

                                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                            <div class="me-2">
                                                <h6 class="mb-0">Contractors</h6>

                                            </div>
                                            <?php if (!empty($cotnractorsname) && count($cotnractorsname) > 0) { ?>
                                                <div class="badge bg-label-primary rounded-pill"
                                                     style="text-align: left">

                                                    <ul>
                                                        <?php
                                                        foreach ($cotnractorsname as $name) {
                                                            ?>
                                                            <li><?php echo $name; ?></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </li>
                                    <li class="d-flex mb-4">

                                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                            <div class="me-2">
                                                <h6 class="mb-0">Start date</h6>

                                            </div>
                                            <div class="badge bg-label-primary rounded-pill"><?php echo date('d-m-Y', strtotime($rowsproject->startdate)); ?></div>
                                        </div>
                                    </li>
                                    <li class="d-flex mb-4">

                                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                            <div class="me-2">
                                                <h6 class="mb-0">End date</h6>

                                            </div>
                                            <div class="badge bg-label-primary rounded-pill"><?php echo date('d-m-Y', strtotime($rowsproject->enddate)); ?></div>
                                        </div>
                                    </li>
                                    <li class="d-flex mb-4">

                                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                            <div class="me-2">
                                                <h6 class="mb-0">Budget used</h6>

                                            </div>
                                            <div class="badge bg-label-primary rounded-pill"><?php echo number_format($rowsproject->budget_used, 2, ".", ","); ?></div>
                                        </div>
                                    </li>
                                    <li class="d-flex mb-4">

                                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                            <div class="me-2">
                                                <h6 class="mb-0">Remain Budget</h6>
                                            </div>
                                            <div class="badge bg-label-primary rounded-pill"><?php echo number_format((float)$rowsproject->totalestimatedcost - floatval($rowsproject->budget_used), 2, ".", ","); ?></div>
                                        </div>
                                    </li>
                                    <li class="d-flex mb-4">

                                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                            <div class="me-2">
                                                <h6 class="mb-0">Payment to contractors</h6>
                                            </div>
                                            <div class="badge bg-label-primary rounded-pill">
                                                <?php if ($rowsproject->contrator_payments != '') {
                                                    echo number_format($rowsproject->contrator_payments,2, ".", ",");
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="d-flex mb-4">

                                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                            <div class="me-2">
                                                <h6 class="mb-0">Payment to suppliers</h6>
                                            </div>
                                            <div class="badge bg-label-primary rounded-pill">
                                                <?php if ($rowsproject->supplier_payments != '') {
                                                    echo number_format($rowsproject->supplier_payments,2, ".", ",");
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="d-flex mb-4">
                                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                            <div class="me-2">
                                                <h6 class="mb-0">Payment to others</h6>
                                            </div>
                                            <div class="badge bg-label-primary rounded-pill">
                                                <?php if ($rowsproject->other_payments != '') {
                                                    echo number_format($rowsproject->other_payments,2, ".", ",");
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="d-flex mb-4">
                                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                            <div class="me-2">
                                                <h6 class="mb-0">Sold Properties</h6>
                                            </div>
                                            <div class="badge bg-label-primary rounded-pill">
                                                <?php if ($totalsellprop != '') {
                                                    echo number_format($totalsellprop,2, ".", ",");
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--/ Project Statistics -->

                <?php } ?>

            <?php } ?>

        </div>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->
<?php include('partials/footer.php'); ?>
