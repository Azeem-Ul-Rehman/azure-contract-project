<?php include('partials/header.php'); ?>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Company
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Add Company</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="companyname" placeholder="Company Name" name="companyname" aria-label="Company Name"/>
                                        <label for="companyname">Company Name</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="phoneno" placeholder="Phone No" name="phoneno" aria-label="Phone No"/>
                                        <label for="phoneno">Phone No</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="email" class="form-control required" id="email" placeholder="Email" name="email" aria-label="Email"/>
                                        <label for="email">Email</label>
                                    </div>
                                </div>
                                <!-- /Second column -->
                            </div>
							<div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="date" class="form-control required" id="found_date" placeholder="Found date" name="found_date" aria-label="Found date"/>
                                        <label for="found_date">Found date</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="crib_no" placeholder="CRIB No" name="crib_no" aria-label="CRIB No"/>
                                        <label for="crib_no">CRIB No</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="kvk_no" placeholder="KVK No" name="kvk_no" aria-label="KVK No"/>
                                        <label for="kvk_no">KVK No</label>
                                    </div>
                                </div>
                                <!-- /Second column -->
                            </div>
							<div class="row g-4 mb-4">
							<label for="company_partners">Company Partners</label>
							<?php 
							$partnersoptions = '';
								if(!empty($partners)){
									foreach($partners as $rowspartners){ 
							?>
								<div class="col-md-2">
									<div class="form-group1" style="margin-left:20px;">
										<input class="form-check-input" name="company_partners[]"
											type="checkbox"
											value="<?php echo $rowspartners->partner_name;?>"
											id="partnerid<?php echo $rowspartners->partner_id;?>">
										<label class="form-check-label"
											for="inlineCheckbox1"><?php echo $rowspartners->partner_name; ?></label>
									</div>
								</div>
							<?php }}?>
                            </div>
							<div class="row">
                                <div class="col-md-12">
                                    <div class="form-floating form-floating-outline mb-4">
										<textarea id="address" class="form-control required" name="address" aria-label="Address"></textarea>
                                        <label for="found_date">Address</label>
                                    </div>
                                </div>
                                <!-- /Second column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

