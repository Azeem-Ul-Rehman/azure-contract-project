<?php include('partials/header.php'); ?>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Edit Payments
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Edit Payments</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <input type="hidden" name="paymentid" id="paymentid"
                               value="<?php echo $paymentsval->paymentid; ?>"/>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="paidto" id="paidto" class="form-control required">
                                            <option value="">Select Paid to</option>
                                            <option value="Contractor" <?php if ($paymentsval->paidto == 'Contractor') { ?> selected="selected" <?php } ?>>
                                                Contractor
                                            </option>
                                            <option value="Financial Institution" <?php if ($paymentsval->paidto == 'Financial Institution') { ?> selected="selected" <?php } ?>>
                                                Financial Institution
                                            </option>
                                            <option value="Other" <?php if ($paymentsval->paidto == 'Other') { ?> selected="selected" <?php } ?>>
                                                Other
                                            </option>
                                        </select>
                                        <label for="paidto">Paid To</label>
                                    </div>
                                </div>
                                <div class="col-md-4 displaysuppliercls"
                                     style="<?php if ($paymentsval->paidto == 'Supplier') { ?> display:block; <?php } else { ?> display:none; <?php } ?>">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select onchange="getsuppliersorders(this.value);" name="suppliersval"
                                                id="suppliersval" class="form-control required">
                                            <option value="">Select Suppliers</option>
                                            <?php foreach ($supplierslist as $supplierval) { ?>
                                                <option <?php if ($paymentsval->suppliersval == $supplierval->suppliers_id) { ?> selected="selected" <?php } ?>
                                                        value="<?php echo $supplierval->suppliers_id; ?>"><?php echo $supplierval->suppliers_name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="suppliersval">Suppliers</label>
                                    </div>
                                </div>
                                <div class="col-md-4 displaycontractorcls"
                                     style="<?php if ($paymentsval->paidto == 'Supplier') { ?> display:none; <?php } else { ?> display:block; <?php } ?>">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="contractorval" id="contractorval" class="form-control required">
                                            <option value="">Select contractor</option>
                                            <?php foreach ($contractors as $rowscontractors) { ?>
                                                <option <?php if ($paymentsval->contractorid == $rowscontractors->contractor_id) { ?> selected="selected" <?php } ?>
                                                        value="<?php echo $rowscontractors->contractor_id; ?>"><?php echo $rowscontractors->contractor_name; ?></option>
                                            <?php } ?>

                                        </select>
                                        <label for="contractorval">Contractor</label>
                                    </div>
                                </div>
                                <div class="col-md-4 displaycontractorcls" style="<?php if ($paymentsval->paidto == 'Contractor') { ?> display:block; <?php } else { ?> display:none; <?php } ?>">
                                    <div class="form-floating form-floating-outline mb-4">

                                        <select
                                                id="projectsection" name="projectsection[]"
                                                class="select2 form-select required" multiple
                                                data-allow-clear="true">
                                            <?php if(!empty($projectsections)){ ?>
                                                <?php foreach($projectsections as $rowssection){
                                                    $selectedvalue = '';
                                                    $projectsectionsarr = explode(',', $paymentsval->projectsection);
                                                    if (in_array($rowssection->projectsectionname, $projectsectionsarr)) {
                                                        $selectedvalue = "selected='selected'";
                                                    }
                                                    ?>
                                                    <option <?php echo $selectedvalue; ?> value="<?php echo $rowssection->projectsectionname; ?>"><?php echo $rowssection->projectsectionname; ?></option>
                                                <?php }}?>
                                        </select>

                                        <label for="projectsection">Project Section</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="project" id="project" class="form-control required">
                                            <option value="">Select project</option>
                                            <?php foreach ($projectlist as $rowsproject) { ?>
                                                <option <?php if ($paymentsval->project == $rowsproject->projectid) { ?> selected="selected" <?php } ?>
                                                        value="<?php echo $rowsproject->projectid; ?>"><?php echo $rowsproject->project_name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="project">Project</label>
                                    </div>
                                </div>
<!--                                <div class="col-md-4 shownotsuppliercls"-->
<!--                                     style="--><?php //if ($paymentsval->paidto == 'Supplier') { ?>/* display:none; */<?php //} else { ?>/* display:block; */<?php //} ?><!--">-->
<!--                                    <div class="form-floating form-floating-outline mb-4">-->
<!--                                        <input type="text" class="form-control required" id="purchaseorder"-->
<!--                                               placeholder="Purchase order"-->
<!--                                               value="--><?php //echo $paymentsval->purchaseorder; ?><!--" name="purchaseorder"-->
<!--                                               aria-label="Purchase order"/>-->
<!--                                        <label for="purchaseorder">Purchase order</label>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="paidbycompany" id="paidbycompany" class="form-control required">
                                            <?php foreach ($companylist as $rowscompany) {
                                                $selectedcompany = '';
                                                if ($rowscompany->company_name == $paymentsval->paidbycompany) {
                                                    $selectedcompany = 'selected="selected"';
                                                }
                                                ?>
                                                <option <?php echo $selectedcompany; ?>
                                                        value="<?php echo $rowscompany->company_name; ?>"><?php echo $rowscompany->company_name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="paidbycompany">Paid by (Company)</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="paymentmethod" id="paymentmethod" class="form-control required">
                                            <?php if (!empty($paymentmethods)) { ?>
                                                <?php foreach ($paymentmethods as $rowspaymentmethods) { ?>
                                                    <option <?php if ($paymentsval->paymentmethod == $rowspaymentmethod->paymentmethod) { ?> selected="selected" <?php } ?>
                                                            value="<?php echo $rowspaymentmethods->paymentmethod; ?>"><?php echo $rowspaymentmethods->paymentmethod; ?></option>
                                                <?php }
                                            } ?>
                                            <option <?php if ($paymentsval->paymentmethod == 'Other') { ?> selected="selected" <?php } ?>
                                                    value="Other">Other
                                            </option>
                                        </select>
                                        <label for="paymentmethod">Method</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="number" class="form-control required" id="Amount"
                                               placeholder="Amount" name="Amount"
                                               value="<?php echo $paymentsval->Amount; ?>" aria-label="Amount"/>
                                        <label for="Amount">Amount</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive displaysuppliercls" id="getsuppliersordersid"
                                         style="<?php if ($paymentsval->paidto == 'Supplier') { ?> display:block; <?php } else { ?> display:none; <?php } ?>">
                                        <?php if (!empty($purchaseorders)) { ?>
                                            <h4>Invoice(s)</h4>
                                            <table id="order-listing" class="table">
                                                &nbsp;&nbsp;<tr><input type="checkbox" class="selectallcls"
                                                                       name="selectall" value="1"/> All
                                                </tr>
                                                <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Date</th>
                                                    <th>invoice Nr</th>
                                                    <th>Project</th>
                                                    <th>Amount</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($purchaseorders as $rowsorder) {

                                                    $whereprojects = array('projectid' => $rowsorder->projectid);
                                                    $projectsval = $this->adminmodel->getSingle(TBLPROJECTS, $whereprojects);

                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <input <?php if ($rowsorder->is_addinvoice == 1) { ?> checked="checked" <?php } ?>
                                                                    type="checkbox" class="chksinglecls"
                                                                    name="invoiceno"
                                                                    id="invoiceno<?php echo $rowsorder->orderid; ?>"
                                                                    value="<?php echo $rowsorder->orderid; ?>"/></td>
                                                        <td><?php echo date('d/m/Y', strtotime($rowsorder->orderdate)); ?></td>
                                                        <td><?php echo $rowsorder->invoicenr; ?></td>
                                                        <td><?php echo $projectsval->project_name; ?></td>
                                                        <td><?php echo $rowsorder->invoiceamount; ?></td>
                                                        <td>
                                                            <button type="button" class="btn btn-outline-danger"
                                                                    onclick="show_confirmdelteforpayment(<?php echo $rowsorder->orderid; ?>)">
                                                                <i class="mdi mdi-trash-can-outline"></i></button>
                                                        </td>
                                                    </tr>

                                                <?php } ?>
                                                </tbody>
                                            </table>
                                            <br><br>
                                        <?php } ?>
                                    </div>
                                </div>


                                <!-- <div class="col-md-6">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="date" class="form-control valid" aria-invalid="false" id="paymentdate" placeholder="Payment date" value="<?php echo $paymentsval->paymentdate; ?>" name="paymentdate" aria-label="Payment date"/>
                                        <label for="paymentdate">Payment date</label>
                                    </div>
                                </div> -->
                                <div class="col-md-12">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <textarea id="paymentdescription" class="form-control required"
                                                  name="paymentdescription" aria-label="Payment Description"
                                                  placeholder="Payment Description"><?php echo $paymentsval->paymentdescription; ?></textarea>
                                        <label for="paymentdescription">Payment Description</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

<script src="adminassets/js/formpickers.js"></script>

<script>
    function getsuppliersorders(suppliers_id) {
        $.ajax({
            url: "admin/getsuppliersorders",
            type: "POST",
            data: {
                suppliers_id: suppliers_id
            },
            success: function (data) {
                $('#getsuppliersordersid').html(data);
            }
        });

    }

    function blockSpecialChar(e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));
    }
</script>
<script>
    $(document).ready(function () {
        $('.selectallcls').click(function () {
            if ($(this).prop('checked') == true) {
                $('.chksinglecls').prop('checked', true);
            } else {
                $('.chksinglecls').prop('checked', false);
            }
        });

        $('#paidinvoiceid').click(function () {
            swal({
                title: "Are you sure?",
                text: "You want to paid all invoice status of the Paid this order?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        var suppliers_id = $('#suppliersval').val();
                        location.href = "<?php echo base_url();?>administrator/paidinvoiceaddpayment?suppliers_id=" + suppliers_id;
                    } else {

                    }
                });
        });

    })

</script>
<script>
    $(document).ready(function () {
        $('#paidto').change(function () {
            /*if($(this).val()=='Supplier')
            {
                $('.shownotsuppliercls').css('display','none');
                $('.displaysuppliercls').css('display','block');
            }else{
                $('.shownotsuppliercls').css('display','block');
                $('.displaysuppliercls').css('display','none');
            }

            if($(this).val()=='Contractor')
            {
                $('.displaycontractorcls').css('display','block');
            }else{
                $('.displaycontractorcls').css('display','none');   displaysuppliercls shownotsuppliercls
            }*/

            /*if($(this).val()=='Supplier')
            {
                $('.shownotsuppliercls').css('display','none');
                $('.displaysuppliercls').css('display','block');

            }else if($(this).val()=='Contractor'){

                $('.shownotsuppliercls').css('display','none');
                $('.displaycontractorcls').css('display','block');
                $('.displaysuppliercls').css('display','none');

            }else{
                $('.shownotsuppliercls').css('display','block');
                $('.displaysuppliercls').css('display','none');
            }*/

            if ($(this).val() == 'Supplier') {
                $('.shownotsuppliercls').css('display', 'none');
                $('.displaysuppliercls').css('display', 'block');
                $('.displaycontractorcls').css('display', 'block');

            } else if ($(this).val() == 'Contractor') {

                $('.shownotsuppliercls').css('display', 'none');
                $('.displaysuppliercls').css('display', 'none');
                $('.displaycontractorcls').css('display', 'block');
                $('#getsuppliersordersid').html('');

            } else if ($(this).val() == 'Financial Institution') {

                $('.shownotsuppliercls').css('display', 'none');
                $('.displaysuppliercls').css('display', 'none');
                $('.displaycontractorcls').css('display', 'none');
                $('#getsuppliersordersid').html('');

            } else {
                $('.shownotsuppliercls').css('display', 'block');
                $('.displaysuppliercls').css('display', 'none');
                $('.displaycontractorcls').css('display', 'block');
                // $('#purchaseorder').removeClass('required');
                $('#purchaseorderlabelid').html('Purchase Order/ Invoice');
                $('#getsuppliersordersid').html('');
            }

        });
    });
</script>
</script>
<script>
    $(document).ready(function () {
        $('.selectallcls').click(function () {
            if ($(this).prop('checked') == true) {
                $('.chksinglecls').prop('checked', true);
            } else {
                $('.chksinglecls').prop('checked', false);
            }
        });

        $('#paidinvoiceid').click(function () {
            swal({
                title: "Are you sure?",
                text: "You want to paid all invoice status of the Paid this order?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        var suppliers_id = $('#suppliersval').val();
                        location.href = "<?php echo base_url();?>administrator/paidinvoiceaddpayment?suppliers_id=" + suppliers_id;
                    } else {

                    }
                });
        });

    })
</script>

<script>
    function show_confirmdelteforpayment(orderid) {

        swal({
            title: "Are you sure?",
            text: "You want to delete this payments",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    location.href = "<?php echo base_url();?>administrator/unpaidinvoicesfrompayment?orderid=" + orderid;

                } else {
                    //swal("Your imaginary file is safe!");
                }

            });
    }
</script>

