<?php include('partials/header.php'); ?>
<!-- Content wrapper -->
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4"><span class="text-muted fw-light">Home /</span> Purchase Orders List</h4>

        <!-- Product List Table -->
        <div class="card">

            <div class="card-datatable table-responsive">
                <div class="row">
                    <div class="col-12">
                        <?php if ($this->session->flashdata('success')) { ?>

                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                <table class="datatables-products table">
                    <thead class="table-light">
                    <tr>
                        <th>User</th>
                        <th>Suppliers</th>
                        <th>Project</th>
                        <th>Contractor</th>
                        <th>ORDER ID</th>
                        <th>Invoice NR</th>
                        <th>Invoice Amount</th>
                        <th>Purchase Order Image</th>
                        <th>Verify Status</th>
                        <th>Order date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $usertype = $this->session->userdata('admin_usertype');
                    $where = array('usertype' => $usertype, 'menutab' => 'Purchase orders');
                    $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);

                    if (!empty($purchaseorderslist)) {

                        $i = 1;
                        foreach (array_reverse($purchaseorderslist) as $perreq) {
                            $where = array('projectid' => $perreq->projectid);
                            $projectsval = $this->adminmodel->getSingle(TBLPROJECTS, $where);

                            $where = array('suppliers_id' => $perreq->suppliers);
                            $suppliersval = $this->adminmodel->getSingle(TBLSUPPLIERS, $where);

                            $where = array('contractor_id' => $perreq->contractorsid);
                            $contractorsval = $this->adminmodel->getSingle(TBLCONTRACTORS, $where);

                            $whereusers = array('userid' => $perreq->userid);
                            $usersval = $this->adminmodel->getSingle(ADMINUSERS, $whereusers);
                            ?>
                            <tr>
                                <td><?php echo $usersval->firstname . ' ' . $usersval->lastname; ?></td>
                                <td><?php echo $suppliersval->suppliers_name; ?></td>
                                <td><?php echo $projectsval->project_name; ?></td>
                                <td><?php echo $contractorsval->contractor_name; ?></td>
                                <td><?php echo $perreq->orderid; ?></td>
                                <td>
                                    <?php if ($perreq->invoiceimage !== '' && !is_null($perreq->invoiceimage)) { ?>
                                        <a id="image-open"
                                           href="javascript:void(0)"
                                           data-title="Invoice Image"
                                           data-src="uploads/invoices/<?php echo $perreq->invoiceimage; ?>"><?php echo $perreq->invoicenr; ?>
                                        </a>
                                    <?php } else { ?>
                                        <?php echo $perreq->invoicenr; ?>
                                    <?php } ?>


                                </td>
                                <td><?php echo $perreq->invoiceamount; ?></td>
                                <td>
                                    <?php if ($perreq->perchaseorderimage != '') { ?>
                                        <a href="javascript:void(0)"
                                           id="image-open"
                                           data-title="Purchase Order Image"
                                           data-src="uploads/invoices/<?php echo $perreq->perchaseorderimage; ?>"><img
                                                    style="width: 30px; height: 30px;border-radius: 100%;"
                                                    src="uploads/invoices/<?php echo $perreq->perchaseorderimage; ?>"
                                                    width="100"/></a>
                                    <?php } else { ?>
                                        No Image
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if ($perreq->ispaid == 1) { ?>
                                        Paid
                                    <?php } else { ?>
                                        <?php if ($perreq->is_verified == 1) { ?>
                                            VERIFIED
                                        <?php } else if ($perreq->is_verified == 2) { ?>
                                            COMPLETED
                                        <?php } else { ?>
                                            <span style='color:red;'>New</span>
                                        <?php } ?>

                                    <?php } ?>
                                </td>
                                <td><?php echo date('d-m-Y', strtotime($perreq->orderdate)); ?></td>
                                <td class="action-clm">
                                    <button class="btn btn-sm btn-icon dropdown-toggle hide-arrow"
                                            data-bs-toggle="dropdown" aria-expanded="false"><i
                                                class="mdi mdi-dots-vertical me-2"></i></button>
                                    <div class="dropdown-menu dropdown-menu-end m-0" style="">

                                        <?php if ($chkvalied->allowfor_complete == 1 || $usertype == 1) { ?>
                                            <?php if ($perreq->is_verified == 1) { ?>
                                                <a href="administrator/editpurchaseorder?orderid=<?php echo $perreq->orderid; ?>"
                                                   class="dropdown-item">Complete</a>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php if ($chkvalied->allowfor_pay == 1 || $usertype == 1) { ?>
                                            <?php if ($perreq->is_verified == 2 && $perreq->ispaid == 0) { ?>
                                                <a href="administrator/editpurchaseorder?orderid=<?php echo $perreq->orderid; ?>"
                                                   class="dropdown-item">Pay</a>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php if ($chkvalied->allowfor_verify == 1 || $usertype == 1) { ?>
                                            <?php if ($perreq->is_verified == 0) { ?>
                                                <a href="administrator/editpurchaseorder?orderid=<?php echo $perreq->orderid; ?>"
                                                   class="dropdown-item">Verify</a>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php if ($chkvalied->allowfor_modify == 1 || $usertype == 1) { ?>
                                            <a href="administrator/editpurchaseorder?orderid=<?php echo $perreq->orderid; ?>"
                                               class="dropdown-item">Change</a>
                                        <?php } ?>
                                        <a href="administrator/orderdetails?orderid=<?php echo $perreq->orderid; ?>"
                                           class="dropdown-item">View</a>

                                        <?php if ($perreq->ispaid != 1) { ?>
                                            <a class="dropdown-item" href="javascript:void(0)"
                                               onclick="show_confirmdelte(<?php echo $perreq->orderid; ?>)">
                                                Delete
                                            </a>
                                        <?php } ?>
                                    </div>


                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- / Content -->
    <?php include('partials/sticky-footer.php'); ?>
</div>


<div class="modal fade" id="image-modal" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg modal-simple modal-enable-otp modal-dialog-centered">
        <div class="modal-content p-3 p-md-5">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center mb-4">
                    <h3 class="mb-2" id="title">Purchase order image</h3>
                </div>
                <div class="text-center">
                    <img src="" alt="img" class="img-fluid image-src" style="width: 50%">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Content wrapper -->
<?php include('partials/footer.php'); ?>

<script>
    $(document).ready(function () {
        $('#startdate').on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
        $('#enddate').on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
        $('.datatables-products').DataTable({
            dom: '<"card-header d-flex border-top rounded-0 flex-wrap py-md-0"<"me-5 ms-n2"f><"d-flex justify-content-start justify-content-md-end align-items-baseline"<"dt-action-buttons d-flex align-items-start align-items-md-center justify-content-sm-center mb-3 mb-sm-0 gap-3"lB>>>t<"row mx-1"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            order: [[4, 'desc']],
            lengthMenu: [7, 10, 20, 50, 70, 100],
            buttons: [{
                extend: "collection",
                className: "btn btn-label-secondary dropdown-toggle me-3",
                text: '<i class="mdi mdi-export-variant me-1"></i><span class="d-none d-sm-inline-block">Export </span>',
                buttons: [{
                    extend: "print",
                    text: '<i class="mdi mdi-printer-outline me-1" ></i>Print',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 5, 6, 7, 8]
                    }
                }, {
                    extend: "csv",
                    text: '<i class="mdi mdi-file-document-outline me-1" ></i>Csv',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 5, 6, 7, 8]
                    }
                }, {
                    extend: "excel",
                    text: '<i class="mdi mdi-file-excel-outline me-1"></i>Excel',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 5, 6, 7, 8]
                    }
                }, {
                    extend: "pdf",
                    text: '<i class="mdi mdi-file-pdf-box me-1"></i>Pdf',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 5, 6, 7, 8]
                    }
                }, {
                    extend: "copy",
                    text: '<i class="mdi mdi-content-copy me-1"></i>Copy',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 5, 6, 7, 8]
                    }
                }]
            }],
        });
    });
</script>
<script>
    function show_confirmverify(orderid) {
        Swal.fire({
            title: "Are you sure?",
            text: "You want to verify this order?",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            customClass: {
                confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                cancelButton: "btn btn-outline-secondary waves-effect"
            },
            buttonsStyling: !1
        }).then((e) => {

            if (e.value) {
                location.href = "<?php echo base_url();?>administrator/verifyorders?orderid=" + orderid;
            } else {
                if (e.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: "Cancelled",
                        text: "Cancelled Suspension :)",
                        icon: "error",
                        customClass: {confirmButton: "btn btn-success waves-effect"}
                    })
                }
            }
        });
    }

    function show_confirmdelte(id) {

        Swal.fire({
            title: "Are you sure?",
            text: "You want to delete this orders?",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            customClass: {
                confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                cancelButton: "btn btn-outline-secondary waves-effect"
            },
            buttonsStyling: !1
        }).then(function (e) {

            if (e.value) {
                location.href = "<?php echo base_url();?>administrator/deleteorders?orderid=" + id;
            } else {
                if (e.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: "Cancelled",
                        text: "Cancelled Suspension :)",
                        icon: "error",
                        customClass: {confirmButton: "btn btn-success waves-effect"}
                    })
                }
            }
        })

    }

    function show_completebyadmin(orderid) {
        swal({
            title: "Are you sure?",
            text: "You want to complete this order?",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            customClass: {
                confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                cancelButton: "btn btn-outline-secondary waves-effect"
            },
            buttonsStyling: !1
        }).then((e) => {

            if (e.value) {
                location.href = "<?php echo base_url();?>administrator/completeorder?orderid=" + orderid;
            } else {
                if (e.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: "Cancelled",
                        text: "Cancelled Suspension :)",
                        icon: "error",
                        customClass: {confirmButton: "btn btn-success waves-effect"}
                    })
                }
            }
        });
    }

    function show_confirmpayed(orderid) {
        swal({
            title: "Are you sure?",
            text: "You want to change the status of the Paid this order?",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            customClass: {
                confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                cancelButton: "btn btn-outline-secondary waves-effect"
            },
            buttonsStyling: !1
        }).then((e) => {

            if (e.value) {
                location.href = "<?php echo base_url();?>administrator/changepaidstatusorder?orderid=" + orderid;
            } else {
                if (e.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: "Cancelled",
                        text: "Cancelled Suspension :)",
                        icon: "error",
                        customClass: {confirmButton: "btn btn-success waves-effect"}
                    })
                }
            }
        });
    }


</script>

<script>
    $(document).on('click', '#image-open', function () {
        var title = $(this).data('title');
        var src = $(this).data('src');
        $('.image-src').attr('src', src);
        $('#title').text(title);
        $('#image-modal').modal('show');
    })
</script>
