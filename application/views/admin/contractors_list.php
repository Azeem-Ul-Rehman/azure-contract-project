

<?php include('partials/header.php'); ?>
<!-- Content wrapper -->
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4"><span class="text-muted fw-light">Home /</span> Contractors List</h4>

        <!-- Product List Table -->
        <div class="card">

            <div class="card-datatable table-responsive">
                <div class="row">
                    <div class="col-12">
                        <?php if ($this->session->flashdata('success')) { ?>

                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                <table class="datatables-products table">
                    <thead class="table-light">
                    <tr>
                        <th>Contractors Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Type</th>
                        <th>Persons</th>
                        <th>CRIB No</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $usertype = $this->session->userdata('admin_usertype');
                    $where = array('usertype' => $usertype, 'menutab' => 'Contractors');
                    $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);

                    if (!empty($contractors)) {

                        $i = 1;
                        foreach ($contractors as $perreq) {
                            ?>
                            <tr>
                                <?php if ($chkvalied->allowfor_edit == 1 || $usertype == 1) { ?>
                                    <td>
                                        <a href="administrator/contractors_details/?contractor_id=<?php echo $perreq->contractor_id; ?>"><?php echo $perreq->contractor_name; ?></a>
                                    </td>
                                <?php } else { ?>
                                    <td>
                                        <a href="javascript:void(0);"><?php echo $perreq->contractor_name; ?></a>
                                    </td>
                                <?php } ?>


                                <td><?php echo $perreq->contractor_address; ?></td>
                                <td><?php echo $perreq->contractor_phone; ?></td>
                                <td><?php echo $perreq->contractor_type; ?></td>
                                <td><?php echo $perreq->contractor_person; ?></td>
                                <td><?php echo $perreq->contractor_cribno; ?></td>
                                <td class="action-clm">
                                    <?php if ($chkvalied->allowfor_modify == 1 || $usertype == 1) { ?>
                                        <a href="administrator/editcontractor?contractor_id=<?php echo $perreq->contractor_id; ?>"
                                           class="btn btn-primary"><i class="mdi mdi-pencil-outline"></i></a>
                                    <?php } ?>
                                    <?php if ($chkvalied->allow_delete == 1 || $usertype == 1) { ?>
                                        <button class="btn btn-outline-danger"
                                                onclick="show_confirmdelte(<?php echo $perreq->contractor_id; ?>)"><i
                                                    class="mdi mdi-trash-can-outline"></i></button>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- / Content -->
    <?php include('partials/sticky-footer.php'); ?>
</div>
<style>
    .display-none {
        display: none !important;
    }
</style>
<?php if ($chkvalied->allowfor_add == 1 || $usertype == 1) {

    $display = '';
} else {
    $display = 'display-none';
}
?>
<!-- Content wrapper -->
<?php include('partials/footer.php'); ?>

<script>
    $(document).ready(function () {
        $('#startdate').on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
        $('#enddate').on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
        $('.datatables-products').DataTable({
            dom: '<"card-header d-flex border-top rounded-0 flex-wrap py-md-0"<"me-5 ms-n2"f><"d-flex justify-content-start justify-content-md-end align-items-baseline"<"dt-action-buttons d-flex align-items-start align-items-md-center justify-content-sm-center mb-3 mb-sm-0 gap-3"lB>>>t<"row mx-1"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            lengthMenu: [7, 10, 20, 50, 70, 100],
            buttons: [{
                extend: "collection",
                className: "btn btn-label-secondary dropdown-toggle me-3",
                text: '<i class="mdi mdi-export-variant me-1"></i><span class="d-none d-sm-inline-block">Export </span>',
                buttons: [{
                    extend: "print",
                    text: '<i class="mdi mdi-printer-outline me-1" ></i>Print',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                }, {
                    extend: "csv",
                    text: '<i class="mdi mdi-file-document-outline me-1" ></i>Csv',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                }, {
                    extend: "excel",
                    text: '<i class="mdi mdi-file-excel-outline me-1"></i>Excel',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                }, {
                    extend: "pdf",
                    text: '<i class="mdi mdi-file-pdf-box me-1"></i>Pdf',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                }, {
                    extend: "copy",
                    text: '<i class="mdi mdi-content-copy me-1"></i>Copy',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                }]
            }, {
                text: '<i class="mdi mdi-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add New Contractor</span>',
                className: "add-new btn btn-primary ms-n1 <?php echo $display; ?>",
                action: function () {
                    window.location.href = "administrator/addnewcontractor"
                }
            }],
        });
    });
</script>
<script>
    function show_confirmdelte(contractor_id) {

        Swal.fire({
            title: "Are you sure?",
            text: "You want to delete this contractor!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            customClass: {
                confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                cancelButton: "btn btn-outline-secondary waves-effect"
            },
            buttonsStyling: !1
        }).then(function (e) {

            if (e.value) {
                location.href = "<?php echo base_url();?>administrator/deletecontractor?contractor_id=" + contractor_id;
            } else {
                if (e.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: "Cancelled",
                        text: "Cancelled Suspension :)",
                        icon: "error",
                        customClass: {confirmButton: "btn btn-success waves-effect"}
                    })
                }
            }
        })

    }

</script>
