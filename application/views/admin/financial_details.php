<?php include('partials/header.php'); ?>
<style>
    p {
        display: inline-flex;
    }
</style>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Financial Institute Details
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Financial Institute Details</h5>

                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row">
                                <!-- First column-->
                                <div class="col-md-12">
                                    <!-- Product Information -->

                                    <fieldset class="mb-4">

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Name : </b>
                                                    </label>

                                                </div>
                                                <div class="col-2 text-left">
                                                    <p><?php echo $financialinstitutes->financialinstitutename; ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Phone No : </b>
                                                    </label>

                                                </div>
                                                <div class="col-2">
                                                    <p>
                                                        <?php echo $financialinstitutes->financialinstitutephone; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Address : </b>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <p>
                                                        <?php echo $financialinstitutes->financialinstituteaddress; ?>
                                                    </p>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Account no : </b>
                                                    </label>
                                                </div>
                                                <div class="col-10">
                                                    <p>
                                                        <?php echo $financialinstitutes->financialinstituteaccountno; ?>
                                                    </p>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Account Loan : </b>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <p>
                                                        <?php echo $financialinstitutes->financialinstituteaccountloan; ?>
                                                    </p>
                                                </div>
                                            </div>


                                        </div>

                                    </fieldset>
                                    <div class="card mb-4">
                                        <div class="card-header p-0">
                                            <div class="nav-align-top">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect active"
                                                                role="tab"
                                                                data-bs-toggle="tab" data-bs-target="#navs-top-home"
                                                                aria-controls="navs-top-home" aria-selected="false"
                                                                tabindex="-1">
                                                            Loans
                                                        </button>
                                                    </li>
                                                    <span class="tab-slider"
                                                          style="left: 198.3px; width: 127.65px; bottom: 0px;"></span>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content p-0">
                                                <div class="tab-pane fade active show" id="navs-top-home"
                                                     role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Project Name</th>
                                                                <th>Loan Amount</th>
                                                                <th>Interest Rate</th>
                                                                <th>Period</th>
                                                                <th>Redemption Amount</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>                                        <?php foreach ($financialloans as $rowsloan) {
                                                                $where = array('projectid' => $rowsloan->project);
                                                                $projectvals = $this->adminmodel->getSingle(TBLPROJECTS, $where); ?>
                                                                <tr>
                                                                    <td>
                                                                        <a href="administrator/projectlist"><?php echo $projectvals->project_name; ?></a>
                                                                    </td>
                                                                    <td><?php echo $rowsloan->loanamount; ?></td>
                                                                    <td><?php echo $rowsloan->interestrate; ?></td>
                                                                    <td><?php echo $rowsloan->period; ?></td>
                                                                    <td><?php echo $rowsloan->redemptionamount; ?></td>
                                                                </tr>                                        <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /Second column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>
