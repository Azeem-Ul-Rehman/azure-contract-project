<?php include('partials/header.php'); ?>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Edit Sell Property
        </h4>
        <form id="my_form" name="my_form" method="post" action="administrator/updatesellpropertyaction" enctype="multipart/form-data" onsubmit="return checklasttab()">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Edit Sell Property</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
						    <input id="id" value="<?php echo $editsellproperty->id; ?>" class="form-control" name="hdnsellproperty" type="hidden">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="project_id" id="project_id" class="form-control required">
											<option value="">Select Project</option>
											<?php foreach($projects as $project){ ?>
												<option <?php if($editsellproperty->project_id == $project->projectid){ echo "selected";} ?> value="<?php echo $project->projectid; ?>"><?php echo $project->project_name; ?></option>
											<?php } ?>
										</select>
                                        <label for="project_id">Project</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="project_type_id" id="project_type_id" class="form-control required">
											<option value="">Select Project Type</option>
											<?php foreach($projectTypes as $projectType){ ?>
												<option <?php if($editsellproperty->project_type_id == $projectType->id){ echo "selected";} ?> value="<?php echo $projectType->id; ?>"><?php echo $projectType->projecttype; ?></option>
											<?php } ?>
										</select>
                                        <label for="project_type_id">Project Type</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                    <textarea id="address" class="form-control required" name="address" placeholder="Kavel Nr. / Address" aria-label="Kavel Nr. / Address"><?php echo $editsellproperty->address; ?></textarea>
                                        <label for="address">Kavel Nr. / Address</label>
                                    </div>
                                </div>
                                <!-- /Second column -->
                            </div>
							<div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="real_estate" placeholder="Real Estate" name="real_estate" aria-label="Real Estate" value="<?php echo $editsellproperty->real_estate; ?>"/>
                                        <label for="real_estate">Real Estate</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="buyer_name" placeholder="Buyer Name" name="buyer_name" aria-label="Buyer Name" value="<?php echo $editsellproperty->buyer_name; ?>"/>
                                        <label for="buyer_name">Buyer Name</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <textarea id="buyer_address" class="form-control required" name="buyer_address" placeholder="Buyer Address" aria-label="Buyer Address"><?php echo $editsellproperty->buyer_address; ?></textarea>
                                        <label for="buyer_address">Buyer Address</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="buyer_phone" placeholder="Buyer Phone" name="buyer_phone" aria-label="Buyer Phone" value="<?php echo $editsellproperty->buyer_phone; ?>"/>
                                        <label for="buyer_phone">Buyer Phone</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="email" class="form-control required" id="buyer_email" placeholder="Buyer Email" name="buyer_email" aria-label="Buyer Email" value="<?php echo $editsellproperty->buyer_email; ?>"/>
                                        <label for="buyer_email">Buyer Email</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="numnber" class="form-control required" id="selling_price" placeholder="Selling Price" name="selling_price" aria-label="Selling Price" value="<?php echo $editsellproperty->selling_price; ?>"/>
                                        <label for="selling_price">Selling Price</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="other_charges" placeholder="Other Charges" name="other_charges" aria-label="Other Charges" value="<?php echo $editsellproperty->other_charges; ?>"/>
                                        <label for="other_charges">Other Charges</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="date" class="form-control required" id="agreement_date" placeholder="Agreement Date" name="agreement_date" aria-label="Agreement Date" value="<?php echo date('Y-m-d', strtotime($editsellproperty->agreement_date)); ?>"/>
                                        <label for="agreement_date">Agreement Date</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="date" class="form-control required" id="delivery_date" placeholder="Delivery Date" name="delivery_date" aria-label="Delivery Date" value="<?php echo date('Y-m-d', strtotime($editsellproperty->delivery_date)); ?>"/>
                                        <label for="delivery_date">Delivery Date</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Document Add -->
							<div id="newRowdocuments">
                            <?php if(!empty($projectdocuments)){
                                foreach($projectdocuments as $rowsdocument){?>
								<div class="row" id="inputFormdocuments">
									<div class="col-md-5">
										<div class="form-floating form-floating-outline mb-4">
											<input type="text" name="documentname[]" id="documentname" class="form-control m-input" value="<?php echo $rowsdocument->document_name; ?>" placeholder="Document Name" autocomplete="off" aria-label="Document Name">
											<label for="documentname">Document Name</label>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-floating form-floating-outline mb-4">
											<input type="file" name="filename[]" id="filename" class="form-control m-input" placeholder="Upload Document" autocomplete="off" aria-label="Upload Document">
											<label for="filename">Upload Document</label>
                                            <input type="hidden" name="hdndocumentname[]" value="<?php echo $rowsdocument->document_file; ?>" />
										</div>
									</div>
                                    <div class="col-md-1">
                                        <?php $getimgtype = explode('.',$rowsdocument->document_file);
                                        if($getimgtype[1]=='jpg' || $getimgtype[1]=='png' || $getimgtype[1]=='jpeg' || $getimgtype[1]=='gif'){?>
                                        <a href="uploads/documents/<?php echo $rowsdocument->document_file; ?>" download> <img src="uploads/documents/<?php echo $rowsdocument->document_file; ?>" style="width:50px; height:50px;" /></a>
                                        <?php }else if($getimgtype[1]=='docs'){	echo '<a href="uploads/documents/'.$rowsdocument->document_file.'" download> <img src="uploads/wordimg.png" style="width:50px; height:50px;"  /></a>';}
                                        else if($getimgtype[1]=='xlsx' || $getimgtype[1]=='csv'){ echo '<a href="uploads/documents/'.$rowsdocument->document_file.'"  download> <img src="uploads/excelimg.png" style="width:50px; height:50px;" /></a>';}
                                        else if($getimgtype[1]=='pdf'){ echo '<a href="uploads/documents/'.$rowsdocument->document_file.'"  download> <img src="uploads/pdfimg.png" style="width:50px; height:50px;" /></a>';}
                                        else{ echo '<a href="uploads/documents/'.$rowsdocument->document_file.'" download> <img src="uploads/wordimg.png" style="width:50px; height:50px;"  /></a>';?>
                                        <?php } ?>  
                                    </div>
									<div class="col-md-2">
									<button id="removedocuments" type="button" class="btn btn-danger">Remove</button>
									</div>
									
								</div>
                                <?php }}?>
							</div>
							<div class="row g-4 mb-4">
								<div class="action-btns">
									<button id="addRowdocument" type="button" class="btn btn-info">Add New Document</button>
								</div>
                            </div>

                            <!-- Check List Add -->
							<div id="newRowCheckListSection" class="newRowCheckListSection">
                                <?php if(!empty($checkList)){ ?>
                                    <?php foreach ($checkList as $key => $sectionDetail) { ?>
                                    <div class="row" id="inputCheckListSection">
                                        <div class="col-10 col-lg-10">
                                            <div class="form-floating form-floating-outline mb-4">
                                                <input type="text" name="section[]" id="section" class="form-control section m-input" placeholder="Section Name" autocomplete="off" aria-label="Section Name" data-id = "<?php echo $key; ?>" value="<?php echo $sectionDetail->section; ?>">
                                                <label for="section">Section Name</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button id="removeCheckListSection" type="button" class="btn btn-danger">Remove</button>
                                        </div>
                                        <?php
                                            $where = array('check_list_section_id' => $sectionDetail->id);
                                            $itemLists = $this->adminmodel->getwhere(TBLCHECKLISTITEMS,$where);
                                        ?>
                                        <div class="col-md-12" id="newRowCheckListItem<?php echo $key; ?>">
                                            <input type="hidden" value= "<?php if(!empty($itemLists)){ echo count($itemLists) - 1; }else{ echo "0"; } ?>" id="checkListIndex<?php echo $key; ?>">
                                            <input type="hidden" value ="" name="itemIds<?php echo $key; ?>" id="itemIds<?php echo $key;?>">
                                            <?php if(!empty($itemLists)){ ?>
                                            <?php foreach ($itemLists as $itemKey => $itemDetail) {?>
                                                <div class="row" id="inputCheckListItem<?php echo $key;?><?php echo $itemKey;?>">
                                                    <div class="col-md-4">
                                                        <div class="form-floating form-floating-outline mb-4">
                                                            <input type="text" name="item<?php echo $key;?>[]" id="item" class="form-control m-input" placeholder="Item" autocomplete="off" aria-label="Item" value="<?php echo $itemDetail->item;?>">
                                                            <label for="item">Item</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="form-group1">
                                                            <input class="form-check-input" <?php if($itemDetail->item_condition == "Goed"){echo "checked";} ?> name="condition<?php echo $key;?><?php echo $itemKey;?>" type="radio" value="Goed" id="radioGoed">
                                                            <label class="form-check-label" for="inlineCheckbox1">Goed</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="form-group1">
                                                            <input class="form-check-input" <?php if($itemDetail->item_condition == "Slecht"){echo "checked";} ?> name="condition<?php echo $key;?><?php echo $itemKey;?>" type="radio" value="Slecht" id="radioSlecht">
                                                            <label class="form-check-label"for="inlineCheckbox1">Slecht</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-floating form-floating-outline mb-4">
                                                            <textarea id="remarks" class="form-control required" name="remarks<?php echo $key;?>[]" placeholder="Remarks" aria-label="Remarks"><?php echo $itemDetail->remarks;?></textarea>
                                                            <label for="remarks">Remarks</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button id="removeCheckListItem" onclick="removeCheckListItemFun('<?php echo $key;?>', '<?php echo $itemKey;?>')" type="button" data-id="<?php echo $itemKey;?>" class="btn btn-danger removeCheckListItem<?php echo $key;?>">Remove</button>
                                                    </div>
                                                </div>
                                                <?php }}?>
                                        </div>
                                        
                                        <div class="col-md-12 mb-4">
                                            <div class="action-btns">
                                                <button id="addRowCheckListItem" onclick="addRowCheckListItemFun('<?php echo $key;?>')" type="button" class="btn btn-info">Add New Item</button>
                                            </div>
                                        </div>
                                        
                                    </div>
                                <?php }}?>
								
							</div>
                            
							<div class="row g-4 mb-4">
								<div class="action-btns">
									<button id="addRowCheckListSection" type="button" class="btn btn-info">Add New Section</button>
								</div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

<script>
    $("#addRowdocument").click(function () {
		var html = '';
		html += '<div class="row" id="inputFormdocuments">';
		html += '<div class="col-md-5">';
		html += '<div class="form-floating form-floating-outline  mb-4">';
		html += '<input type="text" name="documentname[]" id="documentname" class="form-control m-input" placeholder="Document Name" autocomplete="off" aria-label="Document Name">';
		html += '<label for="documentname">Document Name</label>';
		html += '</div>';
		html += '</div>';

		html += '<div class="col-md-5">';
		html += '<div class="form-floating form-floating-outline mb-4">';
		html += '<input type="file" name="filename[]" id="filename" class="form-control m-input" placeholder="Upload Document" autocomplete="off" aria-label="Upload Document">';
		html += '<label for="filename">Upload Document</label>';
		html += '</div>';
		html += '</div>';
		html += '<div class="col-md-2">';
		html += '<button id="removedocuments" type="button" class="btn btn-danger">Remove</button>';
		html += '</div>';
		html += '</div>';
		
		$('#newRowdocuments').append(html);
	});

	// remove row
	$(document).on('click', '#removedocuments', function () {
		$(this).closest('#inputFormdocuments').remove();
	});


    $("#addRowCheckListSection").click(function () {
        var id =  parseInt($('#checkSectionIndex').val()) + 1;
        $('#checkSectionIndex').val(id);
		var html = '';
		html += '<div class="row" id="inputCheckListSection">';

		html += '<div class="col-10 col-lg-10">';
		html += '<div class="form-floating form-floating-outline  mb-4">';
		html += '<input type="text" name="section[]" id="section" class="form-control section m-input" placeholder="Section Name" autocomplete="off" aria-label="Section Name" data-id = "'+id+'">';
		html += '<label for="section">Section Name</label>';
		html += '</div>';
		html += '</div>';

		html += '<div class="col-md-2">';
		html += '<button id="removeCheckListSection" type="button" class="btn btn-danger">Remove</button>';
		html += '</div>';

        //Item First Row

        html += '<div class="col-md-12" id="newRowCheckListItem'+id+'">';
        html +='<input type="hidden" value= "0" id="checkListIndex'+id+'">';
        html +='<input type="hidden" value ="" name="itemIds'+id+'" id="itemIds'+id+'">'; 
        html += '<div class="row" id="inputCheckListItem'+id+'0">';

		html += '<div class="col-md-4">';
		html += '<div class="form-floating form-floating-outline  mb-4">';
		html += '<input type="text" name="item'+id+'[]" id="item" class="form-control m-input" placeholder="Item" autocomplete="off" aria-label="Item">';
		html += '<label for="item">Item</label>';
		html += '</div>';
		html += '</div>';

        html += '<div class="col-md-1">';
		html += '<div class="form-group1">';
		html += '<input class="form-check-input" name="condition'+id+'0" type="radio" value="Goed" id="radioGoed">';
		html += ' <label class="form-check-label" for="inlineCheckbox1">Goed</label>';
		html += '</div>';
		html += '</div>';

        html += '<div class="col-md-1">';
		html += '<div class="form-group1">';
		html += '<input class="form-check-input" name="condition'+id+'0" type="radio" value="Slecht" id="radioSlecht">';
		html += ' <label class="form-check-label" for="inlineCheckbox1">Slecht</label>';
		html += '</div>';
		html += '</div>';

        html += '<div class="col-md-4">';
		html += '<div class="form-floating form-floating-outline  mb-4">';
		html += '<textarea id="remarks" class="form-control required" name="remarks'+id+'[]" placeholder="Remarks" aria-label="Remarks"></textarea>';
		html += '<label for="remarks">Remarks</label>';
		html += '</div>';
		html += '</div>';

        html += '<div class="col-md-2">';
		html += '<button id="removeCheckListItem" data-id="0" onclick="removeCheckListItemFun('+id+', 0)" type="button" class="btn btn-danger removeCheckListItem'+id+'">Remove</button>';
		html += '</div>';

        html += '</div>';
		html += '</div>';

        html += '<div class="col-md-12 mb-4">';
        html += '<div class="action-btns">';
		html += '<button id="addRowCheckListItem" onclick="addRowCheckListItemFun('+id+')" type="button" class="btn btn-info">Add New Item</button>';
		html += '</div>';
        html += '</div>';

        html += '</div>';

		
		$('#newRowCheckListSection').append(html);
	});

	// remove row
	$(document).on('click', '#removeCheckListSection', function () {
		$(this).closest('#inputCheckListSection').remove();
	});

    function addRowCheckListItemFun(id){
        debugger;
        var item_id =  parseInt($('#checkListIndex'+id+'').val()) + 1;
        $('#checkListIndex'+id+'').val(item_id);
        var html = '';
        html += '<div class="row" id="inputCheckListItem'+id+''+item_id+'">';

		html += '<div class="col-md-4">';
		html += '<div class="form-floating form-floating-outline  mb-4">';
		html += '<input type="text" name="item'+id+'[]" id="item" class="form-control m-input" placeholder="Item" autocomplete="off" aria-label="Item">';
		html += '<label for="item">Item</label>';
		html += '</div>';
		html += '</div>';

        html += '<div class="col-md-1">';
		html += '<div class="form-group1">';
		html += '<input class="form-check-input" name="condition'+id+''+item_id+'" type="radio" value="Goed" id="radioGoed">';
		html += ' <label class="form-check-label" for="inlineCheckbox1">Goed</label>';
		html += '</div>';
		html += '</div>';

        html += '<div class="col-md-1">';
		html += '<div class="form-group1">';
		html += '<input class="form-check-input" name="condition'+id+''+item_id+'" type="radio" value="Slecht" id="radioSlecht">';
		html += ' <label class="form-check-label" for="inlineCheckbox1">Slecht</label>';
		html += '</div>';
		html += '</div>';

        html += '<div class="col-md-4">';
		html += '<div class="form-floating form-floating-outline  mb-4">';
		html += '<textarea id="remarks" class="form-control required" name="remarks'+id+'[]" placeholder="Remarks" aria-label="Remarks"></textarea>';
		html += '<label for="remarks">Remarks</label>';
		html += '</div>';
		html += '</div>';

        html += '<div class="col-md-2">';
		html += '<button id="removeCheckListItem" data-id="'+item_id+'" onclick="removeCheckListItemFun('+id+', '+item_id+')" type="button" class="btn btn-danger removeCheckListItem'+id+'">Remove</button>';
		html += '</div>';

        html += '</div>';

        $('#newRowCheckListItem'+id+'').append(html);
    }


    function removeCheckListItemFun(id, itemId){
        $('#inputCheckListItem'+id+''+itemId+'').remove();
    }

    function checklasttab(){
        debugger;
        array = [];
        $('.section').each(function(index) {
            var id = parseInt($(this).attr("data-id"));
            array.push(parseInt($(this).attr("data-id")));
            arrayItem = [];
            $('.removeCheckListItem'+id+'').each(function(index) {
                arrayItem.push(parseInt($(this).attr("data-id")));
            });
            $("#itemIds"+id+"").val(arrayItem.join(","));
        });
        $("#sectionIds").val(array.join(","));
        return true;
    }

</script>

