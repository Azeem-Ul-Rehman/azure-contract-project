<?php include('partials/header.php'); ?>
<!-- partial -->
<!-- Content wrapper -->
<!-- Content wrapper -->
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Users
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div
                                class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Edit Users</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row g-4 mb-4">
                                <!-- First column-->
                                <div class="col-md-4">
                                    <!-- Product Information -->
                                    <input value="<?php echo $this->input->get('userid'); ?>" name="hdnid" type="hidden">

                                    <div class="form-floating form-floating-outline">
                                        <select id="usertype" name="usertype" class="select2 form-select required"
                                                data-allow-clear="true">
                                            <?php foreach ($usersgroups as $rowsgroup) { ?>
                                                <option  <?php if($usersinfo->usertype==$rowsgroup->id){ ?> selected="selected" <?php } ?> value="<?php echo $rowsgroup->id; ?>"><?php echo $rowsgroup->groupname; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="usertype">Usertype</label>
                                    </div>


                                </div>
                                <div class="col-md-4">

                                    <div class="form-floating form-floating-outline">
                                        <input type="text" id="firstname" name="firstname" class="form-control required"
                                               value="<?php echo $usersinfo->firstname; ?>"
                                               placeholder="First Name"/>
                                        <label for="firstname">First Name *</label>
                                    </div>


                                </div>
                                <div class="col-md-4">


                                    <div class="form-floating form-floating-outline">
                                        <input id="lastname" class="form-control required" name="lastname" type="text"
                                               value="<?php echo $usersinfo->lastname; ?>"
                                               placeholder="Last Name">
                                        <label for="lastname">Last Name *</label>
                                    </div>


                                </div>
                                <!-- /Second column -->
                            </div>
                            <div class="row g-4 mb-4">
                                <div class="col-md-6">

                                    <div class="form-floating form-floating-outline">
                                        <input type="text" id="username" name="username" class="form-control required"
                                               onchange="check_username(this.value);"
                                               value="<?php echo $usersinfo->username; ?>"
                                               readonly
                                               placeholder="Username"/>
                                        <label for="username">Username *</label>
                                        <p style="display:none;color:red;" class="chkusername_cls"></p>
                                    </div>


                                </div>
                                <div class="col-md-6">


                                    <div class="form-floating form-floating-outline">
                                        <input id="phone" class="form-control required" name="phone" type="text"

                                               value="<?php echo $usersinfo->phone; ?>"
                                               placeholder="Phone">
                                        <label for="phone">Phone *</label>
                                    </div>


                                </div>
                            </div>
                            <div class="row g-4">
                                <div class="col-md-6">

                                    <div class="form-floating form-floating-outline">
                                        <input type="password" minlength="5" id="password" name="password"
                                               value="<?php echo $usersinfo->show_passkey; ?>"
                                               class="form-control required"
                                               placeholder="Password"/>
                                        <label for="password">Password *</label>
                                    </div>


                                </div>
                                <div class="col-md-6">


                                    <div class="form-floating form-floating-outline">
                                        <input id="email" onchange="check_email(this.value);"
                                               readonly="readonly" value="<?php echo $usersinfo->email; ?>"
                                               class="form-control required email" name="email" type="email"
                                               placeholder="Email">
                                        <label for="email">Email * </label>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

<script>
    function check_email(email) {
        $('.chkmail_cls').css('display', 'none');
        $('.chkmail_cls').text('');
        if (email != '') {
            $.ajax({
                url: "admin/chkuser_email",
                type: "POST",
                data: {
                    email: email
                },
                success: function (data) {
                    if (data != 'true') {
                        $('#email').val('');
                        $('.chkmail_cls').css('display', 'block');
                        $('.chkmail_cls').text('This email is already exist!');
                        return false;
                    } else {
                        $('.chkmail_cls').css('display', 'none');
                        $('.chkmail_cls').text('');
                    }
                }
            });
        }
    }

    function check_username(username) {
        $('.chkusername_cls').css('display', 'none');
        $('.chkusername_cls').text('');
        if (username != '') {
            $.ajax({
                url: "admin/chkuser_username",
                type: "POST",
                data: {
                    username: username
                },
                success: function (data) {
                    if (data != 'true') {
                        $('#username').val('');
                        $('.chkusername_cls').css('display', 'block');
                        $('.chkusername_cls').text('This username is already exist!');
                        return false;
                    } else {
                        $('.chkusername_cls').css('display', 'none');
                        $('.chkusername_cls').text('');
                    }
                }
            });
        }
    }
</script>
