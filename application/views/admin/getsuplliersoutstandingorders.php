<?php if (!empty($purchaseorders)) { ?>
    <h4>Invoice(s)</h4>
    <div class="row">
        <div class="col-md-4" style="color: color: blue;">TOTAL OUTSTANDING INVOICES: <span class="totalOutstanding"
                                                                                            style="color:red;">0.00</span>
        </div>
        <div class="col-md-4" style="color: color: blue;">SELECTED IN THIS PAYMENT: <span class="selectedPayments"
                                                                                          style="color:red;">0.00</span>
        </div>
        <div class="col-md-4" style="color: color: blue;">DIFFERENCE: <span class="difference"
                                                                            style="color:red;">0.00</span></div>
    </div>
    <table id="order-listing" class="table">&nbsp;&nbsp;
        <div class="nopaid"></div>
        <thead>
        <tr>
            <th>INVOICE NR</th>
            <th>PURCHASE ORDER</th>
            <th>INVOICE AMOUNT</th>
            <th>INVOICE DATE</th>
            <th>ADD TO THIS PAYMENT</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $totalOutStanding = 0;
        foreach ($purchaseorders as $key => $rowsorder) {
            $whereprojects = array('projectid' => $rowsorder->projectid);
            $projectsval = $this->adminmodel->getSingle(TBLPROJECTS, $whereprojects);

            $totalOutStanding += $rowsorder->invoiceamount;

            ?>
            <tr class="row<?php echo $key; ?>">
                <td><?php echo $rowsorder->invoicenr; ?></td>
                <td><?php echo $projectsval->project_name; ?></td>
                <td><?php echo $rowsorder->invoiceamount; ?></td>
                <td><?php echo date('d/m/Y', strtotime($rowsorder->orderdate)); ?></td>
                <td>
                    <input type="checkbox" class="chksinglecls" name="invoiceno[]"
                        <?php if (!is_null($rowsorder->payment_id) && $rowsorder->payment_id !== '') {
                            echo 'checked';
                        } ?>
                           data-payment-checked="<?php if (!is_null($rowsorder->payment_id) && $rowsorder->payment_id !== '') {
                               echo 'true';
                           } else {
                               echo 'false';
                           } ?>"
                           data-amount="<?php echo $rowsorder->invoiceamount; ?>"
                           data-orderid="<?php echo $rowsorder->orderid; ?>"
                           id="invoiceno<?php echo $rowsorder->orderid; ?>" value="<?php echo $rowsorder->orderid; ?>"/>
                </td>
            </tr>
        <?php }
        ?>
        </tbody>
    </table>
    <br><br>
    <script src="//code.jquery.com/jquery-latest.min.js"
            type="text/javascript"></script>
    <script>
        $(document).ready(function () {


            $('.totalOutstanding').text('<?php echo number_format($totalOutStanding, 2)?>')
            $('.selectallcls').click(function () {
                if ($(this).prop('checked') == true) {
                    $('.chksinglecls').prop('checked', true);
                } else {
                    $('.chksinglecls').prop('checked', false);
                }

                // calculatePrices();
            });

            $(document).on('change', '.chksinglecls', function () {
                calculatePrices();
            })

            function calculatePrices() {
                var checkboxes = $('.chksinglecls');
                var totalOutstandingAmount = parseFloat($('#Amount').val()).toFixed(2);
                var selectedAmount = 0;
                var html = '';
                $.each(checkboxes, function () {
                    var $this = $(this);


                    // check if the checkbox is checked
                    if ($this.is(":checked") === true) {
                        // if($this.data('payment-checked') === false){
                        selectedAmount = selectedAmount + parseFloat($this.data('amount'));
                        // }

                    } else {
                        var orderId = $this.data('orderid');
                        html += '<input type="hidden" name="invoicenopaid[]" value="' + orderId + '" />'

                    }


                });


                $('.nopaid').html(html);
                $('.selectedPayments').text(parseFloat(selectedAmount).toFixed(2));
                $('.difference').text(parseFloat(totalOutstandingAmount).toFixed(2) - parseFloat(selectedAmount).toFixed(2));

            }

            $(document).on('focusout', '#Amount', function () {
                calculatePrices();
            });
            <?php if($title === 'Edit Payments'){ ?>
            calculatePrices();
            <?php } ?>
        });
    </script>
<?php } ?>
