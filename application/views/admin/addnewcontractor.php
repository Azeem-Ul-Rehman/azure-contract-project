<?php include('partials/header.php'); ?><!-- partial --><!-- Content wrapper --><!-- Content wrapper -->
<style>    #contractor_assignproject-error {
        margin-top: 53px !important;
        position: absolute !important;
    }</style>
<div class="content-wrapper">    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y"><h4 class="py-3 mb-4"><span
                    class="text-muted fw-light">Home/</span> Contractors </h4>
        <form id="my_form" name="my_form" method="post" action="">            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Add Contractor</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type --> <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible"><a href="#" class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>                        <?php } ?>
                        <div class="card-body">
                            <div class="row g-4 mb-4 g-4">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><input type="text"
                                                                                            id="contractor_name"
                                                                                            name="contractor_name"
                                                                                            class="form-control required"
                                                                                            placeholder="Contractor Name"/>
                                        <label for="contractor_name">Contractor Name *</label></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><input id="contractor_phone"
                                                                                            class="form-control required"
                                                                                            name="contractor_phone"
                                                                                            type="text"
                                                                                            placeholder="Contractor Phone No">
                                        <label for="contractor_phone">Contractor Phone No *</label></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><select id="contractor_type"
                                                                                             name="contractor_type"
                                                                                             class="select2 form-select required"
                                                                                             data-allow-clear="true">                                            <?php if (!empty($contractortypes)) { ?><?php foreach ($contractortypes as $rowscontractors) { ?>
                                                <option value="<?php echo $rowscontractors->contractortype; ?>"><?php echo $rowscontractors->contractortype; ?></option>                                                <?php }
                                            } ?>                                        </select> <label
                                                for="contractor_type">Contractor Type</label></div>
                                </div>                                <!-- /Second column -->
                            </div>
                            <div class="row g-4 mb-4 g-4">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><input id="contractor_person"
                                                                                            class="form-control required"
                                                                                            name="contractor_person"
                                                                                            type="text"
                                                                                            placeholder="Contractor persons">
                                        <label for="contractor_person">Contractor persons *</label></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><input type="number"
                                                                                            id="contractor_cribno"
                                                                                            name="contractor_cribno"
                                                                                            class="form-control required"
                                                                                            placeholder="CRIB No."/>
                                        <label for="contractor_cribno">CRIB No. *</label></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <select
                                                id="contractor_assignproject" name="contractor_assignproject[]"
                                                class="select2 form-select required" multiple
                                                data-allow-clear="true">
                                            <?php foreach ($projectslist as $rowsval) { ?>

                                                <?php $selectedvalue = '';
                                                $loansarr = explode(',', $contractors->contractor_assignproject); ?>
                                                <option <?php echo $selectedvalue; ?>
                                                        value="<?php echo $rowsval->projectid; ?>"><?php echo $rowsval->project_name; ?></option>                                            <?php } ?>
                                        </select>
                                        <label for="contractor_assignproject">Assigned Project</label></div>
                                </div>
                            </div>
                            <div class="row g-4 mb-4 g-4">
                                <div class="col-md-6">
                                    <div class="form-floating form-floating-outline"><input type="number"
                                                                                            id="contractor_kvkno"
                                                                                            name="contractor_kvkno"
                                                                                            class="form-control required"
                                                                                            placeholder="KVK No."/>
                                        <label for="contractor_kvkno">KVK No. *</label></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating form-floating-outline"><input id="contractor_payments"
                                                                                            class="form-control required"
                                                                                            name="contractor_payments"
                                                                                            type="number"
                                                                                            placeholder="Contractor Payments">
                                        <label for="contractor_payments">Contractor Payments</label></div>
                                </div>
                            </div>
                            <div class="row g-4">
                                <div class="col-md-12">
                                    <div class="form-floating form-floating-outline"><textarea id="contractor_address"
                                                                                               class="form-control required"
                                                                                               name="contractor_address"
                                                                                               type="text"
                                                                                               placeholder="Contractor address"></textarea>
                                        <label for="contractor_address">Contractor address *</label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>    <!-- / Content --> <?php include('partials/sticky-footer.php'); ?>
</div><!-- Content wrapper --><?php include('partials/footer.php'); ?>
