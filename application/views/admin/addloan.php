<?php include('partials/header.php'); ?>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Add New Loan
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Add New Loan</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
						<input type="hidden" name="hdncompanyid" id="hdncompanyid" value="<?php echo $company_id;?>" />
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
										<select name="companyname" id="companyname" class="form-control required">
											<option value="">Select Company</option>
											<?php foreach($companylist as $rowscompany){ ?> <option
											value="<?php echo $rowscompany->company_id; ?>">
											<?php echo $rowscompany->company_name; ?></option> <?php } ?>
										</select>
                                        <label for="companyname">Company Name</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
										<select name="financialinstitution" id="financialinstitution" class="form-control required">
											<option value="">Select Financial Institution</option>
											<?php foreach($financialinstitutelist as $rowsfinancialinstitute){ ?>
											<option
												value="<?php echo $rowsfinancialinstitute->financialinstituteid; ?>">
												<?php echo $rowsfinancialinstitute->financialinstitutename; ?>
											</option> <?php } ?>
										</select>
                                        <label for="financialinstitution">Financial Institution</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
										<select name="project" id="project" class="form-control required">
											<option value="">Select Project</option>
											<?php foreach($projectlist as $rowsproject){ ?> <option
											value="<?php echo $rowsproject->projectid; ?>">
											<?php echo $rowsproject->project_name; ?></option> <?php } ?>
										</select>
                                        <label for="project">Project</label>
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="number" class="form-control required" id="loanamount" placeholder="Loan Amount" name="loanamount" aria-label="Loan Amount"/>
                                        <label for="loanamount">Loan Amount</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="number" class="form-control required" id="interestrate" placeholder="Interest rate" name="interestrate" aria-label="Interest rate"/>
                                        <label for="interestrate">Interest rate</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="number" class="form-control required" id="period" placeholder="Period" name="period" aria-label="Period"/>
                                        <label for="period">Period</label>
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="startdate" placeholder="First Date" name="firstdate" aria-label="First Date"/>
                                        <label for="startdate">First Date</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="enddate" placeholder="End Date" name="enddate" aria-label="End Date"/>
                                        <label for="enddate">End Date</label>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="number" class="form-control required" id="redemptionamount" placeholder="Redemption Amount" name="redemptionamount" aria-label="Redemption Amount"/>
                                        <label for="redemptionamount">Redemption Amount</label>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

<script src="adminassets/js/formpickers.js"></script>

