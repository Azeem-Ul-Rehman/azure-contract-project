<?php include('partials/header.php'); ?>

<div class="content-wrapper">


    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Purchase Orders
        </h4>
        <form id="my_form" name="my_form" method="post" action="" enctype="multipart/form-data">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div
                                class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Add Order</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary" value="Save" name="savebtn">Submit</button>

                            </div>
                        </div>

                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">

                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="row">

                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-success alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row g-4 mb-4">

                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <select
                                                id="suppliers" name="suppliers" class="form-select required"
                                                data-allow-clear="true">
                                            <option value="">Select Suppliers</option>
                                            <?php
                                            if (!empty($supplierslist)) {
                                                foreach ($supplierslist as $rowssuppliers) {
                                                    ?>
                                                    <option value="<?php echo $rowssuppliers->suppliers_id; ?>"><?php echo $rowssuppliers->suppliers_name; ?></option>

                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <label for="suppliers">Suppliers</label>
                                    </div>


                                </div>
                                <div class="col-md-4">

                                    <div class="form-floating form-floating-outline">
                                        <select

                                                id="projects" name="projects" class="form-select required"
                                                data-allow-clear="true">
                                            <option value="">Select project</option>
                                            <?php
                                            if (!empty($projectlist)) {
                                                foreach ($projectlist as $rowsprojects) {
                                                    ?>
                                                    <option
                                                            value="<?php echo $rowsprojects->projectid; ?>"><?php echo $rowsprojects->project_name; ?></option>

                                                    <?php
                                                }
                                            }

                                            ?>
                                        </select>
                                        <label for="projects">Projects</label>
                                    </div>


                                </div>
                                <div class="col-md-4">


                                    <div class="form-floating form-floating-outline">
                                        <select
                                                id="contractors" name="contractors" class="form-select required"
                                                data-allow-clear="true">
                                            <option value="">Select Contractor</option>
                                            <?php
                                            if (!empty($contractorslist)) {
                                                foreach ($contractorslist as $rowscontractors) {
                                                    ?>
                                                    <option
                                                            value="<?php echo $rowscontractors->contractor_id; ?>"><?php echo $rowscontractors->contractor_name; ?></option>

                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <label for="contractors">Contractors</label>
                                    </div>


                                </div>

                            </div>

                            <div class="row g-4 mb-4">
                                <input type="hidden" name="paymentmethod" id="paymentmethod" value="other">
                                <label for="projectsection">Project Section</label>
                                <?php
                                $projectSections = ['Foundation', 'Doors', 'Roof', 'Windows'];
                                foreach ($projectSections as $projectSection) { ?>
                                    <div class="col-3 col-lg-3">
                                        <div class="form-group1" style="margin-left:20px;">
                                            <input class="form-check-input" name="projectsection[]" type="checkbox"
                                                   value="<?php echo $projectSection; ?>"
                                                   id="projectsection<?php echo $$projectSection; ?>">
                                            <label class="form-check-label"
                                                   for="projectsection<?php echo $projectSection; ?>"><?php echo $projectSection; ?></label>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row g-4 mb-4">
                                <div class="col-md-6">
                                    <div class="form-floating form-floating-outline">
                                        <input id="perchaseorderimage" class="form-control"
                                               name="perchaseorderimage"
                                               onchange="return ValidateFileUpload()" type="file">
                                        <label for="perchaseorderimage">Purchase Order</label>
                                    </div>


                                </div>
                            </div>

                            <div class="row mt-4 g-2">

                                <h5 class="card-title mb-4">Purchase Order Items</h5>
                                <div id="inputFormRow">
                                    <div class="form-group mb-3" style="display: inline-flex;width: 100%">

                                        <input style="width:40%"
                                               type="text"
                                               placeholder="Item Name" name="itemname[]"
                                               id="itemname" class="form-control"/>&nbsp;&nbsp;

                                        <input style="width:40%"
                                               type="number"
                                               placeholder="Item Quantity" name="qty[]" id="qty"
                                               class="form-control"/>&nbsp;&nbsp;

                                        <div class="input-group-append">
                                            <button id="removeRow" type="button"
                                                    class="btn btn-danger">Remove
                                            </button>
                                        </div>

                                    </div>
                                </div>
                                <div id="newRowpartner"></div>

                            </div>
                            <div class="row g-4 mb-4">
                                <div class="col-md-2">
                                    <button id="addRow" type="button" class="btn btn-info">Add New Item</button>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>


    <?php include('partials/sticky-footer.php'); ?>
</div>



<?php include('partials/footer.php'); ?>

<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('perchaseorderimage');
        var FileUploadPath = fuData.value;
        if (FileUploadPath === '') {
            Swal.fire("Please upload an image");
        } else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
            if (Extension === "gif" || Extension === "png" || Extension === "bmp" || Extension === "jpeg" || Extension === "jpg" || Extension === "pdf"  || Extension === "doc"  || Extension === "docx" || Extension === "xls") {
                if (fuData.files && fuData.files[0]) {}
            } else {
                Swal.fire("Photo only allows file types of GIF, PNG, JPG, JPEG, BMP, PDF, DOC, DOCX, XLS. ");
            }
        }
    }

</script>
<script>
    $(document).ready(function () {

        $("#addRow").click(function () {
            var html = '';
            html += '<div id="inputFormRow">';
            html += '<div class="form-group mb-3" style="display: inline-flex;width: 100%">';
            html += '<input style="width:40%" type="text" placeholder="Item Name" name="itemname[]" id="itemname" class="form-control" />&nbsp;&nbsp;';
            html += '<input style="width:40%" type="number" placeholder="Item Quantity" name="qty[]" id="qty" class="form-control" />&nbsp;&nbsp;';
            html += '<div class="input-group-append">';
            html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
            html += '</div>';
            html += '</div>';

            $('#newRowpartner').append(html);
        });


        $(document).on('click', '#removeRow', function () {
            $(this).closest('#inputFormRow').remove();
        });
    });
</script>

