<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CheckList PDF</title>
    <style>
        @page {
            margin: 10px 20px 10px 20px;
        }

        @page {
            size: A5 margin: 5px;
        }

    </style>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&family=Oswald:wght@400;700&family=Oswald:wght@500&display=swap"
          rel="stylesheet">

    <style>

        #pdf-content {
            width: 100%;
        }

        h2 {
            text-align: center;
        }

        table {
            border: 1px solid black;
            border-spacing: 0;
            margin: auto;
        }

        td, th {
            border: 1px solid #000000;
            padding: 5px;
        }

        .main-door-head {
            background-color: #f2ca9c;
        }

        #content {
            padding: 0 2rem;
        }

        .contractors {
            display: inline-flex;
            justify-content: center;
            align-items: center;
            /*gap: 20rem;*/
            margin-top: 4rem;
        }

        .signature-continer div {
            border-bottom: 1px dashed #000000;
            width: 200px;
        }

        .signature-continer label {
            text-align: center;
        }
    </style>
</head>
<body>
<div id="content">
    <h2>House Inspection Checklist</h2>
    <table id="pdf-content" style="border: none;">
        <tbody style="border: none;">
            <tr style="border: none;">
                <td style="border: none;">
                    <strong style="float: left;">Project: </strong>
                    <?php
                        $where = array('projectid' => $editsellproperty->project_id);
                        $projectDetail = $this->adminmodel->getSingle(TBLPROJECTS, $where); 
                    ?>
                    &nbsp;&nbsp;<?php echo $projectDetail->project_name;?> 
                </td>
            </tr>
            <tr style="border: none;">
                <td style="border: none;">
                    <strong style="float: left;">Lot Nr / Address: </strong>
                    &nbsp;&nbsp;<?php echo $editsellproperty->address;?> 
                </td>
            </tr>
            <tr style="border: none;">
                <td style="border: none;">
                    <strong style="float: left;">Buyer Name: </strong>
                    &nbsp;&nbsp;<?php echo $editsellproperty->buyer_name;?> 
                </td>
            </tr>
            <tr style="border: none;">
                <tr>
                    <td style="border: none;">
                        <strong >Agreement Date:</strong>
                        <span><?php echo date('d-m-Y', strtotime($editsellproperty->agreement_date)); ?></span>
                    </td>
                    <td style="border: none;">
                        <strong >Delivery Date:</strong>
                        <span ><?php echo date('d-m-Y', strtotime($editsellproperty->delivery_date)); ?></span>
                    </td>
                </tr>
            </tr>
        </tbody>
    </table>
    <br>
    <table id="pdf-content">
        <tbody>
        <?php if ($sectionList != null && !empty($sectionList)) {
            $SectionName = "";
            foreach ($sectionList as $key => $sectionData) {
                ?>
                <?php if ($SectionName != $sectionData->section_sort) { ?>
                    <tr class="main-door-head">
                        <th colspan="5"><?php echo $sectionData->section; ?></th>
                    </tr>
                    <?php $SectionName = $sectionData->section_sort;
                } ?>

                <tr>
                    <td><?php echo $key +1; ?></td>
                    <td><?php echo $sectionData->item; ?></td>
                    <td style="width:75px;">
                        <div class="form-group1">
                            <input <?php if ($sectionData->item_condition == "Goed") {
                                echo "checked";
                            } ?> class="form-check-input" type="radio" value="Goed" id="radioGoed" disabled>
                            <label class="form-check-label" for="inlineCheckbox1">Goed</label>
                        </div>
                    </td>
                    <td style="width:75px;">
                        <div class="form-group1">
                            <input <?php if ($sectionData->item_condition == "Slecht") {
                                echo "checked";
                            } ?> class="form-check-input" type="radio" value="Slecht" id="radiSlecht" disabled>
                            <label class="form-check-label" for="inlineCheckbox1">Slecht</label>
                        </div>
                    </td>
                    <td><?php echo $sectionData->remarks ?? ''; ?></td>
                </tr>
            <?php }
        } ?>

        </tbody>
    </table>

    <table cellspacing="0" cellpadding="0" style="width: 100%; text-align: left; margin-top: 4rem;border: none">
        <tr style="vertical-align: top;">
            <td style="text-align: left; width: 80%;border: none">
                <label>Buyer:</label><br>
                <?php if (!is_null($editsellproperty->buyer_signature)) { ?>
                    <img id="saveSignatureSeller" style="width:180px;height:80px;"
                         src="data:image/jpeg;base64,<?php echo base64_encode(file_get_contents('uploads/signature/' . $editsellproperty->buyer_signature)); ?>"
                         alt="Saved image png"/>
                <?php } ?><br>
                <div class="signature">----------------------------------</div>
            </td>
            <td style="text-align: left; width: 20%;border: none">
                <label>Seller:</label><br>
                <?php if (!is_null($editsellproperty->seller_signature)) { ?>
                <img id="saveSignatureSeller" style="width:180px;height:80px;"
                     src="data:image/jpeg;base64,<?php echo base64_encode(file_get_contents('uploads/signature/' . $editsellproperty->seller_signature)); ?>"
                     alt="Saved image png"/>
                <?php } ?>
                <br>
                <div class="signature">----------------------------------</div>
            </td>
        </tr>
    </table>
</div>
<?php if ($fileType == "print") { ?>
    <script>
        setTimeout(function () {
            prepareAndPrint();
        }, 500);

        function prepareAndPrint() {
            const prtHtml = document.getElementById('content').innerHTML;
            let stylesHtml = '';
            for (const node of [...document.querySelectorAll('link[rel="stylesheet"], style')]) {
                stylesHtml += node.outerHTML;
            }

            const iframe = document.createElement('iframe');
            iframe.style.display = 'none';
            document.body.appendChild(iframe);

            iframe.onload = function () {
                const iframeDoc = iframe.contentWindow.document;
                iframeDoc.write(`<!DOCTYPE html>
                    <html>
                    <head>
                        ${stylesHtml}
                    </head>
                    <body>
                        ${prtHtml}
                    </body>
                    </html>`);
                iframeDoc.close();

                iframe.contentWindow.onafterprint = function () {
                    document.body.removeChild(iframe);
                };

                iframe.contentWindow.focus();
                iframe.contentWindow.print();
                iframe.contentWindow.close();
            };

            iframe.src = 'about:blank'; // Trigger the iframe's onload event
        }
    </script>
<?php } ?>
</body>
</html>
