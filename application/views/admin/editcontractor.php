<?php include('partials/header.php'); ?><!-- partial -->
<style>    #contractor_assignproject-error {
        margin-top: 53px !important;
        position: absolute !important;
    }</style>
<div class="content-wrapper">    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y"><h4 class="py-3 mb-4"><span
                    class="text-muted fw-light">Home/</span> Contractors </h4>
        <form id="my_form" name="my_form" method="post" action="">            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Edit Contractor</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type --> <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible"><a href="#" class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>                        <?php } ?>
                        <div class="card-body"><input id="contractor_id"
                                                      value="<?php echo $contractors->contractor_id; ?>"
                                                      class="form-control" name="hdncontractor" type="hidden">
                            <div class="row g-4 mb-4 g-4">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><input type="text"
                                                                                            id="contractor_name"
                                                                                            name="contractor_name"
                                                                                            class="form-control required"
                                                                                            value="<?php echo $contractors->contractor_name; ?>"
                                                                                            placeholder="Contractor Name"/>
                                        <label for="contractor_name">Contractor Name *</label></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><input id="contractor_phone"
                                                                                            class="form-control required"
                                                                                            name="contractor_phone"
                                                                                            type="text"
                                                                                            value="<?php echo $contractors->contractor_phone; ?>"
                                                                                            placeholder="Contractor Phone No">
                                        <label for="contractor_phone">Contractor Phone No *</label></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><select id="contractor_type"
                                                                                             name="contractor_type"
                                                                                             class="select2 form-select required"
                                                                                             data-allow-clear="true">                                            <?php if (!empty($contractortypes)) { ?><?php foreach ($contractortypes as $rowscontractors) { ?>
                                                <option <?php if ($rowscontractors->contractortype == $contractors->contractor_type) { ?> selected="selected" <?php } ?>
                                                        value="<?php echo $rowscontractors->contractortype; ?>"><?php echo $rowscontractors->contractortype; ?></option>                                                <?php }
                                            } ?>                                        </select> <label
                                                for="contractor_type">Contractor Type</label></div>
                                </div>                                <!-- /Second column -->
                            </div>
                            <div class="row g-4 mb-4 g-4">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><input id="contractor_person"
                                                                                            class="form-control required"
                                                                                            name="contractor_person"
                                                                                            type="text"
                                                                                            value="<?php echo $contractors->contractor_person; ?>"
                                                                                            placeholder="Contractor persons">
                                        <label for="contractor_person">Contractor persons *</label></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><input type="number"
                                                                                            id="contractor_cribno"
                                                                                            name="contractor_cribno"
                                                                                            class="form-control required"
                                                                                            value="<?php echo $contractors->contractor_cribno; ?>"
                                                                                            placeholder="CRIB No."/>
                                        <label for="contractor_cribno">CRIB No. *</label></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline"><select
                                                id="contractor_assignproject" name="contractor_assignproject[]"
                                                class="select2 form-select required" multiple
                                                data-allow-clear="true">
                                            <?php foreach ($projectslist as $rowsval) { ?>

                                                <?php $selectedvalue = '';
                                                $loansarr = explode(',', $contractors->contractor_assignproject);
                                                if (in_array($rowsval->projectid, $loansarr)) {
                                                    $selectedvalue = "selected='selected'";
                                                } ?>
                                                <option <?php echo $selectedvalue; ?>
                                                        value="<?php echo $rowsval->projectid; ?>"><?php echo $rowsval->project_name; ?></option>                                            <?php } ?>
                                        </select> <label for="contractor_assignproject">Assigned Project</label></div>
                                </div>
                            </div>
                            <div class="row g-4 mb-4 g-4">
                                <div class="col-md-6">
                                    <div class="form-floating form-floating-outline"><input type="number"
                                                                                            id="contractor_kvkno"
                                                                                            name="contractor_kvkno"
                                                                                            class="form-control required"
                                                                                            value="<?php echo $contractors->contractor_kvkno; ?>"
                                                                                            placeholder="KVK No."/>
                                        <label for="contractor_kvkno">KVK No. *</label></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating form-floating-outline"><input id="contractor_payments"
                                                                                            class="form-control"
                                                                                            name="contractor_payments"
                                                                                            type="number" readonly
                                                                                            value="<?php echo $contractoramount; ?>"
                                                                                            placeholder="Contractor Payments">
                                        <label for="contractor_payments">Contractor Payments</label></div>
                                </div>
                            </div>
                            <div class="row g-4">
                                <div class="col-md-12">
                                    <div class="form-floating form-floating-outline"><textarea id="contractor_address"
                                                                                               class="form-control required"
                                                                                               name="contractor_address"
                                                                                               type="text"
                                                                                               placeholder="Contractor address"><?php echo $contractors->contractor_address; ?></textarea>
                                        <label for="contractor_address">Contractor address *</label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>    <!-- / Content --> <?php include('partials/sticky-footer.php'); ?>
</div><!-- Content wrapper --><?php include('partials/footer.php'); ?>
