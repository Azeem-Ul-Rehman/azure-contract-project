
<?php include('partials/header.php'); ?>
<!-- Content wrapper -->
<style>
    /* Absolute Center Spinner */
    .loading {
        position: fixed;
        z-index: 9999999999;
        height: 2em;
        width: 2em;
        overflow: show;
        margin: auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
    }

    /* Transparent Overlay */
    .loading:before {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));

        background: -webkit-radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));
    }

    /* :not(:required) hides these rules from IE9 and below */
    .loading:not(:required) {
        /* hide "loading..." text */
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        background-color: transparent;
        border: 0;
    }

    .loading:not(:required):after {
        content: '';
        display: block;
        font-size: 10px;
        width: 1em;
        height: 1em;
        margin-top: -0.5em;
        -webkit-animation: spinner 150ms infinite linear;
        -moz-animation: spinner 150ms infinite linear;
        -ms-animation: spinner 150ms infinite linear;
        -o-animation: spinner 150ms infinite linear;
        animation: spinner 150ms infinite linear;
        border-radius: 0.5em;
        -webkit-box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
        box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
    }

    /* Animation */

    @-webkit-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-moz-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-o-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
</style>
<div class="content-wrapper">
    <!-- Content -->
    <div class="loading" style="display: none">Loading</div>
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4"><span class="text-muted fw-light">Home /</span> All Sell Properties List</h4>

        <!-- Product List Table -->
        <div class="card">

            <div class="card-datatable table-responsive">
                <div class="row">
                    <div class="col-12">
                        <?php if ($this->session->flashdata('success')) { ?>

                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                            </div>

                        <?php } ?>

                    </div>
                    <div class="col-12">
                        <span id="success-msg"></span>
                    </div>
                </div>
                <table class="datatables-products table">
                    <thead class="table-light">
                    <tr>
                        <th></th>
                        <th>Project Name</th>
                        <th>Project Type</th>
                        <th>Kavel Nr. / Address</th>
                        <th>Real Estate</th>
                        <th>Buyer Name</th>
                        <th>Selling Price</th>
                        <th>Agreement Date</th>
                        <th>Delivery Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $usertype = $this->session->userdata('admin_usertype');
                    $where = array('usertype' => $usertype, 'menutab' => 'Sell Properties');
                    $chkvalied = $this->adminmodel->getSingle(SETTINGPERMISSION, $where);

                    if (!empty($sellPropertyList)) {
                        $i = 1;
                        foreach ($sellPropertyList as $perreq) {
                            $where = array('projectid' => $perreq->project_id);
                            $projectDetail = $this->adminmodel->getSingle(TBLPROJECTS, $where);
                            
                            $where = array('id' => $perreq->project_type_id);
                            $projectTypeDetail = $this->adminmodel->getSingle(PROJECTTYPE, $where);
                            ?>
                            <tr>
                                <td class="control sorting_1" tabindex="0" style=""></td>
                                <?php if ($chkvalied->allowfor_edit == 1 || $usertype == 1) { ?>
                                    <td>
                                        <?php echo $projectDetail->project_name; ?>
                                    </td>
                                <?php } else { ?>
                                    <td><a href="javascript:void(0);"><?php echo $projectDetail->project_name; ?></a>
                                    </td>
                                <?php } ?>
                                <td><?php echo $projectTypeDetail->projecttype; ?></td>
                                <td><?php echo $perreq->address; ?></td>
                                <td><?php echo $perreq->real_estate; ?></td>
                                <td><?php echo $perreq->buyer_name; ?></td>
                                <td>
                                    <span style="float:right;"><?php echo number_format($perreq->selling_price, 2, ".", ","); ?></span>
                                </td>
                                <td><?php echo date('d-m-Y', strtotime($perreq->agreement_date)); ?></td>
                                <td><?php echo date('d-m-Y', strtotime($perreq->delivery_date)); ?></td>
                                <td class="action-clm">
                                    <button class="btn btn-sm btn-icon dropdown-toggle hide-arrow"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="mdi mdi-dots-vertical me-2"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-end m-0" style="">
                                        <?php if ($chkvalied->allowfor_modify == 1 || $usertype == 1) { ?>
                                            <a href="administrator/editsellproperty?id=<?php echo $perreq->id; ?>"
                                               class="dropdown-item">Edit</a>
                                        <?php } ?>
                                        <?php if ($chkvalied->allowfor_edit == 1 || $usertype == 1) { ?>
                                            <a href="administrator/sellpropertydetail/?id=<?php echo $perreq->id; ?>"
                                               class="dropdown-item">View</a>
                                        <?php } ?>
                                        <?php if ($chkvalied->allow_delete == 1 || $usertype == 1) { ?>
                                            <a class="dropdown-item" href="javascript:void(0)"
                                               onclick="show_confirmdelte(<?php echo $perreq->id; ?>)">Delete
                                            </a>
                                        <?php } ?>
                                        <a href="administrator/propertychecklistpdf?id=<?php echo $perreq->id; ?>"
                                           class="dropdown-item">PDF</a>
                                        <a href="administrator/propertychecklistprint?id=<?php echo $perreq->id; ?>"
                                           class="dropdown-item">Print</a>
                                        <a id="email-invoice" href="javascript:void(0)"
                                           data-sell-id="<?php echo $perreq->id ?>"
                                           class="dropdown-item">Buyer Email</a>
                                    </div>
                                </td>
                            </tr>


                            <?php $i++;
                        }
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- / Content -->
    <?php include('partials/sticky-footer.php'); ?>
</div>
<style>
    .display-none {
        display: none !important;
    }
</style>
<?php if ($chkvalied->allowfor_add == 1 || $usertype == 1) {
    $display = '';
} else {
    $display = 'display-none';
}
?>

<!-- Content wrapper -->
<?php include('partials/footer.php'); ?>

<script>
    $(document).ready(function () {
        $('#startdate').on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
        $('#enddate').on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
        $('.datatables-products').DataTable({
            dom: '<"card-header d-flex border-top rounded-0 flex-wrap py-md-0"<"me-5 ms-n2"f><"d-flex justify-content-start justify-content-md-end align-items-baseline"<"dt-action-buttons d-flex align-items-start align-items-md-center justify-content-sm-center mb-3 mb-sm-0 gap-3"lB>>>t<"row mx-1"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            lengthMenu: [7, 10, 20, 50, 70, 100],
            buttons: [{
                extend: "collection",
                className: "btn btn-label-secondary dropdown-toggle me-3",
                text: '<i class="mdi mdi-export-variant me-1"></i><span class="d-none d-sm-inline-block">Export </span>',
                buttons: [{
                    extend: "print",
                    text: '<i class="mdi mdi-printer-outline me-1" ></i>Print',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                }, {
                    extend: "csv",
                    text: '<i class="mdi mdi-file-document-outline me-1" ></i>Csv',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                }, {
                    extend: "excel",
                    text: '<i class="mdi mdi-file-excel-outline me-1"></i>Excel',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                }, {
                    extend: "pdf",
                    text: '<i class="mdi mdi-file-pdf-box me-1"></i>Pdf',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                }, {
                    extend: "copy",
                    text: '<i class="mdi mdi-content-copy me-1"></i>Copy',
                    className: "dropdown-item",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                }]
            }, {
                text: '<i class="mdi mdi-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add Sell Property</span>',
                className: "add-new btn btn-primary ms-n1  <?php echo $display; ?>",
                action: function () {
                    window.location.href = "administrator/addsellproperty"
                }
            }],
        });
    });
</script>
<script>
    function show_confirmdelte(id) {

        Swal.fire({
            title: "Are you sure?",
            text: "You want to delete this sell property!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            customClass: {
                confirmButton: "btn btn-primary me-2 waves-effect waves-light",
                cancelButton: "btn btn-outline-secondary waves-effect"
            },
            buttonsStyling: !1
        }).then(function (e) {

            if (e.value) {
                location.href = "<?php echo base_url();?>administrator/deletesellproperty?id=" + id;
            } else {
                if (e.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: "Cancelled",
                        text: "Cancelled Suspension :)",
                        icon: "error",
                        customClass: {
                            confirmButton: "btn btn-success waves-effect"
                        }
                    })
                }
            }
        })

    }
</script>
<script>
    $(document).on('click', '#email-invoice', function () {
        var id = $(this).data('sell-id');
        $('.loading').show();
        $.ajax({

            url: '<?php echo $base_url; ?> administrator/propertychecklistemail?id=' + id,
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
            },
            complete: function () {
                $('.loading').hide();
                setTimeout(function () {
                    $('span#success-msg').html('');
                }, 4000);
            },
            success: function (json) {
                if (json['status'] === true) {
                    $('span#success-msg').html('<div class="alert alert-success alert-dismissible" role="alert">' + json["message"] + '</div>');

                } else {
                    $('span#success-msg').html('<div class="alert alert-danger alert-dismissible" role="alert">' + json["message"] + '</div>');

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('.loading').hide();
                $('span#success-msg').html('<div class="alert alert-danger alert-dismissible" role="alert">' + json["message"] + '</div>');


            }
        });
    });
</script>
