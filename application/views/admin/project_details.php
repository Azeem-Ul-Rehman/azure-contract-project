<?php include('partials/header.php'); ?>
<style>
    p {
        display: inline-flex;
    }
</style>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Project Details
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Project Details</h5>

                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row">
                                <!-- First column-->
                                <div class="col-md-12">
                                    <!-- Product Information -->

                                    <fieldset>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 145px;">
                                                    <label for="companyname"><b>Name : </b>
                                                    </label>

                                                </div>
                                                <div class="col-6 text-left">
                                                    <p><?php echo $editproject->project_name; ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 145px;">
                                                    <label for="companyname"><b>Description : </b>
                                                    </label>

                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo $editproject->projectdesc; ?>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 145px;">
                                                    <label for="companyname"><b>Type : </b>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo $editproject->projecttype; ?>
                                                    </p>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 145px;">
                                                    <label for="companyname"><b>Location : </b>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo $editproject->location; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 145px;">
                                                    <label for="companyname"><b>Start Date : </b>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <p> <?php echo date('d-m-Y', strtotime($editproject->startdate)); ?></p>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 145px;">
                                                    <label for="companyname"><b>End Date : </b>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo date('d-m-Y', strtotime($editproject->enddate)); ?></p>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 145px;">
                                                    <label for="companyname"><b>Estimated Cost : </b></label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo number_format($editproject->totalestimatedcost, 2, ".", ","); ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 145px;">
                                                    <label for="companyname"><b>Total Payment : </b></label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo number_format($totalpayment + floatval($supplierpayments), 2, ".", ","); ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 145px;">
                                                    <label for="companyname"><b>Total Balance : </b></label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo number_format($editproject->totalestimatedcost - $totalpayment - floatval($supplierpayments), 2, ".", ","); ?></p>
                                                </div>
                                            </div>
                                        </div>


                                    </fieldset>
                                    <div class="card mb-4">
                                        <div class="card-header p-0">
                                            <div class="nav-align-top">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect active"
                                                                role="tab"
                                                                data-bs-toggle="tab" data-bs-target="#tab-partners"
                                                                aria-controls="tab-partners" aria-selected="false"
                                                                tabindex="-1">
                                                            Partners
                                                        </button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect" role="tab"
                                                                data-bs-toggle="tab"
                                                                data-bs-target="#tab-financial-institute"
                                                                aria-controls="tab-financial-institute"
                                                                aria-selected="false"
                                                                tabindex="-1">
                                                            Financing
                                                        </button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect" role="tab"
                                                                data-bs-toggle="tab" data-bs-target="#tab-contractors"
                                                                aria-controls="tab-contractors" aria-selected="false"
                                                                tabindex="-1">
                                                            Contractors
                                                        </button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect" role="tab"
                                                                data-bs-toggle="tab" data-bs-target="#tab-payment"
                                                                aria-controls="tab-payment" aria-selected="false"
                                                                tabindex="-1">
                                                            Supplier Payments
                                                        </button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect" role="tab"
                                                                data-bs-toggle="tab" data-bs-target="#tab-documents"
                                                                aria-controls="tab-documents" aria-selected="false"
                                                                tabindex="-1">
                                                            Documents
                                                        </button>
                                                    </li>
                                                    <span class="tab-slider"
                                                          style="left: 198.3px; width: 127.65px; bottom: 0px;"></span>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content p-0">
                                                <div class="tab-pane fade active show" id="tab-partners"
                                                     role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Sno</th>
                                                                <th>Partners Name</th>
                                                                <th>Investment Amount</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $k = 1;
                                                            if (!empty($partners)) {
                                                                foreach ($partners as $rowspartners) {
                                                                    $where = array('partner_id' => $rowspartners->partnerid);
                                                                    $partnersval = $this->adminmodel->getSingle(TBLPARTNER, $where);

                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $k; ?></td>
                                                                        <td>
                                                                            <a href="administrator/partners_list"><?php echo $partnersval->partner_name; ?></a>
                                                                        </td>
                                                                        <td><?php echo number_format($rowspartners->investmentamount, 2, ".", ","); ?></td>

                                                                    </tr>
                                                                    <?php $k++;
                                                                }
                                                            } else {

                                                                ?>
                                                                <tr>
                                                                    <td colspan="3" style="text-align:center;">No Record
                                                                        Found
                                                                    </td>
                                                                </tr>

                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="tab-financial-institute" role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Sno</th>
                                                                <th>Financial Institute</th>
                                                                <th>Financed Amount</th>
                                                                <th>Repayment Period</th>
                                                                <th>Repayment Amount</th>
                                                                <!--<th>Actions</th>-->
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $j = 1;
                                                            $where = array('project' => $editproject->projectid, 'is_deleted' => 0);
                                                            $projectloans = $this->adminmodel->getwhere(TBLCOMPANYLOAN, $where);


                                                            if (!empty($projectloans)) {

                                                                foreach ($projectloans as $rowloans) {

                                                                    $where = array('financialinstituteid' => $rowloans->financialinstitution);
                                                                    $loansinst = $this->adminmodel->getwhere(TBLFINANCIALINSTITUTE, $where);

                                                                    foreach ($loansinst as $rowfininst) {
                                                                        $fininst = $rowfininst->financialinstitutename;
                                                                        $finamt = $rowfininst->financedamount;
                                                                        $finper = $rowfininst->repaymentperiod;
                                                                        $repampt = $rowfininst->repaymentamount;
                                                                    }

                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $j; ?></td>
                                                                        <td>
                                                                            <a href="administrator/financialinstitutionlist"><?php echo $fininst; ?></a>
                                                                        </td>
                                                                        <td><?php echo number_format($rowloans->loanamount, 2, ".", ","); ?></td>
                                                                        <td><?php echo $rowloans->period . " Months"; ?></td>
                                                                        <td><?php echo number_format($rowloans->redemptionamount, 2, ".", ","); ?></td>

                                                                    </tr>
                                                                    <?php $j++;
                                                                }
                                                            } else {
                                                                ?>

                                                                <tr>
                                                                    <td colspan="5" style="text-align:center;">No Record
                                                                        Found
                                                                    </td>
                                                                </tr>

                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="tab-contractors" role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Contractor Name</th>
                                                                <th>Project amount</th>
                                                                <th>Payments</th>
                                                                <th>Outstanding payment</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            if (!empty($contractors)) {
                                                                $n = 1;
                                                                foreach ($contractors as $rowscontractors) {

                                                                    $where = array('contractor_id' => $rowscontractors->contractorid);
                                                                    $contractorsval = $this->adminmodel->getSingle(TBLCONTRACTORS, $where);

                                                                    $where = array('contractorid' => $rowscontractors->contractorid, 'project' => $editproject->projectid);
                                                                    $paymentdetailsval = $this->adminmodel->getSingle(TBLPAYMENTS, $where);

                                                                    $name= 'Contractor';
                                                                    $sql = $this->db->query('select SUM(Amount) as totalpayment from ' . TBLPAYMENTS . ' where project="' . $editproject->projectid . '" and contractorid=' . $rowscontractors->contractorid . ' and paidto = "'.$name.'" and is_deleted=0');
                                                                    $getrows = $sql->row();
                                                                    $totalpayment = $getrows->totalpayment ?? 0;
                                                                    if ($contractorsval) {
                                                                        ?>
                                                                        <tr>
                                                                            <td>
                                                                                <a href="administrator/contractors_list"><?php echo $contractorsval->contractor_name; ?></a>
                                                                            </td>
                                                                            <td><?php echo number_format($rowscontractors->projectamount, 2, ".", ","); ?></td>
                                                                            <td><?php echo number_format($totalpayment, 2, ".", ","); ?></td>
                                                                            <td><?php echo number_format($rowscontractors->projectamount - $totalpayment, 2, ".", ",");//echo $rowscontractors->outstandingpayment;
                                                                                ?></td>

                                                                        </tr>
                                                                    <?php }
                                                                    $n++;
                                                                }

                                                            } else {
                                                                ?>

                                                                <tr>
                                                                    <td colspan="5" style="text-align:center;">No Record
                                                                        Found
                                                                    </td>
                                                                </tr>

                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="tab-payment" role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Payment Amount</th>
                                                                <th>Method</th>
                                                                <th>Supplier</th>
                                                                <th>Contractor</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            if (!empty($payments)) {
                                                                $k = 1;
                                                                foreach ($payments as $rowspayments) {
                                                                    $where = array('suppliers_id' => $rowspayments->suppliers);
                                                                    $projectsuppliers = $this->adminmodel->getSingle(TBLSUPPLIERS, $where);

                                                                    $where = array('contractor_id' => $rowspayments->contractorsid);
                                                                    $projectcontractors = $this->adminmodel->getSingle(TBLCONTRACTORS, $where);
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo date('d-m-Y', strtotime($rowspayments->paymentdate)); ?></td>
                                                                        <td><?php echo number_format($rowspayments->invoiceamount, 2, ".", ","); ?></td>
                                                                        <td><?php echo $rowspayments->paymentmethod; ?></td>
                                                                        <td><?php echo $projectsuppliers->suppliers_name; ?></td>
                                                                        <td><?php echo $projectcontractors->contractor_name; ?></td>

                                                                    </tr>
                                                                    <?php $k++;
                                                                }
                                                            } else {
                                                                ?>

                                                                <tr>
                                                                    <td colspan="5" style="text-align:center;">No Record
                                                                        Found
                                                                    </td>
                                                                </tr>

                                                            <?php } ?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="tab-documents" role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Sno</th>
                                                                <th>Project Name</th>
                                                                <th>Documents Name</th>
                                                                <th>Documents</th>
                                                                <th>Date</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            if (!empty($documents)) {
                                                                $n = 1;
                                                                foreach ($documents as $rowsdocuments) {

                                                                    $where = array('projectid' => $rowsdocuments->projectid);
                                                                    $projectdetails = $this->adminmodel->getSingle(TBLPROJECTS, $where);

                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $n; ?></td>
                                                                        <td>
                                                                            <a href="administrator/project_details/?projectid=<?php echo $rowsdocuments->projectid; ?>"><?php echo $projectdetails->project_name; ?></a>
                                                                        </td>
                                                                        <td><?php echo $rowsdocuments->documentname; ?></td>

                                                                        <?php
                                                                        $getimgtype = explode('.', $rowsdocuments->documentfile);
                                                                        if ($getimgtype[1] == 'jpg' || $getimgtype[1] == 'png' || $getimgtype[1] == 'jpeg' || $getimgtype[1] == 'gif') {

                                                                            ?>
                                                                            <td>
                                                                                <img id="image-open"
                                                                                     src="uploads/documents/<?php echo $rowsdocuments->documentfile; ?>"
                                                                                     style="width:50px;"/>
                                                                            </td>


                                                                            <?php
                                                                        } else if ($getimgtype[1] == 'docs') {

                                                                            echo '<td><a href="uploads/documents/' . $rowsdocuments->documentfile . '" download> <img src="uploads/wordimg.png" style="width:50px;"  /></a></td>';

                                                                        } else if ($getimgtype[1] == 'xlsx' || $getimgtype[1] == 'csv') {

                                                                            echo '<td><a href="uploads/documents/' . $rowsdocuments->documentfile . '"  download> <img src="uploads/excelimg.png" style="width:50px;" /></a></td>';

                                                                        } else if ($getimgtype[1] == 'pdf') {


                                                                            echo '<td><a href="uploads/documents/' . $rowsdocuments->documentfile . '"  download> <img src="uploads/pdfimg.png" style="width:50px;" /></a></td>';

                                                                        } else {

                                                                            echo '<td><a href="uploads/documents/' . $rowsdocuments->documentfile . '" download> <img src="uploads/wordimg.png" style="width:50px;"  /></a></td>';

                                                                            ?>


                                                                        <?php } ?>


                                                                        <td><?php echo date('d-m-Y', strtotime($rowsdocuments->addeddate)); ?></td>
                                                                    </tr>
                                                                    <?php $n++;
                                                                }
                                                            } else {
                                                                ?>
                                                                <tr>
                                                                    <td colspan="5" style="text-align:center;">No Record
                                                                        Found
                                                                    </td>
                                                                </tr>

                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /Second column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->
<div class="modal fade" id="image-modal" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg modal-simple modal-enable-otp modal-dialog-centered">
        <div class="modal-content p-3 p-md-5">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center mb-4">
                    <h3 class="mb-2">Document Image</h3>
                </div>
                <div class="text-center">
                    <img src="" alt="img" class="img-fluid image-src" style="width: 50%">
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('partials/footer.php'); ?>
<script>
    $(document).on('click', '#image-open', function () {
        var src = $(this).attr('src');
        $('.image-src').attr('src', src);
        $('#image-modal').modal('show');
    })
</script>
