<?php include('partials/header.php'); ?>
<!-- partial -->
<!-- Content wrapper -->
<!-- Content wrapper -->
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        /* display: none; <- Crashes Chrome on hover */
        -webkit-appearance: none;
        margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
    }

    input[type=number] {
        -moz-appearance:textfield; /* Firefox */
    }
</style>
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Add Project
        </h4>
        <form id="my_form" name="my_form" method="post" action="" enctype="multipart/form-data">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Add New Project</h5>
                            <div class="action-btns">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="project_name"
                                               placeholder="Project Name" name="project_name"
                                               aria-label="Project Name"/>
                                        <label for="project_name">Project Name</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select name="companyname" id="companyname" class="form-control required">
                                            <option value="">Select Company</option>
                                            <?php foreach ($companylist as $rowscompany) { ?>
                                                <option value="<?php echo $rowscompany->company_id; ?>"><?php echo $rowscompany->company_name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="companyname">Company Name</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <select id="projecttype" class="form-control required" name="projecttype">
                                            <?php if (!empty($projecttypes)) { ?>
                                                <?php foreach ($projecttypes as $rowsprojecttype) { ?>
                                                    <option value="<?php echo $rowsprojecttype->projecttype; ?>"><?php echo $rowsprojecttype->projecttype; ?></option>
                                                <?php }
                                            }
                                            ?>
                                        </select>
                                        <label for="projecttype">Project Type</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <textarea id="projectdesc" class="form-control" name="projectdesc"
                                                  placeholder="Description" aria-label="Description"></textarea>
                                        <label for="projectdesc">Description</label>
                                    </div>
                                </div>
                                <!--								<div class="col-md-6">-->
                                <!--                                    <div class="form-floating form-floating-outline mb-4">-->
                                <!--										<textarea id="projectdetails" class="form-control" name="projectdetails" placeholder="Details" aria-label="Details"></textarea>-->
                                <!--                                        <label for="projectdetails">Details</label>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="location"
                                               placeholder="Location" name="location" aria-label="Location"/>
                                        <label for="location">Location</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="startdate"
                                               placeholder="Start date" name="startdate" aria-label="Start date"/>
                                        <label for="startdate">Start date</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control required" id="enddate"
                                               placeholder="End date" name="enddate" aria-label="End date"/>
                                        <label for="enddate">End date</label>
                                    </div>
                                </div>
                            </div>
                            <!--							<div class="row g-4 mb-4">-->
                            <!--								<label for="loans">Loans</label>-->
                            <!--								--><?php //foreach($loanlist as $rowloan){ ?><!-- -->
                            <!--									<div class="col-md-4">-->
                            <!--										<div class="form-group1" style="margin-left:20px;">-->
                            <!--											<input class="form-check-input" name="loans[]" type="checkbox" value="-->
                            <?php //echo $rowloan->financialinstitutename; ?><!--" id="partnerid-->
                            <?php //echo $rowloan->financialinstituteid;?><!--">-->
                            <!--											<label class="form-check-label" for="partnerid-->
                            <?php //echo $rowloan->financialinstituteid;?><!--">-->
                            <?php //echo $rowloan->financialinstitutename; ?><!--</label>-->
                            <!--										</div>-->
                            <!--									</div>-->
                            <!--								--><?php //} ?>
                            <!--							</div>-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="number" class="form-control required" id="totalestimatedcost"
                                               placeholder="Total Estimated Cost" name="totalestimatedcost"
                                               aria-label="Total Estimated Cost"/>
                                        <label for="totalestimatedcost">Total Estimated Cost</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating form-floating-outline mb-4">
                                        <input type="number" class="form-control required" id="contractoramount"
                                               placeholder="Contractor Amount" name="contractoramount"
                                               aria-label="Contractor Amount"/>
                                        <label for="contractoramount">Contractor Amount</label>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-floating form-floating-outline mb-4" style=" pointer-events: none;
            opacity: 1.4;">
                                        <input type="number" class="form-control" id="totalpayment"
                                               style="background: #d3d3d34f !important;border: #d3d3d34f !important;"
                                               placeholder="Total Payment" name="totalpayment" value="0"
                                               readonly="readonly" aria-label="Total Payment"/>
                                        <label for="totalpayment" style="color:#636578 !important;">Total Payment</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating form-floating-outline mb-4" style=" pointer-events: none;
            opacity: 1.4;">
                                        <input type="number" readonly="readonly" class="form-control" id="ballence"
                                               placeholder="Balance" name="ballence"  value="0"    style="background: #d3d3d34f !important;border: #d3d3d34f !important;" aria-label="Balance"/>
                                        <label for="ballence"  style="color:#636578 !important;">Balance</label>
                                    </div>
                                </div>
                                <!--                                <div class="col-md-4">-->
                                <!--                                    <div class="form-floating form-floating-outline mb-4">-->
                                <!--                                        <input type="number" class="form-control " id="estimatedprofit" placeholder="Estimated profit" name="estimatedprofit" aria-label="Estimated profit"/>-->
                                <!--                                        <label for="estimatedprofit">Estimated profit</label>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                                <!--								<div class="col-md-4">-->
                                <!--                                    <div class="form-floating form-floating-outline mb-4">-->
                                <!--                                        <input type="number" class="form-control " id="actualprofit" placeholder="Actual profit" name="actualprofit" aria-label="Actual profit"/>-->
                                <!--                                        <label for="actualprofit">Actual profit</label>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </div>
                            <!-- Partner Add -->
                            <div id="newRowpartner">
                                <div class="row mt-4 g-2" id="inputFormRow">
                                    <?php
                                    if (!empty($partners)) {
                                        $partnersdropdown = '<option value="">Select Partner</option>';
                                        foreach ($partners as $rowspartners) {
                                            $partnersdropdown .= '<option value="' . $rowspartners->partner_id . '">' . $rowspartners->partner_name . '</option>';
                                        }

                                    }

                                    ?>
                                    <div class="col-md-5">
                                        <div class="form-floating form-floating-outline form-floating-select2">
                                            <select name="partnername[]" id="partnername" class="form-control">
                                                <?php echo $partnersdropdown; ?>
                                            </select>
                                            <label for="partnername">Partner</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-floating form-floating-outline">
                                            <input type="number" name="investmentamount[]" id="investmentamount"
                                                   class="form-control m-input" placeholder="Investment Amount"
                                                   autocomplete="off" aria-label="Investment Amount">
                                            <label for="investmentamount">Investment Amount</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button id="removeRow" type="button" class="btn btn-danger">Remove</button>
                                    </div>

                                </div>
                            </div>
                            <div class="row mt-4 mb-4 mt-4">
                                <div class="action-btns">
                                    <button id="addRow" type="button" class="btn btn-info">Add Partner to Project
                                    </button>
                                </div>
                            </div>
                            <!-- Contractors Add -->
                            <div id="newRowContractors">
                                <div class="row mt-4 g-2 inputFormRowContractors1" id="inputFormRowContractors">
                                    <?php
                                    if (!empty($contractors)) {
                                        $contractorsdropdown = '<option value="">Select Contractors</option>';
                                        foreach ($contractors as $rowscontractors) {
                                            $contractorsdropdown .= '<option value="' . $rowscontractors->contractor_id . '">' . $rowscontractors->contractor_name . '</option>';
                                        }

                                    }

                                    ?>
                                    <div class="col-md-4">
                                        <div class="form-floating form-floating-outline form-floating-select2 ">
                                            <select name="contractors[]" id="contractors"
                                                    class="form-control contractors1">
                                                <?php echo $contractorsdropdown; ?>
                                            </select>
                                            <label for="contractors">Contractors</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-floating form-floating-outline">
                                            <input type="number" name="projectamount[]" id="projectamount"
                                                   class="form-control m-input projectamount1"
                                                   onchange="calculatetotalProjectpayment('1');"
                                                   onblur="calculatetotalProjectpayment('1');"
                                                   placeholder="Project Amount" autocomplete="off"
                                                   aria-label="Project Amount">
                                            <label for="projectamount">Project Amount</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-floating form-floating-outline" style="pointer-events: none;opacity: 1.4;">
                                            <input type="number" value="0" readonly name="payments[]" id="payments"
                                                   class="form-control m-input contractorpaymentcls payments1"
                                                   style="background: #d3d3d34f !important;border: #d3d3d34f !important;"
                                                   placeholder="Payments" autocomplete="off" aria-label="Payments"
                                                   onchange="calculatetotalpayment();"
                                                   onblur="calculatetotalpayment();">
                                            <label for="payments" style="color:#636578 !important;">Payments</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="pointer-events: none;opacity: 1.4;">
                                        <div class="form-floating form-floating-outline">
                                            <input type="number" name="outstandingpayment[]" id="outstandingpayment"
                                                   class="form-control m-input outstandingpayment1"
                                                   style="background: #d3d3d34f !important;border: #d3d3d34f !important;"
                                                   placeholder="Outstanding Amount" autocomplete="off"
                                                   value="0"
                                                   aria-label="Outstanding Amount">
                                            <label for="outstandingpayment" style="color:#636578 !important;">Outstanding Amount</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button id="removecontractors" type="button" class="btn btn-danger">Remove
                                        </button>
                                    </div>

                                </div>
                            </div>
                            <div class="row mt-4 mb-4 mt-4">
                                <div class="action-btns">
                                    <button id="addRowcontractors" type="button" class="btn btn-info">Add Contractor to
                                        Project
                                    </button>
                                </div>
                            </div>
                            <!-- Document Add -->
                            <div id="newRowdocuments">
                                <div class="row mt-4 g-2" id="inputFormdocuments">
                                    <div class="col-md-5">
                                        <div class="form-floating form-floating-outline">
                                            <input type="text" name="documentname[]" id="documentname"
                                                   class="form-control m-input" placeholder="Document Name"
                                                   autocomplete="off" aria-label="Document Name">
                                            <label for="documentname">Document Name</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-floating form-floating-outline">
                                            <input type="file" name="filename[]" id="filename"
                                                   class="form-control m-input" placeholder="Upload Document"
                                                   autocomplete="off" aria-label="Upload Document">
                                            <label for="filename">Upload Document</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button id="removedocuments" type="button" class="btn btn-danger">Remove
                                        </button>
                                    </div>

                                </div>
                            </div>
                            <div class="row mt-4 mb-4 mt-4">
                                <div class="action-btns">
                                    <button id="addRowdocument" type="button" class="btn btn-info">Add Document to
                                        Project
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>
<script src="adminassets/js/formpickers.js"></script>

<script>

    $(document).ready(function () {
        // add row
        $("#addRow").click(function () {
            var html = '';
            html += '<div class="row mt-4 g-2" id="inputFormRow">';
            html += '<div class="col-md-5">';
            html += '<div class="form-floating form-floating-outline">';
            html += '<select name="partnername[]" id="partnername" class="form-control">';
            html += '<?php echo $partnersdropdown; ?>';
            html += '</select>';
            html += '<label for="partnername">Partner</label>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-md-5">';
            html += '<div class="form-floating form-floating-outline">';
            html += '<input type="number" name="investmentamount[]" id="investmentamount" class="form-control m-input" placeholder="Investment Amount" autocomplete="off" aria-label="Investment Amount">';
            html += '<label for="investmentamount">Investment Amount</label>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-md-2">';
            html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
            html += '</div>';
            html += '</div>';

            $('#newRowpartner').append(html);
        });

        // remove row
        $(document).on('click', '#removeRow', function () {
            $(this).closest('#inputFormRow').remove();
        });


        var index = 1;


        $("#addRowcontractors").click(function () {
            index = index + 1;
            var html = '';
            html += '<div class="row mt-4 g-2 inputFormRowContractors' + index + '" id="inputFormRowContractors">';
            html += '<div class="col-md-4">';
            html += '<div class="form-floating form-floating-outline">';
            html += '<select name="contractors[]" id="contractors" class="form-control contractors' + index + '">';
            html += '<?php echo $contractorsdropdown; ?>';
            html += '</select>';
            html += '<label for="contractors">Contractors</label>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-md-2">';
            html += '<div class="form-floating form-floating-outline">';
            html += '<input type="number" name="projectamount[]" id="projectamount" class="form-control m-input projectamount' + index + '" placeholder="Project Amount" autocomplete="off" aria-label="Project Amount" onchange="calculatetotalProjectpayment(' + index + ');" onblur="calculatetotalProjectpayment(' + index + ');">';
            html += '<label for="projectamount">Project Amount</label>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-md-2">';
            html += '<div class="form-floating form-floating-outline" style="pointer-events: none;opacity: 1.4;">';
            html += '<input type="number"  style="background: #d3d3d34f !important;border: #d3d3d34f !important;" name="payments[]" id="payments" value="0"  readonly class="form-control m-input contractorpaymentcls payments' + index + '" placeholder="Payments" autocomplete="off" aria-label="Payments" onchange="calculatetotalpayment();" onblur="calculatetotalpayment();">';
            html += '<label for="payments" style="color:#636578 !important;">Payments</label>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-md-2">';
            html += '<div class="form-floating form-floating-outline" style="pointer-events: none;opacity: 1.4;">';
            html += '<input type="number" value="0" style="background: #d3d3d34f !important;border: #d3d3d34f !important;" name="outstandingpayment[]" id="outstandingpayment" readonly class="form-control m-input outstandingpayment' + index + '" placeholder="Outstanding Amount" autocomplete="off" aria-label="Outstanding Amount">';
            html += '<label for="outstandingpayment" style="color:#636578 !important;">Outstanding Amount</label>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-md-2">';
            html += '<button id="removecontractors" type="button" class="btn btn-danger">Remove</button>';
            html += '</div>';
            html += '</div>';

            $('#newRowContractors').append(html);
        });

        // remove row
        $(document).on('click', '#removecontractors', function () {
            $(this).closest('#inputFormRowContractors').remove();
            index = index - 1;
        });


        $("#addRowpayments").click(function () {
            var html = '';
            html += '<div id="inputFormRowpayments">';
            html += '<div class="input-group mb-3">';
            html += '<select style="width:30%" name="paymentmethod[]" id="paymentmethod" class="form-control">';
            html += '<option value="Check">Check</option>';
            html += '<option value="Cash">Cash</option>';
            html += '<option value="Swipe">Swipe</option>';
            html += '<option value="Transfer">Transfer</option>';
            html += '<option value="other">Other</option>';
            html += '</select>&nbsp;&nbsp;';
            html += '<input style="width:30%" type="number" name="paymentsval[]" id="paymentsval" class="form-control m-input newpaymentcls" onchange="calculatetotalpayment();" onblur="calculatetotalpayment();" placeholder="Investment Amount" autocomplete="off">';
            html += '<div class="input-group-append">';
            html += '<button id="removepayments" type="button" class="btn btn-danger">Remove</button>';
            html += '</div>';
            html += '</div>';

            $('#newRowpayments').append(html);
        });

        // remove row
        $(document).on('click', '#removepayments', function () {
            $(this).closest('#inputFormRowpayments').remove();
        });


        $("#addRowdocument").click(function () {
            var html = '';
            html += '<div class="row mt-4 g-2" id="inputFormdocuments">';
            html += '<div class="col-md-5">';
            html += '<div class="form-floating form-floating-outline">';
            html += '<input type="text" name="documentname[]" id="documentname" class="form-control m-input" placeholder="Document Name" autocomplete="off" aria-label="Document Name">';
            html += '<label for="documentname">Document Name</label>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-md-5">';
            html += '<div class="form-floating form-floating-outline">';
            html += '<input type="file" name="filename[]" id="filename" class="form-control m-input" placeholder="Upload Document" autocomplete="off" aria-label="Upload Document">';
            html += '<label for="filename">Upload Document</label>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-md-2">';
            html += '<button id="removedocuments" type="button" class="btn btn-danger">Remove</button>';
            html += '</div>';
            html += '</div>';

            $('#newRowdocuments').append(html);
        });

        // remove row
        $(document).on('click', '#removedocuments', function () {
            $(this).closest('#inputFormdocuments').remove();
        });
    });

    function blockSpecialChar(e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));
    }
</script>
<script>


    function calculatetotalpayment() {
        var totalpaymentsval = 0;
        $('.newpaymentcls').each(function () {
            totalpaymentsval = parseInt(totalpaymentsval) + parseInt($(this).val());
        });

        var totalcontractorspaymentsval = 0;
        $('.contractorpaymentcls').each(function () {
            totalcontractorspaymentsval = parseInt(totalcontractorspaymentsval) + parseInt($(this).val());
        });


        var currentvalue = parseInt(totalpaymentsval) + parseInt(totalcontractorspaymentsval);


        $('#totalpayment').val(currentvalue);

        var totalestimatedcost = $('#totalestimatedcost').val();

        var getballence = totalestimatedcost - currentvalue;

        $('#ballence').val(getballence);
    }

    function calculatetotalProjectpayment(index) {
        var projectAmount = $('.projectamount'+index).val();
        var paymentAmount = $('.payments'+index).val();

        var outstandingAmount = parseFloat(projectAmount) - parseFloat(paymentAmount);
        $('.outstandingpayment'+index).val(outstandingAmount);
    }
</script>

