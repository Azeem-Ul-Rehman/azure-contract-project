<?php include('partials/header.php'); ?>
<style>
    p {
        display: inline-flex;
    }
</style>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Bank Details
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Bank Details</h5>

                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
						<?php 
							$where=array('is_deleted'=>0,'company_id'=>$banks->companyid);
							$companylist = $this->adminmodel->getSingle(TBLCOMPANY,$where);
						?>
                        <div class="card-body">
                            <div class="row">
                                <!-- First column-->
                                <div class="col-md-12">
                                    <!-- Product Information -->

                                    <fieldset>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Company Name :</b>
                                                    </label>

                                                </div>
                                                <div class="col-2 text-left">
                                                    <p><?php echo $companylist->company_name;?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Bank Name :</b>
                                                    </label>

                                                </div>
                                                <div class="col-2">
                                                    <p><?php echo $banks->bankname; ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Account Type :</b>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <p><?php echo $banks->accounttype; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Account Name :</b>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <p><?php echo $banks->accountname; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Start Ballence :</b>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <p><?php echo $banks->startballence; ?></p>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2">
                                                    <label for="companyname"><b>Current Ballence :</b>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <p><?php echo $banks->currentballence; ?></p>
                                                </div>
                                            </div>


                                        </div>

                                    </fieldset>
                                </div>
                                <!-- /Second column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>
