<?php include('partials/header.php'); ?>
<!-- partial -->
<!-- Content wrapper -->
<!-- Content wrapper -->
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Settings
        </h4>
        <form id="my_form" name="my_form" method="post">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div
                                class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Settings</h5>
                        </div>
                        <!-- Add Project Type -->

                        <div class="row">
                            <!-- First column-->
                            <div class="col-md-12">
                                <div class="alert alert-success alert-dismissible" style="display: none">
                                    <strong>Success!</strong> Permissions updated Successfully
                                </div>
                            </div>
                        </div>


                        <div class="card-body">
                            <div class="row g-4 mb-4">
                                <!-- First column-->
                                <div class="col-md-12">
                                    <!-- Product Information -->


                                    <div class="form-floating form-floating-outline">
                                        <select id="permissiontype" name="permissiontype"
                                                class="select2 form-select required"
                                                data-allow-clear="true">
                                            <?php foreach ($usersgroups as $rowsgroup) { ?>
                                                <option value="<?php echo $rowsgroup->id; ?>"><?php echo $rowsgroup->groupname; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="permissiontype">Permission Type</label>
                                    </div>


                                </div>
                            </div>
                            <div class="col-12" id="permissionpageid">
                                <!-- Permission table -->
                                <h5>Manager</h5>
                                <div class="table-responsive">
                                    <table class="table table-flush-spacing">

                                        <tbody>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Companies</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[0]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Compnies" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddcompany" id="chkaddcompanyid"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddcompanyid">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[0]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Compnies" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditcompany" id="chkaddcompanyid"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditcompany">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[0]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Compnies" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifycompany" id="chkaddcompanyid"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifycompany">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[0]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Compnies" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletecompany" id="chkaddcompanyid"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletecompany">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Projects</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[1]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Projects" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddprojects" id="chkaddprojects"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddprojects">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[1]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Projects" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditprojects" id="chkeditprojects"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditprojects">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[1]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Projects" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifyprojects" id="chkmodifyprojects"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifyprojects">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[1]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Projects" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeleteprojects" id="chkdeleteprojects"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeleteprojects">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Users</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[2]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Users" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddusers" id="chkaddusers"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddusers">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[2]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Users" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditusers" id="chkeditusers"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditusers">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[2]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Users" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifyusers" id="chkmodifyusers"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifyusers">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[2]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Users" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeleteusers" id="chkdeleteusers"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeleteusers">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Partners</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[3]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Partners" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddpartners" id="chkaddpartners"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddpartners">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[3]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Partners" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditpartners" id="chkeditpartners"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditpartners">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[3]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Partners" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifypartners" id="chkmodifypartners"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifypartners">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[3]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Partners" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletepartners" id="chkdeletepartners"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletepartners">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Suppliers</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[4]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Suppliers" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddsuppliers" id="chkaddsuppliers"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddsuppliers">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[4]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Suppliers" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditsuppliers" id="chkeditsuppliers"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditsuppliers">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[4]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Suppliers" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifysuppliers" id="chkmodifysuppliers"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifysuppliers">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[4]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Suppliers" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletesuppliers" id="chkdeletesuppliers"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletesuppliers">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Contractors</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[5]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Contractors" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddcontractos" id="chkaddcontractos"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddcontractos">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[5]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Contractors" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditcontractos" id="chkeditcontractos"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditcontractos">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[5]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Contractors" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifycontractos" id="chkmodifycontractos"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifycontractos">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[5]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Contractors" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletecontractos" id="chkdeletecontractos"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletecontractos">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Loans</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[6]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Loans" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddloans" id="chkaddloans"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddloans">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[6]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Loans" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditloans" id="chkeditloans"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditloans">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[6]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Loans" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifyloans" id="chkmodifyloans"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifyloans">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[6]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Loans" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeleteloans" id="chkdeleteloans"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeleteloans">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Bank Accounts</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[7]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Bank Accounts" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddbankaccounts" id="chkaddbankaccounts"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddbankaccounts">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[7]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Bank Accounts" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditbankaccounts" id="chkeditbankaccounts"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditbankaccounts">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[7]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Bank Accounts" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifybankaccounts" id="chkmodifybankaccounts"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifybankaccounts">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[7]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Bank Accounts" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletebankaccounts" id="chkdeletebankaccounts"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletebankaccounts">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Financial Institute</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[8]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Financial Institute" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddfinancialinstitute"
                                                                id="chkaddfinancialinstitute"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddfinancialinstitute">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[8]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Financial Institute" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditfinancialinstitute"
                                                                id="chkeditfinancialinstitute"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditfinancialinstitute">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[8]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Financial Institute" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifyfinancialinstitute"
                                                                id="chkmodifyfinancialinstitute"
                                                                value="1"/>
                                                        <label class="form-check-label"
                                                               for="chkmodifyfinancialinstitute">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[8]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Financial Institute" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletefinancialinstitute"
                                                                id="chkdeletefinancialinstitute"
                                                                value="1"/>
                                                        <label class="form-check-label"
                                                               for="chkdeletefinancialinstitute">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Payments</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[9]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Payments" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddpayments" id="chkaddpayments"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddpayments">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[9]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Payments" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditpayments" id="chkeditpayments"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditpayments">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[9]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Payments" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifypayments" id="chkmodifypayments"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifypayments">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[9]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Payments" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletepayments" id="chkdeletepayments"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletepayments">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Documents</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[10]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Documents" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkadddocument" id="chkadddocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkadddocument">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[10]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Documents" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditdocument" id="chkeditdocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditdocument">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[10]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Documents" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifydocument" id="chkmodifydocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifydocument">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[10]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Documents" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletedocument" id="chkdeletedocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletedocument">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Purchase orders</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[11]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Purchase orders" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkadddocument" id="chkadddocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkadddocument">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[11]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Purchase orders" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditdocument" id="chkeditdocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditdocument">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[11]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Purchase orders" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifydocument" id="chkmodifydocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifydocument">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[11]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Purchase orders" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletedocument" id="chkdeletedocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletedocument">
                                                            Delete </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[11]->allowfor_verify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Purchase orders" rel1="2" rel2="verify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkverifydocument" id="chkverifydocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkverifydocument">
                                                            Verify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[11]->allowfor_complete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Purchase orders" rel1="2" rel2="complete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkcompletedocument" id="chkcompletedocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkcompletedocument">
                                                            Complete </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[11]->allowfor_pay == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Purchase orders" rel1="2" rel2="pay"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkpaydocument" id="chkpaydocument"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkpaydocument">
                                                            Pay </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Dashboard</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[12]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Dashboard" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkadddashboard" id="chkadddashboard"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkadddashboard">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[12]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Dashboard" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditdashboard" id="chkeditdashboard"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditdashboard">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[12]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Dashboard" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifydashboard" id="chkmodifydashboard"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifydashboard">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[12]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Dashboard" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletedashboard" id="chkdeletedashboard"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletedashboard">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Sell Properties</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[13]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Sell Properties" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddsellproperties" id="chkaddsellproperties"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddsellproperties">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[13]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Sell Properties" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditsellproperties" id="chkeditsellproperties"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditsellproperties">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[13]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Sell Properties" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifysellproperties" id="chkmodifysellproperties"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifysellproperties">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[13]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Sell Properties" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletesellproperties" id="chkdeletesellproperties"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletesellproperties">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap fw-medium">Settings</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[14]->allowfor_add == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Settings" rel1="2" rel2="add"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkaddsettings" id="chkaddsettings"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkaddsettings">
                                                            Add </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[14]->allowfor_edit == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Settings" rel1="2" rel2="edit"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkeditsettings" id="chkeditsettings"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkeditsettings">
                                                            View </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[14]->allowfor_modify == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Settings" rel1="2" rel2="modify"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkmodifysettings" id="chkmodifysettings"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkmodifysettings">
                                                            Modify </label>
                                                    </div>
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input <?php if ($settings[14]->allow_delete == 1) { ?> checked="checked" <?php } ?>
                                                                rel="Settings" rel1="2" rel2="delete"
                                                                class="permissioncls form-check-input" type="checkbox"
                                                                name="chkdeletesettings" id="chkdeletesettings"
                                                                value="1"/>
                                                        <label class="form-check-label" for="chkdeletesettings">
                                                            Delete </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- Permission table -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>

<script>
    $(document).ready(function () {
        $(document).on('change', '.permissioncls', function () {
            var usertype = $(this).attr('rel1');
            var menutype = $(this).attr('rel');
            var permissiontype = $(this).attr('rel2');
            var permissionstatus = 0;
            if ($(this).prop('checked') == true) {
                permissionstatus = $(this).val();
            }
            $.ajax({
                url: "administrator/managepermissionstatus",
                type: "POST",
                data: {
                    usertype: usertype,
                    menutype: menutype,
                    permissiontype: permissiontype,
                    permissionstatus
                },
                success: function (data) {
                    $('.alert-dismissible').show()
                    setTimeout(function () {
                        $(".alert-dismissible").delay(1000).fadeOut('slow');
                    }, 5000);
                }
            });
        });
        $(document).on('change', '#permissiontype', function () {
            var permissionusergroup = $(this).val();
            $.ajax({
                url: "administrator/managepermissiontype",
                type: "POST",
                data: {permissionusergroup: permissionusergroup},
                success: function (data) {
                    $('#permissionpageid').html(data);
                }
            });
        });
    });
</script>
