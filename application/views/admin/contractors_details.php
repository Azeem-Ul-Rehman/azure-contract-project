<?php include('partials/header.php'); ?>
<style>    p {
        display: inline-flex;
    }</style>
<div class="content-wrapper">    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y"><h4 class="py-3 mb-4"><span
                    class="text-muted fw-light">Home/</span>
            Contractors Details </h4>
        <form id="my_form" name="my_form" method="post" action="">            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Contractors Details</h5></div>
                        <!-- Add Project Type --> <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible"><a href="#" class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>                        <?php } ?>
                        <div class="card-body">
                            <div class="row">                                <!-- First column-->
                                <div class="col-md-12">
                                    <!-- Product Information -->
                                    <fieldset>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Name : </b> </label>
                                                </div>
                                                <div class="col-2 text-left">
                                                    <p><?php echo $contractorsval->contractor_name; ?></p></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Phone No : </b>
                                                    </label></div>
                                                <div class="col-2">
                                                    <p>                                                        <?php echo $contractorsval->contractor_phone; ?>                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Type : </b> </label>
                                                </div>
                                                <div class="col-2">
                                                    <p>                                                        <?php echo $contractorsval->contractor_type; ?>                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Address : </b>
                                                    </label></div>
                                                <div class="col-2">
                                                    <p>                                                        <?php echo $contractorsval->contractor_address; ?>                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Persons: </b>
                                                    </label></div>
                                                <div class="col-2">
                                                    <p>                                                        <?php echo $contractorsval->contractor_person; ?>                                                    </p>
                                                </div>
                                            </div>
                                        </div> <?php $assignproject = explode(',', $contractorsval->contractor_assignproject); ?>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Assign Project: </b>
                                                    </label></div>
                                                <div class="col-10">
                                                    <p>
                                                        <?php
                                                        $totalpayment = 0;
                                                        $contractor_id = $this->input->get('contractor_id', true);
                                                        foreach ($projectlist as $rowsassignproject) {

                                                            $where = array('projectid' => $rowsassignproject->projectid);
                                                            $assingprojectval = $this->adminmodel->getSingle(TBLPROJECTS, $where);
                                                            $sqlcontractors = $this->db->query('select SUM(payments) as totalpayments  from ' . PROJECTCONTRACTORS . ' a where projectid="' . $rowsassignproject->projectid . '" and contractorid=' . $contractor_id);
                                                            $getrows = $sqlcontractors->row();
                                                            $totalpayment += $getrows->totalpayments ?? 0;


                                                            ?>

                                                            <?php echo ucfirst($assingprojectval->project_name); ?> ,                                                        <?php } ?>                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Payments : </b>
                                                    </label></div>
                                                <div class="col-2">
                                                    <p>    <?php echo number_format($totalpayment, 2, ".", ","); ?>                                                                                        </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>CRIB No : </b>
                                                    </label></div>
                                                <div class="col-2">
                                                    <p>                                                        <?php echo $contractorsval->contractor_cribno; ?>                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2"><label for="companyname"><b>Kvk No : </b>
                                                    </label></div>
                                                <div class="col-2">
                                                    <p>                                                        <?php echo $contractorsval->contractor_kvkno; ?>                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="card mb-4">
                                        <div class="card-header p-0">
                                            <div class="nav-align-top">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect active"
                                                                role="tab" data-bs-toggle="tab"
                                                                data-bs-target="#navs-top-home"
                                                                aria-controls="navs-top-home" aria-selected="false"
                                                                tabindex="-1"> Projects
                                                        </button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect"
                                                                role="tab" data-bs-toggle="tab"
                                                                data-bs-target="#navs-top-profile"
                                                                aria-controls="navs-top-profile"
                                                                aria-selected="false" tabindex="-1"> Payments
                                                        </button>
                                                    </li>
                                                    <span class="tab-slider"
                                                          style="left: 198.3px; width: 127.65px; bottom: 0px;"></span>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content p-0">
                                                <div class="tab-pane fade active show" id="navs-top-home"
                                                     role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Project Name</th>
                                                                <th>Contractor Amount</th>
                                                                <th>Payments</th>
                                                                <th>Outstanding Balance</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $contractor_id = $this->input->get('contractor_id', true);
                                                            if (!empty($projectlist)) {
                                                                foreach ($projectlist as $rowsproject) {
                                                                    $sqlcontractors = $this->db->query('select SUM(projectamount) as totalcontractorpayment, SUM(payments) as totalpayments  from ' . PROJECTCONTRACTORS . ' a where projectid="' . $rowsproject->projectid . '" and contractorid=' . $contractor_id);
                                                                    $getrows = $sqlcontractors->row();
                                                                    $totalcontractoramount = $getrows->totalcontractorpayment ?? 0;
                                                                    $totalpayment = $getrows->totalpayments ?? 0;
                                                                    $outstandingpayment = (float)$totalcontractoramount - (float)$totalpayment; ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="administrator/projectlist"><?php echo $rowsproject->project_name; ?></a>
                                                                        </td>
                                                                        <td><?php echo number_format($totalcontractoramount, 2, ".", ","); ?> </td>
                                                                        <td><?php echo number_format($totalpayment, 2, ".", ","); ?></td>
                                                                        <td><?php echo number_format($outstandingpayment, 2, ".", ","); ?></td>
                                                                    </tr>
                                                                <?php }
                                                            } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="navs-top-profile" role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <!--<th>Sno</th>                                                                <th>Paid to</th>-->
                                                                <th>Project Name</th>
                                                                <!--<th>Project Section</th>-->
                                                                <th>Payment method</th>
                                                                <th>Amount</th>
                                                                <th>Payment date</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>                                                            <?php if (!empty($paymentslist)) {
                                                                $m = 1;
                                                                foreach ($paymentslist as $rowspayments) {
                                                                    $where = array('projectid' => $rowspayments->project);
                                                                    $projectsval = $this->adminmodel->getSingle(TBLPROJECTS, $where); ?>
                                                                    <tr>
                                                                        <!--<td><?php echo $m; ?></td>										  <td><?php echo $rowspayments->project; ?></td>-->
                                                                        <td>
                                                                            <a href="administrator/project_details/?projectid=<?php echo $rowspayments->project; ?>"><?php echo $projectsval->project_name; ?></a>
                                                                        </td>
                                                                        <!--<td><?php echo $rowspayments->paymentmethod; ?></td>-->
                                                                        <td><?php echo $rowspayments->paymentmethod; ?></td>
                                                                        <td><?php echo number_format($rowspayments->Amount, 2, ".", ","); ?></td>
                                                                        <td><?php echo date('d/m/Y', strtotime($rowspayments->paymentdate)); ?></td>
                                                                    </tr>                                                                    <?php $m++;
                                                                }
                                                            } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                <!-- /Second column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>    <!-- / Content --> <?php include('partials/sticky-footer.php'); ?>
</div><!-- Content wrapper --><?php include('partials/footer.php'); ?>
