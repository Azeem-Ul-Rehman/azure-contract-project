<?php include('partials/header.php'); ?>


<!-- Content wrapper -->
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4"><span class="text-muted fw-light">User Profile /</span> Profile</h4>

        <!-- Header -->
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="user-profile-header-banner">

                        <img src="backend-assets/img/pages/profile-banner.png" alt="Banner image"
                             class="rounded-top"/>
                    </div>
                    <div class="user-profile-header d-flex flex-column flex-sm-row text-sm-start text-center mb-4">
                        <div class="flex-shrink-0 mt-n2 mx-sm-0 mx-auto">
                            <?php if ($usersinfo->profile_pic != '') { ?>
                                <img
                                        src="uploads/profile/<?php echo $usersinfo->profile_pic; ?>"
                                        alt="<?php echo $usersinfo->firstname . ' ' . $usersinfo->lastname; ?>"
                                        class="d-block h-auto ms-0 ms-sm-4 rounded user-profile-img"/>
                            <?php } else { ?>
                                <img
                                        src="backend-assets/img/avatars/1.png"
                                        alt="user image"
                                        class="d-block h-auto ms-0 ms-sm-4 rounded user-profile-img"/>
                            <?php } ?>
                        </div>
                        <div class="flex-grow-1 mt-3 mt-sm-5">
                            <div
                                    class="d-flex align-items-md-end align-items-sm-start align-items-center justify-content-md-between justify-content-start mx-4 flex-md-row flex-column gap-4">
                                <div class="user-profile-info">
                                    <h4><?php echo $usersinfo->firstname . ' ' . $usersinfo->lastname; ?></h4>
                                    <ul
                                            class="list-inline mb-0 d-flex align-items-center flex-wrap justify-content-sm-start justify-content-center gap-2">
                                        <li class="list-inline-item">
                                            <i class="mdi mdi-email-outline me-1 mdi-20px"></i
                                            ><span class="fw-medium"><?php echo $usersinfo->email; ?></span>
                                        </li>
                                        <li class="list-inline-item">
                                            <i class="mdi mdi-phone-alert-outline me-1 mdi-20px"></i><span
                                                    class="fw-medium"><?php echo $usersinfo->phone; ?></span>
                                        </li>

                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Header -->

        <div class="card mb-4">
            <div class="card-header p-0">
                <div class="nav-align-top">
                    <ul class="nav nav-tabs" role="tablist">
                         <!--  <li class="nav-item" role="presentation">
                            <button type="button" class="nav-link waves-effect active"
                                    role="tab"
                                    data-bs-toggle="tab" data-bs-target="#navs-top-home"
                                    aria-controls="navs-top-home" aria-selected="false"
                                    tabindex="-1">
                                Profile Info
                            </button>
                        </li> -->
                        <li class="nav-item" role="presentation">
                            <button type="button" class="nav-link waves-effect active" role="tab"
                                    data-bs-toggle="tab" data-bs-target="#navs-top-profile"
                                    aria-controls="navs-top-profile" aria-selected="false"
                                    tabindex="-1">
                                Profile Picture
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button type="button" class="nav-link waves-effect" role="tab"
                                    data-bs-toggle="tab" data-bs-target="#navs-top-change-password"
                                    aria-controls="navs-top-change-password" aria-selected="false"
                                    tabindex="-1">
                                Change Password
                            </button>
                        </li>
                        <span class="tab-slider"
                              style="left: 198.3px; width: 127.65px; bottom: 0px;"></span>
                    </ul>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content p-0">
                   
                    <div class="tab-pane fade active show" id="navs-top-profile" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card mb-4">
                                    <h5 class="card-header"><span class="badge badge-warning" style="color: red">Note : </span>
                                        <p class="d-inline ml-3 text-muted">Image size is limited to not greater than
                                            5MB .</p></h5>
                                    <div class="card-body">
                                        <form method="post"
                                              action="administrator/updateprofilepicture" enctype=multipart/form-data>
                                            <div class="col-md-4">
                                                <div class="form-floating form-floating-outline mb-4">
                                                    <input id="file" class="form-control m-input"  name="file" type="file" placeholder="Upload Profile Image" autocomplete="off" aria-label="Document">
                                                    <label for="filename">Upload Profile Image</label>
                                                </div>
                                            </div>
                                            <div class="row g-4 mb-4">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success mr-2">Update</button>
                                                    <button class="btn btn-outline-danger">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="navs-top-change-password" role="tabpanel">
                        <form action="administrator/updatepassword" id="updateformprofile" method="post">
                            <div class="row g-4 mb-4">
                                <div class="col-md-6">

                                    <div class="form-floating form-floating-outline">
                                        <input type="password"
                                               class="form-control" name="currentpassword" id="currentpassword"
                                               placeholder="Current Password">
                                        <label for="currentpassword">Current Password *</label>
                                    </div>


                                </div>
                                <div class="col-md-6">


                                    <div class="form-floating form-floating-outline">
                                        <input class="form-control required"
                                               placeholder="New Password"
                                               id="newpassword" type="password" name="newpassword"/>
                                        <label for="newpassword">New Password *</label>
                                    </div>


                                </div>
                                <!-- /Second column -->
                            </div>
                            <div class="row">
                                <div class="form-group mt-5">
                                    <button type="submit" class="btn btn-success mr-2">Update</button>
                                    <button class="btn btn-outline-danger">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / Content -->
    <?php include('partials/sticky-footer.php'); ?>

</div>
<!-- Content wrapper -->


<!-- Content wrapper -->
<?php include('partials/footer.php'); ?>


