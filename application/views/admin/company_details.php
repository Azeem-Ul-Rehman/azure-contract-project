<?php include('partials/header.php'); ?>
<style>
    p {
        display: inline-flex;
    }
</style>
<div class="content-wrapper">
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4">
            <span class="text-muted fw-light">Home/</span>
            Company Details
        </h4>
        <form id="my_form" name="my_form" method="post" action="">
            <!-- Sticky Actions -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header sticky-element bg-label-secondary d-flex justify-content-sm-between align-items-sm-center flex-column flex-sm-row">
                            <h5 class="card-title mb-sm-0 me-2">Company Details</h5>

                        </div>
                        <!-- Add Project Type -->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="row">
                                <!-- First column-->
                                <div class="col-12 col-lg-8">
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <div class="card-body">
                            <div class="row">
                                <!-- First column-->
                                <div class="col-md-12">
                                    <!-- Product Information -->

                                    <fieldset>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 110px;">
                                                    <label for="companyname"><b>Name : </b>
                                                    </label>

                                                </div>
                                                <div class="col-6 text-left">
                                                    <p><?php echo $editcompany->company_name; ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 110px;">
                                                    <label for="companyname"><b>Phone No : </b>
                                                    </label>

                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo $editcompany->phoneno; ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 110px;">
                                                    <label for="companyname"><b>Email : </b>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo $editcompany->email; ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 120px;">
                                                    <label for="companyname"><b>Found Date : </b>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo $editcompany->found_date; ?>
                                                    </p>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 110px;">
                                                    <label for="companyname"><b>Partners : </b>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo $editcompany->company_partners; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 110px;">
                                                    <label for="companyname"><b>Address : </b>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <p> <?php echo $editcompany->address; ?></p>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 110px;">
                                                    <label for="companyname"><b>CRIB No : </b>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo $editcompany->crib_no; ?></p>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-2" style="min-width: 110px;">
                                                    <label for="companyname"><b>Kvk No : </b>
                                                    </label>
                                                </div>
                                                <div class="col-6">
                                                    <p><?php echo $editcompany->kvk_no; ?></p>
                                                </div>
                                            </div>


                                        </div>


                                    </fieldset>
                                    <div class="card mb-4">
                                        <div class="card-header p-0">
                                            <div class="nav-align-top">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect active"
                                                                role="tab"
                                                                data-bs-toggle="tab" data-bs-target="#tab-projects"
                                                                aria-controls="tab-projects" aria-selected="false"
                                                                tabindex="-1">
                                                            Projects
                                                        </button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect" role="tab"
                                                                data-bs-toggle="tab" data-bs-target="#tab-bank-account"
                                                                aria-controls="tab-bank-account" aria-selected="false"
                                                                tabindex="-1">
                                                            Bank Accounts
                                                        </button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button type="button" class="nav-link waves-effect" role="tab"
                                                                data-bs-toggle="tab" data-bs-target="#tab-loan"
                                                                aria-controls="tab-loan" aria-selected="false"
                                                                tabindex="-1">
                                                            Loans
                                                        </button>
                                                    </li>
                                                    <span class="tab-slider"
                                                          style="left: 198.3px; width: 127.65px; bottom: 0px;"></span>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content p-0">
                                                <div class="tab-pane fade active show" id="tab-projects"
                                                     role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th><span style="text-align: center;">Name</span></th>
                                                                <th>Start Date</th>
                                                                <th>End Date</th>
                                                                <th>Estimated Cost</th>
                                                                <th>Payments</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            if (!empty($projectlist)) {
                                                                foreach ($projectlist as $rowsproject) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="administrator/project_details/?projectid=<?php echo $rowsproject->projectid; ?>"><?php echo $rowsproject->project_name; ?></a>
                                                                        </td>
                                                                        <!--<td><?php echo $rowsproject->project_name; ?></td>-->
                                                                        <td><?php echo date('d-m-Y', strtotime($rowsproject->startdate)); ?></td>
                                                                        <td><?php echo date('d-m-Y', strtotime($rowsproject->enddate)); ?></td>
                                                                        <td>
                                                                            <span style="float:right;"><?php echo number_format($rowsproject->totalestimatedcost, 2, ".", ","); ?></span>
                                                                        </td>
                                                                        <td>
                                                                            <span style="float:right;"><?php echo number_format(floatval($rowsproject->supplier_payments) + floatval($rowsproject->contrator_payments) + floatval($rowsproject->other_payments), 2, ".", ","); ?></span>
                                                                        </td>
                                                                    </tr>
                                                                <?php }


                                                            } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="tab-bank-account" role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Account Type</th>
                                                                <th>Account Name</th>
                                                                <th>Start Balance</th>
                                                                <th>Current Balance</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            if (!empty($banklist)) {
                                                                foreach ($banklist as $rowsproject) {

                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="administrator/bank_details/?bank_id=<?php echo $rowsproject->companybankid; ?>"><?php echo $rowsproject->bankname; ?></a>
                                                                        </td>
                                                                        <td><?php echo $rowsproject->accounttype; ?></td>
                                                                        <td><?php echo $rowsproject->accountname; ?></td>
                                                                        <td><?php echo number_format($rowsproject->startballence, 2, ".", ","); ?></td>
                                                                        <td><?php echo number_format($rowsproject->currentballence, 2, ".", ","); ?></td>
                                                                    </tr>
                                                                <?php }

                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="tab-loan" role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table id="order-listingnew" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Institution</th>
                                                                <th>Project</th>
                                                                <th>Loan</th>
                                                                <th>Interest</th>
                                                                <th>Period</th>
                                                                <th>FirstDate</th>
                                                                <th>EndDate</th>
                                                                <th>Redemption</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            if (!empty($loanlist)) {
                                                                foreach ($loanlist as $rowsproject) {

                                                                    $where = array('financialinstituteid' => $rowsproject->financialinstitution);
                                                                    $financialinstitute = $this->adminmodel->getSingle(TBLFINANCIALINSTITUTE, $where);

                                                                    $where = array('projectid' => $rowsproject->project);
                                                                    $projects = $this->adminmodel->getSingle(TBLPROJECTS, $where);

                                                                    ?>
                                                                    <tr>

                                                                        <td>
                                                                            <a href="administrator/financial_details/?financialinstituteid=<?php echo $financialinstitute->financialinstituteid; ?>"><?php echo $financialinstitute->financialinstitutename; ?></a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="administrator/project_details/?projectid=<?php echo $projects->projectid; ?>"><?php echo $projects->project_name; ?></a>
                                                                        </td>
                                                                        <td><?php echo number_format($rowsproject->loanamount, 2, ".", ","); ?></td>
                                                                        <td><?php echo $rowsproject->interestrate . "%"; ?></td>
                                                                        <td><?php echo $rowsproject->period . " Months"; ?></td>
                                                                        <td><?php echo $rowsproject->firstdate; ?></td>
                                                                        <td><?php echo $rowsproject->enddate; ?></td>
                                                                        <td><?php echo number_format($rowsproject->redemptionamount, 2, ".", ","); ?></td>
                                                                    </tr>
                                                                <?php }
                                                            } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /Second column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- / Content -->

    <?php include('partials/sticky-footer.php'); ?>
</div>
<!-- Content wrapper -->


<?php include('partials/footer.php'); ?>
