<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('getallajaxchatshtml'))
{
	function getallajaxchatshtml($perpage,$rowno) 
	{
		 $CI =& get_instance();
	     $CI->load->model('adminmodel');
		 $CI->load->library('pagination');
		// Row per page
		$rowperpage = $perpage;

		$username = $CI->input->post('username',true);	
		$created = $CI->input->post('created',true);
		// Row position
		if($rowno != 0){
		  $rowno = ($rowno-1) * $rowperpage;
		}
		 $allchat =$CI->adminmodel->getallsearchchatajax(CHAT,$rowno,$rowperpage,$username,$created);
		 $totalchat =$CI->adminmodel->getallsearchchatajaxcount(CHAT,$username,$created);
		
		// Pagination Configuration
		$config['base_url'] =  base_url().'administrator/searchchatajax';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $totalchat;
		$config['per_page'] = $rowperpage;

		// Initialize
		$CI->pagination->initialize($config);
		$data =' <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Sno</th>
    					    <th>Username</th>
                            <th>Usertype</th>
                            <th>Text</th>
    					              <th>Created Date</th>
    					             
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>';
		if(!empty($allchat))
		{  
			$i=1;	
			foreach($allchat as $rows)
			{  
			$data .='<tr>
	                    <td>'.$i.'</td>
  						<td>'.$rows->username.' </td>
                        <td>'.$rows->usertype.'</td>
	                    <td>'.$rows->text.'</td>
	                    <td>'.date('d M Y', $rows->created).'</td>             
	                    <td>
						
						<a data-toggle="modal" href="javascript:void(0);" data-target="#viewchat'.$rows->id.'" class="btn btn-primary"><i class="fa fa-eye"></i></a>
						
				        <button class="btn btn-outline-danger" onclick="show_confirmdelte('.$rows->id.')"><i class="fa fa-trash-o"></i></button>

	                    </td>
	                </tr>';
					
					
					$data .='<div class="modal fade" id="viewchat'.$rows->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
							<div class="modal-dialog" role="document">
							  <div class="modal-content">
								<div class="modal-header"style="background-color:#ff9800;">
								  <h5 class="modal-title" style="color:#FFF !important;" id="exampleModalLabel-2">View Chat</h5>
								  <button type="button" style="color:#FFF !important;" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								  </button>
								</div>
								<div class="modal-body">'.$rows->text.'</div>
							  </div>
							</div>
						  </div>';
							  
	                $i++;
			 }
		} 
		else{
			$data .='<tr><td colspan="7"><h3 class="text-center" style="color:red;margin-top:15px;">Sorry, no results found. Please modify your filter selection.</h3></td>    </tr>';
			
			}
		
		$data .= ' </tbody>
                    </table>';
		$arrjson = json_encode(array('chat'=>$data,'totalchat'=>$totalchat,'pagination'=>$CI->pagination->create_links(),'row'=>$rowno));
		return $arrjson;
	}
}

if (!function_exists('active_link')) {
    function active_link($path, $className = 'active')
    {
        $CI =& get_instance();
        $uri_string = $CI->uri->uri_string();

        // Home is usually at / && has 0 total segments
        if ($path === '/' && ($CI->uri->total_segments() === 0)) {
            $ret_val = 'active';
        } else {
            $ret_val = ($uri_string === $path) ? $className : '';
        }

        return $ret_val;

    }
}
if (!function_exists('active_open')) {
    function active_open($path, $className = 'open')
    {
        $CI =& get_instance();
        $uri_string = $CI->uri->uri_string();

        // Home is usually at / && has 0 total segments
        if ($path === '/' && ($CI->uri->total_segments() === 0)) {
            $ret_val = 'open';
        } else {
            $ret_val = ($uri_string === $path) ? $className : '';
        }

        return $ret_val;

    }
}


