<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() 
    {
		parent::__construct();
		$this->load->model('adminmodel','',TRUE);
		$this->load->helper('admin');
		$this->load->library('pagination');
		error_reporting(0);
    } 
    
	function _chk_if_login()
	{
		if($this->session->userdata('admin_logged_in') == false && $this->session->userdata('admin_userid') == '')
		{
			$array_items = array('admin_userid' =>'', 'admin_usertype' =>'', 'admin_username' =>'' ,'admin_profilepic'=>'','admin_email' =>'','admin_logged_in' =>false);
			 $this->session->unset_userdata($array_items);
			redirect('administrator/user-login');
			exit();   
		}
	} 
	
	
	function _chk_if_permissions($menutab,$permissionlabel)
	{
		$usertype = $this->session->userdata('admin_usertype');
		$where = array('usertype'=>$usertype,'menutab'=>$menutab,$permissionlabel=>1);
		$chkpermission = $this->adminmodel->getSingle(SETTINGPERMISSION,$where); 
				
		if($usertype==1){
			
		}else if($chkpermission->settingsid==''){
			//redirect('administrator/dashboard');
            $message = 'You dont have permission to access this route.';
            throw new RuntimeException('CI Error: '.$message);
            $this->session->set_flashdata('error','You dont have permission to access this route.');
//			redirect($_SERVER['HTTP_REFERER']);

			exit();   
		}
	} 
	

	function generateRandomString($length = 10)
	{
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	public function random_string($length) 
    {
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));

		for ($i = 0; $i < $length; $i++) {
		$key .= $keys[array_rand($keys)];
		}

		return $key;
	}
	
	/*show index page for  user  Start*/
	public function index()
	{
		$this->_chk_if_login();
		
		//$this->_chk_if_permissions('Dashboard','allowfor_edit');
		
		$userid = $this->session->userdata('admin_userid');
		
		//totalcompany totalpartners
		$where = array('is_deleted'=>0);
		$config['totalcompany'] =$this->adminmodel->getCount(TBLCOMPANY,$where); 
		
		$where = array('is_deleted'=>0);
		$config['totalpartners'] =$this->adminmodel->getCount(TBLPARTNER,$where); 
		
		$where = array('is_deleted'=>0);
		$config['totalcontractors'] =$this->adminmodel->getCount(TBLCONTRACTORS,$where); 
		
		$where = array('is_deleted'=>0);
		$config['totalsuppliers'] =$this->adminmodel->getCount(TBLSUPPLIERS,$where); 
		
		$where = array('is_deleted'=>0);
		$config['totalloans'] =$this->adminmodel->getCount(TBLCOMPANYLOAN,$where); 
		
		$where = array('is_deleted'=>0);
		$config['totalbankaccount'] =$this->adminmodel->getCount(TBLCOMPANYBANK,$where); 
		
		$where = array('is_deleted'=>0);
		$config['totalprojects'] =$this->adminmodel->getCount(TBLPROJECTS,$where); 
		
		$where = array('is_deleted'=>0);
		$config['totalfinancialinstitute'] =$this->adminmodel->getCount(TBLFINANCIALINSTITUTE,$where); 
		
		$where = array('is_deleted'=>0);
		$config['getprojects'] =$this->adminmodel->getwhere(TBLPROJECTS,$where); 
		
		$config['title']='dashboard';
		$this->load->view('admin/dashboard',$config);
		
	}
	
	/*login for  user  Start*/
	public function login()
	{
		$this->load->library('form_validation');	
		$this->form_validation->set_rules('loginusername', 'User Name', 'required|trim');
		$this->form_validation->set_rules('loginpass', 'Password', 'required');
		if ($this->form_validation->run() == TRUE)
		{	
			$loginusername=$this->input->post('loginusername',true);
			$password=$this->input->post('loginpass',true);
			//$remember=$this->input->post('remember_me',true);
			
			$result=$this->adminmodel->login($loginusername,$password);
				
			if($result) 
			{
				redirect('administrator/my-profile');
			}
			else
			{  
				$this->session->set_flashdata('message','Your email or password is incorrect.');
				redirect('administrator/user-login');
			}
		}
	    else{
			$config['title']='login';
			$this->load->view('admin/login',$config);
		   }
	}	
    
	/*login for  user  End*/
	/*logout for  user  start*/
	public function logout()
	{
		$newdata = array('admin_userid' =>'', 'admin_username' =>'', 'admin_usertype'=>'', 'admin_email' =>'','admin_profilepic'=>'','admin_logged_in' =>false);
		$this->session->unset_userdata($newdata);
		redirect('administrator/user-login');
	}
		
	/*logout for  user  End*/
	public function my_profile()
	{
		$this->_chk_if_login();
		$userid = $this->session->userdata('admin_userid');
		
		$where = array('userid'=>$userid);
		$config['usersinfo'] = $this->adminmodel->getSingle(ADMINUSERS,$where);
       
		$config['title']='My profile';
	    $this->load->view('admin/my_profile',$config);
	}
	public function updatemyprofile()
	{
		$this->_chk_if_login();
		$userid = $this->session->userdata('admin_userid');
		if($_POST)
	    { 
			/* check email address allready register or not */ 
			$this->load->library('form_validation');
			$this->form_validation->set_rules('firstname', 'First Name', 'required|trim');
			$this->form_validation->set_rules('lastname', 'Last Name', 'required|trim');
			$this->form_validation->set_rules('username', 'User Name', 'required|trim');
			$this->form_validation->set_rules('phone', 'Phone Number', 'required|trim');								
			$data=array(
				'firstname'=>  $this->input->post('firstname',true),
				'lastname'=>  $this->input->post('lastname',true),
				'username'=>  $this->input->post('username',true),
				'phone'=>$this->input->post('phone',true),	
				'email'=>$this->input->post('email',true),
			);
			
			$where=array('userid'=>$userid);
			$this->adminmodel->update_data(ADMINUSERS,$where,$data);
	   
			$this->session->set_flashdata('success','You profile information updated successfully.');
			redirect('administrator/my-profile');	
		}
	}
	
	public function updateprofilepicture()
	{
		$this->_chk_if_login();
		$userid = $this->session->userdata('admin_userid');
		
		if($_FILES['file']['name'] !='')
		{
			$fileName=date('Ymdhis').'_'.$_FILES['file']['name'];
			$chkfile=move_uploaded_file($_FILES['file']["tmp_name"],"uploads/profile/".$fileName);
			if($chkfile){
				$profilepicture=$fileName;
			}else{
				$profilepicture='';
			}
		}	
		
		if($profilepicture!=''){
			
			$data=array('profile_pic'=>  $profilepicture);
			$where=array('userid'=>$userid);
			$this->adminmodel->update_data(ADMINUSERS,$where,$data);
			$newdata = array(
				'admin_profilepic'    => $profilepicture,
					   );
			$this->session->set_userdata($newdata);
		}
   
		$this->session->set_flashdata('success','You profile picture updated successfully.');
		redirect('administrator/my-profile');
		
	}
	
	public function updatepassword()
	{
		$this->_chk_if_login();
		$userid = $this->session->userdata('admin_userid');
		$currentpassword = $this->input->post('currentpassword',true);
		$newpassword = $this->input->post('newpassword',true);
		//currentpassword newpassword
		
		$where = array('userid'=>$userid,'show_passkey'=>$currentpassword);
		$chkprofile = $this->adminmodel->getSingle(ADMINUSERS,$where);
		
		if($chkprofile->userid!=''){
			$hashed_password = password_hash($newpassword, PASSWORD_DEFAULT);	
			
			$data=array('password'=> $hashed_password,'show_passkey'=>$newpassword);
			$where=array('userid'=>$userid);
			$this->adminmodel->update_data(ADMINUSERS,$where,$data);
			
			$this->session->set_flashdata('success','You password changed successfully.');
			redirect('administrator/my-profile');
			
		}else{
			
			$this->session->set_flashdata('error','You current password is not valid.');
			redirect('administrator/my-profile');
		}

	}
	
	/****************************** Settings *******************************/
	
	public function settings()
	{
		$this->_chk_if_login();
		
		$where=array('usertype'=>2);
		$config['settings'] = $this->adminmodel->getwhere(SETTINGPERMISSION,$where);
		
		$wherecontractors=array('usertype'=>3);
		$config['settingscontractors'] = $this->adminmodel->getwhere(SETTINGPERMISSION,$wherecontractors);
		
		$where = array('is_deleted'=>0);
		$config['usersgroups'] =$this->adminmodel->getwhere(USERGROUP,$where);
		
		$config['title']='Settings';
		$this->load->view('admin/settings',$config);
	}
	
	
	
	/***************** Manage company  ********************/
    public function company_list()
	{
		$this->_chk_if_login();
		
		//$this->_chk_if_permissions('Compnies','allowfor_edit');
		
		$where=array('is_deleted'=>0);
		$config['companylist'] = $this->adminmodel->getwhere(TBLCOMPANY,$where);
		
		$config['title']='All Company';
		$this->load->view('admin/company_list',$config);
	}
	
	
	public function addnewcompany()
	{
		$this->_chk_if_login();
		
		$this->_chk_if_permissions('Compnies','allowfor_add');
		
		error_reporting(0);
		if($_POST){
						
			$company_partners = implode(',',$this->input->post('company_partners',true));
			$data=array(
				'company_name'=>  $this->input->post('companyname',true),
				'phoneno'=>  $this->input->post('phoneno',true),
				'found_date'=>  $this->input->post('found_date',true),
				'email'=>  $this->input->post('email',true),
				'address'=>  $this->input->post('address',true),
				'company_partners'=>  $company_partners,
				'crib_no'=>  $this->input->post('crib_no',true),
				'kvk_no'=>  $this->input->post('kvk_no',true),
				'create_datetime'=> date('Y-m-d H:i:s'),
			);
			$insert = $this->adminmodel->insert_data(TBLCOMPANY,$data);
			
			$this->session->set_flashdata('success','You have added new company successfully.');
			redirect('administrator/company_list');
	           			
		}else{
			
			$where=array('is_deleted'=>0);
			$config['partners'] = $this->adminmodel->getwhere(TBLPARTNER,$where);
			
		    $config['title']='Add New Company';
			$this->load->view('admin/addnewcompany',$config);	
		}
	}
	
	public function companyaddproject()
	{
		$this->_chk_if_login();
		if($_POST){
			$data=array(
				'companyid'=>  $this->input->post('hdncompanyid',true),
				'projectname'=>  $this->input->post('projectname',true),
				'startdate'=>  $this->input->post('startdate',true),
				'enddate'=>  $this->input->post('enddate',true),
				'estimatedcost'=>  $this->input->post('estimatedcost',true),
				'payments'=>  $this->input->post('payments',true),
			);
			$insert = $this->adminmodel->insert_data(TBLCOMPANYPROJECT,$data);
			
			$where=array('companyid'=>$this->input->post('hdncompanyid',true));
			$config['projectlist'] = $this->adminmodel->getwhere(TBLCOMPANYPROJECT,$where);
			$this->load->view('admin/getcompanyprojectpage',$config);	
		}
	}
	
	public function companyaddBank()
	{
		$this->_chk_if_login();
		if($_POST){
			$data=array( 
				'companyid'=>  $this->input->post('hdncompanyid',true),
				'bankname'=>  $this->input->post('bankname',true),
				'accounttype'=>  $this->input->post('accounttype',true),
				'accountname'=>  $this->input->post('accountname',true),
				'startballence'=>  $this->input->post('startballence',true),
				'currentballence'=>  $this->input->post('currentballence',true),
			);
			$insert = $this->adminmodel->insert_data(TBLCOMPANYBANK,$data);
			
			$where=array('companyid'=>$this->input->post('hdncompanyid',true));
			$config['banklist'] = $this->adminmodel->getwhere(TBLCOMPANYBANK,$where);
			$this->load->view('admin/getcompanybankpage',$config);	
		}
	}
	 
	public function companyaddLoans()
	{
		$this->_chk_if_login();
		if($_POST){
			$data=array( 
				'companyid'=>  $this->input->post('hdncompanyid',true),
				'financialinstitution'=>  $this->input->post('financialinstitution',true),
				'project'=>  $this->input->post('project',true),
				'loanamount'=>  $this->input->post('loanamount',true),
				'interestrate'=>  $this->input->post('interestrate',true),
				'period'=>  $this->input->post('period',true),
				'firstdate'=>  $this->input->post('firstdate',true),
				'enddate'=>  $this->input->post('enddate',true),
				'redemptionamount'=>  $this->input->post('redemptionamount',true),
			);
			$insert = $this->adminmodel->insert_data(TBLCOMPANYLOAN,$data);
			
			$where=array('companyid'=>$this->input->post('hdncompanyid',true));
			$config['loanlist'] = $this->adminmodel->getwhere(TBLCOMPANYLOAN,$where);
			$this->load->view('admin/getcompanyloanpage',$config);	
		}
	}
	
	public function editcompany()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Compnies','allowfor_modify');
		
		$company_id = $this->input->get('company_id',true);
				
		$where = array('company_id'=>$company_id);
		$config['editcompany'] = $this->adminmodel->getSingle(TBLCOMPANY,$where);
		
		$where=array('is_deleted'=>0);
		$config['partners'] = $this->adminmodel->getwhere(TBLPARTNER,$where);

		$config['title']='Edit Company';
		$this->load->view('admin/editcompany',$config);
	}
	
	public function updatecompany_action()
	{
		$this->_chk_if_login();
		
		
			$company_id = $this->input->post('hdncompany',true);
			$company_partners = implode(',',$this->input->post('company_partners',true));
			$data=array(
				'company_name'=>  $this->input->post('companyname',true),
				'phoneno'=>  $this->input->post('phoneno',true),
				'found_date'=>  $this->input->post('found_date',true),
				'email'=>  $this->input->post('email',true),
				'address'=>  $this->input->post('address',true),
				'company_partners'=>  $company_partners,
				'crib_no'=>  $this->input->post('crib_no',true),
				'kvk_no'=>  $this->input->post('kvk_no',true),
				
			);
			
			$where=array('company_id'=>$company_id);
			$this->adminmodel->update_data(TBLCOMPANY,$where,$data);
			
			$this->session->set_flashdata('success','You have updated company successfully.');
			redirect('administrator/company_list');
		
	}
	
	public function company_details()
	{
		$this->_chk_if_login();
		
		$this->_chk_if_permissions('Compnies','allowfor_edit');
		
		$company_id = $this->input->get('company_id',true);
		
		$where = array('company_id'=>$company_id);
		$config['editcompany'] = $this->adminmodel->getSingle(TBLCOMPANY,$where);
		
		$where = array('is_deleted'=>0,'companyid'=>$company_id);
		$config['loanlist'] =$this->adminmodel->getwhere(TBLCOMPANYLOAN,$where); 
	
		$where = array('is_deleted'=>0,'companyid'=>$company_id);
		$config['banklist'] =$this->adminmodel->getwhere(TBLCOMPANYBANK,$where); 
		
		$where = array('is_deleted'=>0,'companyname'=>$company_id);
		$config['projectlist'] =$this->adminmodel->getwhere(TBLPROJECTS,$where);

		$config['title']='Company Details';
		$this->load->view('admin/company_details',$config);
	}

	public function deletecompany()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Compnies','allow_delete');
		
		$company_id = $this->input->get('company_id');
		
		$where=array('company_id'=>$company_id);
		$data=array('is_deleted'=>1);
		$this->adminmodel->update_data(TBLCOMPANY,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted company successfully.');
		redirect('administrator/company_list');
		
	}
	
	/*****************  Manage Loan *************************/
	
	
	public function loan_list()
	{
		$this->_chk_if_login();
		//$this->_chk_if_permissions('Loans','allowfor_edit');
		
		$where=array('is_deleted'=>0);
		$config['loanlist'] = $this->adminmodel->getwhere(TBLCOMPANYLOAN,$where);
		
		$config['title']='All Loan';
		$this->load->view('admin/loan_list',$config);
	}
	
	public function addloan()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Loans','allowfor_add');
		
		error_reporting(0);
		
		if($_POST){
			$data=array( 
				'companyid'=>$this->input->post('companyname',true), 
				'financialinstitution'=>  $this->input->post('financialinstitution',true),
				'project'=>  $this->input->post('project',true),
				'loanamount'=>  $this->input->post('loanamount',true),
				'interestrate'=>  $this->input->post('interestrate',true),
				'period'=>  $this->input->post('period',true),
				'firstdate'=>  $this->input->post('firstdate',true),
				'enddate'=>  $this->input->post('enddate',true),
				'redemptionamount'=>  $this->input->post('redemptionamount',true),
			);
			$insert = $this->adminmodel->insert_data(TBLCOMPANYLOAN,$data);
			
			$this->session->set_flashdata('success','You have added new loan successfully.');
			redirect('administrator/loan_list');
	           			
		}else{
			
			$where=array('is_deleted'=>0);
			$config['financialinstitutelist'] = $this->adminmodel->getwhere(TBLFINANCIALINSTITUTE,$where);
			
			$where=array('is_deleted'=>0);
			$config['projectlist'] = $this->adminmodel->getwhere(TBLPROJECTS,$where);
			
			$where=array('is_deleted'=>0);
			$config['companylist'] = $this->adminmodel->getwhere(TBLCOMPANY,$where);
		
		    $config['title']='Add New Loan';
			$this->load->view('admin/addloan',$config);	
		}
	}
	
	public function editloan()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Loans','allowfor_modify');
		
		error_reporting(0);
		if($_POST){
			
			$companyloanid = $this->input->post('companyloanid',true);
			$data=array( 
				'companyid'=>$this->input->post('companyname',true),
				'financialinstitution'=>  $this->input->post('financialinstitution',true),
				'project'=>  $this->input->post('project',true),
				'loanamount'=>  $this->input->post('loanamount',true),
				'interestrate'=>  $this->input->post('interestrate',true),
				'period'=>  $this->input->post('period',true),
				'firstdate'=>  $this->input->post('firstdate',true),
				'enddate'=>  $this->input->post('enddate',true),
				'redemptionamount'=>  $this->input->post('redemptionamount',true),
			);
			
			$where=array('companyloanid'=>$companyloanid);
			$this->adminmodel->update_data(TBLCOMPANYLOAN,$where,$data);
		
			$this->session->set_flashdata('success','You have updated loan successfully.');
			redirect('administrator/loan_list');
	           			
		}else{
			$loanid = $this->input->get('loanid',true);
			
			$where=array('companyloanid'=>$loanid);
			$config['loandetails'] = $this->adminmodel->getSingle(TBLCOMPANYLOAN,$where,$data);
			
			$where=array('is_deleted'=>0);
			$config['financialinstitutelist'] = $this->adminmodel->getwhere(TBLFINANCIALINSTITUTE,$where);
			
			$where=array('is_deleted'=>0);
			$config['projectlist'] = $this->adminmodel->getwhere(TBLPROJECTS,$where);
			
			$where=array('is_deleted'=>0);
			$config['companylist'] = $this->adminmodel->getwhere(TBLCOMPANY,$where);
			
		    $config['title']='Edit Loan';
			$this->load->view('admin/editloan',$config);	
		}
	}
	
	public function deleteloan()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Loans','allow_delete');
		
		$loanid = $this->input->get('loanid',true);
		
		$where = array('companyloanid'=>$loanid);
		$data=array('is_deleted'=>1);
		$this->adminmodel->update_data(TBLCOMPANYLOAN,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted loan successfully.');
		redirect('administrator/loan_list');
		
	}
	
	/**************** Manage Bank Account ***************/ 
	
	public function bankaccountlist()
	{
		$this->_chk_if_login();
		//$this->_chk_if_permissions('Bank Accounts','allowfor_edit');
		
		$where=array('is_deleted'=>0);
		$config['banklist'] = $this->adminmodel->getwhere(TBLCOMPANYBANK,$where);
		
		$config['title']='All Bank Accounts';
		$this->load->view('admin/bankaccountlist',$config);
	}
	
	public function addbankaccount()
	{
		$this->_chk_if_login();
		
		$this->_chk_if_permissions('Bank Accounts','allowfor_add');
		
		if($_POST){
			//companyname
			$data=array( 
				'bankname'=>  $this->input->post('bankname',true),
				'companyid'=> $this->input->post('companyname',true),
				'accounttype'=>  $this->input->post('accounttype',true),
				'accountname'=>  $this->input->post('accountname',true),
				'startballence'=>  $this->input->post('startballence',true),
				'currentballence'=>  $this->input->post('currentballence',true),
			);
			$insert = $this->adminmodel->insert_data(TBLCOMPANYBANK,$data);
			
			$this->session->set_flashdata('success','You have added new bank successfully.');
			redirect('administrator/bankaccountlist');
	           			
		}else{
			
			$where=array('is_deleted'=>0);
			$config['companylist'] = $this->adminmodel->getwhere(TBLCOMPANY,$where);
								
		    $config['title']='Add New Bank';
			$this->load->view('admin/addbankaccount',$config);	
		}
	}
	
	public function editbankaccount()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Bank Accounts','allowfor_modify');
		
		if($_POST){
			
			$bank_id = $this->input->post('hdnbankid',true);
			$data=array( 
				'bankname'=>  $this->input->post('bankname',true),
				'companyid'=> $this->input->post('companyname',true),
				'accounttype'=>  $this->input->post('accounttype',true),
				'accountname'=>  $this->input->post('accountname',true),
				'startballence'=>  $this->input->post('startballence',true),
				'currentballence'=>  $this->input->post('currentballence',true),
			);
			
			$where=array('companybankid'=>$bank_id);
			$this->adminmodel->update_data(TBLCOMPANYBANK,$where,$data);
		
			$this->session->set_flashdata('success','You have updated bank successfully.');
			redirect('administrator/bankaccountlist');
	           			
		}else{
			$bank_id = $this->input->get('bank_id',true);
			
			$where=array('companybankid'=>$bank_id);
			$config['banks'] = $this->adminmodel->getSingle(TBLCOMPANYBANK,$where);
			
			$where=array('is_deleted'=>0);
			$config['companylist'] = $this->adminmodel->getwhere(TBLCOMPANY,$where);
			
		    $config['title']='Edit Bank';
			$this->load->view('admin/editbankaccount',$config);	
		}
	}
	
	
	public function bank_details()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Bank Accounts','allowfor_edit');
		
		$bank_id = $this->input->get('bank_id',true);
		
		$where=array('companybankid'=>$bank_id);
		$config['banks'] = $this->adminmodel->getSingle(TBLCOMPANYBANK,$where);
								
		$config['title']='Bank Details';
		$this->load->view('admin/bank_details',$config);	
		
	}
	
	public function deletebankaccount()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Bank Accounts','allow_delete');
		
		$bank_id = $this->input->get('bank_id',true);
		
		$where=array('companybankid'=>$bank_id);
		$data=array('is_deleted'=>1);
		$this->adminmodel->update_data(TBLCOMPANYBANK,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted bank successfully.');
		redirect('administrator/bankaccountlist');
		
	}
	
	/* Manage Partners */
	public function partners_list()
	{
		$this->_chk_if_login();
		//$this->_chk_if_permissions('Partners','allowfor_edit');
		
		$where=array('is_deleted'=>0);
		$config['partners'] = $this->adminmodel->getwhere(TBLPARTNER,$where);
		
		$config['title']='All Partners';
		$this->load->view('admin/partners_list',$config);
	}
	
	
	public function addpartners()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Partners','allowfor_add');
		
		if($_POST){
			
			$data=array(
				'partner_name'=>  $this->input->post('partner_name',true),
				'partner_phone'=>  $this->input->post('partner_phone',true),
				'partner_email'=>  $this->input->post('partner_email',true),
				'partner_address'=>  $this->input->post('partner_address',true),
				'partner_cribno'=>  $this->input->post('partner_cribno',true),
				'registerdate'=> date('Y-m-d H:i:s'),
			);
			$insert = $this->adminmodel->insert_data(TBLPARTNER,$data);
			
			$this->session->set_flashdata('success','You have added new partner successfully.');
			redirect('administrator/partners_list');
	           			
		}else{
		    $config['title']='Add Partners';
			$this->load->view('admin/addpartners',$config);	
		}
	}
	
	
	public function editpartners()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Partners','allowfor_modify');
		
		if($_POST){
			$partner_id = $this->input->post('hdnpartner',true);
			$data=array(
				'partner_name'=>  $this->input->post('partner_name',true),
				'partner_phone'=>  $this->input->post('partner_phone',true),
				'partner_email'=>  $this->input->post('partner_email',true),
				'partner_address'=>  $this->input->post('partner_address',true),
				'partner_cribno'=>  $this->input->post('partner_cribno',true),
			);
						
			$where = array('partner_id'=>$partner_id);
			$this->adminmodel->update_data(TBLPARTNER,$where,$data);
			
			$this->session->set_flashdata('success','You have updated partner successfully.');
			redirect('administrator/partners_list');
			
		}else{
			
			$partner_id = $this->input->get('partner_id',true);
				
			$where = array('partner_id'=>$partner_id);

			$config['partnersval'] = $this->adminmodel->getSingle(TBLPARTNER,$where);
		
		    $config['title']='Edit Partners';
			$this->load->view('admin/editpartners',$config);	
		}
	}
	
	
	public function deletepartner()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Partners','allow_delete');
		
		$partner_id = $this->input->get('partner_id',true);
		
		$where = array('partner_id'=>$partner_id);
		$data=array('is_deleted'=>1);
		$this->adminmodel->update_data(TBLPARTNER,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted partner successfully.');
		redirect('administrator/partners_list');
		
	}
	
	/* Manage Suppliers */
	
	public function suppliers_list()
	{
		$this->_chk_if_login();
		//$this->_chk_if_permissions('Suppliers','allowfor_edit');
		
		$where=array('is_deleted'=>0);
		$config['supplierslist'] = $this->adminmodel->getwhere(TBLSUPPLIERS,$where);
		
		$config['title']='All Suppliers';
		$this->load->view('admin/suppliers_list',$config);
	}
	
	
	public function addsuppliers()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Suppliers','allowfor_add');
		
		if($_POST){
					
			$data=array(
				'suppliers_name'=>  $this->input->post('suppliers_name',true),
				'suppliers_type'=>  $this->input->post('suppliers_type',true),
				'suppliers_address'=>  $this->input->post('suppliers_address',true),
				'suppliers_phone'=>  $this->input->post('suppliers_phone',true),
				'suppliers_bankacno'=>  $this->input->post('suppliers_bankacno',true),
				'suppliers_outstandingamount'=>  $this->input->post('suppliers_outstandingamount',true),
				'suppliers_payments'=>  $this->input->post('suppliers_payments',true),
				'suppliers_kvkno'=>  $this->input->post('suppliers_kvkno',true),
				'suppliers_datetime'=> date('Y-m-d H:i:s'),
			);
			$insert = $this->adminmodel->insert_data(TBLSUPPLIERS,$data);
			
			$this->session->set_flashdata('success','You have added new suppliers successfully.');
			redirect('administrator/suppliers_list');
	           			
		}else{
		    $config['title']='Add Suppliers';
			$this->load->view('admin/addsuppliers',$config);	
		}
	}
	
	
	public function editsuppliers()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Suppliers','allowfor_modify');
		
		if($_POST){
			
			$suppliers_id = $this->input->post('hdnsuppliers',true);
			
			$data=array(
				'suppliers_name'=>  $this->input->post('suppliers_name',true),
				'suppliers_type'=>  $this->input->post('suppliers_type',true),
				'suppliers_address'=>  $this->input->post('suppliers_address',true),
				'suppliers_phone'=>  $this->input->post('suppliers_phone',true),
				'suppliers_bankacno'=>  $this->input->post('suppliers_bankacno',true),
				'suppliers_outstandingamount'=>  $this->input->post('suppliers_outstandingamount',true),
				'suppliers_payments'=>  $this->input->post('suppliers_payments',true),
				'suppliers_kvkno'=>  $this->input->post('suppliers_kvkno',true),
			);
			
			$where = array('suppliers_id'=>$suppliers_id);
			$this->adminmodel->update_data(TBLSUPPLIERS,$where,$data);
			
			$this->session->set_flashdata('success','You have updated suppliers successfully.');
			redirect('administrator/suppliers_list');
								
		}else{
			
			$suppliers_id = $this->input->get('suppliers_id',true);
				
			$where = array('suppliers_id'=>$suppliers_id);
			$config['editsuppliers'] = $this->adminmodel->getSingle(TBLSUPPLIERS,$where);
			
			$sql = $this->db->query('select SUM(invoiceamount) as totalpayment from '.PURCHASEORDERS.' where suppliers="'.$suppliers_id.'" and is_verified=2');
			$getrows = $sql->row();
			$totalpayment = $getrows->totalpayment;
			
			$sqlpaid = $this->db->query('select SUM(invoiceamount) as totalpaidpayment from '.PURCHASEORDERS.' where suppliers="'.$suppliers_id.'" and is_verified=2 and ispaid=1');
			$getpaidrows = $sqlpaid->row();
			$totalpaidpayment = $getpaidrows->totalpaidpayment;
				
			//$config['outstandingpayment'] = $totalpayment - $totalpaidpayment;
			$config['paidpayments'] = $totalpaidpayment;
			
			
			 $sqloutstandingpayment = $this->db->query('select SUM(invoiceamount) as totaloutstandingpayment from '.PURCHASEORDERS.' where suppliers="'.$suppliers_id.'" and ispaid=0 and is_verified!=0');
			 $getrows = $sqloutstandingpayment->row();
			 $config['outstandingpayment'] = $getrows->totaloutstandingpayment;
							  
					
		    $config['title']='Edit Suppliers';
			$this->load->view('admin/editsuppliers',$config);	
		}
	}
	
	public function view_suppliers()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Suppliers','allowfor_edit');
		
		$suppliers_id = $this->input->get('suppliers_id',true);
			
		$where = array('suppliers_id'=>$suppliers_id);
		$config['viewsuppliers'] = $this->adminmodel->getSingle(TBLSUPPLIERS,$where);
		
		$sql = $this->db->query('select SUM(invoiceamount) as totalpayment from '.PURCHASEORDERS.' where suppliers="'.$suppliers_id.'" and is_verified=2');
		$getrows = $sql->row();
		$totalpayment = $getrows->totalpayment;
		
		$sqlpaid = $this->db->query('select SUM(invoiceamount) as totalpaidpayment from '.PURCHASEORDERS.' where suppliers="'.$suppliers_id.'" and is_verified=2 and ispaid=1');
		$getpaidrows = $sqlpaid->row();
		$totalpaidpayment = $getpaidrows->totalpaidpayment;
		
		$config['outstandingpayment'] = $totalpayment - $totalpaidpayment;
		$config['paidpayments'] = $totalpaidpayment;
		
		$where = array('suppliers'=>$suppliers_id,'is_verified'=>2);
		$config['listofinvoice'] = $this->adminmodel->getWhere(PURCHASEORDERS,$where);
		
		$wherepaid = array('suppliers'=>$suppliers_id,'is_verified'=>2,'ispaid'=>0);
		$config['listofinvoicenotpaid'] = $this->adminmodel->getWhere(PURCHASEORDERS,$wherepaid);
						
		$config['title']='View Suppliers';
		$this->load->view('admin/view_suppliers',$config);	
	}
	
	
	public function deletesuppliers()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Suppliers','allow_delete');
		
		$suppliers_id = $this->input->get('suppliers_id');
		
		$where = array('suppliers_id'=>$suppliers_id);
		$data=array('is_deleted'=>1); 
		$this->adminmodel->update_data(TBLSUPPLIERS,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted Suppliers successfully.');
		redirect('administrator/suppliers_list');
		
	}
	
	/****************** Contractors list *********************/
	
	public function contractors_list()
	{
		$this->_chk_if_login();
		//$this->_chk_if_permissions('Contractors','allowfor_edit');
		
		$where=array('is_deleted'=>0);
		$config['contractors'] = $this->adminmodel->getwhere(TBLCONTRACTORS,$where);
		
		$config['title']='All Contractors';
		$this->load->view('admin/contractors_list',$config);
	}
	
	
	public function addnewcontractor()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Contractors','allowfor_add');
		
		if($_POST){
			$contractor_assignproject = implode(',',$this->input->post('contractor_assignproject',true));
			 
			$data=array(
				'contractor_name'=>  $this->input->post('contractor_name',true),
				'contractor_phone'=>  $this->input->post('contractor_phone',true),
				'contractor_type'=>  $this->input->post('contractor_type',true),
				'contractor_person'=>  $this->input->post('contractor_person',true),
				'contractor_address'=>  $this->input->post('contractor_address',true),
				'contractor_assignproject'=>  $contractor_assignproject,
				'contractor_cribno'=>  $this->input->post('contractor_cribno',true),
				'contractor_kvkno'=>  $this->input->post('contractor_kvkno',true),
				'contractor_payments'=>  $this->input->post('contractor_payments',true),
				'create_date'=> date('Y-m-d H:i:s'),
			);
			$insert = $this->adminmodel->insert_data(TBLCONTRACTORS,$data);
			
			$this->session->set_flashdata('success','You have added new contractors successfully.');
			redirect('administrator/contractors_list');
	           			
		}else{
			
			$where = array('is_deleted'=>0);
			$config['projectslist'] = $this->adminmodel->getwhere(TBLPROJECTS,$where);
			
			$where=array('is_deleted'=>0);
			$config['contractortypes'] = $this->adminmodel->getwhere(CONTRACTORTYPE,$where);
			
		    $config['title']='Add Contractors';
			$this->load->view('admin/addnewcontractor',$config);	
		}
	}
	
	
	public function editcontractor()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Contractors','allowfor_modify');
		
		if($_POST){
			
			$contractor_id = $this->input->post('hdncontractor',true);
			$contractor_assignproject = implode(',',$this->input->post('contractor_assignproject',true));
			$data=array(
				'contractor_name'=>  $this->input->post('contractor_name',true),
				'contractor_phone'=>  $this->input->post('contractor_phone',true),
				'contractor_type'=>  $this->input->post('contractor_type',true),
				'contractor_person'=>  $this->input->post('contractor_person',true),
				'contractor_address'=>  $this->input->post('contractor_address',true),
				'contractor_assignproject'=>  $contractor_assignproject,
				'contractor_cribno'=>  $this->input->post('contractor_cribno',true),
				'contractor_kvkno'=>  $this->input->post('contractor_kvkno',true),
				'contractor_payments'=>  $this->input->post('contractor_payments',true),
			);
			$where=array('contractor_id'=>$contractor_id);
			$this->adminmodel->update_data(TBLCONTRACTORS,$where,$data);
			
			$this->session->set_flashdata('success','You have updated contractors successfully.');
			redirect('administrator/contractors_list');
		
			
			
		}else{
			
			$contractor_id = $this->input->get('contractor_id',true);
				
			$where = array('contractor_id'=>$contractor_id);

			$config['contractors'] = $this->adminmodel->getSingle(TBLCONTRACTORS,$where);
			
			$where = array('is_deleted'=>0);
			$config['projectslist'] = $this->adminmodel->getwhere(TBLPROJECTS,$where);
			
			
			$sql = $this->db->query('select SUM(contractoramount) as totalcontractorpayment from '.TBLPAYMENTS.' a JOIN '.TBLPROJECTS.' b ON a.project=b.projectid where contractorid='.$contractor_id);
			$getrows = $sql->row();
			$config['contractoramount']= $getrows->totalcontractorpayment;
		
			/*$sql = $this->db->query('select SUM(Amount) as totalcontractorpayment from '.TBLPAYMENTS.' where contractorid='.$contractor_id);
			$getrows = $sql->row();
			$config['contractoramount']= $getrows->totalcontractorpayment;*/
			
			$where=array('is_deleted'=>0);
			$config['contractortypes'] = $this->adminmodel->getwhere(CONTRACTORTYPE,$where);
					
		    $config['title']='Edit Contractors';
			$this->load->view('admin/editcontractor',$config);	
		}
	}
	
	
	public function deletecontractor()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Contractors','allow_delete');
		
		$contractor_id = $this->input->get('contractor_id');
		
		$where = array('contractor_id'=>$contractor_id);
		$data=array('is_deleted'=>1);
		$this->adminmodel->update_data(TBLCONTRACTORS,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted contractor successfully.');
		redirect('administrator/contractors_list');
		
	}
	
	
	public function contractors_details()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Contractors','allowfor_edit');
		
		$contractor_id = $this->input->get('contractor_id',true);
			
		$where = array('contractor_id'=>$contractor_id);
		$config['contractorsval'] = $this->adminmodel->getSingle(TBLCONTRACTORS,$where);
		
		
		//$sql = $this->db->query('select SUM(contractoramount) as totalcontractorpayment from '.TBLPROJECTS.' where contractorid='.$contractor_id);
		//$getrows = $sql->row();
		//$config['contractoramount']= $getrows->totalcontractorpayment;
		
		$sql = $this->db->query('select SUM(contractoramount) as totalcontractorpayment from '.TBLPAYMENTS.' a JOIN '.TBLPROJECTS.' b ON a.project=b.projectid where contractorid='.$contractor_id);
		$getrows = $sql->row();
		$config['contractoramount']= $getrows->totalcontractorpayment;
		
				
		$sql = $this->db->query('select SUM(Amount) as totalpayment from '.TBLPAYMENTS.' where contractorid='.$contractor_id);
		$getrows = $sql->row();
		$config['totalpayment'] = $getrows->totalpayment;
		
		
		//$where = array('contractorid'=>$contractor_id);
		//$config['projectlist'] = $this->adminmodel->getWhere(TBLPROJECTS,$where);
		
		$sqlproject = $this->db->query('select * from '.TBLPAYMENTS.' a JOIN '.TBLPROJECTS.' b ON a.project=b.projectid  where contractorid='.$contractor_id.' group by b.projectid');
		$config['projectlist'] = $sqlproject->result();
		
		
		$where = array('contractorid'=>$contractor_id);
		$config['paymentslist'] = $this->adminmodel->getWhere(TBLPAYMENTS,$where);
		 
		
		$config['title']='Contractors Details';
		$this->load->view('admin/contractors_details',$config);	
	}

	/*************************** Financial Institute *************************/	
	//financialinstitutionlist addfinancialinstitution editfinancialinstitution deletefinancialinstitution
	
	public function financialinstitutionlist()
	{
		$this->_chk_if_login();
		//$this->_chk_if_permissions('Financial Institute','allowfor_edit');
		
		$where=array('is_deleted'=>0);
		$config['financialinstitutes'] = $this->adminmodel->getwhere(TBLFINANCIALINSTITUTE,$where);
		
		$config['title']='All Financial Institute';
		$this->load->view('admin/financialinstitutionlist',$config);
	}
	
	
	public function addfinancialinstitution()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Financial Institute','allowfor_add');
		
		if($_POST){
			
			
			$data=array(
				'financialinstitutename'=>  $this->input->post('financialinstitutename',true),
				'financialinstituteaddress'=>  $this->input->post('financialinstituteaddress',true),
				'financialinstitutephone'=>  $this->input->post('financialinstitutephone',true),
				'financialinstituteaccountno'=>  $this->input->post('financialinstituteaccountno',true),
			);
			$insert = $this->adminmodel->insert_data(TBLFINANCIALINSTITUTE,$data);
			
			$this->session->set_flashdata('success','You have added new Financial Institute successfully.');
			redirect('administrator/financialinstitutionlist');
	           			
		}else{
			
			$sqlquery = $this->db->query('SELECT * FROM '.TBLCOMPANYLOAN.' JOIN '.TBLFINANCIALINSTITUTE.' ON financialinstitution=financialinstituteid Group by financialinstituteid ASC');
			$config['loanlist'] = $sqlquery->result(); //$this->adminmodel->getwhere(TBLCOMPANYLOAN,$where);
			
		    $config['title']='Add Financial Institute';
			$this->load->view('admin/addfinancialinstitution',$config);	
			
		}
	}
	
	
	public function editfinancialinstitution()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Financial Institute','allowfor_modify');
		
		if($_POST){
			
			$financialinstituteid = $this->input->post('financialinstituteid',true);
			$data=array(
				'financialinstitutename'=>  $this->input->post('financialinstitutename',true),
				'financialinstituteaddress'=>  $this->input->post('financialinstituteaddress',true),
				'financialinstitutephone'=>  $this->input->post('financialinstitutephone',true),
				'financialinstituteaccountno'=>  $this->input->post('financialinstituteaccountno',true),
			);
			$where=array('financialinstituteid'=>$financialinstituteid);
			$this->adminmodel->update_data(TBLFINANCIALINSTITUTE,$where,$data);
			
			$this->session->set_flashdata('success','You have updated Financial Institute successfully.');
			redirect('administrator/financialinstitutionlist');
		
			
		}else{
			
			$financialinstituteid = $this->input->get('financialinstituteid',true);
				
			$where = array('financialinstituteid'=>$financialinstituteid);
			$config['financialinstitutes'] = $this->adminmodel->getSingle(TBLFINANCIALINSTITUTE,$where);
			
			$sqlquery = $this->db->query('SELECT * FROM '.TBLCOMPANYLOAN.' JOIN '.TBLFINANCIALINSTITUTE.' ON financialinstitution=financialinstituteid Group by financialinstituteid ASC');
			$config['loanlist'] = $sqlquery->result();
		
		    $config['title']='Edit Financial Institute';
			$this->load->view('admin/editfinancialinstitution',$config);	
		}
	}
	
	public function financial_details()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Financial Institute','allowfor_edit');
		
		$financialinstituteid = $this->input->get('financialinstituteid',true);
		
		$where = array('financialinstituteid'=>$financialinstituteid);
		$config['financialinstitutes'] = $this->adminmodel->getSingle(TBLFINANCIALINSTITUTE,$where);
		
		$where = array('financialinstitution'=>$financialinstituteid);
		$config['financialloans'] = $this->adminmodel->getWhere(TBLCOMPANYLOAN,$where);
		
		$config['title']='Financial Institute Details';
		$this->load->view('admin/financial_details',$config);	
	}
	
	public function deletefinancialinstitution()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Financial Institute','allow_delete');
		
		$financialinstituteid = $this->input->get('financialinstituteid',true);
		
		$where = array('financialinstituteid'=>$financialinstituteid);
		$data=array('is_deleted'=>1);
		$this->adminmodel->update_data(TBLFINANCIALINSTITUTE,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted Financial Institute successfully.');
		redirect('administrator/financialinstitutionlist');
		
	}

	/************************** Manage Payment *************************/
	
	public function paymentlist()
	{
		$this->_chk_if_login();
		//$this->_chk_if_permissions('Payments','allowfor_edit');
		
		$where=array('is_deleted'=>0);
		$config['paymentslist'] = $this->adminmodel->getwhere(TBLPAYMENTS,$where);
		
		$config['title']='All Payments';
		$this->load->view('admin/paymentlist',$config);
	}
	
	public function addpayment()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Payments','allowfor_add');
		
		if($_POST){
			
			$paymentdate = date('Y-m-d',strtotime($this->input->post('paymentdate',true)));
			
			$invoiceno =  implode(',',$this->input->post('invoiceno',true));
			$invoicenoexp = explode(',',$invoiceno);
			
			if(!empty($invoicenoexp))
			{	
				$suppliers_id = $this->input->post('suppliersval',true);
				foreach($invoicenoexp as $rowsinvoice){ 
					$data=array('is_addinvoice'=>1,'ispaid'=>1,'paymentdate'=>date('Y-m-d H:i:s'));
					$where=array('is_verified !='=>0,'suppliers'=>$suppliers_id,'orderid'=>$rowsinvoice);
					$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
				}
			}
			
		
			$data=array(
				'contractorid'=>  $this->input->post('contractorval',true),
				'project'=>  $this->input->post('project',true),
				'suppliersval'=> $this->input->post('suppliersval',true),
				'purchaseorder'=>  $this->input->post('purchaseorder',true),
				'paidbycompany'=>  $this->input->post('paidbycompany',true),
				'paymentmethod'=>  $this->input->post('paymentmethod',true),
				'paidto'=>  $this->input->post('paidto',true),
				'paymentdescription'=>  $this->input->post('paymentdescription',true),
				'Amount'=>  $this->input->post('Amount',true),
				'projectsection'=>  $this->input->post('projectsection',true),
				'paymentdate'=> $paymentdate,
			);
			$insert = $this->adminmodel->insert_data(TBLPAYMENTS,$data);
			
			$this->session->set_flashdata('success','You have added new payments successfully.');
			redirect('administrator/paymentlist');
	           			
		}else{
			
			$where=array('is_deleted'=>0);
			$config['companylist'] = $this->adminmodel->getwhere(TBLCOMPANY,$where);
			
			//$where=array('is_deleted'=>0);
			//$config['documentlist'] = $this->adminmodel->getwhere(TBLDOCUMENTS,$where);
			
			$where = array('is_deleted'=>0);
			$config['contractors'] = $this->adminmodel->getwhere(TBLCONTRACTORS,$where);
			
			$where=array('is_deleted'=>0);
			$config['projectlist'] = $this->adminmodel->getwhere(TBLPROJECTS,$where);
			
			$where=array('is_deleted'=>0,'is_verified !='=>0,'ispaid'=>0);
			$config['purchaseorders'] = $this->adminmodel->getwhere(PURCHASEORDERS,$where);
			
			$where=array('is_deleted'=>0);
			$config['supplierslist'] = $this->adminmodel->getwhere(TBLSUPPLIERS,$where);
			
			$where=array('is_deleted'=>0);
			$config['paymentmethods'] = $this->adminmodel->getwhere(PAYMENTMETHOD,$where);
			
			$where=array('is_deleted'=>0);
			$config['projectsections'] = $this->adminmodel->getwhere(PROJECTSECTION,$where);
					
		    $config['title']='Add Payments';
			$this->load->view('admin/addpayment',$config);	
			
		}
	}
	
	public function getsuppliersorders()
	{
		$this->_chk_if_login();
		$suppliersid = $this->input->post('suppliers_id',true);
		$where=array('is_deleted'=>0,'is_verified !='=>0,'ispaid'=>0,'suppliers'=>$suppliersid);
		$config['purchaseorders'] = $this->adminmodel->getwhere(PURCHASEORDERS,$where);
			
		$this->load->view('admin/getsuplliersorders',$config);	
		
	}
	
	public function paidinvoiceaddpayment()
	{
		$this->_chk_if_login();
		
		$suppliers_id = $this->input->get('suppliers_id',true);
			
		$data=array('ispaid'=>1,'paymentdate'=>date('Y-m-d H:i:s'));
		$where=array('ispaid'=>0,'is_verified !='=>0,'suppliers'=>$suppliers_id);
		$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
		
		$this->session->set_flashdata('success','You have updated payments successfully.');
		redirect('administrator/addpayment');
	}
	
	
	public function unpaidinvoicesfrompayment()
	{
		$this->_chk_if_login();
		
		$orderid = $this->input->get('orderid',true);
			
		$data=array('is_addinvoice'=>0,'ispaid'=>0,'paymentdate'=>'0000-00-00 00:00:00');
		$where=array('orderid'=>$orderid);
		$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
		
		$this->session->set_flashdata('success','You have unpaid payments successfully.');
		redirect('administrator/editpayment?paymentid='.$orderid);
	}
	
	public function editpayment()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Payments','allowfor_modify');
		
		if($_POST){
			
			$invoiceno =  implode(',',$this->input->post('invoiceno',true));
			$invoicenoexp = explode(',',$invoiceno);
			
			if(!empty($invoicenoexp))
			{	
				$suppliers_id = $this->input->post('suppliersval',true);
				foreach($invoicenoexp as $rowsinvoice){ 
					$data=array('ispaid'=>1,'paymentdate'=>date('Y-m-d H:i:s'));
					$where=array('is_verified !='=>0,'suppliers'=>$suppliers_id,'orderid'=>$rowsinvoice);
					$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
				}
			}
			
			$paymentid = $this->input->post('paymentid',true);
			$data=array(
				'contractorid'=>  $this->input->post('contractorval',true),
				'project'=>  $this->input->post('project',true),
				'suppliersval'=> $this->input->post('suppliersval',true),
				'purchaseorder'=>  $this->input->post('purchaseorder',true),
				'paidbycompany'=>  $this->input->post('paidbycompany',true),
				'paymentmethod'=>  $this->input->post('paymentmethod',true),
				'paidto'=>  $this->input->post('paidto',true),
				'paymentdescription'=>  $this->input->post('paymentdescription',true),
				'Amount'=>  $this->input->post('Amount',true),
			);
			
			$where=array('paymentid'=>$paymentid);
			$this->adminmodel->update_data(TBLPAYMENTS,$where,$data);
			
			$this->session->set_flashdata('success','You have updated payments successfully.');
			redirect('administrator/paymentlist');
		
			
			
		}else{
			
			$where=array('is_deleted'=>0);
			$config['companylist'] = $this->adminmodel->getwhere(TBLCOMPANY,$where);
			
			$paymentid = $this->input->get('paymentid',true);
			$where = array('paymentid'=>$paymentid);
			$config['paymentsval'] = $this->adminmodel->getSingle(TBLPAYMENTS,$where);
			
			$where=array('is_deleted'=>0);
			$config['projectlist'] = $this->adminmodel->getwhere(TBLPROJECTS,$where);
			
			$where=array('is_deleted'=>0);
			$config['documentlist'] = $this->adminmodel->getwhere(TBLDOCUMENTS,$where);
			
			$where = array('is_deleted'=>0);
			$config['contractors'] = $this->adminmodel->getwhere(TBLCONTRACTORS,$where);
			//'ispaid'=>0,
			$where=array('is_deleted'=>0,'is_verified !='=>0,'suppliers'=>$config['paymentsval']->suppliersval);
			$config['purchaseorders'] = $this->adminmodel->getwhere(PURCHASEORDERS,$where);
			
			$where=array('is_deleted'=>0);
			$config['supplierslist'] = $this->adminmodel->getwhere(TBLSUPPLIERS,$where);
			
			$where=array('is_deleted'=>0);
			$config['paymentmethods'] = $this->adminmodel->getwhere(PAYMENTMETHOD,$where);	

			$where=array('is_deleted'=>0);
			$config['projectsections'] = $this->adminmodel->getwhere(PROJECTSECTION,$where);		
		
		    $config['title']='Edit Payments';
			$this->load->view('admin/editpayment',$config);	
		}
	}
	
	
	public function deletepayment()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Payments','allow_delete');
		
		$paymentid = $this->input->get('paymentid',true);
		
		$where = array('paymentid'=>$paymentid);
		$data=array('is_deleted'=>1);
		$this->adminmodel->update_data(TBLPAYMENTS,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted payment successfully.');
		redirect('administrator/paymentlist');
		
	}
	
	/************************** Manage Documents *************************/
	
	public function documentlist()
	{
		$this->_chk_if_login();
		//$this->_chk_if_permissions('Documents','allowfor_edit');
		
		$where=array('is_deleted'=>0);
		$config['documentslist'] = $this->adminmodel->getwhere(TBLDOCUMENTS,$where);
		
		$config['title']='All Documents';
		$this->load->view('admin/documentlist',$config);
	}
	
	public function adddocuments()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Documents','allowfor_add');
		
		if($_POST){
			
			$userid = $this->session->userdata('admin_userid');
			
			
			
			if($_FILES['documentimg']['name'] !='')
			{
				
				$allowed_image_extension = array("png","jpg","jpeg","JPEG","JPG","PNG","docx","xlsx","csv","pdf","PDF");
				// Get image file extension
				$file_extension = pathinfo($_FILES["documentimg"]["name"], PATHINFO_EXTENSION);
				
				// Validate file input to check if is not empty
				if (!file_exists($_FILES["documentimg"]["tmp_name"])) {
					$this->session->set_flashdata('error','Choose image file to upload.');
					redirect('administrator/adddocuments');
				}else if (! in_array($file_extension, $allowed_image_extension)) {
					$this->session->set_flashdata('error','Please upload valid documents only.');
					redirect('administrator/adddocuments');
				}   
				
				
				$fileName=date('Ymdhis').'_'.$_FILES['documentimg']['name'];
				$chkfile=move_uploaded_file($_FILES['documentimg']["tmp_name"],"uploads/documents/".$fileName);
				if($chkfile){
					$documentimg=$fileName;
				}else{
					$documentimg='';
				}
			}	
			
			
		
			$data=array(
				'docname'=>  $this->input->post('docname',true),
				'pdid'=> $this->input->post('projectname',true),
				'documenttype'=> $file_extension,
				'userid'=>  $userid,
				'documentimg'=>  $documentimg,
				'documentadddate'=>  date('Y-m-d'),
			);
			$insert = $this->adminmodel->insert_data(TBLDOCUMENTS,$data);
			
			$this->session->set_flashdata('success','You have added new documents successfully.');
			redirect('administrator/documentlist');
	           			
		}else{
			
			$where=array('is_deleted'=>0);
			$config['projectlist'] = $this->adminmodel->getwhere(TBLPROJECTS,$where);
		
		    $config['title']='Add Documents';
			$this->load->view('admin/adddocuments',$config);	
			
		}
	}
	
	
	public function editdocuments()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Documents','allowfor_modify');
		
		if($_POST){
			 
			$documentid = $this->input->post('documentid',true);
			if($_FILES['documentimg']['name'] !='')
			{
				$allowed_image_extension = array("png","jpg","jpeg","JPEG","JPG","PNG","docx","xlsx","csv","pdf","PDF");
				// Get image file extension
				$file_extension = pathinfo($_FILES["documentimg"]["name"], PATHINFO_EXTENSION);
				
				// Validate file input to check if is not empty
				if (!file_exists($_FILES["documentimg"]["tmp_name"])) {
					$this->session->set_flashdata('error','Choose image file to upload.');
					redirect('administrator/adddocuments');
				}else if (! in_array($file_extension, $allowed_image_extension)) {
					$this->session->set_flashdata('error','Please upload valid documents only.');
					redirect('administrator/adddocuments');
				}   
				
				$fileName=date('Ymdhis').'_'.$_FILES['documentimg']['name'];
				$chkfile=move_uploaded_file($_FILES['documentimg']["tmp_name"],"uploads/documents/".$fileName);
				if($chkfile){
					$documentimg = $fileName;
				}else{
					$documentimg = $this->input->post('hdnimg',true);
				}
			}else{
				$documentimg = $this->input->post('hdnimg',true);
			}				
		    
			
			if($file_extension!='')
			{
				$documenttype = $file_extension;
			}else{
				$documenttype = $this->input->post('hdndocumenttype',true); 
			}
		   
			$data=array(
				'docname'=>  $this->input->post('docname',true),
				'documentimg'=>  $documentimg,
				'pdid'=> $this->input->post('projectname',true),
				'documenttype'=> $documenttype,
			);
			
			$where=array('documentid'=>$documentid);
			$this->adminmodel->update_data(TBLDOCUMENTS,$where,$data);
			
			$this->session->set_flashdata('success','You have updated documents successfully.');
			redirect('administrator/documentlist');
		
			
			
		}else{
			
			$documentid = $this->input->get('documentid',true);
			$where = array('documentid'=>$documentid);
			$config['documentsval'] = $this->adminmodel->getSingle(TBLDOCUMENTS,$where);
			
			$where=array('is_deleted'=>0);
			$config['projectlist'] = $this->adminmodel->getwhere(TBLPROJECTS,$where);
		
		    $config['title']='Edit Documents';
			$this->load->view('admin/editdocuments',$config);	
		}
	}
	
	
	public function deletedocuments()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Documents','allow_delete');
			
		$documentid = $this->input->get('documentid',true);
		$where = array('documentid'=>$documentid);
		$data=array('is_deleted'=>1);
		$this->adminmodel->update_data(TBLDOCUMENTS,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted documents successfully.');
		redirect('administrator/documentlist');
	}
	
	/***************************** Manage Projects *****************************/

	public function projectlist()
	{
		
		$this->_chk_if_login();
		//$this->_chk_if_permissions('Projects','allowfor_edit');
		//echo $this->db->last_query();
		//die;
		
		$where=array('is_deleted'=>0);
		$config['projectlists'] = $this->adminmodel->getwhere(TBLPROJECTS,$where);
		
		$config['title']='All Projects';
		$this->load->view('admin/projectlist',$config);
	}
	
	public function addprojects()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Projects','allowfor_add');
		
		if($_POST){
			
			$loans = implode(',',$this->input->post('loans',true));
			$data=array(
				'project_name'=>$this->input->post('project_name',true), 
				'companyname'=>  $this->input->post('companyname',true),
				'projectdesc'=>  $this->input->post('projectdesc',true),
				'projectdetails'=>  $this->input->post('projectdetails',true),
				'projecttype'=>  $this->input->post('projecttype',true),
				'location'=>  $this->input->post('location',true),
				'startdate'=>  $this->input->post('startdate',true),
				'enddate'=>  $this->input->post('enddate',true),
				'contractoramount' => $this->input->post('contractoramount',true),
				'loans'=>  $loans,
				'totalestimatedcost'=>  $this->input->post('totalestimatedcost',true),
				'totalpayment'=>  $this->input->post('totalpayment',true),
				'ballence'=>  $this->input->post('ballence',true),
				'estimatedprofit'=>  $this->input->post('estimatedprofit',true),
				'actualprofit'=>  $this->input->post('actualprofit',true),
				'status'=>  'pending',
				'postdate'=>  date('Y-m-d'),
			);
			
			$insert = $this->adminmodel->insert_data(TBLPROJECTS,$data);
			
			
			$partnerimplode = implode(',',$this->input->post('partnername',true));
			$investmentamountimplode = implode(',',$this->input->post('investmentamount',true));
			
			$partnerexp = explode(',',$partnerimplode);
			$investmentamountexp = explode(',',$investmentamountimplode);
			
			for($i=0; $i<count($partnerexp); $i++)
			{
				$datapartner=array(
					'projectid'=>  $insert,
					'partnerid'=>  $partnerexp[$i],
					'investmentamount'=>  $investmentamountexp[$i],
				);
				$insertpartner = $this->adminmodel->insert_data(PROJECTPARTNER,$datapartner);
			}
			
			//financialinstitute financedamount repaymentperiod repaymentamount
			/*$financialinstituteimplode = implode(',',$this->input->post('financialinstitute',true));
			$financedamountimplode = implode(',',$this->input->post('financedamount',true));
			$repaymentperiodimplode = implode(',',$this->input->post('repaymentperiod',true));
			$repaymentamountimplode = implode(',',$this->input->post('repaymentamount',true));
			
			$financialinstituteexp = explode(',',$financialinstituteimplode);
			$financedamountexp = explode(',',$financedamountimplode);
			$repaymentperiodexp = explode(',',$repaymentperiodimplode);
			$repaymentamountexp = explode(',',$repaymentamountimplode);
			
			for($k=0; $k<count($financialinstituteexp); $k++)
			{
				$datafinancial=array(
					'projectid'=>  $insert,
					'financialinstituteid'=>  $financialinstituteexp[$k],
					'financedamount'=>  $financedamountexp[$k],
					'repaymentperiod'=>  $repaymentperiodexp[$k],
					'repaymentamount'=>  $repaymentamountexp[$k],
				);
				$insertfinancial = $this->adminmodel->insert_data(PROJECTFINANCIAL,$datafinancial);
			}*/
			
			
			$contractorsimplode = implode(',',$this->input->post('contractors',true));
			$projectamountimplode = implode(',',$this->input->post('projectamount',true));
			$paymentsimplode = implode(',',$this->input->post('payments',true));
			$outstandingpaymentimplode = implode(',',$this->input->post('outstandingpayment',true));
			
			$contractorsexp = explode(',',$contractorsimplode);
			$projectamountexp = explode(',',$projectamountimplode);
			$paymentsexp = explode(',',$paymentsimplode);
			$outstandingpaymentexp = explode(',',$outstandingpaymentimplode);
			
			//contractors projectamount payments outstandingpayment
			for($k=0; $k<count($contractorsexp); $k++)
			{
				$datafinancial=array(
					'projectid'=>  $insert,
					'contractorid'=>  $contractorsexp[$k],
					'projectamount'=>  $projectamountexp[$k],
					'payments'=>  $paymentsexp[$k],
					'outstandingpayment'=>  $outstandingpaymentexp[$k],
				);
				$insertcontractors = $this->adminmodel->insert_data(PROJECTCONTRACTORS,$datafinancial);
			}
			
			
			$paymentmethodimplode = implode(',',$this->input->post('paymentmethod',true));
			$paymentsvalimplode = implode(',',$this->input->post('paymentsval',true));
			
			$paymentmethodexp = explode(',',$paymentmethodimplode);
			$paymentsvalexp = explode(',',$paymentsvalimplode);
			
			for($i=0; $i<count($paymentmethodexp); $i++)
			{
				$datapayments=array(
					'projectid'=>  $insert,
					'paymentmethod'=>  $partnerexp[$i],
					'paymentamount'=>  $paymentsvalexp[$i],
					'paymentdate'=> date('Y-m-d H:i:s'),
				);
				$insertpartner = $this->adminmodel->insert_data(PROJECTPAYMENTS,$datapayments);
			}
			
						
			$documentnameimplode = implode(',',$this->input->post('documentname',true));
			$filenameimplode = implode(',',$_FILES['filename']['name']);
			$tmpnameimplode = implode(',',$_FILES['filename']['tmp_name']);
			
			$documentnameexp = explode(',',$documentnameimplode);
			$filenameexp = explode(',',$filenameimplode);
			$tmpnameexp = explode(',',$tmpnameimplode);
			
			
			for($i=0; $i<count($documentnameexp); $i++)
			{
				
				$fileName=date('Ymdhis').'_'.$filenameexp[$i];
				$chkfile = move_uploaded_file($tmpnameexp[$i],"uploads/documents/".$fileName);
				if($chkfile){
					$documentfilename=$fileName;
				}else{
					$documentfilename='';
				}
				
								
				$datadocument=array(
					'projectid'=>  $insert,
					'documentname'=>  $documentnameexp[$i],
					'documentfile'=>  $documentfilename,
					'addeddate'=> date('Y-m-d H:i:s'),
				);
				$insertdocument = $this->adminmodel->insert_data(PROJECTDOCUMENT,$datadocument);
			}
			
			
			$this->session->set_flashdata('success','You have added new projects successfully.');
			redirect('administrator/projectlist');
	           			
		}else{
			
			$where=array('is_deleted'=>0);
			
			$sqlquery = $this->db->query('SELECT * FROM '.TBLCOMPANYLOAN.' JOIN '.TBLFINANCIALINSTITUTE.' ON financialinstitution=financialinstituteid Group by financialinstituteid ASC');
			$config['loanlist'] = $sqlquery->result(); //$this->adminmodel->getwhere(TBLCOMPANYLOAN,$where);
			
			$where=array('is_deleted'=>0);
			$config['partners'] = $this->adminmodel->getwhere(TBLPARTNER,$where);
			
			$where=array('is_deleted'=>0);
			$config['financialinstitute'] = $this->adminmodel->getwhere(TBLFINANCIALINSTITUTE,$where);
			
			$where=array('is_deleted'=>0);
			$config['companylist'] = $this->adminmodel->getwhere(TBLCOMPANY,$where);
			
			$where=array('is_deleted'=>0);
			$config['contractors'] = $this->adminmodel->getwhere(TBLCONTRACTORS,$where);
			
			$where=array('is_deleted'=>0);
			$config['projecttypes'] = $this->adminmodel->getwhere(PROJECTTYPE,$where);
			
				
		    $config['title']='Add Projects';
			$this->load->view('admin/addprojects',$config);	
			
		}
	}
	
	public function project_details()
	{
		
		$this->_chk_if_login();
		$this->_chk_if_permissions('Projects','allowfor_edit');
		
		$projectid = $this->input->get('projectid',true);
		
				
		$where = array('projectid'=>$projectid);
		$config['editproject'] = $this->adminmodel->getSingle(TBLPROJECTS,$where);
		
		$where=array('projectid'=>$projectid);
		$config['partners'] = $this->adminmodel->getwhere(PROJECTPARTNER,$where);
		
		$where=array('projectid'=>$projectid);
		$config['financialinstitute'] = $this->adminmodel->getwhere(PROJECTFINANCIAL,$where);
		
		$where=array('projectid'=>$projectid); 
		$config['contractors'] = $this->adminmodel->getwhere(PROJECTCONTRACTORS,$where);
		
		$where=array('project'=>$projectid);
		$config['payments'] = $this->adminmodel->getwhere(TBLPAYMENTS,$where);
		
		$where=array('projectid'=>$projectid);
		$config['documents'] = $this->adminmodel->getwhere(PROJECTDOCUMENT,$where);
		
		$sql = $this->db->query('select SUM(Amount) as totalpayment from '.TBLPAYMENTS.' where project="'.$projectid.'"');
		$getrows = $sql->row();
		$config['totalpayment'] = $getrows->totalpayment;
			
	
		$config['title']='Project Details';
		$this->load->view('admin/project_details',$config);
	}
	
	public function editprojects()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Projects','allowfor_modify');
		
		if($_POST){
			 
			$projectid = $this->input->post('projectid',true);
			
			$loans = implode(',',$this->input->post('loans',true));
			$data=array(
				'project_name'=>$this->input->post('project_name',true), 
				'companyname'=>  $this->input->post('companyname',true),
				'projectdesc'=>  $this->input->post('projectdesc',true),
				'projectdetails'=>  $this->input->post('projectdetails',true),
				'projecttype'=>  $this->input->post('projecttype',true),
				'location'=>  $this->input->post('location',true),
				'startdate'=>  $this->input->post('startdate',true),
				'enddate'=>  $this->input->post('enddate',true),
				'contractoramount' => $this->input->post('contractoramount',true),
				'loans'=>  $loans,
				'totalestimatedcost'=>  $this->input->post('totalestimatedcost',true),
				'totalpayment'=>  $this->input->post('totalpayment',true),
				'ballence'=>  $this->input->post('ballence',true),
				'estimatedprofit'=>  $this->input->post('estimatedprofit',true),
				'actualprofit'=>  $this->input->post('actualprofit',true),
				'status'=>  'pending',
			);
			
			$where=array('projectid'=>$projectid);
			$this->adminmodel->update_data(TBLPROJECTS,$where,$data);
			
			
			/******************** Partners update ***********************/
			$partnerimplode = implode(',',$this->input->post('partnername',true));
			$investmentamountimplode = implode(',',$this->input->post('investmentamount',true));

			$partnerexp = explode(',',$partnerimplode);
			$investmentamountexp = explode(',',$investmentamountimplode);
			
			$wheredeletepartner = array('projectid'=>$projectid);
			$this->adminmodel->delete(PROJECTPARTNER,$wheredeletepartner);
			
			for($i=0; $i<count($partnerexp); $i++)
			{
				$datapartner=array(
					'projectid'=>  $projectid,
					'partnerid'=>  $partnerexp[$i],
					'investmentamount'=>  $investmentamountexp[$i],
				);
				$insertpartner = $this->adminmodel->insert_data(PROJECTPARTNER,$datapartner);
			}
			
			/******************** Financial institute update ***********************/
			//financialinstitute financedamount repaymentperiod repaymentamount
			/*$financialinstituteimplode = implode(',',$this->input->post('financialinstitute',true));
			$financedamountimplode = implode(',',$this->input->post('financedamount',true));
			$repaymentperiodimplode = implode(',',$this->input->post('repaymentperiod',true));
			$repaymentamountimplode = implode(',',$this->input->post('repaymentamount',true));
			
			$financialinstituteexp = explode(',',$financialinstituteimplode);
			$financedamountexp = explode(',',$financedamountimplode);
			$repaymentperiodexp = explode(',',$repaymentperiodimplode);
			$repaymentamountexp = explode(',',$repaymentamountimplode);
			
			$wherefinancialinstitute = array('projectid'=>$projectid);
			$this->adminmodel->delete(PROJECTFINANCIAL,$wherefinancialinstitute);
			
			for($k=0; $k<count($financialinstituteexp); $k++)
			{
				$datafinancial=array(
					'projectid'=>  $projectid,
					'financialinstituteid'=>  $financialinstituteexp[$k],
					'financedamount'=>  $financedamountexp[$k],
					'repaymentperiod'=>  $repaymentperiodexp[$k],
					'repaymentamount'=>  $repaymentamountexp[$k],
				);
				$insertfinancial = $this->adminmodel->insert_data(PROJECTFINANCIAL,$datafinancial);
			}*/
			
			/******************** Contractors update ***********************/
			
			$contractorsimplode = implode(',',$this->input->post('contractors',true));
			$projectamountimplode = implode(',',$this->input->post('projectamount',true));
			$paymentsimplode = implode(',',$this->input->post('payments',true));
			$outstandingpaymentimplode = implode(',',$this->input->post('outstandingpayment',true));
			
			$contractorsexp = explode(',',$contractorsimplode);
			$projectamountexp = explode(',',$projectamountimplode);
			$paymentsexp = explode(',',$paymentsimplode);
			$outstandingpaymentexp = explode(',',$outstandingpaymentimplode);
			
			$wherecontractors = array('projectid'=>$projectid);
			$this->adminmodel->delete(PROJECTCONTRACTORS,$wherecontractors);
			
			//contractors projectamount payments outstandingpayment
			for($k=0; $k<count($contractorsexp); $k++)
			{
				$datafinancial=array(
					'projectid'=>  $projectid,
					'contractorid'=>  $contractorsexp[$k],
					'projectamount'=>  $projectamountexp[$k],
					'payments'=>  $paymentsexp[$k],
					'outstandingpayment'=>  $outstandingpaymentexp[$k],
				);
				$insertcontractors = $this->adminmodel->insert_data(PROJECTCONTRACTORS,$datafinancial);
			}
			
			/******************** Payments update ***********************/
			
			$paymentmethodimplode = implode(',',$this->input->post('paymentmethod',true));
			$paymentsvalimplode = implode(',',$this->input->post('paymentsval',true));
			
			$paymentmethodexp = explode(',',$paymentmethodimplode);
			$paymentsvalexp = explode(',',$paymentsvalimplode);
			
			$wherepayments = array('projectid'=>$projectid);
			$this->adminmodel->delete(PROJECTPAYMENTS,$wherepayments);
			
			for($i=0; $i<count($paymentmethodexp); $i++)
			{
				$datapayments=array(
					'projectid'=>  $projectid,
					'paymentmethod'=>  $partnerexp[$i],
					'paymentamount'=>  $paymentsvalexp[$i],
					'paymentdate'=> date('Y-m-d H:i:s'),
				);
				$insertpartner = $this->adminmodel->insert_data(PROJECTPAYMENTS,$datapayments);
			}
			
			/******************** Documents update ***********************/
			
			$documentnameimplode = implode(',',$this->input->post('documentname',true));
			$hdndocumentnameimplode = implode(',',$this->input->post('hdndocumentname',true));
			$filenameimplode = implode(',',$_FILES['filename']['name']);
			$tmpnameimplode = implode(',',$_FILES['filename']['tmp_name']);
			
			$documentnameexp = explode(',',$documentnameimplode);
			$hdndocumentnamexplode = explode(',',$hdndocumentnameimplode);
			$filenameexp = explode(',',$filenameimplode);
			$tmpnameexp = explode(',',$tmpnameimplode);
			
			$wheredocuments = array('projectid'=>$projectid);
			$this->adminmodel->delete(PROJECTDOCUMENT,$wheredocuments);
			
			for($i=0; $i<count($documentnameexp); $i++)
			{
				
				$fileName=date('Ymdhis').'_'.$filenameexp[$i];
				$chkfile = move_uploaded_file($tmpnameexp[$i],"uploads/documents/".$fileName);
				if($chkfile){
					$documentfilename=$fileName;
				}else{
					$documentfilename = $hdndocumentnamexplode[$i];
				}
				
								
				$datadocument=array(
					'projectid'=>  $projectid,
					'documentname'=>  $documentnameexp[$i],
					'documentfile'=>  $documentfilename,
					'addeddate'=> date('Y-m-d H:i:s'),
				);
				$insertdocument = $this->adminmodel->insert_data(PROJECTDOCUMENT,$datadocument);
			}
			
			
			$this->session->set_flashdata('success','You have updated Project successfully.');
			redirect('administrator/projectlist');
		
			
			
		}else{
			
			$projectid = $this->input->get('projectid',true);
			
			$where = array('projectid'=>$projectid);
			$config['projectsval'] = $this->adminmodel->getSingle(TBLPROJECTS,$where);
			
			
			$where=array('is_deleted'=>0);
			$config['partners'] = $this->adminmodel->getwhere(TBLPARTNER,$where);
			
			$where=array('is_deleted'=>0);
			$config['financialinstitute'] = $this->adminmodel->getwhere(TBLFINANCIALINSTITUTE,$where);
			
			$where=array('is_deleted'=>0);
			$config['companylist'] = $this->adminmodel->getwhere(TBLCOMPANY,$where);
			
			$where=array('is_deleted'=>0);
			$config['contractors'] = $this->adminmodel->getwhere(TBLCONTRACTORS,$where);
			
			$sqlquery = $this->db->query('SELECT * FROM '.TBLCOMPANYLOAN.' JOIN '.TBLFINANCIALINSTITUTE.' ON financialinstitution=financialinstituteid Group by financialinstituteid ASC');
			$config['loanlist'] = $sqlquery->result();
			
			
			$where=array('projectid'=>$projectid);
			$config['projectpartners'] = $this->adminmodel->getwhere(PROJECTPARTNER,$where);
			
			$where=array('projectid'=>$projectid);
			$config['projectfinancialinstitute'] = $this->adminmodel->getwhere(PROJECTFINANCIAL,$where);
		
			$where=array('projectid'=>$projectid);
			$config['projectcontractors'] = $this->adminmodel->getwhere(PROJECTCONTRACTORS,$where);
			
			$where=array('projectid'=>$projectid);
			$config['projectpayments'] = $this->adminmodel->getwhere(PROJECTPAYMENTS,$where);
		
			$where=array('projectid'=>$projectid);
			$config['projectdocuments'] = $this->adminmodel->getwhere(PROJECTDOCUMENT,$where);
			//echo $this->db->last_query();
			//die;
			
			$where=array('is_deleted'=>0);
			$config['projecttypes'] = $this->adminmodel->getwhere(PROJECTTYPE,$where);
			
		    $config['title']='Edit Projects';
			$this->load->view('admin/editprojects',$config);	
		}
	}
	
	
	public function deletedprojects()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Projects','allow_delete');
			
		$projectid = $this->input->get('projectid',true);
		$where = array('projectid'=>$projectid);
		$data=array('is_deleted'=>1);
		$this->adminmodel->update_data(TBLPROJECTS,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted projects successfully.');
		redirect('administrator/projectlist');
	}
	
	/******************************** *****************************/
	
	public function purchaseorders()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Purchase orders','allowfor_edit');
		
		$where = array('is_deleted'=>0);
		$config['purchaseorderslist'] =$this->adminmodel->getwhere(PURCHASEORDERS,$where);
		
		$config['title']='Purchase Orders';
		$this->load->view('admin/purchaseorders',$config);
		
	}
	
	public function addpurchaseorder()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Purchase orders','allowfor_add');
		
		if($_POST)
		{
			/*$fileName=date('Ymdhis').'_'.$_FILES['invoiceimg']['name'];
			$chkfile = move_uploaded_file($_FILES['invoiceimg']['tmp_name'],"uploads/invoices/".$fileName);
			if($chkfile){
				$invoiceimage=$fileName; 
			}else{
				$invoiceimage='';
			}*/
			
			if($_FILES['perchaseorderimage']['name']!='')
			{
				$allowed_image_extension = array("png","jpg","jpeg","JPEG","JPG","PNG","bmp","BMP","gif","GIF");
				// Get image file extension
				$file_extension = pathinfo($_FILES["perchaseorderimage"]["name"], PATHINFO_EXTENSION);
				
				// Validate file input to check if is not empty
				if (!file_exists($_FILES["perchaseorderimage"]["tmp_name"])) {
					$this->session->set_flashdata('error','Choose image file to upload.');
					redirect('administrator/addpurchaseorder');
				}else if (! in_array($file_extension, $allowed_image_extension)) {
					$this->session->set_flashdata('error','Please upload valid image only.');
					redirect('administrator/addpurchaseorder');
				} 
				
				$purchaseorderimg=date('Ymdhis').'_'.$_FILES['perchaseorderimage']['name'];
				$chkfile = move_uploaded_file($_FILES['perchaseorderimage']['tmp_name'],"uploads/invoices/".$purchaseorderimg);
				
				if($chkfile){
					$purchaseorderimage=$purchaseorderimg; 
				}else{
					$purchaseorderimage='';
				}
				
			}else{
					$purchaseorderimage='';
			}
			
			
			//$invoicedate = date('Y-m-d H:i:s',strtotime($this->input->post('invoicedate',true)));
			//$paymentdate = date('Y-m-d H:i:s',strtotime($this->input->post('paymentdate',true)));
			
			$userid = $this->session->userdata('admin_userid');
			
			/*$data=array(
				'userid'=>$userid, 
				'suppliers'=>$this->input->post('suppliers',true), 
				'projectid'=>  $this->input->post('projects',true),
				'contractorsid'=>  $this->input->post('contractors',true),
				'invoicenr'=>  $this->input->post('invoicenr',true),
				'invoiceamount'=>  $this->input->post('invoiceamount',true),
				'invoiceimage'=>  $invoiceimage,
				'invoicedate'=>  $invoicedate,
				'paymentdate'=>  $paymentdate,
				'purchaseorderimg'=> $purchaseorderimg,
				'orderdate'=>  date('Y-m-d H:i:s'),
			);*/
			
			$data=array(
				'userid'=>$userid, 
				'suppliers'=>$this->input->post('suppliers',true), 
				'projectid'=>  $this->input->post('projects',true),
				'contractorsid'=>  $this->input->post('contractors',true),
				'projectsection'=>  $this->input->post('projectsection',true),
				'paymentmethod'=>  $this->input->post('paymentmethod',true),
				'perchaseorderimage'=> $purchaseorderimg,
				'orderdate'=>  date('Y-m-d H:i:s'),
			);
			$insert = $this->adminmodel->insert_data(PURCHASEORDERS,$data);
			
			//itemname qty unitprice totalprice
			$itemnameimplode = implode(',',$this->input->post('itemname',true));
			$qtyimplode = implode(',',$this->input->post('qty',true));
						
			$itemnameexp = explode(',',$itemnameimplode);
			$qtyexp = explode(',',$qtyimplode);
			
			
			for($i=0; $i<count($itemnameexp); $i++)
			{
				$dataordersitems=array(
					'purchaseorderid'=>  $insert,
					'itemname'=>  $itemnameexp[$i],
					'qty'=>  $qtyexp[$i],
				);
				$insertitems = $this->adminmodel->insert_data(ORDERITEMS,$dataordersitems);
			}
			
			$this->session->set_flashdata('success','You have placed orders successfully.');
			redirect('administrator/myorders');
		}
		
		
		$where=array('is_deleted'=>0);
		$config['supplierslist'] = $this->adminmodel->getwhere(TBLSUPPLIERS,$where);
		
		$where = array('is_deleted'=>0);
		$config['projectlist'] =$this->adminmodel->getwhere(TBLPROJECTS,$where);

		$where = array('is_deleted'=>0);
		$config['contractorslist'] =$this->adminmodel->getwhere(TBLCONTRACTORS,$where); 
			
		$config['title']='Add Purchase Orders';
		$this->load->view('admin/addpurchaseorder',$config);
	}
	
	
	public function editpurchaseorder()
	{
		error_reporting(0);
		$this->_chk_if_login();
		$this->_chk_if_permissions('Purchase orders','allowfor_modify');
		
		if($_POST)
		{
			
			$invoicedate = date('Y-m-d H:i:s',strtotime($this->input->post('invoicedate',true)));
			$paymentdate = date('Y-m-d H:i:s',strtotime($this->input->post('paymentdate',true)));
			
			$orderid = $this->input->post('hdnorderid',true); 
			
			if($this->input->post('is_verified',true)==1)
			{
			
				if($_FILES['invoiceimg']['name']!='')
				{
					$fileName=date('Ymdhis').'_'.$_FILES['invoiceimg']['name'];
					$chkfile = move_uploaded_file($_FILES['invoiceimg']['tmp_name'],"uploads/invoices/".$fileName);
					if($chkfile){
						$invoiceimage = $fileName; 
					}else{
						$invoiceimage = $this->input->post('hdninvoiceimg',true); 
					}
				}else{
						$invoiceimage = $this->input->post('hdninvoiceimg',true); 
				}
			
				if(isset($_POST['completebtn'])=='Complete'){
					
					
					$data=array(
						'invoicenr'=>  $this->input->post('invoicenr',true),
						'invoiceamount'=>  $this->input->post('invoiceamount',true),
						'invoiceimage'=>  $invoiceimage,
						'is_verified'=> 2,
						'invoicedate'=>  $invoicedate,
					);
					
					$wheregetorder = array('orderid'=>$orderid);
					$ordersval = $this->adminmodel->getSingle(PURCHASEORDERS,$wheregetorder);
		
					$datapayments=array(
						'contractorid'=>  $ordersval->contractorsid,
						'project'=>  $ordersval->projectid,
						'suppliersval'=> $ordersval->suppliers,
						'purchaseorder'=>  $this->input->post('invoicenr',true),
						'paidbycompany'=>  '',
						'paymentmethod'=>  $ordersval->paymentmethod,
						'paidto'=>  $ordersval->suppliers,
						'paymentdescription'=>  '',
						'Amount'=>  $this->input->post('invoiceamount',true),
						'projectsection'=>  $ordersval->projectsection,
						'paymentdate'=> date('Y-m-d H:i:s'),
					);
					$insert = $this->adminmodel->insert_data(TBLPAYMENTS,$datapayments);
					
				}else{
					
					$data=array(
						'invoicenr'=>  $this->input->post('invoicenr',true),
						'invoiceamount'=>  $this->input->post('invoiceamount',true),
						'invoiceimage'=>  $invoiceimage,
						'invoicedate'=>  $invoicedate,
					);
				}
			
				
				
				$where = array('orderid'=>$orderid);
				$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
				//&& $this->input->post('ispaid',true)==1
			}else if($this->input->post('is_verified',true)==2){
				
				if(isset($_POST['paybtn'])=='Pay'){
					
					$data=array(
						'invoiceamount'=>  $this->input->post('invoiceamount',true),
						'invoiceimage'=>  $invoiceimage,
						'ispaid'=> 1,
						'paymentdate'=>  $paymentdate,
					);
					
				}else{
					
					$data=array(
						'invoiceamount'=>  $this->input->post('invoiceamount',true),
						'invoiceimage'=>  $invoiceimage,
						'paymentdate'=>  $paymentdate,
					);

				}
				
				$where = array('orderid'=>$orderid);
				$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
			
			}else if($this->input->post('is_verified',true)==0){
				
				if($_FILES['perchaseorderimage']['name']!='')
				{
					$purchaseorderimg=date('Ymdhis').'_'.$_FILES['perchaseorderimage']['name'];
					$chkfile = move_uploaded_file($_FILES['perchaseorderimage']['tmp_name'],"uploads/invoices/".$purchaseorderimg);
					if($chkfile){
						$purchaseorderimg = $purchaseorderimg; 
					}else{
						$purchaseorderimg = $this->input->post('hdnperchaseorderimage',true);  
					}
				}else{
					$purchaseorderimg = $this->input->post('hdnperchaseorderimage',true);  
				}
			
				$data=array(
					'suppliers'=>$this->input->post('suppliers',true), 
					'projectid'=>  $this->input->post('projects',true),
					'contractorsid'=>  $this->input->post('contractors',true),
					'perchaseorderimage'=> $purchaseorderimg,
				);
				
				$where = array('orderid'=>$orderid);
				$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
			}
			
			
			
			//itemname qty unitprice totalprice
			
			if($this->input->post('is_verified',true)==0)
			{
				$itemnameimplode = implode(',',$this->input->post('itemname',true));
				$qtyimplode = implode(',',$this->input->post('qty',true));
				
				$itemnameexp = explode(',',$itemnameimplode);
				$qtyexp = explode(',',$qtyimplode);
			
				$where = array('purchaseorderid'=>$orderid);
				$this->adminmodel->delete(ORDERITEMS,$where);
				
				for($i=0; $i<count($itemnameexp); $i++)
				{
					$dataordersitems=array(
						'purchaseorderid'=>  $orderid,
						'itemname'=>  $itemnameexp[$i],
						'qty'=>  $qtyexp[$i],
					);
					$insertitems = $this->adminmodel->insert_data(ORDERITEMS,$dataordersitems);
				}
			}
			
			$this->session->set_flashdata('success','You have updated orders successfully.');
			
			if($this->session->userdata('admin_usertype')==1){
				redirect('administrator/purchaseorders');
			}else{
				redirect('administrator/myorders');
			}
		}
		
		$orderid = $this->input->get('orderid',true);
		
		$where=array('orderid'=>$orderid);
		$config['orderval'] = $this->adminmodel->getSingle(PURCHASEORDERS,$where);
		
		$where=array('purchaseorderid'=>$orderid);
		$config['ordersitems'] = $this->adminmodel->getwhere(ORDERITEMS,$where);
		
		$where=array('is_deleted'=>0);
		$config['supplierslist'] = $this->adminmodel->getwhere(TBLSUPPLIERS,$where);
		
		$where = array('is_deleted'=>0);
		$config['projectlist'] =$this->adminmodel->getwhere(TBLPROJECTS,$where);

		$where = array('is_deleted'=>0);
		$config['contractorslist'] =$this->adminmodel->getwhere(TBLCONTRACTORS,$where); 
			
		$config['title']='Edit Purchase Orders';
		$this->load->view('admin/editpurchaseorder',$config);
	}
	
	public function orderdetails()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Purchase orders','allowfor_edit');
		
		$orderid = $this->input->get('orderid',true);
		
		$where=array('orderid'=>$orderid);
		$config['ordersval'] = $this->adminmodel->getSingle(PURCHASEORDERS,$where);
		
		$where = array('purchaseorderid'=>$orderid);
		$config['ordersitemlist'] =$this->adminmodel->getwhere(ORDERITEMS,$where); 
		
		$where = array('projectid'=>$config['ordersval']->projectid);
		$config['projectsval'] = $this->adminmodel->getSingle(TBLPROJECTS,$where);
							  
		$where = array('suppliers_id'=>$config['ordersval']->suppliers);
		$config['suppliersval'] = $this->adminmodel->getSingle(TBLSUPPLIERS,$where);
		  
		$where = array('contractor_id'=>$config['ordersval']->contractorsid);
		$config['contractorsval'] = $this->adminmodel->getSingle(TBLCONTRACTORS,$where);
		
		$whereusers = array('userid'=>$config['ordersval']->userid);
		$config['usersval'] = $this->adminmodel->getSingle(ADMINUSERS,$whereusers);
		
		$config['title']='Orders Details';
		$this->load->view('admin/orderdetails',$config);
	}
	
	
	public function myorders()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Purchase orders','allowfor_edit');
		
		$userid = $this->session->userdata('admin_userid');
		
		$where = array('is_deleted'=>0,'userid'=>$userid);
		$config['orderlist'] =$this->adminmodel->getwhere(PURCHASEORDERS,$where);
		
		$config['title']='My Orders';
		$this->load->view('admin/myorders',$config);
	}
	
	/*show Sub Admin  Start*/
	public function userslist()
	{
		$this->_chk_if_login();
		//$this->_chk_if_permissions('Users','allowfor_edit');
		
		$where=array('usertype !='=>1,'is_deleted'=>0);
		$config['all_data'] = $this->adminmodel->getWhereorderby(ADMINUSERS,$where,'userid');
		
		$where = array('is_deleted'=>0);
		$config['usersgroups'] =$this->adminmodel->getwhere(USERGROUP,$where);
		
		$config['title'] = 'Users';
		$this->load->view('admin/userslist',$config);
	}
	
	
	/* create new subadmin (User with 'usertype' = 2) */
	public function addnewusers()
	{   
		$this->_chk_if_login();
		$this->_chk_if_permissions('Users','allowfor_add');
		
		$config['title']='New Users';
		
		if($_POST)
	    { 
			$password = $this->input->post('password',true);
			$hashed_password = password_hash($password, PASSWORD_DEFAULT);	
			$usertype= $this->input->post('usertype',true);
			
			$data=array(
				'firstname'=>  $this->input->post('firstname',true),
				'lastname'=>  $this->input->post('lastname',true),
				'username'=>  $this->input->post('username',true),
				'phone'=> $this->input->post('phone',true),
				'password'=>  $hashed_password,
				'show_passkey'=>  $password,
				'usertype'=> $usertype,
				'email'=>$this->input->post('email',true),
				'createddate'=> date('Y-m-d H:i:s'),
			);
			
						
			$last_id = $this->adminmodel->insert_data(ADMINUSERS,$data);
			$this->session->set_flashdata('success','You have created new users successfully.');
			redirect('administrator/userslist');
			
		}
		
		$where = array('is_deleted'=>0);
		$config['usersgroups'] =$this->adminmodel->getwhere(USERGROUP,$where);
		
		$this->_chk_if_login();
		$config['title']='add-users';
		$this->load->view('admin/addnewusers',$config);
		
	}
	
	
	
	public function editusers()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Users','allowfor_modify');
		
		if($_POST)
	    { 
			$password = $this->input->post('password',true);
			$hashed_password = password_hash($password, PASSWORD_DEFAULT);	
			
			$hdnid = $this->input->post('hdnid',true);
			$usertype= $this->input->post('usertype',true);
			
			$data=array(
				'firstname'=>  $this->input->post('firstname',true),
				'lastname'=>  $this->input->post('lastname',true),
				'username'=>  $this->input->post('username',true),
				'phone'=> $this->input->post('phone',true),
				'password'=>  $hashed_password,
				'usertype'=> $usertype,
				'show_passkey'=>  $password,
				'email'=>$this->input->post('email',true),
				
			);
			
			$where=array('userid'=>$hdnid);
			$this->adminmodel->update_data(ADMINUSERS,$where,$data);
	   
			$this->session->set_flashdata('success','You have update users details successfully.');
			redirect('administrator/userslist');
		}
		
		$id = $this->input->get('userid');
		
		$where = array('is_deleted'=>0);
		$config['usersgroups'] =$this->adminmodel->getwhere(USERGROUP,$where);
		
		$where=array('userid'=>$id);
		$config['usersinfo'] = $this->adminmodel->getSingle(ADMINUSERS,$where);
					
		$config['title']='Edit-User';
		$this->load->view('admin/editusers',$config);
	}
	
	public function deleteusers()
	{
	   	$this->_chk_if_login();
		$this->_chk_if_permissions('Users','allow_delete');
		
		$id = $this->input->get('userid'); 

		$data = array('is_deleted'=>1);
		$where=array('userid'=>$id);
		$this->adminmodel->update_data(ADMINUSERS,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted users successfully.');
		redirect('administrator/userslist');
	}
	
	public function verifyorders()
	{
	   	$this->_chk_if_login();
		$orderid = $this->input->get('orderid'); 
		$userid = $this->session->userdata('admin_userid');

		$data = array('is_verified'=>1,'verifiedby'=>$userid,'verifieddate'=>date('Y-m-d H:i:s'));
		$where=array('orderid'=>$orderid);
		$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
			
		$this->session->set_flashdata('success','You have verified order successfully.');
		redirect('administrator/purchaseorders');
	}
	
	public function completeorder()
	{
	   	$this->_chk_if_login();
		$orderid = $this->input->get('orderid'); 
		$userid = $this->session->userdata('admin_userid');

		$data = array('is_verified'=>2,'verifiedby'=>$userid,'verifieddate'=>date('Y-m-d H:i:s'));
		$where=array('orderid'=>$orderid);
		$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
			
		$this->session->set_flashdata('success','You have completed order successfully.');
		
		if($this->session->userdata('admin_usertype')==1){
			redirect('administrator/purchaseorders');
		}else{
			redirect('administrator/myorders');
		}
			
	}
	
	public function changepaidstatusorder()
	{
	   	$this->_chk_if_login();
		$orderid = $this->input->get('orderid'); 
		$userid = $this->session->userdata('admin_userid');

		$data = array('ispaid'=>1);
		$where=array('orderid'=>$orderid);
		$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
			
		$this->session->set_flashdata('success','You have paid this order successfully.');
		redirect('administrator/purchaseorders');
	}
	
	
	
	public function deleteorders()
	{
		$this->_chk_if_login();
		$this->_chk_if_permissions('Purchase orders','allow_delete');
		
		$orderid = $this->input->get('orderid'); 
		
		$data = array('is_deleted'=>1);
		$where=array('orderid'=>$orderid);
		$this->adminmodel->update_data(PURCHASEORDERS,$where,$data);
			
		$this->session->set_flashdata('success','You have deleted order successfully.');
		redirect('administrator/purchaseorders');
	}
	
	public function managepermissionstatus()
	{
		$this->_chk_if_login();
		$usertype = $this->input->post('usertype',true); 
		$menutype = $this->input->post('menutype',true);
		$permissiontype = $this->input->post('permissiontype',true);
		$permissionstatus = $this->input->post('permissionstatus',true);
		
		if($permissiontype=='add'){
			$data = array('allowfor_add'=>$permissionstatus);
		}else if($permissiontype=='edit'){
			$data = array('allowfor_edit'=>$permissionstatus);
		}else if($permissiontype=='modify'){
			$data = array('allowfor_modify'=>$permissionstatus);
		}else if($permissiontype=='delete'){
			$data = array('allow_delete'=>$permissionstatus);
		}
		
		$where=array('usertype'=>$usertype,'menutab'=>$menutype);
		$this->adminmodel->update_data('tbl_settingpermission',$where,$data);
	}
	
	
	
	
	/**************************** User Group ************************/
	
	public function usersgroup()
	{
		$this->_chk_if_login();
		$where=array('is_deleted'=>0);
		$config['usergroups'] = $this->adminmodel->getwhere(USERGROUP,$where);
		
		$config['title'] = 'Users Group';
		$this->load->view('admin/usersgroup',$config);
	}

	public function addnewgroups()
	{   
		$this->_chk_if_login();
		
		if($_POST)
	    { 
			$data=array('groupname'=>$this->input->post('groupname',true));
			$insert_id = $this->adminmodel->insert_data(USERGROUP,$data);
			
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Compnies')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Projects')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Users')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Partners')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Suppliers')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Contractors')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Loans')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Bank Accounts')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Financial Institute')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Payments')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Documents')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Purchase orders')");
			$this->db->query("INSERT INTO `tbl_settingpermission` (`usertype`, `menutab`) VALUES ('".$insert_id."', 'Dashboard')");
			
			$this->session->set_flashdata('success','You have created new user group successfully.');
			redirect('administrator/usersgroup');
		}
		
		$this->_chk_if_login();
		$config['title']='Add-Group';
		$this->load->view('admin/addnewgroups',$config);
	}
	
	public function editdnewgroups()
	{
		$this->_chk_if_login();
		if($_POST)
	    { 
			$hdnid = $this->input->post('hdnid',true);
			$data=array('groupname'=>$this->input->post('groupname',true));
			$where=array('id'=>$hdnid);
			$this->adminmodel->update_data(USERGROUP,$where,$data);
	   
			$this->session->set_flashdata('success','You have update users details successfully.');
			redirect('administrator/usersgroup');
		}
		
		$id = $this->input->get('id');
		
		$where=array('id'=>$id);
		$config['usersgroups'] = $this->adminmodel->getSingle(USERGROUP,$where);
					
		$config['title']='Edit-Group';
		$this->load->view('admin/editdnewgroups',$config);
	}
	
	public function deletegroups()
	{
		$this->_chk_if_login();
		$groupid = $this->input->get('groupid');
		
		$data=array('is_deleted'=>1);
		$where=array('id'=>$groupid);
		$this->adminmodel->update_data(USERGROUP,$where,$data);
		
		$this->session->set_flashdata('success','You have de delete group successfully.');
		redirect('administrator/usersgroup');
	}
	
	
	/**************************** Project Type ************************/
	
	public function projecttype()
	{
		$this->_chk_if_login();
		$where=array('is_deleted'=>0);
		$config['projectypes'] = $this->adminmodel->getwhere(PROJECTTYPE,$where);
		
		$config['title'] = 'Project Type';
		$this->load->view('admin/projecttype',$config);
	}

	public function addprojecttype()
	{   
		$this->_chk_if_login();
		
		if($_POST)
	    { 
			$data=array('projecttype'=>$this->input->post('projecttype',true));
			$insert_id = $this->adminmodel->insert_data(PROJECTTYPE,$data);
			$this->session->set_flashdata('success','You have created new project type successfully.');
			redirect('administrator/projecttype');
		}
		
		$this->_chk_if_login();
		$config['title']='Add Project Type';
		$this->load->view('admin/addprojecttype',$config);
	}
	
	public function editprojecttype()
	{
		$this->_chk_if_login();
		if($_POST)
	    { 
			$hdnid = $this->input->post('hdnid',true);
			$data=array('projecttype'=>$this->input->post('projecttype',true));
			$where=array('id'=>$hdnid);
			$this->adminmodel->update_data(PROJECTTYPE,$where,$data);
	   
			$this->session->set_flashdata('success','You have update project type successfully.');
			redirect('administrator/projecttype');
		}
		
		$id = $this->input->get('id');
		
		$where=array('id'=>$id);
		$config['projectstypeval'] = $this->adminmodel->getSingle(PROJECTTYPE,$where);
					
		$config['title']='Edit Project Type';
		$this->load->view('admin/editprojecttype',$config);
	}
	
	
	public function deleteprojecttype()
	{
		$this->_chk_if_login();
		$id = $this->input->get('id');
		
		$where = array('id'=>$id);
		$this->adminmodel->delete(PROJECTTYPE,$where);
					
		$this->session->set_flashdata('success','You have deleted project type successfully.');
		redirect('administrator/projecttype');
	}
	
	
	/**************************** Contractors Type ************************/
	
	public function contractorstype()
	{
		$this->_chk_if_login();
		$where=array('is_deleted'=>0);
		$config['contractorstypes'] = $this->adminmodel->getwhere(CONTRACTORTYPE,$where);
		
		$config['title'] = 'Contractors Type';
		$this->load->view('admin/contractorstype',$config);
	}

	public function addcontractorstype()
	{   
		$this->_chk_if_login();
		
		if($_POST)
	    { 
			$data=array('contractortype'=>$this->input->post('contractortype',true));
			$insert_id = $this->adminmodel->insert_data(CONTRACTORTYPE,$data);
			$this->session->set_flashdata('success','You have created new contractor type successfully.');
			redirect('administrator/contractorstype');
		}
		
		$this->_chk_if_login();
		$config['title']='Add Contractor Type';
		$this->load->view('admin/addcontractorstype',$config);
	}
	
	public function editcontractorstype()
	{
		$this->_chk_if_login();
		if($_POST)
	    { 
			$hdnid = $this->input->post('hdnid',true);
			$data=array('contractortype'=>$this->input->post('contractortype',true));
			$where=array('id'=>$hdnid);
			$this->adminmodel->update_data(CONTRACTORTYPE,$where,$data);
	   
			$this->session->set_flashdata('success','You have update contractor type successfully.');
			redirect('administrator/contractorstype');
		}
		
		$id = $this->input->get('id');
		
		$where=array('id'=>$id);
		$config['contractorsval'] = $this->adminmodel->getSingle(CONTRACTORTYPE,$where);
					
		$config['title']='Edit Contractor Type';
		$this->load->view('admin/editcontractorstype',$config);
	}
	
	public function deletecontractorstype()
	{
		$this->_chk_if_login();
		$id = $this->input->get('id');
		
		$where = array('id'=>$id);
		$this->adminmodel->delete(CONTRACTORTYPE,$where);
					
		$this->session->set_flashdata('success','You have deleted Contractor type successfully.');
		redirect('administrator/contractorstype');
	}
	
	
	/**************************** Payment Method ************************/
	
	public function paymentmethod()
	{
		$this->_chk_if_login();
		$where=array('is_deleted'=>0);
		$config['paymentmethods'] = $this->adminmodel->getwhere(PAYMENTMETHOD,$where);
		
		$config['title'] = 'Payment Method';
		$this->load->view('admin/paymentmethod',$config);
	}

	public function addpaymentmethod()
	{   
		$this->_chk_if_login();
		
		if($_POST)
	    { 
			$data=array('paymentmethod'=>$this->input->post('paymentmethod',true));
			$insert_id = $this->adminmodel->insert_data(PAYMENTMETHOD,$data);
			$this->session->set_flashdata('success','You have created new Payment Method successfully.');
			redirect('administrator/paymentmethod');
		}
		
		$this->_chk_if_login();
		$config['title']='Add Payment Method';
		$this->load->view('admin/addpaymentmethod',$config);
	}
	
	public function editpaymentmethod()
	{
		$this->_chk_if_login();
		if($_POST)
	    { 
			$hdnid = $this->input->post('hdnid',true);
			$data=array('paymentmethod'=>$this->input->post('paymentmethod',true));
			$where=array('id'=>$hdnid);
			$this->adminmodel->update_data(PAYMENTMETHOD,$where,$data);
	   
			$this->session->set_flashdata('success','You have update payment method successfully.');
			redirect('administrator/paymentmethod');
		}
		
		$id = $this->input->get('id');
		
		$where=array('id'=>$id);
		$config['paymentmethodval'] = $this->adminmodel->getSingle(PAYMENTMETHOD,$where);
					
		$config['title']='Edit Payment Method';
		$this->load->view('admin/editpaymentmethod',$config);
	}
	
	public function deletepaymentmethod()
	{
		$this->_chk_if_login();
		$id = $this->input->get('id');
		
		$where = array('id'=>$id);
		$this->adminmodel->delete(PAYMENTMETHOD,$where);
					
		$this->session->set_flashdata('success','You have deleted payment method successfully.');
		redirect('administrator/paymentmethod');
	}
	
	
	/**************************** Project Sections************************/
	
	public function projectsection()
	{
		$this->_chk_if_login();
		$where=array('is_deleted'=>0);
		$config['projectsections'] = $this->adminmodel->getwhere(PROJECTSECTION,$where);
		
		$config['title'] = 'Project sections';
		$this->load->view('admin/projectsection',$config);
	}

	public function addprojectsection()
	{   
		$this->_chk_if_login();
		
		if($_POST)
	    { 
			$data=array('projectsectionname'=>$this->input->post('projectsectionname',true));
			$insert_id = $this->adminmodel->insert_data(PROJECTSECTION,$data);
			$this->session->set_flashdata('success','You have created new project section successfully.');
			redirect('administrator/projectsection');
		}
		
		$this->_chk_if_login();
		$config['title']='Add Payment Method';
		$this->load->view('admin/addprojectsection',$config);
	}
	
	public function editprojectsection()
	{
		$this->_chk_if_login();
		if($_POST)
	    { 
			$hdnid = $this->input->post('hdnid',true);
			$data=array('projectsectionname'=>$this->input->post('projectsectionname',true));
			$where=array('id'=>$hdnid);
			$this->adminmodel->update_data(PROJECTSECTION,$where,$data);
	   
			$this->session->set_flashdata('success','You have update project section successfully.');
			redirect('administrator/projectsection');
		}
		
		$id = $this->input->get('id');
		
		$where=array('id'=>$id);
		$config['projectsectionsval'] = $this->adminmodel->getSingle(PROJECTSECTION,$where);
					
		$config['title']='Edit Project Sections';
		$this->load->view('admin/editprojectsection',$config);
	}
	
	public function deleteprojectsection()
	{
		$this->_chk_if_login();
		$id = $this->input->get('id');
		
		$where = array('id'=>$id);
		$this->adminmodel->delete(PROJECTSECTION,$where);
					
		$this->session->set_flashdata('success','You have deleted project section successfully.');
		redirect('administrator/projectsection');
	}
	
	
	public function managepermissiontype()
	{
		$this->_chk_if_login();
		$permissionusergroup = $this->input->post('permissionusergroup',true);
		
		$where=array('usertype'=>$permissionusergroup);
		$config['settingsall'] = $this->adminmodel->getwhere(SETTINGPERMISSION,$where);
		
		$where=array('id'=>$permissionusergroup);
		$config['getuserstype'] = $this->adminmodel->getSingle(USERGROUP,$where);
		
		
		/*$where=array('usertype'=>2);
		$config['settings'] = $this->adminmodel->getwhere(SETTINGPERMISSION,$where);
		
		$wherecontractors=array('usertype'=>3);
		$config['settingscontractors'] = $this->adminmodel->getwhere(SETTINGPERMISSION,$wherecontractors);*/
		
		$config['permissionusergroup'] = $permissionusergroup;
		
		$this->load->view('admin/getpermissiontypepage',$config);
	}
	
}
