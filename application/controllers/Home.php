<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
    { 
		parent::__construct();
		$this->load->model('homemodel','',TRUE);
		$this->load->library('session');
		error_reporting(0);
		
		
	}
	
	/* show home page  */
	public function index()
	{
		
  		$config['title'] = 'home';	
				
		$where = array('delete_status'=>0);
		$config['categories'] = $this->homemodel->getwhere(CATALOGCATEGORY,	$where);
		
		$this->load->view('index',$config);
	}

	/* show home page  */
	public function itemlist()
	{
		if($this->session->userdata('mobile')=='')
		{
			redirect('/');
		}
		
  		$config['title'] = 'home';	
		
		$category_url = $this->uri->segment(1);
		
		$where = array('delete_status'=>0,'category_url'=>$category_url);
		$config['categoryval'] = $this->homemodel->getSingle(CATALOGCATEGORY,$where);
		
		$where = array('category_id'=>$config['categoryval']->category_id);
		$config['cataloglist'] = $this->homemodel->getwhere(MANAGECATELOG,$where);
				
		$this->load->view('itemlist',$config);
	}
   
	
   
	public function sendSMS($phone_number) {
	    $randomid = mt_rand(100000,999999); 
	    $content = 'Your OTP IS '. $randomid.' Do not share this code with anyone else.';
	    //$content = 'HELLO MISHRA WHAT ARE YOU DOING. Your CODE IS 12345';
	    
		  $curl = curl_init();
			
		  curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://control.msg91.com/api/sendotp.php?authkey=316128AKZcM7MDo5e3528beP1&template_id=5e3a6eacd6fc051303443d06&mobile=".$phone_number."&message=".$content."&sender=ASHTKA&country=91&otp=".$randomid, 
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_HTTPHEADER => array(
			"content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  //echo $response;
		  return $randomid;
		}

		
	}

	public function check_otpforgot()
	{
		  $user_otp = $this->input->post('user_otp',true);
		  $m_mobile = $this->input->post('m_mobile',true);
		  $msg  = array();
		  
		  $where = array('mobile'=>$m_mobile);
		  $chkotp = $this->homemodel->getSingle(USERS,$where);
		 
		  if($chkotp->otpval == $user_otp){
			  
				$msg['mobile'] = $m_mobile;
				$msg['status'] = '200';
		  }else{
				$msg['status'] = '400';
		  }
		  echo json_encode($msg);
	}
	
	
    function check()
	{
     
      $user_otp = $this->input->post('user_otp',true);
      $m_mobile = $this->input->post('m_mobile',true);
      $msg  = array();
	  
	  $where = array('mobile'=>$m_mobile);
      $chkotp = $this->homemodel->getSingle(USERS,$where);
     
	  if($chkotp->otpval == $user_otp){
		  
		    $updateData = array('is_verify'=>1);
			$where = array('mobile'=>$m_mobile);
			$this->homemodel->update_data(USERS, $where, $updateData);
					
			$newdata = array('mobile' =>$m_mobile,'name'=>$chkotp->name,'user_logged_in'=>false);
			$this->session->set_userdata($newdata);
			
			$msg['status'] = '200';
	  }else{
			$this->session->sess_destroy();
			$newdata = array('mobile' =>'','name'=>'','user_logged_in'=>false);
			$this->session->unset_userdata($newdata);
			$msg['status'] = '400';
	  }
 
      echo json_encode($msg);

    }
	 
	function login(){
     
      $user_mobile = $this->input->post('user_mobile',true);
      $user_password = $this->input->post('user_password',true);
      $msg  = array();
	  
	  $where = array('mobile'=>$user_mobile,'password'=>md5($user_password),'is_verify'=>1);
      $chklogin = $this->homemodel->getSingle(USERS,$where);
     
	  if($chklogin->id != ''){
			
			$newdata = array('mobile' =>$user_mobile,'name'=>$chklogin->name,'user_logged_in'=>false);
			$this->session->set_userdata($newdata);
			
			$msg['status'] = 200;
	  }else{
			$this->session->sess_destroy();
			$newdata = array('mobile' =>'','name'=>'','user_logged_in'=>false);
			$this->session->unset_userdata($newdata);
			$msg['status'] = 400;
	  }
 
      echo json_encode($msg);

    }


	public function reset_password()
	{		
		 $password = md5($this->input->post('resetpwd',true)); 
		 $mobile = $this->input->post('m_mobile',true); 
		 
		 $where = array('mobile'=>$mobile);
		 $chkuser = $this->homemodel->getSingle(USERS, $where);
		 $data = array();
		 if($chkuser->id!='')
		 {
			$updateData = array('password'=>$password);
			$where = array('mobile'=>$mobile);
			$this->homemodel->update_data(USERS, $where, $updateData);
			$data['status'] = 200;
		 }else{
			 $data['status'] = 400;
		 }
		 
		 echo json_encode($data);
	}
    
	public function registration()
	{		
		
		 $name = $this->input->post('name',true); 
		 $password = md5($this->input->post('password',true)); 
		 $mobile = $this->input->post('user_mobile',true); 
		 
		 $where = array('mobile'=>$mobile);
		 $chkuser = $this->homemodel->getSingle(USERS, $where);
		 
		 $data = array();
		 if($chkuser->id!='')
		 {
			$data['status'] = 1;  
			$data['msg'] = 'Mobile no is already exist!';  
			$data['mobile'] = '';  
			
		 }else{
			 
			$addedOn = date('Y-m-d H:i:s');
			$data=array(
				'name'=> $name,
				'mobile'=>$mobile,
				'password'=>$password,
				'addedOn'=>$addedOn,
			);
			
			$lastid = $this->homemodel->insert_data(USERS,$data); 
			
			if($lastid!='')
			{
				$otppassword = $this->sendSMS($mobile);
				
				if($otppassword){
					
					$updateData = array('otpval'=>$otppassword);
					$where = array('mobile'=>$mobile);
					$this->homemodel->update_data('user_credential', $where, $updateData);
				}
			}
			
			
			$data['status'] = 2;  
			$data['msg'] = 'Register successfully';  
			$data['mobile'] = $mobile;  
		 }
		 
		 echo json_encode($data);
		
	}
	
	public function user_forgot()
	{
		 error_reporting(0);
		 $mobile = $this->input->post('user_mobile',true); 
		 
		 $where = array('mobile'=>$mobile);
		 $chkuser = $this->homemodel->getSingle(USERS, $where);
		 
		 $data = array();
		 if($chkuser->id!='')
		 {
			$otppassword = $this->sendSMS($mobile);
			
			if($otppassword){
				
				$updateData = array('otpval'=>$otppassword);
				$where = array('mobile'=>$mobile);
				$this->homemodel->update_data('user_credential', $where, $updateData);
			}
			
			$data['status'] = 200;  
			$data['msg'] = 'Otp sent successfully';  
			$data['mobile'] = $mobile; 
			
		 }else{
			 
			$data['status'] = 400;  
			$data['msg'] = 'Mobile no is already exist!';  
			$data['mobile'] = '';  
						
			 
		 }
		 echo json_encode($data);
	}
    
	
	
     public function logout()
	 {
     	  
     	    $this->session->sess_destroy();
			$newdata = array('mobile' =>'','name'=>'','user_logged_in'=>false);
			$this->session->unset_userdata($newdata);			
			redirect('/');
     }

	
}
