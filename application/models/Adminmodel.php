<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adminmodel extends CI_Model {  
	function login($userid,$password)
	{
		$this->db->where("username",$userid);
		$query=$this->db->get(ADMINUSERS);
		if($query->num_rows()>0)
		{
			 $rows = $query->row();
	         $hashed_password = $rows->password;
			if(password_verify($password, $hashed_password)) 
			{	
				//add all data to session
				$newdata = array(
				'admin_userid' 	=> $rows->userid,
				'admin_usertype'    => $rows->usertype,
				'admin_username'    => $rows->username,
				'admin_email'    => $rows->email,
				'admin_profilepic'    => $rows->profile_pic,
				'admin_logged_in' 	=> TRUE,
					   );
					$this->session->set_userdata($newdata);
					return true;  
			}
			else{
				return false;
				}          
		}
			return false;
	}			    
/* ************* add  data *************** */
	public function insert_data($table,$data)
	{ 
     	$this->db->insert($table,$data);
     	
		$num = $this->db->insert_id();
			return $num;
	}
/* ************* update  data *************** */	
	public function update_data($table,$where,$data)
	{
		 $this->db->where($where);
	     $update = $this->db->update($table,$data);
			if($update)
			{ 
				return TRUE;
			}
			else
			{ 
				return FALSE;
			}
	}
/* ************* update  data All*************** */	
	public function update_data_all($table,$data)
	{
	     $update = $this->db->update($table,$data);
			if($update)
			{ 
				return TRUE;
			}
			else
			{ 
				return FALSE;
			}
	}	
/* ************* get single   data *************** */	
	public function getSingle($table,$where)
	{
		$this->db->where($where);
		$data = $this->db->get($table);
		$get = $data->row();
		$num = $data->num_rows();
		if($num){
			return $get;
		}
		else
		{
			return false;
		}
	}
/* ************* get all data as where class *************** */	
	public function getwhere($table,$where)
	{
		$this->db->where($where);
		$data = $this->db->get($table);
		$get = $data->result();
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}
	/* ************* get all data as where class *************** */	
	public function getwhereorderby($table,$where,$id)
	{
		$this->db->where($where);
		$this->db->order_by($id.' desc');
		$data = $this->db->get($table);
		$get = $data->result();
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}
	
	public function getwhereblog($table,$where)
	{
		$this->db->where($where);
		$this->db->order_by('blog_id','desc');
		$data = $this->db->get($table);
		$get = $data->result();
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}
/* ************* get all data as where class *************** */	
	public function getall_selected($table,$field)
	{
		$this->db->select($field);
		$data = $this->db->get($table);
		$get = $data->result();
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}
/* ************* get all data as where class *************** */	
	public function getall_selected_where($table,$field,$orderby,$where)
	{
		$this->db->select($field);
		$this->db->where($where);
		$this->db->order_by($orderby,'asc');
		$data = $this->db->get($table);
		$get = $data->result();
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}
/* ************* Delete data *************** */	
	public function delete($table,$where)
	{
	    $this->db->where($where);
		$del = $this->db->delete($table);
		if($del){
			return true;
		}else{
			return false;
		}
	}
/* ************* Delete in data *************** */	
	public function delete_multipal($table,$where)
	{
	    $this->db->where_in('Lead_id',$where);
		$del = $this->db->delete($table);
		if($del){
			return true;
		}else{
			return false;
		}
	}
/* ************* get all data as where class *************** */	
	public function getsingle_selected($table,$field,$where)
	{
		$this->db->where($where);
		$this->db->select($field);
		$data = $this->db->get($table);
		$get = $data->row();
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}	
/* ************* Delete data with images in folder *************** */	
	public function delete_image($table,$where)
	{
	
	    $this->db->where($where);
        $query = $this->db->get($table);
		foreach($query->result() as $row)
        {
		    if($row->a_photo!='')
		    { 
             unlink("uploads/".$row->a_photo);
			}
             
        }
		$this->db->where($where);
		$del = $this->db->delete($table);
		if($del){
			return true;
		}else{
			return false;
		}
	}	
/* ************* get all data as where class *************** */     
    public function getAll($table, $id) 
    { 
        $this->db->order_by($id,'asc');
        $data = $this->db->get($table); 
        $get = $data->result(); 
        if($get){ 
            return $get; 
        }else{ 
            return FALSE; 
        } 
    }

/* ************* get all data as where class *************** */     
    public function get_all($table) 
    { 
        $data = $this->db->get($table); 
        $get = $data->result(); 
        if($get){ 
            return $get; 
        }else{ 
            return FALSE; 
        } 
    }	
     public function get_allarray($table) 
    { 
        $data = $this->db->get($table); 
        $get = $data->result_array(); 
        if($get){ 
            return $get; 
        }else{ 
            return FALSE; 
        } 
    }	
	 
	public function getCsvdata($table) 
    { 
        $data = $this->db->get($table); 
        $get = $data->result_array(); 
        if($get){ 
            return $get; 
        }else{ 
            return FALSE; 
        } 
    }	
	
	
/* ************* truncate data *************** */	
	public function truncate($table)
	{
	   $del = $this->db->truncate($table);
		if($del){
			return true;
		}else{
			return false;
		}
	}  
/* ************* get all data as where class *************** */     
    public function getCount($table, $where) 
    { 
	    $this->db->where($where);
        $q = $this->db->get($table); 
		return $q->num_rows();
    }
/* ************* get all chat where class *************** */     
    public function delete_user($table,$where)
	{
	
	    $this->db->where($where);
        $query = $this->db->get($table);
		foreach($query->result() as $row)
        {
		    if($row->profilepic!='')
		    { 
		    	$profilepic= explode('/', $row->profilepic);
		    	unlink('uploads/profilepic/'.$profilepic[4]);
			}   
        }
		$this->db->where($where);
		$del = $this->db->delete($table);
		if($del){
			return true;
		}else{
			return false;
		}
		
	}
	public function getchat($table,$rowno,$rowperpage,$username,$date,$userkey) 
    { 
	   $this->db->select(''.CHAT.'.*,'.ADMINUSERS.'.username');
	   $this->db->from(CHAT);
	   $this->db->join(ADMINUSERS,''.CHAT.'.userkey = '.ADMINUSERS.'.userkey');
	   $this->db->order_by(CHAT.'.id desc');
	   $this->db->limit($rowperpage, $rowno); 
	   $this->db->where(CHAT.'.userkey',$userkey);
		$q = $this->db->get(); 
		$num_rows = $q->num_rows();
		
		if ($num_rows > 0)
		{
			foreach ($q->result() as $rows)
			{
				$data[] = $rows;
			}
			$q->free_result();

			return $data; 

		} 
		else
		{
		return false;
		}
    }
    public function getallchatcount($table,$userkey) 
    { 
	   $this->db->select(''.CHAT.'.*,'.ADMINUSERS.'.username');
	   $this->db->from(CHAT);
	   $this->db->join(ADMINUSERS,''.CHAT.'.userkey = '.ADMINUSERS.'.userkey');
	    $this->db->where(CHAT.'.userkey',$userkey);
		$q = $this->db->get(); 
		$num_rows = $q->num_rows();
		return $num_rows;
    }

     public function getallsearchchatajax($table,$rowno,$rowperpage,$username,$created) 
    { 
		$this->db->select(''.CHAT.'.*,'.ADMINUSERS.'.username');
	    $this->db->from(CHAT);
		$this->db->join(ADMINUSERS,''.CHAT.'.userkey = '.ADMINUSERS.'.userkey');
		 if($username !='')
		{
		  $this->db->where(ADMINUSERS.'.username',$username);
		}
		if($created !='')
		{
		  $this->db->where('STR_TO_DATE(from_unixtime('.CHAT.'.created),"%Y-%m-%d")', $created);
		}
        $this->db->limit($rowperpage, $rowno); 
        $q = $this->db->get(); 
        $num_rows = $q->num_rows();
		if ($num_rows > 0)
        {
            foreach ($q->result() as $rows)
            {
                $data[] = $rows;
            }
            $q->free_result();
		
            return $data; 
        } 
		else
		{
			return false;
		}
    }

/* ************* get all product *************** */   
    public function getallsearchchatajaxcount($table,$username,$created) 
    { 
		$this->db->select(''.CHAT.'.*,'.ADMINUSERS.'.username');
	    $this->db->from(CHAT);
	    $this->db->join(ADMINUSERS,''.CHAT.'.userkey = '.ADMINUSERS.'.userkey');
	    if($username !='')
		{
		  $this->db->where(ADMINUSERS.'.username',$username);
		}
		if($created !='')
		{
		  $this->db->where('STR_TO_DATE(from_unixtime('.CHAT.'.created),"%Y-%m-%d")', $created);
		}
		$q = $this->db->get(); 
		$num_rows = $q->num_rows();
		return $num_rows;
    }

	public function getAllCheckList($peropertySellId) 
    { 
	   $this->db->select(''.TBLCHECKLIST.'.*,SUBSTRING('.TBLCHECKLIST.'.sort, 1, 2) As section_sort,'.TBLCHECKLISTITEMS.'.item_condition,'.TBLCHECKLISTITEMS.'.remarks');
	   $this->db->from(TBLCHECKLIST);
	   $this->db->join(TBLCHECKLISTITEMS,''.TBLCHECKLIST.'.id = '.TBLCHECKLISTITEMS.'.check_list_id', 'LEFT');
	   $this->db->where(TBLCHECKLISTITEMS.'.sell_property_id',$peropertySellId);
	   $this->db->order_by('SUBSTRING('.TBLCHECKLIST.'.Sort, 1, 2)');
		$q = $this->db->get(); 
		$get = $q->result(); 
        if($get){ 
            return $get; 
        }else{ 
            return FALSE; 
        }
    }

	/* ************* get all sell property data *************** */     
    public function getAllSellProperty($table, $id) 
    { 
		$this->db->select('*,SUBSTRING(sort, 1, 2) As section_sort');
        $this->db->order_by('SUBSTRING(Sort, 1, 2) asc');
        $data = $this->db->get($table); 
        $get = $data->result(); 
        if($get){ 
            return $get; 
        }else{ 
            return FALSE; 
        } 
    }

	public function getAllProjects(){
		$sql = $this->db->query("select *,
					@s_p := (select  sum(invoiceamount) from tbl_purchaseorders where tbl_purchaseorders.projectid = tbl_projects.projectid and ispaid =1 and is_deleted = 0)  as supplier_payments ,
					@c_p :=(select  sum(Amount) from tbl_payments where tbl_projects.projectid = tbl_payments.project and paidto = 'Contractor' and is_deleted = 0) as contrator_payments,
					@o_p := (select  sum(Amount) from tbl_payments where tbl_projects.projectid = tbl_payments.project and paidto in('Other', 'Financial Institution') and is_deleted = 0) as other_payments,
					@t_p := (ifnull(@s_p,0) + ifnull(@c_p,0)+ ifnull(@o_p,0)) as budget_used,
					totalestimatedcost - @t_p as balance from tbl_projects
					where is_deleted = 0
		");
		$results = $sql->result();
		return $results;
	}

	public function getAllProjectsByCompanyId($companyID){
		$sql = $this->db->query("select project_name,totalestimatedcost,projectid,startdate,enddate,
					@s_p := (select  sum(invoiceamount) from tbl_purchaseorders where tbl_purchaseorders.projectid = tbl_projects.projectid and ispaid =1 and is_deleted = 0)  as supplier_payments,
					@c_p :=(select  sum(Amount) from tbl_payments where tbl_projects.projectid = tbl_payments.project and paidto = 'Contractor' and is_deleted = 0) as contrator_payments,
					@o_p := (select  sum(Amount) from tbl_payments where tbl_projects.projectid = tbl_payments.project and paidto in('Other', 'Financial Institution') and is_deleted = 0) as other_payments
					from tbl_projects where is_deleted = 0 and companyname ='$companyID'
		");
		$results = $sql->result();
		return $results;
	}

/* end modal */
	
}

?>
