<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Homemodel extends CI_Model {  		    
/* ************* add  data *************** */
	public function insert_data($table,$data)
	{ 
     	$this->db->insert($table,$data);
		$num = $this->db->insert_id();
			return $num;
	}
/* ************* update  data *************** */	
	public function update_data($table,$where,$data)
	{
		 $this->db->where($where);
	     $update = $this->db->update($table,$data);
			if($update)
			{ 
				return TRUE;
			}
			else
			{ 
				return FALSE;
			}
	}
/* ************* update  data All*************** */	
	public function update_data_all($table,$data)
	{
	     $update = $this->db->update($table,$data);
			if($update)
			{ 
				return TRUE;
			}
			else
			{ 
				return FALSE;
			}
	}	
/* ************* get single   data *************** */	
	public function getSingle($table,$where)
	{
		$this->db->where($where);
		$data = $this->db->get($table);
		$get = $data->row();
		$num = $data->num_rows();
		if($num){
			return $get;
		}
		else
		{
			return false;
		}
	}
/* ************* get all data as where class *************** */	
	public function getwhere($table,$where)
	{
		$this->db->where($where);
		$data = $this->db->get($table);
		$get = $data->result();
	 	$str = $this->db->last_query();
	 
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}
	
/* ************* get all data as where class *************** */	
	public function getall_selected($table,$field)
	{
		$this->db->select($field);
		$data = $this->db->get($table);
		$get = $data->result();
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}
/* ************* get all data as where class *************** */	
	public function getall_selected_where($table,$field,$orderby,$where)
	{
		$this->db->select($field);
		$this->db->where($where);
		$this->db->order_by($orderby,'asc');
		$data = $this->db->get($table);
		$get = $data->result();
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}
/* ************* Delete data *************** */	
	public function delete($table,$where)
	{
	    $this->db->where($where);
		$del = $this->db->delete($table);
		if($del){
			return true;
		}else{
			return false;
		}
	}
/* ************* Delete in data *************** */	
	public function delete_multipal($table,$where)
	{
	    $this->db->where_in('Lead_id',$where);
		$del = $this->db->delete($table);
		if($del){
			return true;
		}else{
			return false;
		}
	}
/* ************* get all data as where class *************** */	
	public function getsingle_selected($table,$field,$where)
	{
		$this->db->where($where);
		$this->db->select($field);
		$data = $this->db->get($table);
		$get = $data->row();
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}	
/* ************* Delete data with images in folder *************** */	
	public function delete_image($table,$where)
	{
	
	    $this->db->where($where);
        $query = $this->db->get($table);
		foreach($query->result() as $row)
        {
		    if($row->a_photo!='')
		    { 
             unlink("uploads/".$row->a_photo);
			}
             
        }
		$this->db->where($where);
		$del = $this->db->delete($table);
		if($del){
			return true;
		}else{
			return false;
		}
	}	
/* ************* get all data as where class *************** */     
    public function getAll($table, $id) 
    { 
        $this->db->order_by($id,'asc');
        $data = $this->db->get($table); 
        $get = $data->result(); 
        if($get){ 
            return $get; 
        }else{ 
            return FALSE; 
        } 
    }
/* ************* get all data as where class *************** */     
    public function get_all($table) 
    { 
        $data = $this->db->get($table); 
        $get = $data->result(); 
        if($get){ 
            return $get; 
        }else{ 
            return FALSE; 
        } 
    }	
/* ************* truncate data *************** */	
	public function truncate($table)
	{
	   $del = $this->db->truncate($table);
		if($del){
			return true;
		}else{
			return false;
		}
	}  
/* ************* get all data as where class *************** */     
    public function getCount($table, $where) 
    { 
	    $this->db->where($where);
        $q = $this->db->get($table); 
		return $q->num_rows();
    }


// /* ************* get search a data on price *************** */     
//     public function getCount($table, $where) 
//     { 
// 	    $this->db->where($where);
//         $q = $this->db->get($table); 
// 		return $q->num_rows();
//     }


// $query = $this->db->query($SQL);

    
    
/* ************* get all data as where class with lodemore *************** */	
	public function getwhere_withloadmore($table,$where,$offset,$limit)
	{
	     
	   
		$this->db->where($where);
		$this->db->limit($limit,$offset); 
		$data = $this->db->get($table);
		$get = $data->result();
	   	$str = $this->db->last_query();
	 
		if($get){
			return $get;
		}else{
			return FALSE;
		}
	}
/* end modal */
}
?>
