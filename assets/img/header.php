<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,300i,400,500,600,700,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/css/blue.css">

    <title>Big Deals India | Home </title>
    <style type="text/css">
    .icon-bar {
  position: fixed;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
  z-index: 15;
}

.icon-bar a {
  display: block;
  text-align: center;
  padding: 0px;
  transition: all 0.3s ease;
  color: white;
  font-size: 20px;
}

.icon-bar a:hover {
  background-color: #000;
}

.facebook {
  background: #3B5998;
  color: white;
}

.twitter {
  background: #55ACEE;
  color: white;
}

.google {
  background: #dd4b39;
  color: white;
}.whatsappimg {
  background: #25d366;
  color: white;
}.androidimg {
  background: #a4c639;
  color: white;
}.feedimg {
  background: #f26109;
  color: white;
}.sendimg {
  background: #4db6ac;
  color: white;
}

.linkedin {
  background: #007bb5;
  color: white;
}

.youtube {
  background: #bb0000;
  color: white;
}

.content {
  margin-left: 75px;
  font-size: 30px;
}
</style>
  </head>
 <body class="animated fadeIn">
    <!--== HEADER START ==-->
    <header class="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-primry">
            <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="assets/img/logos.png" alt="logo"/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <?php //echo $search_text ?>
                    <form action="<?php echo base_url('search'); ?>" class="form-inline my-2 my-lg-0 ph_relative">
                        <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search product" aria-label="Search" value="<?php echo $this->input->get('search',true); ?>" >
                        <i class="fa fa-search"></i>
                    </form>
                    <li class="nav-item main-menu">
                        <a class="nav-link" data-toggle="modal" data-target="#filter" href="#">Filter <img src="assets/img/filter.png" alt="filter" /></a>
                    </li>
                    <li class="nav-item main-menu">
                        <?php   if(empty($this->session->userdata('mobile'))){  ?>
                        <a class="nav-link" data-toggle="modal" data-target="#login" href="#">Sign in <img src="assets/img/signin.png" alt="signin" /></a>
                        <?php } else { ?>
                             <a class="nav-link" href="<?php echo base_url(); ?>/logout" > Logout </a>
                       <?php  } ?> 
                    </li>
                    <?php   if(!empty($this->session->userdata('mobile'))){  ?>
                    <li class="nav-item main-menu">
                      <a class="nav-link" href="<?php echo base_url(); ?>/add_product" > My Product </a>                      
                    </li>
                 <?php  } ?> 
                </ul>
                <ul class="list-inline social-i">
                     <li class="list-inline-item"><a href="https://www.facebook.com" target="_blank"><img src="assets/img/fb.png" alt="fb"></a></li>
                    <li class="list-inline-item"><a href="https://www.twitter.com" target="_blank"><img src="assets/img/twitter.png" alt="fb"></a></li>
                    <li class="list-inline-item"><a href="https://www.linkedin.com" target="_blank"><img src="assets/img/linkedin.png" alt="fb"></a></li>
                    <li class="list-inline-item"><a href="https://aboutme.google.com/u/0/?referer=gplus" target="_blank"><img src="assets/img/google-plus.png" alt="fb"></a></li>
                </ul>
            </div>
        </nav>

        <nav class="text-center category_nav">
            <ul class="list-inline">
                <?php //echo $this->uri->segment(1);  ?>
                <li class="list-inline-item <?php if($this->uri->segment(1)==""){echo "active";}?> "><a class="text-decoration-none" href="<?php echo base_url(); ?>">Home</a></li>
                <li class="list-inline-item <?php if($this->uri->segment(1)=="electronics"){echo "active";}?> "><a class="text-decoration-none" href="<?php echo base_url('electronics'); ?>">Electronics</a></li>
                <li class="list-inline-item <?php if($this->uri->segment(1)=="men"){echo "active";}?> "><a class="text-decoration-none" href="<?php echo base_url('men'); ?>">Men</a></li>
                <li class="list-inline-item <?php if($this->uri->segment(1)=="women"){echo "active";}?> "><a class="text-decoration-none" href="<?php echo base_url('women'); ?>">Women</a></li>
                <li class="list-inline-item <?php if($this->uri->segment(1)=="home_furniture"){echo "active";}?> "><a class="text-decoration-none" href="<?php echo base_url('home_furniture'); ?>">Home & Furniture</a></li>
                <!-- <li class="list-inline-item <?php if($this->uri->segment(1)=="offer_zone"){echo "active";}?> "><a class="text-decoration-none" href="<?php echo base_url('offer_zone'); ?>">Offer Zone</a></li> -->
                <li class="list-inline-item <?php if($this->uri->segment(1)=="contact"){echo "active";}?> "><a class="text-decoration-none" href="<?php echo base_url('contact'); ?>">Contact</a></li>
            </ul>
        </nav>
    </header>
      <div class="icon-bar">

  <a target="_blank" href="https://t.me/joinchat/HqT_YhZVfo0yenLR-QV5Qw" class="sendimg"><img src="assets/img/sendimg1.png" alt="fb" style="width: 35px"></a> 
  <a href="#" class="androidimg"><img src="assets/img/androidimg.png" alt="fb" style="width: 35px"></a> 
  <a class="facebook" href="https://m.facebook.com/groups/421446185436891?_rdr" target="_blank"><img src="assets/img/facebookimg.png" alt="fb" style="width: 35px"></a>

  <a target="_blank" href="https://api.whatsapp.com/send?phone=%2B919981067449&text=Hello%2C%20I'm%20about%20..." class="whatsappimg"><img src="assets/img/whatsappimg.png" alt="fb" style="width: 35px"></a> 
  <a  class="twitter" href="https://mobile.twitter.com/aravindan3" target="_blank"><img src="assets/img/twitterimg.png" alt="fb" style="width: 35px"></a>   
</div>
    <!--== HEADER END ==-->
      <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
