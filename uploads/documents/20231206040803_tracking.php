<div class="content-header">
    <div class="container-fluid">

        <form method="post" id="booking_report" class="card basicvalidation" action="<?php echo base_url(); ?>tracking">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group row">
                            <div class="col-sm-12 form-group">
                                <input type="text" required="true" class="form-control form-control-sm datepicker"
                                       value="<?php echo isset($_POST['tracking_date']) ? $_POST['tracking_date'] : ''; ?>"
                                       id="tracking_date" name="tracking_date" placeholder="Tracking Date">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group row">
                            <div class="col-sm-12 form-group">
                                <select id="t_vechicle" required="true" class="form-control selectized"
                                        name="t_vechicle">
                                    <option value="">Select Vehicle</option>
                                    <?php foreach ($vechiclelist as $key => $vechiclelists) { ?>
                                        <option value="<?php echo output($vechiclelists['v_registration_no']) ?>"
                                            <?php echo (isset($_POST['t_vechicle']) && ($_POST['t_vechicle'] == $vechiclelists['v_registration_no'])) ? 'selected':'' ?>
                                        ><?php echo output($vechiclelists['v_name']) . ' - ' . output($vechiclelists['v_registration_no']); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary btn-sm">Load</button>
                    </div>
                </div>
            </div>
        </form>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row-cards">
                    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                    <!--                    <script src="--><?php //echo base_url(); ?><!--assets/map.js"></script>-->

                    <div class="col-lg-12 col-md-12" id="map_canvas" style="width: 100%; height: 475px"></div>
                </div>
            </div>
        </section>
        <!-- /.content -->

        <div class="card">

            <div class="card-body p-0">
                <?php if (!empty($posArray)){ ?>
                <div class="table-responsive">
                    <table class="datatableexport table card-table table-vcenter">
                        <thead>
                        <tr>
                            <th class="w-1">ID</th>
                            <th>Serial No</th>
                            <th>Device Name</th>
                            <th>Is Stop</th>
                            <th>Position Date</th>
                            <th>Lat</th>
                            <th>Lon</th>
                            <th>Heading</th>
                            <th>Speed</th>
                            <th>Location</th>
                            <th>Description</th>
                            <th>Elevation</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $count = 1;
                        foreach ($posArray as $posList) {
                            ?>
                            <tr>
                                <td> <?php echo output($posList['id']) ?></td>
                                <td> <?php echo output($posList['SerialNo']) ?></td>
                                <td> <?php echo output($posList['DeviceName']) ?></td>
                                <td> <?php echo output($posList['IsStop']) ?></td>
                                <td> <?php echo output($posList['Position_Date']) ?></td>
                                <td> <?php echo output($posList['Lat']) ?></td>
                                <td> <?php echo output($posList['Lon']) ?></td>
                                <td> <?php echo output($posList['Heading']) ?></td>
                                <td> <?php echo output($posList['Speed']) ?></td>
                                <td> <?php echo output($posList['Location']) ?></td>
                                <td> <?php echo output($posList['Description']) ?></td>
                                <td> <?php echo output($posList['Elevation']) ?></td>


                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <?php } ?>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <script>
            /**
             * @license
             * Copyright 2019 Google LLC. All Rights Reserved.
             * SPDX-License-Identifier: Apache-2.0
             */
// This example adds an animated symbol to a polyline.
            function initMap() {
                debugger;
                const map = new google.maps.Map(document.getElementById("map_canvas"), {
                    zoom: 14,
                    center: {lat: 12.1503001, lng: -68.91263933},
                    mapTypeId: "roadmap"
                });
                // Define the symbol, using one of the predefined paths ('CIRCLE')
                // supplied by the Google Maps JavaScript API.
                const lineSymbol = {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 6,
                    strokeColor: "black",
                };


                // Create the polyline and add the symbol to it via the 'icons' property.
                const line = new google.maps.Polyline({
                    path: [
                        <?php
                        $rectot = 0;
                        if (!empty($posArray) && count($posArray) > 0) {
                            // output data of each row

                            for ($i = 0; $i <= sizeof($posArray) - 1; $i++) {
                                $rectot = $rectot + 1;
                                echo $posArray[$i]["pos_array"] . ',';

                            }
                        }
                        ?>
                    ],
                    geodesic: true,
                    strokeColor: "blue",
                    strokeOpacity: 0.4,
                    strokeWeight: 8,
                    icons: [
                        {
                            icon: lineSymbol,
                            offset: "100%",
                        },
                    ],
                    map: map,
                });

                animateCircle(line);

                const {AdvancedMarkerElement} = google.maps.importLibrary("marker");
                const tourStops = [

                    <?php
                    if (!empty($diffArray) && count($diffArray) > 0) {
                        // output data of each row
                        for ($j = 0; $j <= sizeof($diffArray) - 1; $j++) {
                            echo $diffArray[$j]["diff_array"] . ',';
                        }
                    }

                    ?>



                ];


                // Create an info window to share between markers.
                const infoWindow = new google.maps.InfoWindow();

                // Create the markers.
                tourStops.forEach(([position, title, minute], i) => {
                    const marker = new google.maps.Marker({
                        position,
                        map,
                        title: `Has stopped for ${minute} minutes at ${title}`,
                        label: `${minute}m`,
                        optimized: false,
                    });

                    // Add a click listener for each marker, and set up the info window.
                    marker.addListener("click", () => {
                        infoWindow.close();
                        infoWindow.setContent(marker.getTitle());
                        infoWindow.open(marker.getMap(), marker);
                    });
                });


            }

            // Use the DOM setInterval() function to change the offset of the symbol
            // at fixed intervals.
            function animateCircle(line) {
                let count = 0;
                var rectot = <?=$rectot?>;
                window.setInterval(() => {
                    count = (count + 1) % 2000;

                    const icons = line.get("icons");

                    icons[0].offset = count / 12 + "%";
                    line.set("icons", icons);
                }, 15);
            }


            initMap();

        </script>

        <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8bnEW2Efz8qAfLxG_NNPtp-YBCuDHcdc&callback=initMap&v=weekly"
                defer
        ></script>
