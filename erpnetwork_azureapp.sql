/*
Navicat MySQL Data Transfer

Source Server         : 3-EASYRENTPRO.NET
Source Server Version : 50741
Source Host           : 162.215.222.36:3306
Source Database       : erpnetwork_azureapp

Target Server Type    : MYSQL
Target Server Version : 50741
File Encoding         : 65001

Date: 2024-02-15 23:27:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `company_bankdetails`
-- ----------------------------
DROP TABLE IF EXISTS `company_bankdetails`;
CREATE TABLE `company_bankdetails` (
  `companybankid` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `bankname` varchar(255) NOT NULL,
  `accounttype` varchar(255) NOT NULL,
  `accountname` varchar(255) NOT NULL,
  `startballence` varchar(255) NOT NULL,
  `currentballence` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`companybankid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of company_bankdetails
-- ----------------------------

-- ----------------------------
-- Table structure for `company_loandetails`
-- ----------------------------
DROP TABLE IF EXISTS `company_loandetails`;
CREATE TABLE `company_loandetails` (
  `companyloanid` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `financialinstitution` varchar(255) NOT NULL,
  `project` varchar(255) NOT NULL,
  `loanamount` varchar(255) NOT NULL,
  `interestrate` varchar(25) NOT NULL,
  `period` varchar(25) NOT NULL,
  `firstdate` varchar(255) NOT NULL,
  `enddate` varchar(255) NOT NULL,
  `redemptionamount` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`companyloanid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of company_loandetails
-- ----------------------------
INSERT INTO `company_loandetails` VALUES ('1', '1', '1', '1', '2500000', '10', '25', '12/21/2023', '12/22/2023', '5000', '0');
INSERT INTO `company_loandetails` VALUES ('2', '1', '2', '1', '5000000', '5', '60', '02/15/2024', '02/15/2024', '5400', '0');

-- ----------------------------
-- Table structure for `company_projectdetails`
-- ----------------------------
DROP TABLE IF EXISTS `company_projectdetails`;
CREATE TABLE `company_projectdetails` (
  `company_projectid` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `projectname` varchar(255) NOT NULL,
  `startdate` varchar(255) NOT NULL,
  `enddate` varchar(255) NOT NULL,
  `estimatedcost` varchar(255) NOT NULL,
  `payments` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`company_projectid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of company_projectdetails
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_adminusers`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_adminusers`;
CREATE TABLE `tbl_adminusers` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `usertype` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1= main admin 2= sub admin',
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `show_passkey` varchar(255) DEFAULT NULL,
  `createddate` datetime NOT NULL,
  `is_deleted` tinyint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_adminusers
-- ----------------------------
INSERT INTO `tbl_adminusers` VALUES ('1', '1', 'super', 'admin', 'admin', '9179597332', 'info@admin.com', '20231221052011_dimg.jpg', '$2y$10$pstPhbR86NrcQEbO2CLT3eeJHjI.ySnOuAQQO0SMXMEQ6mXG5MTDe', 'azure123', '2020-01-10 02:11:03', '0');
INSERT INTO `tbl_adminusers` VALUES ('2', '3', 'amit', 'pandey', 'amit.pandey', '9876543210', 'amittest@gmail.com', '', '$2y$10$9gM2URZAKOhHLoLctZgqoe63o/Ty3xLmA3Ot2rxtpzRevpj4o7Jla', '123456', '2022-07-31 11:50:17', '0');
INSERT INTO `tbl_adminusers` VALUES ('4', '4', 'Charles ', 'Joubert', 'charlie', '6547893210', 'ajay.test@gmail.com', '20220731123224_Tulips.jpg', '$2y$10$WFPOQ2CJO51dzRyiS9x7aOhDEEiOax0GcP7ojPNo/AA9e7n9zTA5O', '123456', '2022-07-31 12:13:00', '0');
INSERT INTO `tbl_adminusers` VALUES ('5', '3', 'Algernon', 'Wedervoort', 'algernon', '3215243534555', 'info@sunsetsolutions.net', '', '$2y$10$aVMk9hkAIR/wj8l4ODxL0uipBlbBRdX1yjrvidabOGz3sPP4H8T5W', '123456', '2022-10-28 19:56:07', '0');
INSERT INTO `tbl_adminusers` VALUES ('6', '2', 'Pedro', 'Wacawa', 'pedro', '5645875', 'pedro@pedro.com', '', '$2y$10$DIlZSt95xBo.XtGav9yyEuj.5WuQc15.Sp5vz8IeS0Fh5UfD4YWL6', '123456', '2023-10-26 17:39:29', '0');

-- ----------------------------
-- Table structure for `tbl_checklist`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_checklist`;
CREATE TABLE `tbl_checklist` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `Sort` int(10) DEFAULT NULL,
  `section` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_id` (`id`) USING BTREE,
  KEY `id_ssort` (`Sort`) USING BTREE,
  KEY `id_sect` (`section`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_checklist
-- ----------------------------
INSERT INTO `tbl_checklist` VALUES ('1', '1001', 'KEUKEN', 'Keukenlades');
INSERT INTO `tbl_checklist` VALUES ('2', '1002', 'KEUKEN', 'Keukendeuren en plint');
INSERT INTO `tbl_checklist` VALUES ('3', '1003', 'KEUKEN', 'Apparatuur');
INSERT INTO `tbl_checklist` VALUES ('4', '1004', 'KEUKEN', 'Kitwerk keukenblad');
INSERT INTO `tbl_checklist` VALUES ('5', '1005', 'KEUKEN', 'Kitwerk wasbak');
INSERT INTO `tbl_checklist` VALUES ('6', '1006', 'KEUKEN', 'Keuken kraan');
INSERT INTO `tbl_checklist` VALUES ('7', '1007', 'KEUKEN', 'Verlichting');
INSERT INTO `tbl_checklist` VALUES ('8', '1008', 'KEUKEN', 'stopcontacten');
INSERT INTO `tbl_checklist` VALUES ('9', '1009', 'KEUKEN', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('10', '1010', 'KEUKEN', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('11', '1011', 'KEUKEN', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('12', '1101', 'Woonkamer / hal', 'Deuren / sluiting / kitwerk');
INSERT INTO `tbl_checklist` VALUES ('13', '1102', 'Woonkamer / hal', 'Ramen - montage / kitwerk');
INSERT INTO `tbl_checklist` VALUES ('14', '1103', 'Woonkamer / hal', 'Verlichting');
INSERT INTO `tbl_checklist` VALUES ('15', '1104', 'Woonkamer / hal', 'stopcontacten');
INSERT INTO `tbl_checklist` VALUES ('16', '1105', 'Woonkamer / hal', 'Airco');
INSERT INTO `tbl_checklist` VALUES ('17', '1106', 'Woonkamer / hal', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('18', '1107', 'Woonkamer / hal', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('19', '1108', 'Woonkamer / hal', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('20', '1109', 'Woonkamer / hal', 'Fan');
INSERT INTO `tbl_checklist` VALUES ('21', '1201', 'WC Woonkamer', 'Deuren / sluiting / kitwerk');
INSERT INTO `tbl_checklist` VALUES ('22', '1202', 'WC Woonkamer', 'Ramen - montage / kitwerk');
INSERT INTO `tbl_checklist` VALUES ('23', '1203', 'WC Woonkamer', 'Verlichting');
INSERT INTO `tbl_checklist` VALUES ('24', '1204', 'WC Woonkamer', 'stopcontacten');
INSERT INTO `tbl_checklist` VALUES ('25', '1205', 'WC Woonkamer', 'Wandmeubel');
INSERT INTO `tbl_checklist` VALUES ('26', '1206', 'WC Woonkamer', 'Kraan / Spiegel');
INSERT INTO `tbl_checklist` VALUES ('27', '1207', 'WC Woonkamer', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('28', '1208', 'WC Woonkamer', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('29', '1209', 'WC Woonkamer', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('30', '1301', 'Slaapkamer voorzijde', 'Deur / sluiting');
INSERT INTO `tbl_checklist` VALUES ('31', '1302', 'Slaapkamer voorzijde', 'Ramen - montage / kitwerk');
INSERT INTO `tbl_checklist` VALUES ('32', '1303', 'Slaapkamer voorzijde', 'Stopcontacten');
INSERT INTO `tbl_checklist` VALUES ('33', '1304', 'Slaapkamer voorzijde', 'Airco');
INSERT INTO `tbl_checklist` VALUES ('34', '1305', 'Slaapkamer voorzijde', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('35', '1306', 'Slaapkamer voorzijde', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('36', '1307', 'Slaapkamer voorzijde', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('37', '1308', 'Slaapkamer voorzijde', 'Fan');
INSERT INTO `tbl_checklist` VALUES ('38', '1309', 'test item', 'test section');
INSERT INTO `tbl_checklist` VALUES ('39', '1401', 'Slaapkamer hoek', 'Deur / sluiting');
INSERT INTO `tbl_checklist` VALUES ('40', '1402', 'Slaapkamer hoek', 'Ramen - montage / kitwerk');
INSERT INTO `tbl_checklist` VALUES ('41', '1403', 'Slaapkamer hoek', 'Stopcontacten');
INSERT INTO `tbl_checklist` VALUES ('42', '1404', 'Slaapkamer hoek', 'Airco');
INSERT INTO `tbl_checklist` VALUES ('43', '1405', 'Slaapkamer hoek', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('44', '1406', 'Slaapkamer hoek', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('45', '1407', 'Slaapkamer hoek', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('46', '1408', 'Slaapkamer hoek', 'Fan');
INSERT INTO `tbl_checklist` VALUES ('47', '1501', 'Kleine badkamer', 'Deur / sluiting');
INSERT INTO `tbl_checklist` VALUES ('48', '1502', 'Kleine badkamer', 'Ramen - montage / kitwerk');
INSERT INTO `tbl_checklist` VALUES ('49', '1503', 'Kleine badkamer', 'Stopcontacten');
INSERT INTO `tbl_checklist` VALUES ('50', '1504', 'Kleine badkamer', 'Wandmeubel');
INSERT INTO `tbl_checklist` VALUES ('51', '1505', 'Kleine badkamer', 'Kraan / Spiegel');
INSERT INTO `tbl_checklist` VALUES ('52', '1506', 'Kleine badkamer', 'WC montage / deksel');
INSERT INTO `tbl_checklist` VALUES ('53', '1507', 'Kleine badkamer', 'Douche kraan set');
INSERT INTO `tbl_checklist` VALUES ('54', '1508', 'Kleine badkamer', 'Douche wand');
INSERT INTO `tbl_checklist` VALUES ('55', '1509', 'Kleine badkamer', 'Kitwerk');
INSERT INTO `tbl_checklist` VALUES ('56', '1510', 'Kleine badkamer', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('57', '1511', 'Kleine badkamer', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('58', '1512', 'Kleine badkamer', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('59', '1601', 'Master Slaapkamer', 'Deur / sluiting');
INSERT INTO `tbl_checklist` VALUES ('60', '1602', 'Master Slaapkamer', 'Ramen');
INSERT INTO `tbl_checklist` VALUES ('61', '1603', 'Master Slaapkamer', 'Stopcontacten');
INSERT INTO `tbl_checklist` VALUES ('62', '1604', 'Master Slaapkamer', 'Airco');
INSERT INTO `tbl_checklist` VALUES ('63', '1605', 'Master Slaapkamer', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('64', '1606', 'Master Slaapkamer', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('65', '1607', 'Master Slaapkamer', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('66', '1608', 'Master Slaapkamer', 'Fan');
INSERT INTO `tbl_checklist` VALUES ('67', '1701', 'Master badkamer', 'Deur / sluiting');
INSERT INTO `tbl_checklist` VALUES ('68', '1702', 'Master badkamer', 'Ramen - montage / kitwerk');
INSERT INTO `tbl_checklist` VALUES ('69', '1703', 'Master badkamer', 'Stopcontacten');
INSERT INTO `tbl_checklist` VALUES ('70', '1704', 'Master badkamer', 'Wandmeubel');
INSERT INTO `tbl_checklist` VALUES ('71', '1705', 'Master badkamer', 'Kraan / Spiegel');
INSERT INTO `tbl_checklist` VALUES ('72', '1706', 'Master badkamer', 'WC montage / deksel');
INSERT INTO `tbl_checklist` VALUES ('73', '1707', 'Master badkamer', 'Douche kraan set');
INSERT INTO `tbl_checklist` VALUES ('74', '1708', 'Master badkamer', 'Douche wand');
INSERT INTO `tbl_checklist` VALUES ('75', '1709', 'Master badkamer', 'Kitwerk');
INSERT INTO `tbl_checklist` VALUES ('76', '1710', 'Master badkamer', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('77', '1711', 'Master badkamer', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('78', '1712', 'Master badkamer', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('79', '1801', 'Walk In Closet', 'Deur / sluiting');
INSERT INTO `tbl_checklist` VALUES ('80', '1802', 'Walk In Closet', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('81', '1803', 'Walk In Closet', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('82', '1804', 'Walk In Closet', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('83', '1901', 'Porch', 'Verlichting');
INSERT INTO `tbl_checklist` VALUES ('84', '1902', 'Porch', 'Fans');
INSERT INTO `tbl_checklist` VALUES ('85', '1903', 'Porch', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('86', '1904', 'Porch', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('87', '1905', 'Porch', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('88', '2001', 'Washok', 'Deur / sluiting');
INSERT INTO `tbl_checklist` VALUES ('89', '2002', 'Washok', 'Ramen - montage / kitwerk');
INSERT INTO `tbl_checklist` VALUES ('90', '2003', 'Washok', 'Stopcontacten');
INSERT INTO `tbl_checklist` VALUES ('91', '2004', 'Washok', 'Boiler');
INSERT INTO `tbl_checklist` VALUES ('92', '2005', 'Washok', 'Plafon');
INSERT INTO `tbl_checklist` VALUES ('93', '2006', 'Washok', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('94', '2007', 'Washok', 'Tegelwerk');
INSERT INTO `tbl_checklist` VALUES ('95', '2101', 'Tuin / Algemeen', 'Scheidingsmuren');
INSERT INTO `tbl_checklist` VALUES ('96', '2102', 'Tuin / Algemeen', 'Muur voorzijde');
INSERT INTO `tbl_checklist` VALUES ('97', '2103', 'Tuin / Algemeen', 'Deksels Septic');
INSERT INTO `tbl_checklist` VALUES ('98', '2104', 'Tuin / Algemeen', 'Airco\'s buiten');
INSERT INTO `tbl_checklist` VALUES ('99', '2105', 'Tuin / Algemeen', 'Schilderwerk');
INSERT INTO `tbl_checklist` VALUES ('100', '2106', 'Tuin / Algemeen', 'Buiten lampen');
INSERT INTO `tbl_checklist` VALUES ('101', '2107', 'Tuin / Algemeen', 'Boeiboord');
INSERT INTO `tbl_checklist` VALUES ('102', '2108', 'Tuin / Algemeen', 'Dak');
INSERT INTO `tbl_checklist` VALUES ('103', '2109', 'Tuin / Algemeen', 'hekwerk / deurtjes');
INSERT INTO `tbl_checklist` VALUES ('104', '1012', 'KEUKEN', 'test');

-- ----------------------------
-- Table structure for `tbl_check_list_items`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_check_list_items`;
CREATE TABLE `tbl_check_list_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sell_property_id` bigint(20) unsigned DEFAULT NULL,
  `check_list_id` bigint(20) DEFAULT NULL,
  `item_condition` varchar(255) DEFAULT NULL,
  `remarks` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_check_list_items
-- ----------------------------
INSERT INTO `tbl_check_list_items` VALUES ('82', '1', '1', 'Slecht', 'zie foto 2', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('83', '1', '2', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('84', '1', '3', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('85', '1', '4', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('86', '1', '5', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('87', '1', '6', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('88', '1', '7', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('89', '1', '8', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('90', '1', '9', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('91', '1', '10', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('92', '1', '11', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('93', '1', '12', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('94', '1', '13', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('95', '1', '14', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('96', '1', '15', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('97', '1', '16', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('98', '1', '17', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('99', '1', '18', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('100', '1', '19', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('101', '1', '20', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('102', '1', '21', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('103', '1', '22', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('104', '1', '23', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('105', '1', '24', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('106', '1', '25', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('107', '1', '26', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('108', '1', '27', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('109', '1', '28', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('110', '1', '29', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('111', '1', '30', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('112', '1', '31', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('113', '1', '32', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('114', '1', '33', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('115', '1', '34', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('116', '1', '35', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('117', '1', '36', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('118', '1', '37', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('119', '1', '38', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('120', '1', '39', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('121', '1', '40', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('122', '1', '41', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('123', '1', '42', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('124', '1', '43', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('125', '1', '44', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('126', '1', '45', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('127', '1', '46', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('128', '1', '47', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('129', '1', '48', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('130', '1', '49', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('131', '1', '50', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('132', '1', '51', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('133', '1', '52', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('134', '1', '53', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('135', '1', '54', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('136', '1', '55', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('137', '1', '56', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('138', '1', '57', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('139', '1', '58', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('140', '1', '59', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('141', '1', '60', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('142', '1', '61', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('143', '1', '62', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('144', '1', '63', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('145', '1', '64', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('146', '1', '65', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('147', '1', '66', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('148', '1', '67', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('149', '1', '68', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('150', '1', '69', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('151', '1', '70', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('152', '1', '71', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('153', '1', '72', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('154', '1', '73', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('155', '1', '74', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('156', '1', '75', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('157', '1', '76', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('158', '1', '77', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('159', '1', '78', 'Goed', '', '2023-12-21 21:18:01');
INSERT INTO `tbl_check_list_items` VALUES ('160', '1', '104', 'Goed', '', '2023-12-21 21:18:01');

-- ----------------------------
-- Table structure for `tbl_compnies`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_compnies`;
CREATE TABLE `tbl_compnies` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `found_date` varchar(255) NOT NULL,
  `company_name` varchar(1000) NOT NULL,
  `company_partners` varchar(500) NOT NULL,
  `phoneno` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `crib_no` varchar(1000) NOT NULL,
  `kvk_no` varchar(255) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=deleted 1=Not deleted',
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_compnies
-- ----------------------------
INSERT INTO `tbl_compnies` VALUES ('1', '2023-12-01', 'Azure Properties', 'Charles Joubert', '5660000', 'info@azure.com', 'Test Address', '123456', '654987', '2023-12-21 17:24:13', '0');

-- ----------------------------
-- Table structure for `tbl_contractors`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_contractors`;
CREATE TABLE `tbl_contractors` (
  `contractor_id` int(11) NOT NULL AUTO_INCREMENT,
  `contractor_name` varchar(255) NOT NULL,
  `contractor_address` varchar(500) NOT NULL,
  `contractor_phone` varchar(25) NOT NULL,
  `contractor_type` varchar(255) NOT NULL,
  `contractor_person` varchar(1000) NOT NULL,
  `contractor_assignproject` text NOT NULL,
  `contractor_payments` varchar(25) NOT NULL,
  `contractor_cribno` varchar(255) NOT NULL,
  `contractor_kvkno` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`contractor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_contractors
-- ----------------------------
INSERT INTO `tbl_contractors` VALUES ('1', 'Pro One Contructions', '0', '000', 'Constructor', 'x', '1,4', '0', '4', '2222', '0', '2023-12-21 18:05:21');
INSERT INTO `tbl_contractors` VALUES ('2', 'BMD Contructions', 'x', '0', 'Constructor', 'x', '3', '', '0', '4', '0', '2023-12-21 18:06:30');
INSERT INTO `tbl_contractors` VALUES ('3', 'Nace Constructions', 'x', '1', 'Constructor', 'x', '2', '0', '1', '1', '0', '2023-12-21 18:07:34');
INSERT INTO `tbl_contractors` VALUES ('4', 'Murray Electric', 'x', '0', 'Electrician', 'x', '1', '0', '1', '1', '0', '2023-12-21 18:08:09');
INSERT INTO `tbl_contractors` VALUES ('5', 'CWM', 'x', '1', 'Constructor', 'a', '3', '0', '1', '1', '0', '2023-12-21 18:09:00');

-- ----------------------------
-- Table structure for `tbl_contractortype`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_contractortype`;
CREATE TABLE `tbl_contractortype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contractortype` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_contractortype
-- ----------------------------
INSERT INTO `tbl_contractortype` VALUES ('1', 'Constructor', '0');
INSERT INTO `tbl_contractortype` VALUES ('2', 'Electrician', '0');
INSERT INTO `tbl_contractortype` VALUES ('3', 'Painter', '0');
INSERT INTO `tbl_contractortype` VALUES ('5', 'Plumber', '0');

-- ----------------------------
-- Table structure for `tbl_documents`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_documents`;
CREATE TABLE `tbl_documents` (
  `documentid` int(11) NOT NULL AUTO_INCREMENT,
  `pdid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `docname` varchar(255) NOT NULL,
  `documenttype` varchar(255) NOT NULL,
  `documentimg` varchar(255) NOT NULL,
  `documentdesc` text NOT NULL,
  `documentadddate` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`documentid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_documents
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_financialinstitute`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_financialinstitute`;
CREATE TABLE `tbl_financialinstitute` (
  `financialinstituteid` int(11) NOT NULL AUTO_INCREMENT,
  `financialinstitutename` varchar(255) NOT NULL,
  `financialinstituteaddress` varchar(1000) NOT NULL,
  `financialinstitutephone` varchar(255) NOT NULL,
  `financialinstituteaccountno` varchar(255) NOT NULL,
  `financialinstituteaccountloan` text NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`financialinstituteid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_financialinstitute
-- ----------------------------
INSERT INTO `tbl_financialinstitute` VALUES ('1', 'Banco di Caribe', '', '', '', '', '0');
INSERT INTO `tbl_financialinstitute` VALUES ('2', 'Maduro & Curiels Bank', '', '', '', '', '0');
INSERT INTO `tbl_financialinstitute` VALUES ('3', 'Orco Bank', '0', '0', '0', '', '0');

-- ----------------------------
-- Table structure for `tbl_itemdetails`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_itemdetails`;
CREATE TABLE `tbl_itemdetails` (
  `itemid` int(11) NOT NULL AUTO_INCREMENT,
  `purchaseorderid` int(11) NOT NULL,
  `itemname` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_itemdetails
-- ----------------------------
INSERT INTO `tbl_itemdetails` VALUES ('1', '1', 'Palu 2x6 20 ft', '25');
INSERT INTO `tbl_itemdetails` VALUES ('2', '1', 'Palu 1x6 20 ft', '12');
INSERT INTO `tbl_itemdetails` VALUES ('3', '2', '1x6 20 ft', '34');
INSERT INTO `tbl_itemdetails` VALUES ('4', '2', '', '0');
INSERT INTO `tbl_itemdetails` VALUES ('7', '3', 'Klabu', '25');

-- ----------------------------
-- Table structure for `tbl_loans`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_loans`;
CREATE TABLE `tbl_loans` (
  `loan_id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `finance_company` varchar(1000) NOT NULL,
  `finance_project` varchar(255) NOT NULL,
  `loan_amount` varchar(255) NOT NULL DEFAULT '0',
  `interestrate` varchar(255) NOT NULL,
  `period` int(11) NOT NULL,
  `first_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `redemption_amount` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdate` datetime NOT NULL,
  PRIMARY KEY (`loan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_loans
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_partner`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_partner`;
CREATE TABLE `tbl_partner` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_name` varchar(255) NOT NULL,
  `partner_email` varchar(255) NOT NULL,
  `partner_phone` varchar(255) NOT NULL,
  `partner_address` varchar(1000) NOT NULL,
  `partner_cribno` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL COMMENT '0=deleted 1=not eleted',
  `registerdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`partner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_partner
-- ----------------------------
INSERT INTO `tbl_partner` VALUES ('1', 'Charles Joubert', 'xxx@xx.com', '123456', 'Charlies Address', '123456', '0', '2023-12-21 17:23:40');
INSERT INTO `tbl_partner` VALUES ('2', 'Lunee Vastgoed', 'info@ddd.com', '6', '0', '111111', '0', '2023-12-21 18:11:33');
INSERT INTO `tbl_partner` VALUES ('3', 'Vidanova Bank', 'ddd@ff.com', '222', 'rrr', '222222', '0', '2023-12-21 18:11:57');

-- ----------------------------
-- Table structure for `tbl_paymentmethod`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_paymentmethod`;
CREATE TABLE `tbl_paymentmethod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paymentmethod` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_paymentmethod
-- ----------------------------
INSERT INTO `tbl_paymentmethod` VALUES ('1', 'Cash', '0');
INSERT INTO `tbl_paymentmethod` VALUES ('4', 'Swipe', '0');
INSERT INTO `tbl_paymentmethod` VALUES ('5', 'Online Transfer', '0');
INSERT INTO `tbl_paymentmethod` VALUES ('6', 'Credit', '0');
INSERT INTO `tbl_paymentmethod` VALUES ('7', 'Credit Note', '0');

-- ----------------------------
-- Table structure for `tbl_payments`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_payments`;
CREATE TABLE `tbl_payments` (
  `paymentid` int(11) NOT NULL AUTO_INCREMENT,
  `project` varchar(255) NOT NULL,
  `contractorid` int(11) NOT NULL,
  `suppliersval` int(11) NOT NULL,
  `purchaseorder` varchar(255) NOT NULL,
  `paidbycompany` varchar(255) NOT NULL,
  `paymentmethod` varchar(1000) NOT NULL,
  `paidto` varchar(255) NOT NULL,
  `projectsection` varchar(255) DEFAULT NULL,
  `paymentdescription` text NOT NULL,
  `Amount` varchar(255) NOT NULL,
  `paymentdate` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`paymentid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_payments
-- ----------------------------
INSERT INTO `tbl_payments` VALUES ('1', '1', '1', '1', '254', 'Azure Properties', 'Cash', 'Supplier', 'Foundation', 'pay', '500', '2023-12-21 18:41:36', '0');
INSERT INTO `tbl_payments` VALUES ('2', '2', '1', '1', '2345', '', 'other', '1', 'Foundation', '', '300', '2023-12-21 21:22:45', '0');
INSERT INTO `tbl_payments` VALUES ('3', '1', '3', '1', '25', '', 'other', '1', 'Foundation,Doors', '', '250', '2024-02-15 15:17:30', '0');

-- ----------------------------
-- Table structure for `tbl_projectcontractors`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_projectcontractors`;
CREATE TABLE `tbl_projectcontractors` (
  `projectcontractorsid` int(11) NOT NULL AUTO_INCREMENT,
  `projectid` int(11) NOT NULL,
  `contractorid` int(11) NOT NULL,
  `projectamount` varchar(255) NOT NULL,
  `payments` varchar(255) NOT NULL,
  `outstandingpayment` varchar(255) NOT NULL,
  PRIMARY KEY (`projectcontractorsid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_projectcontractors
-- ----------------------------
INSERT INTO `tbl_projectcontractors` VALUES ('6', '4', '0', '', '', '');
INSERT INTO `tbl_projectcontractors` VALUES ('8', '3', '0', '', '', '');
INSERT INTO `tbl_projectcontractors` VALUES ('9', '2', '0', '', '', '');
INSERT INTO `tbl_projectcontractors` VALUES ('11', '1', '1', '250000', '', '');
INSERT INTO `tbl_projectcontractors` VALUES ('12', '1', '4', '149999', '', '');

-- ----------------------------
-- Table structure for `tbl_projectfinancial`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_projectfinancial`;
CREATE TABLE `tbl_projectfinancial` (
  `financialid` int(11) NOT NULL AUTO_INCREMENT,
  `projectid` int(11) NOT NULL,
  `financialinstituteid` int(11) NOT NULL,
  `financedamount` varchar(255) NOT NULL,
  `repaymentperiod` varchar(255) NOT NULL,
  `repaymentamount` varchar(255) NOT NULL,
  PRIMARY KEY (`financialid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_projectfinancial
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_projectpartner`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_projectpartner`;
CREATE TABLE `tbl_projectpartner` (
  `projectpartnerid` int(11) NOT NULL AUTO_INCREMENT,
  `projectid` int(11) NOT NULL,
  `partnerid` int(11) NOT NULL,
  `investmentamount` varchar(255) NOT NULL,
  PRIMARY KEY (`projectpartnerid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_projectpartner
-- ----------------------------
INSERT INTO `tbl_projectpartner` VALUES ('6', '4', '0', '');
INSERT INTO `tbl_projectpartner` VALUES ('8', '3', '0', '');
INSERT INTO `tbl_projectpartner` VALUES ('9', '2', '0', '');
INSERT INTO `tbl_projectpartner` VALUES ('12', '1', '1', '500000');
INSERT INTO `tbl_projectpartner` VALUES ('13', '1', '2', '200000');

-- ----------------------------
-- Table structure for `tbl_projectpayments`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_projectpayments`;
CREATE TABLE `tbl_projectpayments` (
  `projectpaymentid` int(11) NOT NULL AUTO_INCREMENT,
  `projectid` int(11) NOT NULL,
  `paymentmethod` varchar(255) NOT NULL,
  `paymentamount` varchar(255) NOT NULL,
  `paymentdate` datetime NOT NULL,
  PRIMARY KEY (`projectpaymentid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_projectpayments
-- ----------------------------
INSERT INTO `tbl_projectpayments` VALUES ('6', '4', '', '', '2023-12-21 18:13:11');
INSERT INTO `tbl_projectpayments` VALUES ('8', '3', '', '', '2023-12-21 18:13:56');
INSERT INTO `tbl_projectpayments` VALUES ('9', '2', '', '', '2023-12-21 18:14:14');
INSERT INTO `tbl_projectpayments` VALUES ('11', '1', '1', '', '2023-12-21 18:17:07');

-- ----------------------------
-- Table structure for `tbl_projects`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_projects`;
CREATE TABLE `tbl_projects` (
  `projectid` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) NOT NULL,
  `companyname` varchar(255) NOT NULL,
  `projectdesc` text,
  `projectdetails` text,
  `projecttype` varchar(255) NOT NULL,
  `location` text,
  `startdate` varchar(255) DEFAULT NULL,
  `enddate` varchar(255) DEFAULT NULL,
  `loans` text,
  `totalestimatedcost` varchar(255) DEFAULT NULL,
  `totalpayment` varchar(255) DEFAULT NULL,
  `ballence` varchar(255) DEFAULT NULL,
  `contractoramount` varchar(255) DEFAULT NULL,
  `estimatedprofit` varchar(255) DEFAULT NULL,
  `actualprofit` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `postdate` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `numberoflots` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`projectid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_projects
-- ----------------------------
INSERT INTO `tbl_projects` VALUES ('1', 'Bella Vista Estate', '1', null, null, 'Residential Villa', 'Persuesweg', '12/21/2023', '12/21/2023', null, '3500000', '', '', '42548', '', '', 'Pending', '2023-12-21 00:00:00', '0', '200');
INSERT INTO `tbl_projects` VALUES ('2', 'Lyra Projects', '1', null, null, 'Residential Villa', 'Lyraweg', '12/28/2023', '12/21/2023', null, '15000000', '', '', '0', '', '', 'Pending', '2023-12-21 00:00:00', '0', '25');
INSERT INTO `tbl_projects` VALUES ('3', 'Hypique', '1', null, null, 'Residential Villa', 'xx', '12/21/2023', '12/21/2023', null, '4500000', '', '', '100000', '', '', 'Ongoing', '2023-12-21 00:00:00', '0', '25');
INSERT INTO `tbl_projects` VALUES ('4', 'Cas Cora', '1', null, null, 'Residential Villa', 'Cas Coraweg', '12/21/2023', '12/21/2023', null, '5000000', '', '', '0', '', '', 'Ongoing', '2023-12-21 00:00:00', '0', '50');

-- ----------------------------
-- Table structure for `tbl_projectsdocument`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_projectsdocument`;
CREATE TABLE `tbl_projectsdocument` (
  `projectdocumentid` int(11) NOT NULL AUTO_INCREMENT,
  `projectid` int(11) NOT NULL,
  `documentname` varchar(255) NOT NULL,
  `documentfile` varchar(255) NOT NULL,
  `addeddate` datetime NOT NULL,
  PRIMARY KEY (`projectdocumentid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_projectsdocument
-- ----------------------------
INSERT INTO `tbl_projectsdocument` VALUES ('6', '4', '', '', '2023-12-21 18:13:11');
INSERT INTO `tbl_projectsdocument` VALUES ('8', '3', '', '', '2023-12-21 18:13:56');
INSERT INTO `tbl_projectsdocument` VALUES ('9', '2', '', '', '2023-12-21 18:14:14');
INSERT INTO `tbl_projectsdocument` VALUES ('11', '1', '', '', '2023-12-21 18:17:07');

-- ----------------------------
-- Table structure for `tbl_projectsection`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_projectsection`;
CREATE TABLE `tbl_projectsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectsectionname` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_projectsection
-- ----------------------------
INSERT INTO `tbl_projectsection` VALUES ('1', 'Doors', '0');
INSERT INTO `tbl_projectsection` VALUES ('2', 'Roof', '0');
INSERT INTO `tbl_projectsection` VALUES ('3', 'Windows', '0');
INSERT INTO `tbl_projectsection` VALUES ('5', 'Foundation', '0');
INSERT INTO `tbl_projectsection` VALUES ('6', 'Tiles', '0');

-- ----------------------------
-- Table structure for `tbl_projecttype`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_projecttype`;
CREATE TABLE `tbl_projecttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projecttype` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_projecttype
-- ----------------------------
INSERT INTO `tbl_projecttype` VALUES ('1', 'Residential Villa', '0');
INSERT INTO `tbl_projecttype` VALUES ('2', 'Appartments', '0');
INSERT INTO `tbl_projecttype` VALUES ('3', 'Hotel', '0');
INSERT INTO `tbl_projecttype` VALUES ('5', 'Commercial Building', '0');
INSERT INTO `tbl_projecttype` VALUES ('8', 'Terrain', '0');
INSERT INTO `tbl_projecttype` VALUES ('9', 'Church', '0');

-- ----------------------------
-- Table structure for `tbl_purchaseorders`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_purchaseorders`;
CREATE TABLE `tbl_purchaseorders` (
  `orderid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `suppliers` varchar(1000) NOT NULL,
  `projectid` int(11) NOT NULL,
  `contractorsid` int(11) NOT NULL,
  `invoicenr` varchar(255) NOT NULL,
  `projectsection` varchar(255) DEFAULT NULL,
  `paymentmethod` varchar(255) DEFAULT NULL,
  `invoiceamount` varchar(255) NOT NULL,
  `invoiceimage` varchar(255) NOT NULL,
  `invoicedate` datetime NOT NULL,
  `paymentdate` datetime NOT NULL,
  `perchaseorderimage` varchar(500) DEFAULT NULL,
  `is_verified` tinyint(4) NOT NULL DEFAULT '0',
  `verifiedby` varchar(255) DEFAULT NULL,
  `verifieddate` datetime NOT NULL,
  `orderdate` datetime NOT NULL,
  `ispaid` tinyint(4) NOT NULL DEFAULT '0',
  `is_addinvoice` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orderid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_purchaseorders
-- ----------------------------
INSERT INTO `tbl_purchaseorders` VALUES ('1', '6', '1', '1', '1', '254', 'Foundation', 'other', '500', '', '2023-12-21 00:00:00', '2023-12-21 00:00:00', null, '2', '1', '2023-12-21 18:40:31', '2023-12-21 18:39:27', '1', '0', '0');
INSERT INTO `tbl_purchaseorders` VALUES ('2', '6', '1', '2', '1', '2345', 'Foundation', 'other', '300', '', '2023-12-21 00:00:00', '0000-00-00 00:00:00', null, '2', '1', '2023-12-21 21:21:46', '2023-12-21 21:20:59', '0', '0', '0');
INSERT INTO `tbl_purchaseorders` VALUES ('3', '6', '1', '1', '3', '25', 'Foundation,Doors', 'other', '250', '20240215031730_2023-03-01_0-04-13.jpg', '2024-02-15 00:00:00', '0000-00-00 00:00:00', '20240215031429_2021-04-21_7-48-59.jpg', '2', '1', '2024-02-15 15:15:50', '2024-02-15 15:14:29', '0', '0', '0');

-- ----------------------------
-- Table structure for `tbl_sell_properties`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sell_properties`;
CREATE TABLE `tbl_sell_properties` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) unsigned NOT NULL,
  `project_type_id` bigint(20) unsigned NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `real_estate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `buyer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `buyer_address` text COLLATE utf8_unicode_ci NOT NULL,
  `buyer_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `buyer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selling_price` decimal(18,1) NOT NULL,
  `other_charges` decimal(18,1) NOT NULL,
  `agreement_date` datetime NOT NULL,
  `delivery_date` datetime NOT NULL,
  `buyer_signature` text COLLATE utf8_unicode_ci,
  `seller_signature` text COLLATE utf8_unicode_ci,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_sell_properties
-- ----------------------------
INSERT INTO `tbl_sell_properties` VALUES ('1', '2', '1', '26', 'X', 'Algernon', 'Kaja suku 22', '673333', 'info@easyrentpro.com', '250000.0', '10000.0', '2023-12-21 00:00:00', '2023-12-21 00:00:00', '20231221091801buyer_signature.png', null, '', '2023-12-21 20:45:10', '0');

-- ----------------------------
-- Table structure for `tbl_sell_property_documents`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sell_property_documents`;
CREATE TABLE `tbl_sell_property_documents` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sell_property_id` bigint(20) unsigned DEFAULT NULL,
  `document_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_file` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_sell_property_documents
-- ----------------------------
INSERT INTO `tbl_sell_property_documents` VALUES ('6', '1', '', '', '2023-12-21 21:18:01');

-- ----------------------------
-- Table structure for `tbl_settingpermission`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_settingpermission`;
CREATE TABLE `tbl_settingpermission` (
  `settingsid` int(11) NOT NULL AUTO_INCREMENT,
  `usertype` varchar(2255) NOT NULL,
  `menutab` varchar(255) NOT NULL,
  `allowfor_add` tinyint(4) NOT NULL DEFAULT '0',
  `allowfor_edit` tinyint(4) NOT NULL DEFAULT '0',
  `allowfor_modify` tinyint(4) NOT NULL DEFAULT '0',
  `allow_delete` tinyint(4) NOT NULL DEFAULT '0',
  `allowfor_verify` tinyint(4) NOT NULL DEFAULT '0',
  `allowfor_complete` tinyint(4) NOT NULL DEFAULT '0',
  `allowfor_pay` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`settingsid`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_settingpermission
-- ----------------------------
INSERT INTO `tbl_settingpermission` VALUES ('1', '1', 'Compnies', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('2', '1', 'Projects', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('3', '1', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('4', '1', 'Partners', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('5', '1', 'Suppliers', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('6', '1', 'Contractors', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('7', '1', 'Loans', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('8', '1', 'Bank Accounts', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('9', '1', 'Financial Institute', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('10', '1', 'Payments', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('11', '1', 'Documents', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('12', '1', 'Purchase orders', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('13', '1', 'Dashboard', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('14', '1', 'Sell Properties', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('15', '2', 'Compnies', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('16', '2', 'Projects', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('17', '2', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('18', '2', 'Partners', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('19', '2', 'Suppliers', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('20', '2', 'Contractors', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('21', '2', 'Loans', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('22', '2', 'Bank Accounts', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('23', '2', 'Financial Institute', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('24', '2', 'Payments', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('25', '2', 'Documents', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('26', '2', 'Purchase orders', '1', '1', '1', '1', '0', '1', '1');
INSERT INTO `tbl_settingpermission` VALUES ('27', '2', 'Dashboard', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('28', '2', 'Sell Properties', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('29', '3', 'Compnies', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('30', '3', 'Projects', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('31', '3', 'Users', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('32', '3', 'Partners', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('33', '3', 'Suppliers', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('34', '3', 'Contractors', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('35', '3', 'Loans', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('36', '3', 'Bank Accounts', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('37', '3', 'Financial Institute', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('38', '3', 'Payments', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('39', '3', 'Documents', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('40', '3', 'Purchase orders', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO `tbl_settingpermission` VALUES ('41', '3', 'Dashboard', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('42', '3', 'Sell Properties', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('43', '4', 'Compnies', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('44', '4', 'Projects', '1', '1', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('45', '4', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('46', '4', 'Partners', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('47', '4', 'Suppliers', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('48', '4', 'Contractors', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('49', '4', 'Loans', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('50', '4', 'Bank Accounts', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('51', '4', 'Financial Institute', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('52', '4', 'Payments', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('53', '4', 'Documents', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('54', '4', 'Purchase orders', '0', '1', '1', '0', '1', '1', '1');
INSERT INTO `tbl_settingpermission` VALUES ('55', '4', 'Dashboard', '1', '1', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission` VALUES ('56', '4', 'Sell Properties', '1', '1', '1', '1', '0', '0', '0');

-- ----------------------------
-- Table structure for `tbl_settingpermission_copy`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_settingpermission_copy`;
CREATE TABLE `tbl_settingpermission_copy` (
  `settingsid` int(11) NOT NULL AUTO_INCREMENT,
  `usertype` varchar(2255) NOT NULL,
  `menutab` varchar(255) NOT NULL,
  `allowfor_add` tinyint(4) NOT NULL DEFAULT '0',
  `allowfor_edit` tinyint(4) NOT NULL DEFAULT '0',
  `allowfor_modify` tinyint(4) NOT NULL DEFAULT '0',
  `allow_delete` tinyint(4) NOT NULL DEFAULT '0',
  `allowfor_verify` tinyint(4) NOT NULL DEFAULT '0',
  `allowfor_complete` tinyint(4) NOT NULL DEFAULT '0',
  `allowfor_pay` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`settingsid`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_settingpermission_copy
-- ----------------------------
INSERT INTO `tbl_settingpermission_copy` VALUES ('1', '2', 'Compnies', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('2', '2', 'Projects', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('3', '2', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('4', '2', 'Partners', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('5', '2', 'Suppliers', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('6', '2', 'Contractors', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('7', '2', 'Loans', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('8', '2', 'Bank Accounts', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('9', '2', 'Financial Institute', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('10', '2', 'Payments', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('11', '2', 'Documents', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('12', '3', 'Compnies', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('13', '3', 'Projects', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('14', '3', 'Users', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('15', '3', 'Partners', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('16', '3', 'Suppliers', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('17', '3', 'Contractors', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('18', '3', 'Loans', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('19', '3', 'Bank Accounts', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('20', '3', 'Financial Institute', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('21', '3', 'Payments', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('22', '3', 'Documents', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('23', '2', 'Purchase orders', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('24', '3', 'Purchase orders', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('25', '5', 'Compnies', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('26', '5', 'Projects', '0', '1', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('27', '5', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('28', '5', 'Partners', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('29', '5', 'Suppliers', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('30', '5', 'Contractors', '0', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('31', '5', 'Loans', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('32', '5', 'Bank Accounts', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('33', '5', 'Financial Institute', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('34', '5', 'Payments', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('35', '5', 'Documents', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('36', '5', 'Purchase orders', '1', '1', '1', '1', '0', '1', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('37', '5', 'Dashboard', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('38', '6', 'Compnies', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('39', '6', 'Projects', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('40', '6', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('41', '6', 'Partners', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('42', '6', 'Suppliers', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('43', '6', 'Contractors', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('44', '6', 'Loans', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('45', '6', 'Bank Accounts', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('46', '6', 'Financial Institute', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('47', '6', 'Payments', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('48', '6', 'Documents', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('49', '6', 'Purchase orders', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('50', '6', 'Dashboard', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('51', '7', 'Compnies', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('52', '7', 'Projects', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('53', '7', 'Users', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('54', '7', 'Partners', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('55', '7', 'Suppliers', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('56', '7', 'Contractors', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('57', '7', 'Loans', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('58', '7', 'Bank Accounts', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('59', '7', 'Financial Institute', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('60', '7', 'Payments', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('61', '7', 'Documents', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('62', '7', 'Purchase orders', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO `tbl_settingpermission_copy` VALUES ('63', '7', 'Dashboard', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('64', '2', 'Sell Properties', '1', '1', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('65', '7', 'Compnies', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('66', '7', 'Projects', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('67', '7', 'Users', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('68', '7', 'Partners', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('69', '7', 'Suppliers', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('70', '7', 'Contractors', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('71', '7', 'Loans', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('72', '7', 'Bank Accounts', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('73', '7', 'Financial Institute', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('74', '7', 'Payments', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('75', '7', 'Documents', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('76', '7', 'Purchase orders', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO `tbl_settingpermission_copy` VALUES ('77', '7', 'Dashboard', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('78', '4', 'Compnies', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('79', '4', 'Purchase orders', '1', '1', '1', '0', '0', '1', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('80', '4', 'Dashboard', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('81', '4', 'Projects', '0', '1', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('82', '4', 'Contractors', '0', '1', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('83', '8', 'Compnies', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('84', '8', 'Projects', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('85', '8', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('86', '8', 'Partners', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('87', '8', 'Suppliers', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('88', '8', 'Contractors', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('89', '8', 'Loans', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('90', '8', 'Bank Accounts', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('91', '8', 'Financial Institute', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('92', '8', 'Payments', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('93', '8', 'Documents', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('94', '8', 'Purchase orders', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('95', '8', 'Dashboard', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('96', '8', 'Sell Properties', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('97', '9', 'Compnies', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('98', '9', 'Projects', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('99', '9', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('100', '9', 'Partners', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('101', '9', 'Suppliers', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('102', '9', 'Contractors', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('103', '9', 'Loans', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('104', '9', 'Bank Accounts', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('105', '9', 'Financial Institute', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('106', '9', 'Payments', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('107', '9', 'Documents', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('108', '9', 'Purchase orders', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('109', '9', 'Dashboard', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('110', '9', 'Sell Properties', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('111', '10', 'Compnies', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('112', '10', 'Projects', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('113', '10', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('114', '10', 'Partners', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('115', '10', 'Suppliers', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('116', '10', 'Contractors', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('117', '10', 'Loans', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('118', '10', 'Bank Accounts', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('119', '10', 'Financial Institute', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('120', '10', 'Payments', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('121', '10', 'Documents', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('122', '10', 'Purchase orders', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('123', '10', 'Dashboard', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('124', '10', 'Sell Properties', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('125', '11', 'Compnies', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('126', '11', 'Projects', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('127', '11', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('128', '11', 'Partners', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('129', '11', 'Suppliers', '0', '1', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('130', '11', 'Contractors', '0', '1', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('131', '11', 'Loans', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('132', '11', 'Bank Accounts', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('133', '11', 'Financial Institute', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('134', '11', 'Payments', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('135', '11', 'Documents', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('136', '11', 'Purchase orders', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO `tbl_settingpermission_copy` VALUES ('137', '11', 'Dashboard', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('138', '11', 'Sell Properties', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('139', '12', 'Compnies', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('140', '12', 'Projects', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('141', '12', 'Users', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('142', '12', 'Partners', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('143', '12', 'Suppliers', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('144', '12', 'Contractors', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('145', '12', 'Loans', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('146', '12', 'Bank Accounts', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('147', '12', 'Financial Institute', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('148', '12', 'Payments', '1', '1', '1', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('149', '12', 'Documents', '1', '1', '1', '1', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('150', '12', 'Purchase orders', '1', '1', '1', '1', '0', '1', '1');
INSERT INTO `tbl_settingpermission_copy` VALUES ('151', '12', 'Dashboard', '1', '1', '0', '0', '0', '0', '0');
INSERT INTO `tbl_settingpermission_copy` VALUES ('152', '12', 'Sell Properties', '1', '1', '1', '1', '0', '0', '0');

-- ----------------------------
-- Table structure for `tbl_suppliers`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_suppliers`;
CREATE TABLE `tbl_suppliers` (
  `suppliers_id` int(11) NOT NULL AUTO_INCREMENT,
  `suppliers_name` varchar(255) NOT NULL,
  `suppliers_type` varchar(1000) NOT NULL,
  `suppliers_phone` tinyint(4) NOT NULL DEFAULT '0',
  `suppliers_address` varchar(1000) NOT NULL,
  `suppliers_bankacno` varchar(255) NOT NULL,
  `suppliers_outstandingamount` varchar(255) NOT NULL,
  `suppliers_payments` varchar(255) NOT NULL,
  `suppliers_kvkno` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `suppliers_datetime` datetime NOT NULL,
  PRIMARY KEY (`suppliers_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_suppliers
-- ----------------------------
INSERT INTO `tbl_suppliers` VALUES ('1', 'Kooyman N.V.', 'type 1', '127', 'test address', '1234567890', '', '', '65416556', '0', '2022-07-04 17:51:33');
INSERT INTO `tbl_suppliers` VALUES ('2', 'Staalen Antillen', 'type 2', '127', 'test 2 address', '123445784745', '', '', '6541235', '0', '2022-07-04 17:52:32');
INSERT INTO `tbl_suppliers` VALUES ('3', 'Building Depot', 'Hardwares', '127', 'Kaya bobo 25', '14234234', '', '', '234234234', '0', '2022-09-06 03:32:22');
INSERT INTO `tbl_suppliers` VALUES ('4', 'Gomez Enterprises', 'Hardware', '127', 'Salinja', '35649858780697657', '', '', '252353425', '0', '2022-09-19 12:29:51');

-- ----------------------------
-- Table structure for `tbl_usergroup`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_usergroup`;
CREATE TABLE `tbl_usergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_usergroup
-- ----------------------------
INSERT INTO `tbl_usergroup` VALUES ('2', 'Worker', '0');
INSERT INTO `tbl_usergroup` VALUES ('3', 'Administrator', '0');
INSERT INTO `tbl_usergroup` VALUES ('4', 'Manager', '0');
